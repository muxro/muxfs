package blocks

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"log"
	"testing"

	"github.com/aclements/go-rabin/rabin"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/blake2s"
)

func BenchmarkChunker(b *testing.B) {
	benchCases := []struct {
		desc string
		size int64
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "20MB", size: 20 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024},
		{desc: "500MB", size: 500 * 1024 * 1024},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			data := randomData("benchchunker", 0, bench.size)

			buf := make([]byte, MaxChunkSize)

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				r := bytes.NewReader(data)
				c := NewChunker(r)

				for {
					_, err := c.Next(buf)
					if err != nil {
						if err == io.EOF {
							break
						}
						panic(err)
					}
				}
			}
		})
	}
}
func BenchmarkRabin(b *testing.B) {
	benchCases := []struct {
		desc string
		size int64
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "20MB", size: 20 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024},
		{desc: "500MB", size: 500 * 1024 * 1024},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			data := randomData("benchchunker", 0, bench.size)

			const (
				window = 64
				min    = 2 * 1024 * 1024
				max    = 8 * 1024 * 1024
				avg    = 4 * 1024 * 1024
			)

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				r := bytes.NewReader(data)

				c := rabin.NewChunker(rabin.NewTable(rabin.Poly64, window), r, min, avg, max)
				for {
					_, err := c.Next()
					if err == io.EOF {
						break
					} else if err != nil {
						log.Fatal(err)
					}
				}
			}
		})
	}
}

func BenchmarkBlake2B(b *testing.B) {
	data := randomData("benchhash", 0, 50*1024*1024)

	b.SetBytes(50 * 1024 * 1024)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		blake2b.Sum256(data)
	}
}

func BenchmarkSHA256(b *testing.B) {
	data := randomData("benchhash", 0, 50*1024*1024)

	b.SetBytes(50 * 1024 * 1024)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		sha256.Sum256(data)
	}
}

func BenchmarkBlake2S(b *testing.B) {
	data := randomData("benchhash", 0, 50*1024*1024)

	b.SetBytes(50 * 1024 * 1024)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		blake2s.Sum256(data)
	}
}

func BenchmarkAES256_CFB_Encrypt(b *testing.B) {
	data := randomData("benchencrypt", 0, 50*1024*1024)

	// Load your secret key from a safe place and reuse it across multiple
	// NewCipher calls. (Obviously don't use this example key for anything
	// real.) If you want to convert a passphrase to a key, use a suitable
	// package like bcrypt or scrypt.
	key, _ := hex.DecodeString("6368616e676520746869732070617373")

	// CBC mode works on blocks so plaintexts may need to be padded to the
	// next whole block. For an example of such padding, see
	// https://tools.ietf.org/html/rfc5246#section-6.2.3.2. Here we'll
	// assume that the plaintext is already of the correct length.
	if len(data)%aes.BlockSize != 0 {
		panic("plaintext is not a multiple of the block size")
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(data))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	b.SetBytes(50 * 1024 * 1024)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		stream.XORKeyStream(ciphertext[aes.BlockSize:], data)
	}
}

func BenchmarkAES256_CTR_Encrypt(b *testing.B) {
	data := randomData("benchencrypt", 0, 50*1024*1024)

	// Load your secret key from a safe place and reuse it across multiple
	// NewCipher calls. (Obviously don't use this example key for anything
	// real.) If you want to convert a passphrase to a key, use a suitable
	// package like bcrypt or scrypt.
	key, _ := hex.DecodeString("6368616e676520746869732070617373")

	// CBC mode works on blocks so plaintexts may need to be padded to the
	// next whole block. For an example of such padding, see
	// https://tools.ietf.org/html/rfc5246#section-6.2.3.2. Here we'll
	// assume that the plaintext is already of the correct length.
	if len(data)%aes.BlockSize != 0 {
		panic("plaintext is not a multiple of the block size")
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(data))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCTR(block, iv)

	b.SetBytes(50 * 1024 * 1024)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		stream.XORKeyStream(ciphertext[aes.BlockSize:], data)
	}
}

func BenchmarkAES256_CBC_Encrypt(b *testing.B) {
	data := randomData("benchencrypt", 0, 50*1024*1024)

	// Load your secret key from a safe place and reuse it across multiple
	// NewCipher calls. (Obviously don't use this example key for anything
	// real.) If you want to convert a passphrase to a key, use a suitable
	// package like bcrypt or scrypt.
	key, _ := hex.DecodeString("6368616e676520746869732070617373")

	// CBC mode works on blocks so plaintexts may need to be padded to the
	// next whole block. For an example of such padding, see
	// https://tools.ietf.org/html/rfc5246#section-6.2.3.2. Here we'll
	// assume that the plaintext is already of the correct length.
	if len(data)%aes.BlockSize != 0 {
		panic("plaintext is not a multiple of the block size")
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(data))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	mode := cipher.NewCBCEncrypter(block, iv)

	b.SetBytes(50 * 1024 * 1024)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		mode.CryptBlocks(ciphertext[aes.BlockSize:], data)
	}
}

func randomData(name string, offset, size int64) []byte {
	if offset < 0 || size <= 0 {
		return nil
	}

	var key byte
	for i, b := range []byte(name) {
		key ^= byte(i) ^ b
	}

	data := make([]byte, size)
	for i := int64(0); i < size; i++ {
		where := (offset + i) * 179
		gen := byte(where) ^ byte(where>>8) ^ byte(where>>16) ^ byte(where>>24)
		data[i] = key ^ gen
	}

	return data
}
