// +build !test

package blocks

import (
	"fmt"
	"os"
)

func init() {
	fmt.Println("please rerun go test with -tags test")
	os.Exit(1)
}
