// +build test

package blocks

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strings"

	pb "gitlab.com/muxro/muxfs/blocks/proto"
)

var (
	errListNotSupported = errors.New("test store does not support listing all blocks")
	errRefsNotSupported = errors.New("test store does not support getting refs")
)

// StoreTest is an interface that stores can implement in order to have their refs tested in other
// packages.
type StoreTest interface {
	Refs() (RefsTest, error)
	List() (StoreListTest, error)
}

type RefsTest map[ID][]Hash

// StoreListTest is an iterator for listing all the blocks in a store.
type StoreListTest interface {
	Next(n int) bool
	List() []*pb.StoreListing
	Err() error
	Close()
}

type diskListTest struct {
	store *DiskStore
	pos   int
	list  []*pb.StoreListing
	err   error
}

type memoryListTest struct {
	store        *MemoryStore
	lower, upper int
	list         []*pb.StoreListing
}

func (r RefsTest) ExistExcept(refRootIDs ...ID) []ID {
	var shouldntExist []ID
	for root := range r {
		var found bool
		for _, findRoot := range refRootIDs {
			if findRoot == root {
				found = true
				break
			}
		}
		if !found {
			shouldntExist = append(shouldntExist, root)
		}
	}
	return shouldntExist
}

func (r RefsTest) String() string {
	w := &strings.Builder{}

	fmt.Fprintln(w, "Hashes: {")
	for ref, hashes := range r {
		fmt.Fprintf(w, "\t")

		fmt.Fprintf(w, "%v: [", ref)
		if len(hashes) == 0 {
			fmt.Fprintf(w, "],\n")
			continue
		}
		fmt.Fprintf(w, "\n")
		for _, hash := range hashes {
			fmt.Fprintf(w, "\t\t%v\n", hash)
		}
		fmt.Fprintf(w, "\t],\n")
	}
	fmt.Fprintln(w, "},")
	fmt.Fprintln(w, "Calculated count: {")
	count := calculateRefCount(r)
	for hash, count := range count {
		fmt.Fprintf(w, "\t%v: %v", hash, count)
		var found bool
	Outer:
		for _, hashes := range r {
			for _, findHash := range hashes {
				if hash == findHash {
					found = true
					break Outer
				}
			}
		}
		if !found {
			fmt.Fprintf(w, ", DANGLING\n")
		} else {
			fmt.Fprintln(w)
		}
	}
	fmt.Fprintln(w, "}")
	return w.String()
}

func calculateRefCount(refs RefsTest) map[Hash]int {
	count := make(map[Hash]int)
	for _, hashes := range refs {
		for _, hash := range hashes {
			count[hash]++
		}
	}
	return count
}

func (s *DiskStore) Refs() (RefsTest, error) {
	return RefsTest(s.refs), nil
}

func (s *MemoryStore) Refs() (RefsTest, error) {
	return RefsTest(s.refs), nil
}

func (s *LRUCache) Refs() (RefsTest, error) {
	return nil, errRefsNotSupported
}

func (s *HTTPStore) Refs() (RefsTest, error) {
	return nil, errRefsNotSupported
}

func (s *EncryptedStore) Refs() (RefsTest, error) {
	return nil, errRefsNotSupported
}

func (s *DiskStore) List() (StoreListTest, error) {
	return &diskListTest{store: s}, nil
}

func (s *EncryptedStore) List() (StoreListTest, error) {
	return nil, errListNotSupported
}

func (s *HTTPStore) List() (StoreListTest, error) {
	return nil, errListNotSupported
}

func (s *MemoryStore) List() (StoreListTest, error) {
	s.mu.RLock()
	defer s.mu.RUnlock()

	var list []*pb.StoreListing
	for key, val := range s.blocks {
		keyCopy := key
		list = append(list, &pb.StoreListing{Hash: keyCopy[:], Size: int64(len(val))})
	}
	return &memoryListTest{store: s, list: list}, nil
}

// List falls through to the base store List method.
func (s *LRUCache) List() (StoreListTest, error) {
	return s.base.(StoreTest).List()
}

func (l *diskListTest) Next(n int) bool {
	if l.err != nil {
		return false
	}

	files, err := ioutil.ReadDir(l.store.BlocksPath)
	if err != nil {
		l.err = err
		return false
	}

	var list []*pb.StoreListing
	var added int
	i := -1
	needToAdd := n
	for _, f := range files {
		// ignore .refs and other files that aren't blocks
		if len(f.Name()) < hashLength {
			continue
		}
		i++
		if needToAdd == 0 {
			break
		}
		if i < l.pos {
			continue
		}

		hash, err := HashFromString(f.Name())
		if err != nil {
			l.err = err
			return false
		}

		list = append(list, &pb.StoreListing{Hash: hash[:], Size: f.Size()})
		added++
		needToAdd--
	}

	l.pos += added
	l.list = list

	if added == 0 {
		return false
	}

	return true
}

func (l *diskListTest) List() []*pb.StoreListing {
	return l.list
}

func (l *diskListTest) Err() error {
	return l.err
}

func (l *diskListTest) Close() {
}

func (l *memoryListTest) Next(n int) bool {
	if l.upper != 0 {
		l.lower += n
	}
	l.upper += n

	if l.upper > len(l.list) {
		l.upper = len(l.list)
	}
	if l.lower >= len(l.list) {
		return false
	}

	return true
}

func (l *memoryListTest) List() []*pb.StoreListing {
	return l.list[l.lower:l.upper]
}

func (l *memoryListTest) Err() error {
	return nil
}

func (l *memoryListTest) Close() {}
