package blocks

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDiskStoreStats(t *testing.T) {
	keys := getBlockKeys(5)
	data := getBlockReaders(5)

	tempPath, err := ioutil.TempDir(os.TempDir(), "disk_store_test")
	require.NoError(t, err)

	store, err := NewDiskStore(tempPath)
	require.NoError(t, err)
	err = store.NewRefRoot(defaultCtx, defaultRefRootID)
	require.NoError(t, err)

	// put blocks
	for i, key := range keys {
		err := store.Put(defaultCtx, defaultRefRootID, key, data[i])
		require.NoError(t, err)
	}

	// get iterator
	stats, err := store.Stats(defaultCtx)
	require.NoError(t, err)
	require.True(t, stats.Used.Available)
	require.True(t, stats.Free.Available)
	const expectedStoreSize = uint64(823) // block size + .refs size
	require.Equal(t, expectedStoreSize, stats.Used.Size)
	require.NotEqual(t, uint64(0), stats.Free.Size)
}

func TestDiskStoreShared(t *testing.T) {
	tempPath, err := ioutil.TempDir(os.TempDir(), "disk_store_test")
	require.NoError(t, err)

	pathShared := filepath.Join(tempPath, "disk_shared")
	pathNotShared := filepath.Join(tempPath, "disk_not_shared")

	storeShared1, err := NewDiskStore(pathShared)
	require.NoError(t, err)
	storeShared2, err := NewDiskStore(pathShared)
	require.NoError(t, err)
	storeNotShared, err := NewDiskStore(pathNotShared)
	require.NoError(t, err)

	require.True(t, storeShared1 == storeShared2)
	require.False(t, storeShared1 == storeNotShared)
	require.False(t, storeShared2 == storeNotShared)
}
