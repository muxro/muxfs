package blocks

import (
	"context"
	fmt "fmt"
)

type StoreKind string

var storeKinds = make(map[StoreKind]func() StoreConfig)

type StoreConfig interface {
	Validate() error
	Instance(ctx context.Context) (Store, error)
}

// GenericStoreConfig embeds the StoreConfig interface because we can't marshal/unmarshal directly
// into an interface.
type GenericStoreConfig struct {
	Config StoreConfig
}

func (c GenericStoreConfig) MarshalYAML() (interface{}, error) {
	return c.Config, nil
}

func (c *GenericStoreConfig) UnmarshalYAML(unmarshal func(interface{}) error) error {
	kind := struct{ Kind StoreKind }{}
	err := unmarshal(&kind)
	if err != nil {
		return err
	}

	storeCfgCtor := storeKinds[kind.Kind]
	storeCfg := storeCfgCtor()
	err = unmarshal(storeCfg)
	if err != nil {
		return err
	}

	c.Config = storeCfg

	return nil
}

func storeUndefinedFieldErr(storeKind StoreKind, field string) error {
	return fmt.Errorf("%v store config must have field '%v'", storeKind, field)
}
