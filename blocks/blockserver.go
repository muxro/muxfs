package blocks

import (
	"bytes"
	"errors"
	fmt "fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"github.com/golang/protobuf/proto"
)

type BlockServer struct {
	store Store
}

func (bs *BlockServer) get(w http.ResponseWriter, r *http.Request) {
	hashStr := chi.URLParam(r, "hash")
	hash, err := HashFromString(hashStr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	rc, err := bs.store.Get(r.Context(), hash)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	defer rc.Close()

	w.Header().Add("Content-Length", fmt.Sprintf("%v", rc.Size()))

	if rs, ok := rc.(io.ReadSeeker); ok {
		http.ServeContent(w, r, hashStr, time.Time{}, rs)
		return
	}

	_, err = io.Copy(w, Reader(r.Context(), rc))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (bs *BlockServer) put(w http.ResponseWriter, r *http.Request) {
	refRootIDStr := chi.URLParam(r, "refRootID")
	refRootID, err := IDFromString(refRootIDStr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	hashStr := chi.URLParam(r, "hash")
	hash, err := HashFromString(hashStr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	readSizer := NewHTTPBlock(r.Body, r.ContentLength)
	err = bs.store.Put(r.Context(), refRootID, hash, readSizer)
	if err != nil {
		if errors.Is(err, refRootNotFoundErr{}) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	r.Body.Close()
}

func (bs *BlockServer) stats(w http.ResponseWriter, r *http.Request) {
	stats, err := bs.store.Stats(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	data, err := proto.Marshal(stats.StoreStats)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_, err = io.Copy(w, bytes.NewReader(data))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (bs *BlockServer) newRefRoot(w http.ResponseWriter, r *http.Request) {
	refRootIDStr := chi.URLParam(r, "refRootID")
	refRootID, err := IDFromString(refRootIDStr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = bs.store.NewRefRoot(r.Context(), refRootID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	r.Body.Close()
}

func (bs *BlockServer) copyRefRoot(w http.ResponseWriter, r *http.Request) {
	dstRefRootIDStr := chi.URLParam(r, "dstRefRootID")
	dstRefRootID, err := IDFromString(dstRefRootIDStr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	srcRefRootIDStr := chi.URLParam(r, "srcRefRootID")
	srcRefRootID, err := IDFromString(srcRefRootIDStr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = bs.store.CopyRefRoot(r.Context(), dstRefRootID, srcRefRootID)
	if err != nil {
		if errors.Is(err, refRootNotFoundErr{}) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	r.Body.Close()
}

func (bs *BlockServer) deleteRefRoot(w http.ResponseWriter, r *http.Request) {
	refRootIDStr := chi.URLParam(r, "refRootID")
	refRootID, err := IDFromString(refRootIDStr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = bs.store.DeleteRefRoot(r.Context(), refRootID)
	if err != nil {
		if errors.Is(err, refRootNotFoundErr{}) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if err == ErrBlockNotFound {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (bs *BlockServer) removeRefs(w http.ResponseWriter, r *http.Request) {
	refRootIDStr := chi.URLParam(r, "refRootID")
	refRootID, err := IDFromString(refRootIDStr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	hashListStr := chi.URLParam(r, "hashListStr")
	hashes, err := hashListStringToHashes(hashListStr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = bs.store.RemoveRefs(r.Context(), refRootID, hashes)
	if err != nil {
		if errors.Is(err, refRootNotFoundErr{}) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if err == ErrBlockNotFound {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func hashListStringToHashes(hashListStr string) (hashes []Hash, err error) {
	hashStrs := strings.Split(hashListStr, ",")
	for _, hashStr := range hashStrs {
		hash, err := HashFromString(hashStr)
		if err != nil {
			return nil, err
		}
		hashes = append(hashes, hash)
	}
	return
}

func HTTPHandler(store Store) http.Handler {
	bs := BlockServer{store}

	r := chi.NewRouter()
	cors := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	r.Use(cors.Handler)

	// .Get, .Has
	r.Get("/blocks/{hash}", bs.get)
	// .Put
	r.Put("/blocks/{refRootID}/{hash}", bs.put)
	// .Stats
	r.Get("/stats", bs.stats)

	// .NewRefRoot
	r.Put("/roots/{refRootID}", bs.newRefRoot)
	// .CopyRefRoot
	r.Put("/roots/{dstRefRootID}/{srcRefRootID}", bs.copyRefRoot)
	// .DeleteRefRoot
	r.Delete("/roots/{refRootID}", bs.deleteRefRoot)

	// .RemoveRefs
	r.Delete("/refs/{refRootID}/{hashListStr}", bs.removeRefs)
	return r
}
