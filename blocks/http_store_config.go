package blocks

import "context"

const StoreHTTP StoreKind = "http"

func init() {
	storeKinds[StoreHTTP] = func() StoreConfig {
		return &HTTPStoreConfig{}
	}
}

type HTTPStoreConfig struct {
	URL string `yaml:"url,omitempty"`
}

func (c *HTTPStoreConfig) Validate() error {
	if c.URL == "" {
		return storeUndefinedFieldErr("http", "URL")
	}
	return nil
}

func (c *HTTPStoreConfig) Instance(ctx context.Context) (Store, error) {
	return NewHTTPStore(c.URL), nil
}

func (c HTTPStoreConfig) MarshalYAML() (interface{}, error) {
	return struct {
		Kind StoreKind `yaml:"kind,omitempty"`
		URL  string    `yaml:"url,omitempty"`
	}{
		Kind: StoreHTTP,
		URL:  c.URL,
	}, nil
}
