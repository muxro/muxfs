package blocks

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMemoryStoreThreadsafe(t *testing.T) {
	r := NewMemoryBlock([]byte("hello"))
	hash := newHash("h")

	store := NewMemoryStore("")
	err := store.NewRefRoot(defaultCtx, defaultRefRootID)
	require.NoError(t, err)

	done := make(chan bool)
	go func() {
		writes := 100000
		for i := 0; i < writes; i++ {
			store.Put(defaultCtx, defaultRefRootID, hash, r)
		}
		done <- true
	}()
	go func() {
		reads := 100000
		for i := 0; i < reads; i++ {
			store.Get(defaultCtx, hash)
		}
		done <- true
	}()

	<-done
	<-done
}

func TestMemoryStoreStats(t *testing.T) {
	keys := getBlockKeys(5)
	data := getBlockReaders(5)

	store := NewMemoryStore("")
	err := store.NewRefRoot(defaultCtx, defaultRefRootID)
	require.NoError(t, err)

	// put blocks
	for i, key := range keys {
		err := store.Put(defaultCtx, defaultRefRootID, key, data[i])
		require.NoError(t, err)
	}

	// get iterator
	stats, err := store.Stats(defaultCtx)
	require.NoError(t, err)
	require.True(t, stats.Used.Available)
	require.False(t, stats.Free.Available)
	require.Equal(t, uint64(5), stats.Used.Size)
}

func TestMemoryStoreShared(t *testing.T) {
	storeShared1 := NewMemoryStore("shared")
	storeShared2 := NewMemoryStore("shared")
	storeNotShared := NewMemoryStore("") // not shared
	require.Equal(t, storeShared1, storeShared2)
	if storeShared1 == storeNotShared || storeShared2 == storeNotShared {
		t.Fatal("stores should not be shared")
	}
}
