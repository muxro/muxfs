package blocks

import (
	"bytes"
	"context"
	fmt "fmt"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
)

var (
	defaultCtx = context.Background()
	// defaultRefRootID = ID(newHash("0"))
	defaultRefRootID = GenerateRefRootID()
)

type StoreTestOptions struct {
	Store         Store
	ListSupported bool
	RefsSupported bool
	Cleanup       func()
}

type storeTestFunc func(*testing.T, string, Store, StoreTestOptions)
type storeBenchFunc func(*testing.B, string, Store, StoreTestOptions)
type storeCtor func() StoreTestOptions

var storeCtors = map[string]storeCtor{
	"MemoryStore": func() StoreTestOptions {
		return StoreTestOptions{
			Store:         NewMemoryStore(""),
			ListSupported: true,
			RefsSupported: true,
		}
	},
	"DiskStore": func() StoreTestOptions {
		tempPath, err := ioutil.TempDir(os.TempDir(), "disk_store_test")
		if err != nil {
			panic(err)
		}
		diskStore, err := NewDiskStore(tempPath)
		if err != nil {
			panic(err)
		}
		return StoreTestOptions{
			Store:         diskStore,
			ListSupported: true,
			RefsSupported: true,
		}
	},
	"HTTPStore": func() StoreTestOptions {
		srv := runBlockServer()
		time.Sleep(10 * time.Millisecond)
		httpStore := NewHTTPStore("http://localhost:8888")
		cleanup := func() {
			srv.Shutdown(context.Background())
		}
		return StoreTestOptions{
			Store:         httpStore,
			ListSupported: false,
			RefsSupported: false,
			Cleanup:       cleanup,
		}
	},
	"EncryptedStore": func() StoreTestOptions {
		return StoreTestOptions{
			Store:         NewEncryptedStore(NewMemoryStore(""), "key"),
			RefsSupported: false,
			ListSupported: false,
		}
	},
	"LRUCache": func() StoreTestOptions {
		lru, err := NewLRUCache(defaultCtx, 101*1024*1024, NewMemoryStore(""), NewMemoryStore(""))
		if err != nil {
			panic(err)
		}
		return StoreTestOptions{
			Store:         lru,
			RefsSupported: false,
			ListSupported: true,
		}
	},
}

var testFuncs = []storeTestFunc{
	testStoreHas,
	testStoreGet,
	testStoreList,
	testStorePut,
	testStoreGCPut,
	testStoreGCNewRefRoot,
	testStoreGCRemoveRefs,
	testStoreGCDeleteRefRoot,
	testStoreGCCopyRefRoot,
}
var benchFuncs = []storeBenchFunc{
	benchStoreGet,
	benchStorePut,
}

func TestStores(t *testing.T) {
	for name, storeFn := range storeCtors {
		for _, tf := range testFuncs {
			opts := storeFn()
			tf(t, name, opts.Store, opts)
			if opts.Cleanup != nil {
				opts.Cleanup()
			}
		}
	}
}

func testStoreList(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	t.Run(storeName+"List", func(t *testing.T) {
		err := store.NewRefRoot(defaultCtx, defaultRefRootID)
		require.NoError(t, err)
		for i := 0; i < 23; i++ {
			key, data := getBlockKeyAndData(i)
			err := store.Put(defaultCtx, defaultRefRootID, key, NewMemoryBlock(data))
			require.NoError(t, err)
		}

		// get iterator
		sl, err := store.(StoreTest).List()

		// List not supported
		if !opts.ListSupported {
			require.Equal(t, errListNotSupported, err)
			return
		}

		require.NoError(t, err)
		defer sl.Close()

		var totalLen int
		for sl.Next(5) {
			list := sl.List()
			totalLen += len(list)

			for _, el := range list {
				var found bool
				for i := 0; i < 23; i++ {
					key, _ := getBlockKeyAndData(i)
					if key == HashFromSlice(el.Hash) {
						found = true
						break
					}
				}
				if !found {
					t.Fatal("did not find element")
				}
			}
		}
		require.Nil(t, sl.Err())

		require.Equal(t, 23, totalLen)
	})
}

func testStorePut(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	t.Run(storeName+"Put", func(t *testing.T) {
		err := store.NewRefRoot(defaultCtx, defaultRefRootID)
		require.NoError(t, err)
		for i := 0; i < 5; i++ {
			key, data := getBlockKeyAndData(i)
			err := store.Put(defaultCtx, defaultRefRootID, key, NewMemoryBlock(data))
			require.NoError(t, err)

			rc, err := store.Get(defaultCtx, key)
			require.NoError(t, err)
			actual, err := ioutil.ReadAll(Reader(defaultCtx, rc))
			rc.Close()
			require.Equal(t, data, actual)
			require.Equal(t, rc.Size(), int64(len(actual)))
		}
	})
}

func testStoreGet(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	t.Run(storeName+"Get", func(t *testing.T) {
		err := store.NewRefRoot(defaultCtx, defaultRefRootID)
		require.NoError(t, err)
		for i := 0; i < 5; i++ {
			key, data := getBlockKeyAndData(i)
			err := store.Put(defaultCtx, defaultRefRootID, key, NewMemoryBlock(data))
			require.NoError(t, err)

			rc, err := store.Get(defaultCtx, key)
			require.NoError(t, err)
			actual, err := ioutil.ReadAll(Reader(defaultCtx, rc))
			rc.Close()
			require.Equal(t, data, actual)
			require.Equal(t, rc.Size(), int64(len(actual)))
		}

		missingKey, _ := getBlockKeyAndData(99)
		rc, err := store.Get(defaultCtx, missingKey)
		require.NotNil(t, err)
		require.Nil(t, rc)
	})
}

func testStoreDeleteRef(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	t.Run(storeName+"DeleteRefRoot", func(t *testing.T) {
		err := store.NewRefRoot(defaultCtx, defaultRefRootID)
		require.NoError(t, err)
		for i := 0; i < 5; i++ {
			key, data := getBlockKeyAndData(i)
			err := store.Put(defaultCtx, defaultRefRootID, key, NewMemoryBlock(data))
			require.NoError(t, err)
		}

		err = store.DeleteRefRoot(defaultCtx, defaultRefRootID)
		require.NoError(t, err)

		for i := 0; i < 5; i++ {
			key, _ := getBlockKeyAndData(i)
			rc, err := store.Get(defaultCtx, key)
			require.NotNil(t, err)
			require.Nil(t, rc)
		}

		missingRef := GenerateRefRootID()
		err = store.DeleteRefRoot(defaultCtx, missingRef)
		require.NotNil(t, err)
	})
}

func testStoreCopyRef(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	t.Run(storeName+"CopyRefRoot", func(t *testing.T) {
		srcRef, dstRef := GenerateRefRootID(), GenerateRefRootID()
		err := store.NewRefRoot(defaultCtx, srcRef)
		require.NoError(t, err)
		err = store.NewRefRoot(defaultCtx, dstRef)
		require.NoError(t, err)
		for i := 0; i < 5; i++ {
			key, data := getBlockKeyAndData(i)
			err := store.Put(defaultCtx, srcRef, key, NewMemoryBlock(data))
			require.NoError(t, err)
		}

		err = store.CopyRefRoot(defaultCtx, dstRef, srcRef)
		require.NoError(t, err)

		err = store.DeleteRefRoot(defaultCtx, srcRef)
		require.NoError(t, err)

		for i := 0; i < 5; i++ {
			key, _ := getBlockKeyAndData(i)
			has, err := store.Has(defaultCtx, key)
			require.NoError(t, err)
			require.True(t, has)
		}

		err = store.DeleteRefRoot(defaultCtx, dstRef)
		require.NoError(t, err)

		for i := 0; i < 5; i++ {
			key, _ := getBlockKeyAndData(i)
			has, err := store.Has(defaultCtx, key)
			require.NoError(t, err)
			require.False(t, has)
		}
	})
}

func testStoreHas(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	t.Run(storeName+"Has", func(t *testing.T) {
		err := store.NewRefRoot(defaultCtx, defaultRefRootID)
		require.NoError(t, err)
		for i := 0; i < 5; i++ {
			key, data := getBlockKeyAndData(i)

			exists, err := store.Has(defaultCtx, key)
			require.NoError(t, err)
			require.False(t, exists)

			err = store.Put(defaultCtx, defaultRefRootID, key, NewMemoryBlock(data))
			require.NoError(t, err)

			exists, err = store.Has(defaultCtx, key)
			require.NoError(t, err)
			require.True(t, exists)
		}

		missingKey, _ := getBlockKeyAndData(99)
		exists, err := store.Has(defaultCtx, missingKey)
		require.False(t, exists)
		require.NoError(t, err)
	})
}

func testStoreGCPut(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	if !opts.RefsSupported {
		return
	}
	t.Run(storeName+"GCPut", func(t *testing.T) {
		root := GenerateRefRootID()

		key, data := getBlockKeyAndData(0)
		// Add to nonexistant root
		err := store.Put(defaultCtx, root, key, NewMemoryBlock(data))
		require.Equal(t, refRootNotFoundErr{refRootID: root}, err)

		err = store.NewRefRoot(defaultCtx, root)
		require.NoError(t, err)
		storetest := store.(StoreTest)

		err = store.Put(defaultCtx, root, key, NewMemoryBlock(data))
		require.NoError(t, err)
		err = store.Put(defaultCtx, root, key, NewMemoryBlock(data))
		require.NoError(t, err)
		refs, err := storetest.Refs()
		require.NoError(t, err)
		require.Equal(t, 1, len(refs[root]))
	})
}
func testStoreGCNewRefRoot(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	if !opts.RefsSupported {
		return
	}
	t.Run(storeName+"GCNewRefRoot", func(t *testing.T) {
		storetest := store.(StoreTest)
		refs, err := storetest.Refs()
		require.NoError(t, err)
		// Refs are empty at the start
		require.Equal(t, 0, len(refs))

		// Add new refRoot
		root1 := GenerateRefRootID()
		err = store.NewRefRoot(defaultCtx, root1)
		require.NoError(t, err)
		hashes1, ok := refs[root1]
		// Check refRoot exists
		require.True(t, ok)
		// Check refRoot is empty
		require.Equal(t, 0, len(hashes1))

		// Add duplicate refRoot
		err = store.NewRefRoot(defaultCtx, root1)
		require.Equal(t, errRefRootAlreadyExists, err)

		// Add another refRoot
		root2 := GenerateRefRootID()
		err = store.NewRefRoot(defaultCtx, root2)
		require.NoError(t, err)

		hashes1, ok = refs[root1]
		// Check refRoot exists
		require.True(t, ok)
		// Check refRoot is empty
		require.Equal(t, 0, len(hashes1))
		hashes2, ok := refs[root2]
		// Check refRoot exists
		require.True(t, ok)
		// Check refRoot is empty
		require.Equal(t, 0, len(hashes2))
	})
}

func testStoreGCRemoveRefs(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	if !opts.RefsSupported {
		return
	}
	t.Run(storeName+"GCRemoveRefs", func(t *testing.T) {
		storetest := store.(StoreTest)
		refs, err := storetest.Refs()
		require.NoError(t, err)
		// Refs are empty at the start
		require.Equal(t, 0, len(refs))

		root := GenerateRefRootID()
		// Try to remove refs for non-existent refRoot
		err = store.RemoveRefs(defaultCtx, root, []Hash{})
		require.Equal(t, refRootNotFoundErr{refRootID: root}, err)

		// Add new refRoot
		err = store.NewRefRoot(defaultCtx, root)
		require.NoError(t, err)

		hashesToRemove := make([]Hash, 6) // 1 extra element that we will add later
		for i := 0; i < 5; i++ {
			key, data := getBlockKeyAndData(i)
			hashesToRemove[i] = key
			err := store.Put(defaultCtx, root, key, NewMemoryBlock(data))
			require.NoError(t, err)
		}

		refs, err = storetest.Refs()
		require.NoError(t, err)
		for i, hash := range refs[root] {
			require.Equal(t, hashesToRemove[i], hash)
		}

		// Add one hash to the end that doesn't exist
		missingKey, _ := getBlockKeyAndData(5)
		hashesToRemove[5] = missingKey

		refsBeforeRemove, err := storetest.Refs()
		require.NoError(t, err)
		// Try removing 5 elements that exist and 1 that doesn't
		err = store.RemoveRefs(defaultCtx, root, hashesToRemove)
		require.Equal(t, refNotFoundErr{hash: hashesToRemove[5], refRootID: root}, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		// Refs should be the same
		require.Equal(t, refsBeforeRemove, refs)

		err = store.RemoveRefs(defaultCtx, root, hashesToRemove[:len(hashesToRemove)-1])
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		require.Equal(t, 0, len(refs[root]))

		// Remove refs that don't exist at all
		err = store.RemoveRefs(defaultCtx, root, hashesToRemove)
		require.Equal(t, refNotFoundErr{hash: hashesToRemove[0], refRootID: root}, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		require.Equal(t, 0, len(refs[root]))
	})
}

func testStoreGCDeleteRefRoot(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	if !opts.RefsSupported {
		return
	}
	t.Run(storeName+"GCDeleteRefRoot", func(t *testing.T) {
		storetest := store.(StoreTest)
		refs, err := storetest.Refs()
		require.NoError(t, err)
		// Refs are empty at the start
		require.Equal(t, 0, len(refs))

		root := GenerateRefRootID()
		// Try to delete refRoot that doesn't exist
		err = store.DeleteRefRoot(defaultCtx, root)
		require.Equal(t, refRootNotFoundErr{refRootID: root}, err)

		err = store.NewRefRoot(defaultCtx, root)
		require.NoError(t, err)

		// Add some hashes to this refRoot
		hashes := make([]Hash, 5) // 1 extra element that we will add later
		for i := 0; i < 5; i++ {
			key, data := getBlockKeyAndData(i)
			hashes[i] = key
			err := store.Put(defaultCtx, root, key, NewMemoryBlock(data))
			require.NoError(t, err)
		}

		// Delete refRoot
		err = store.DeleteRefRoot(defaultCtx, root)
		require.NoError(t, err)

		refs, err = storetest.Refs()
		require.NoError(t, err)
		// Refs are empty
		require.Equal(t, 0, len(refs[root]))
	})
}

func testStoreGCCopyRefRoot(t *testing.T, storeName string, store Store, opts StoreTestOptions) {
	if !opts.RefsSupported {
		return
	}
	t.Run(storeName+"GCCopyRefRoot", func(t *testing.T) {
		storetest := store.(StoreTest)
		refs, err := storetest.Refs()
		require.NoError(t, err)
		// Refs are empty at the start
		require.Equal(t, 0, len(refs))

		srcRoot, dstRoot := GenerateRefRootID(), GenerateRefRootID()
		// Try to copy refRoot that doesn't exist
		err = store.CopyRefRoot(defaultCtx, dstRoot, srcRoot)
		require.Equal(t, refRootNotFoundErr{refRootID: srcRoot}, err)

		err = store.NewRefRoot(defaultCtx, srcRoot)
		require.NoError(t, err)

		// Add some hashes to src refRoot
		hashes := make([]Hash, 5)
		for i := 0; i < 5; i++ {
			key, data := getBlockKeyAndData(i)
			hashes[i] = key
			err := store.Put(defaultCtx, srcRoot, key, NewMemoryBlock(data))
			require.NoError(t, err)
		}

		err = store.CopyRefRoot(defaultCtx, dstRoot, srcRoot)
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		for i, hash := range refs[dstRoot] {
			require.Equal(t, hashes[i], hash)
		}

		err = store.CopyRefRoot(defaultCtx, dstRoot, srcRoot)
		require.Equal(t, errRefRootAlreadyExists, err)
	})
}

func BenchmarkStores(b *testing.B) {
	for name, storeFn := range storeCtors {
		for _, bf := range benchFuncs {
			opts := storeFn()
			bf(b, name, opts.Store, opts)
			if opts.Cleanup != nil {
				opts.Cleanup()
			}
		}
	}
}

func benchStoreGet(b *testing.B, storeName string, store Store, opts StoreTestOptions) {
	if !opts.ListSupported {
		return
	}
	root := GenerateRefRootID()
	store.NewRefRoot(defaultCtx, root)
	for i := 0; i < 100; i++ {
		hash, _ := getBlockKeyAndData(i)
		data := TestData(fmt.Sprintf("%v", i), 0, 1*1024*1024)
		store.Put(defaultCtx, root, hash, NewMemoryBlock(data))
	}
	b.Run(storeName+"Get", func(b *testing.B) {
		b.SetBytes(100 * 1 * 1024 * 1024)

		for n := 0; n < b.N; n++ {
			for i := 0; i < 100; i++ {
				hash, _ := getBlockKeyAndData(i)
				block, _ := store.Get(defaultCtx, hash)
				ReadBlock(defaultCtx, block)
				block.Close()
			}
		}
	})
}

func benchStorePut(b *testing.B, storeName string, store Store, opts StoreTestOptions) {
	if !opts.ListSupported {
		return
	}

	data := TestData("put", 0, 1*1024*1024)

	b.Run(storeName+"Put", func(b *testing.B) {
		root := GenerateRefRootID()
		err := store.NewRefRoot(defaultCtx, root)
		if err != nil {
			panic(err)
		}

		b.SetBytes(1 * 1024 * 1024)
		b.ResetTimer()

		for n := 0; n < b.N; n++ {
			hash := randomHash()
			err := store.Put(defaultCtx, root, hash, NewMemoryBlock(data))
			if err != nil {
				panic(err)
			}
		}

		b.StopTimer()
		store.DeleteRefRoot(defaultCtx, root)
	})
}

func getBlockKeys(size int) (keys []Hash) {
	for i := 0; i < size; i++ {
		keys = append(keys, HashFromSlice(bytes.Repeat([]byte{byte(i)}, hashLength)))
	}
	return
}

func getBlockReaders(size int) (readers []Block) {
	for i := 0; i < size; i++ {
		readers = append(readers, NewMemoryBlock([]byte{byte(i)}))
	}
	return
}

func getBlockKeyAndData(i int) (Hash, []byte) {
	b := []byte{byte(i)}
	return HashFromSlice(bytes.Repeat(b, hashLength)), b
}

func blockData() Block {
	return NewMemoryBlock([]byte("a"))
}

func randomHash() Hash {
	var h Hash
	uuid1, uuid2 := uuid.New(), uuid.New()
	for i := 0; i < 16; i++ {
		h[i+1] = uuid1[i]
		h[i+1+16] = uuid2[i]
	}
	return h
}
