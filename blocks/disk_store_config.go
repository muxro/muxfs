package blocks

import (
	"context"
	fmt "fmt"
)

const StoreDisk StoreKind = "disk"

func init() {
	storeKinds[StoreDisk] = func() StoreConfig {
		return &DiskStoreConfig{}
	}
}

type DiskStoreConfig struct {
	Path string `yaml:"path,omitempty"`
}

func (c *DiskStoreConfig) Validate() error {
	if c.Path == "" {
		return storeUndefinedFieldErr("disk", "Path")
	}
	return nil
}

func (c *DiskStoreConfig) Instance(ctx context.Context) (Store, error) {
	store, err := NewDiskStore(c.Path)
	if err != nil {
		return nil, fmt.Errorf("instancing disk store: %w", err)
	}
	return store, nil
}

func (c DiskStoreConfig) MarshalYAML() (interface{}, error) {
	return struct {
		Kind StoreKind `yaml:"kind,omitempty"`
		Path string    `yaml:"path,omitempty"`
	}{
		Kind: StoreDisk,
		Path: c.Path,
	}, nil
}
