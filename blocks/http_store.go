package blocks

import (
	"context"
	"errors"
	fmt "fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	pb "gitlab.com/muxro/muxfs/blocks/proto"

	"github.com/golang/protobuf/proto"
)

var (
	_ Store       = &HTTPStore{}
	_ BlockCloser = &HTTPBlock{}
)

type HTTPStore struct {
	URL string
}

type HTTPBlock struct {
	rc   io.ReadCloser
	size int64
}

func NewHTTPStore(url string) *HTTPStore {
	key := fmt.Sprintf("HttpStore(%v)", url)
	// share store if we find it
	if store, ok := stores[key]; ok {
		if httpStore, ok := store.(*HTTPStore); ok {
			return httpStore
		}
	}

	// add store to shared stores
	store := &HTTPStore{URL: url}
	stores[key] = store
	return store
}

func (s *HTTPStore) Get(ctx context.Context, hash Hash) (BlockCloser, error) {
	reqURL := fmt.Sprintf("%v/blocks/%v", s.URL, hash.String())

	resp, err := http.Get(reqURL)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("HttpStore: GET request status not 200")
	}

	return NewHTTPBlock(resp.Body, resp.ContentLength), nil
}

func (s *HTTPStore) Has(ctx context.Context, hash Hash) (bool, error) {
	reqURL := fmt.Sprintf("%v/blocks/%v", s.URL, hash.String())

	resp, err := http.Get(reqURL)
	resp.Body.Close()
	if err != nil {
		return false, err
	}

	if resp.StatusCode != http.StatusOK {
		return false, nil
	}

	return true, nil
}

func (s *HTTPStore) Put(ctx context.Context, refRootID ID, hash Hash, block Block) error {
	reqURL := fmt.Sprintf("%v/blocks/%v/%v", s.URL, refRootID.String(), hash.String())

	req, err := http.NewRequest("PUT", reqURL, Reader(ctx, block))
	if err != nil {
		return err
	}
	req.ContentLength = block.Size()

	client := &http.Client{Timeout: 2 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusNotFound {
			return refRootNotFoundErr{refRootID: refRootID}
		}
		return errors.New("HttpStore: PUT request status not 200")
	}

	return nil
}

func (s *HTTPStore) Stats(ctx context.Context) (*StoreStats, error) {
	reqURL := fmt.Sprintf("%v/stats", s.URL)

	resp, err := http.Get(reqURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("HttpStore: STATS request status not 200")
	}

	data, err := ioutil.ReadAll(resp.Body)
	var stats pb.StoreStats
	err = proto.Unmarshal(data, &stats)
	if err != nil {
		return nil, err
	}

	return &StoreStats{StoreStats: &stats}, nil
}

func (s *HTTPStore) NewRefRoot(ctx context.Context, refRootID ID) error {
	reqURL := fmt.Sprintf("%v/roots/%v", s.URL, refRootID.String())

	req, err := http.NewRequest("PUT", reqURL, nil)
	if err != nil {
		return err
	}

	client := &http.Client{Timeout: 2 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return errors.New("HttpStore: NewRefRoot request status not 200")
	}
	return nil
}

func (s *HTTPStore) CopyRefRoot(ctx context.Context, dstRefRootID ID, srcRefRootID ID) error {
	reqURL := fmt.Sprintf("%v/roots/%v/%v", s.URL, dstRefRootID.String(), srcRefRootID.String())

	req, err := http.NewRequest("PUT", reqURL, nil)
	if err != nil {
		return err
	}

	client := &http.Client{Timeout: 2 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == 404 {
			return refRootNotFoundErr{refRootID: srcRefRootID}
		}
		return errors.New("HttpStore: CopyRefRoot request status not 200")
	}

	return nil
}

func (s *HTTPStore) DeleteRefRoot(ctx context.Context, refRootID ID) error {
	reqURL := fmt.Sprintf("%v/roots/%v", s.URL, refRootID.String())

	req, err := http.NewRequest("DELETE", reqURL, nil)
	if err != nil {
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusNotFound {
		return refRootNotFoundErr{refRootID: refRootID}
	}
	if resp.StatusCode == http.StatusNoContent {
		return ErrBlockNotFound
	}
	if resp.StatusCode != http.StatusOK {
		return errors.New("HttpStore: DELETE request status not 200")
	}
	return nil
}

func (s *HTTPStore) RemoveRefs(ctx context.Context, refRootID ID, hashes []Hash) error {
	hashListStr := hashesToString(hashes)
	reqURL := fmt.Sprintf("%v/refs/%v/%v", s.URL, refRootID.String(), hashListStr)

	req, err := http.NewRequest("DELETE", reqURL, nil)
	if err != nil {
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusNotFound {
		return refRootNotFoundErr{refRootID: refRootID}
	}
	if resp.StatusCode == http.StatusNoContent {
		return ErrBlockNotFound
	}
	if resp.StatusCode != http.StatusOK {
		return errors.New("HttpStore: DELETE request status not 200")
	}
	return nil
}

func NewHTTPBlock(rc io.ReadCloser, size int64) *HTTPBlock {
	return &HTTPBlock{rc: rc, size: size}
}

func (b *HTTPBlock) Read(ctx context.Context, p []byte) (n int, err error) {
	return b.rc.Read(p)
}

func (b *HTTPBlock) Skip(ctx context.Context, n int64) error {
	// TODO: implement this with Content-Range
	_, err := io.CopyN(ioutil.Discard, Reader(ctx, b), n)
	return err
}

func (b *HTTPBlock) Size() int64 {
	return b.size
}

func (b *HTTPBlock) Close() error {
	return b.rc.Close()
}

func hashesToString(hashes []Hash) string {
	var hashListStr string
	for i, hash := range hashes {
		if i != 0 {
			hashListStr += ","
		}
		hashListStr += hash.String()
	}
	return hashListStr
}
