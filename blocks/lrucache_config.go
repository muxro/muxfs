package blocks

import (
	"context"

	"github.com/dustin/go-humanize"
)

const StoreLRU StoreKind = "lru"

func init() {
	storeKinds[StoreLRU] = func() StoreConfig {
		return &LRUStoreConfig{}
	}
}

// LRUStoreConfig is an implementation of StoreConfig for the LRUCache store.
type LRUStoreConfig struct {
	HumanSize string              `yaml:"size"`
	Base      *GenericStoreConfig `yaml:"base,omitempty"`
	Cache     *GenericStoreConfig `yaml:"cache,omitempty"`
}

// Instance creates a new instance of a LRUCache.
func (c *LRUStoreConfig) Instance(ctx context.Context) (Store, error) {
	base, err := c.Base.Config.Instance(ctx)
	if err != nil {
		return nil, err
	}
	cache, err := c.Cache.Config.Instance(ctx)
	if err != nil {
		return nil, err
	}

	size, err := humanize.ParseBytes(c.HumanSize)
	if err != nil {
		return nil, err
	}

	return NewLRUCache(ctx, int64(size), cache, base)
}

// Validate checks if this LRUStoreConfig is valid.
func (c *LRUStoreConfig) Validate() error {
	if c.HumanSize == "" {
		return storeUndefinedFieldErr(StoreLRU, "Size")
	}
	if c.Cache == nil {
		return storeUndefinedFieldErr(StoreLRU, "Cache")
	}
	if c.Base == nil {
		return storeUndefinedFieldErr(StoreLRU, "Base")
	}

	err := c.Cache.Config.Validate()
	if err != nil {
		return err
	}
	err = c.Base.Config.Validate()
	if err != nil {
		return err
	}
	return nil
}

// MarshalYAML marshals the store fields along with a "Kind" field.
func (c LRUStoreConfig) MarshalYAML() (interface{}, error) {
	return struct {
		Kind  StoreKind           `yaml:"kind,omitempty"`
		Size  string              `yaml:"size,omitempty"`
		Base  *GenericStoreConfig `yaml:"base,omitempty"`
		Cache *GenericStoreConfig `yaml:"cache,omitempty"`
	}{
		Kind:  StoreLRU,
		Size:  c.HumanSize,
		Cache: c.Cache,
		Base:  c.Base,
	}, nil
}
