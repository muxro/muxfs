package blocks

import (
	"container/list"
	"context"
	"errors"
	"sync"
)

const skipCacheKey contextKey = 0

// LRUCache is an implementation of a Store that acts as a least-recently-used cache. Blocks are
// stored in the `base` store, and also stored in the `cache` store when .Get is called. Subsequent
// .Get calls will fetch the blocks from the `cache` store instead. When maxSize bytes are used in
// the cache store, the least-recently-used blocks will be evicted to make room for new blocks.
type LRUCache struct {
	base     Store
	cache    Store
	cacheRef ID

	maxSize, currentSize int64

	evictList *list.List
	items     map[Hash]*list.Element

	mu sync.Mutex
}

// entry is used to hold a value in the evictList
type entry struct {
	Hash Hash
	Size int64
}

// NewLRUCache creates a new LRUCache store with the specified size limit and stores.
func NewLRUCache(ctx context.Context, size int64, cache Store, base Store) (*LRUCache, error) {
	if size <= 0 {
		panic("Must provide a positive size")
	}
	cacheRefRootID := GenerateRefRootID()
	err := cache.NewRefRoot(ctx, cacheRefRootID)
	if err != nil {
		return nil, err
	}

	store := &LRUCache{
		base:     base,
		cache:    cache,
		cacheRef: cacheRefRootID,

		maxSize:   size,
		evictList: list.New(),
		items:     make(map[Hash]*list.Element),
	}
	return store, nil
}

func (s *LRUCache) Get(ctx context.Context, hash Hash) (BlockCloser, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	// Try to get from s.cache
	exists, err := s.cache.Has(ctx, hash)
	if err != nil {
		return nil, err
	}
	// If exists, return from cache
	if exists {
		// Bump up block usage
		if ent, ok := s.items[hash]; ok {
			s.evictList.MoveToFront(ent)
		}
		return s.cache.Get(ctx, hash)
	}

	// Get from base
	block, err := s.base.Get(ctx, hash)
	if err != nil {
		return nil, err
	}
	if ShouldSkipCache(ctx) {
		return block, nil
	}

	defer block.Close()
	// Insert into cache
	// TODO: don't wait for the whole block to be downloaded before returning it
	err = s.putCache(ctx, hash, block)
	if err != nil {
		return nil, err
	}

	// If the block size is over the cache limit, it will be removed immediately after it's been added
	// Check if the cache still has it, and if not return from the base store instead.
	exists, err = s.cache.Has(ctx, hash)
	if err != nil {
		return nil, err
	}
	if !exists {
		return s.base.Get(ctx, hash)
	}

	// Return from cache
	return s.cache.Get(ctx, hash)
}

// Put falls through to the base store Put method.
func (s *LRUCache) Put(ctx context.Context, refRootID ID, hash Hash, block Block) error {
	return s.base.Put(ctx, refRootID, hash, block)
}

// Has shows if the LRU cache items map, or any of the stores have this block.
func (s *LRUCache) Has(ctx context.Context, hash Hash) (bool, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	_, lruHas := s.items[hash]
	if lruHas {
		return true, nil
	}

	cacheHas, err := s.cache.Has(ctx, hash)
	if err != nil {
		return false, err
	}
	if cacheHas {
		return true, nil
	}

	baseHas, err := s.base.Has(ctx, hash)
	if err != nil {
		return false, err
	}

	return baseHas, nil
}

// Stats falls through to the base store Stats method.
func (s *LRUCache) Stats(ctx context.Context) (*StoreStats, error) {
	return s.base.Stats(ctx)
}

// NewRefRoot falls through to the base store NewRefRoot method.
func (s *LRUCache) NewRefRoot(ctx context.Context, refRootID ID) error {
	return s.base.NewRefRoot(ctx, refRootID)
}

// CopyRefRoot falls through to the base store CopyRefRoot method.
func (s *LRUCache) CopyRefRoot(ctx context.Context, dstRefRootID ID, srcRefRootID ID) error {
	return s.base.CopyRefRoot(ctx, dstRefRootID, srcRefRootID)
}

// DeleteRefRoot falls through to the base store DeleteRefRoot method.
func (s *LRUCache) DeleteRefRoot(ctx context.Context, refRootID ID) error {
	return s.base.DeleteRefRoot(ctx, refRootID)
}

// RemoveRefs falls through to the base store RemoveRefs method.
func (s *LRUCache) RemoveRefs(ctx context.Context, refRootID ID, hashes []Hash) error {
	return s.base.RemoveRefs(ctx, refRootID, hashes)
}

// Add adds a block to the cache.
func (s *LRUCache) putCache(ctx context.Context, hash Hash, block Block) error {
	// Add new item
	err := s.cache.Put(ctx, s.cacheRef, hash, block)
	if err != nil {
		return err
	}
	ent := entry{Hash: hash, Size: block.Size()}
	entry := s.evictList.PushFront(ent)
	s.items[hash] = entry

	s.currentSize += block.Size()

	// Verify size not exceeded
	for s.currentSize > s.maxSize {
		err := s.removeOldest(ctx)
		if err != nil {
			return err
		}
	}
	return nil
}

// removeOldest removes the oldest item from the cache.
func (s *LRUCache) removeOldest(ctx context.Context) error {
	ent := s.evictList.Back()
	if ent != nil {
		return s.removeElement(ctx, ent)
	}
	return errors.New("tried to remove el that doesnt exist")
}

func (s *LRUCache) evictElement(e *list.Element) {
	s.evictList.Remove(e)
	hash := e.Value.(entry).Hash
	delete(s.items, hash)
}

// removeElement is used to remove a given list element from the cache
func (s *LRUCache) removeElement(ctx context.Context, e *list.Element) error {
	hash := e.Value.(entry).Hash
	size := e.Value.(entry).Size
	s.currentSize -= size
	s.evictElement(e)
	return s.cache.RemoveRefs(ctx, s.cacheRef, []Hash{hash})
}

func WithSkipCache(ctx context.Context) context.Context {
	return context.WithValue(ctx, skipCacheKey, true)
}

func ShouldSkipCache(ctx context.Context) bool {
	val := ctx.Value(skipCacheKey)
	skip, ok := val.(bool)
	if !ok {
		return false
	}
	return skip
}
