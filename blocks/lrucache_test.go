package blocks

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLRUCache(t *testing.T) {
	lru, err := NewLRUCache(defaultCtx, 3, NewMemoryStore(""), NewMemoryStore(""))
	require.NoError(t, err)
	err = lru.NewRefRoot(defaultCtx, defaultRefRootID)
	require.NoError(t, err)

	err = lru.Put(defaultCtx, defaultRefRootID, newHash("a"), blockData())
	require.NoError(t, err)

	// get block that exists
	_, err = lru.Get(defaultCtx, newHash("a"))
	require.NoError(t, err)

	// get block that doesnt exist
	_, err = lru.Get(defaultCtx, newHash("z"))
	require.NotNil(t, err)

	// add more blocks
	err = lru.Put(defaultCtx, defaultRefRootID, newHash("b"), blockData())
	require.NoError(t, err)
	err = lru.Put(defaultCtx, defaultRefRootID, newHash("c"), blockData())
	require.NoError(t, err)
	err = lru.Put(defaultCtx, defaultRefRootID, newHash("d"), blockData())
	require.NoError(t, err)

	rc, err := lru.Get(defaultCtx, newHash("b"))
	require.NoError(t, err)
	rc.Close()
	rc, err = lru.Get(defaultCtx, newHash("c"))
	require.NoError(t, err)
	rc.Close()

	require.Equal(t, 3, len(lru.items))

	// check first item exists
	_, ok := lru.items[newHash("a")]
	require.True(t, ok)

	_, err = lru.Get(defaultCtx, newHash("d"))
	require.NoError(t, err)

	require.Equal(t, 3, len(lru.items))

	// check first item evicted
	_, ok = lru.items[newHash("a")]
	require.False(t, ok)
}

func TestLRUCacheEvict(t *testing.T) {
	lru, err := NewLRUCache(defaultCtx, 3, NewMemoryStore(""), NewMemoryStore(""))
	require.NoError(t, err)
	err = lru.NewRefRoot(defaultCtx, defaultRefRootID)
	require.NoError(t, err)

	err = lru.Put(defaultCtx, defaultRefRootID, newHash("a"), blockData())
	require.NoError(t, err)

	// get block that exists
	_, err = lru.Get(defaultCtx, newHash("a"))
	require.NoError(t, err)

	// get block that doesnt exist
	_, err = lru.Get(defaultCtx, newHash("z"))
	require.NotNil(t, err)

	// add more blocks
	err = lru.Put(defaultCtx, defaultRefRootID, newHash("b"), blockData())
	require.NoError(t, err)
	err = lru.Put(defaultCtx, defaultRefRootID, newHash("c"), blockData())
	require.NoError(t, err)
	err = lru.Put(defaultCtx, defaultRefRootID, newHash("d"), blockData())
	require.NoError(t, err)

	rc, err := lru.Get(defaultCtx, newHash("b"))
	require.NoError(t, err)
	rc.Close()
	rc, err = lru.Get(defaultCtx, newHash("c"))
	require.NoError(t, err)
	rc.Close()

	require.Equal(t, 3, len(lru.items))

	// check first item exists
	_, ok := lru.items[newHash("a")]
	require.True(t, ok)

	_, err = lru.Get(defaultCtx, newHash("d"))
	require.NoError(t, err)

	require.Equal(t, 3, len(lru.items))

	// check first item evicted
	_, ok = lru.items[newHash("a")]
	require.False(t, ok)
}

func TestLRUCacheSkipCache(t *testing.T) {
	cacheStore := NewMemoryStore("")
	lru, err := NewLRUCache(defaultCtx, 3, cacheStore, NewMemoryStore(""))
	require.NoError(t, err)
	err = lru.NewRefRoot(defaultCtx, defaultRefRootID)
	require.NoError(t, err)

	hash := newHash("a")
	err = lru.Put(defaultCtx, defaultRefRootID, hash, blockData())
	require.NoError(t, err)

	ctxSkipCache := WithSkipCache(context.Background())
	rc, err := lru.Get(ctxSkipCache, hash)
	require.NoError(t, err)
	rc.Close()

	exists, err := cacheStore.Has(ctxSkipCache, hash)
	require.NoError(t, err)
	require.False(t, exists)
}

func newHash(repeat string) Hash {
	return HashFromSlice([]byte(strings.Repeat(repeat, hashLength)))
}
