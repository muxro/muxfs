package blocks

import (
	"context"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func runBlockServer() *http.Server {
	tempPath, err := ioutil.TempDir(os.TempDir(), "http_store_test")
	if err != nil {
		panic(err)
	}

	diskStore, err := NewDiskStore(tempPath)
	if err != nil {
		panic(err)
	}

	srv := &http.Server{Addr: ":8888"}
	srv.Handler = HTTPHandler(diskStore)
	go func() {
		srv.ListenAndServe()
	}()
	return srv
}

func TestHttpStoreStats(t *testing.T) {
	srv := runBlockServer()
	defer srv.Shutdown(context.Background())
	time.Sleep(10 * time.Millisecond)

	store := NewHTTPStore("http://localhost:8888")

	keys := getBlockKeys(5)
	data := getBlockReaders(5)

	// put blocks
	err := store.NewRefRoot(defaultCtx, defaultRefRootID)
	require.NoError(t, err)
	for i, key := range keys {
		err := store.Put(defaultCtx, defaultRefRootID, key, data[i])
		require.NoError(t, err)
	}

	// get stats
	stats, err := store.Stats(defaultCtx)
	require.NoError(t, err)
	require.True(t, stats.Used.Available)
	require.True(t, stats.Free.Available)
	const expectedStoreSize = uint64(823) // block size + .refs size
	require.Equal(t, expectedStoreSize, stats.Used.Size)
	require.NotEqual(t, uint64(0), stats.Free.Size)
}
