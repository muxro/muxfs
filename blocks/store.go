package blocks

import (
	"context"
	"encoding/hex"
	"errors"
	fmt "fmt"
	"io"

	pb "gitlab.com/muxro/muxfs/blocks/proto"

	"github.com/google/uuid"
)

// 1 byte hash type + 32 bytes hash data
const hashLength = 33

var (
	stores = make(map[string]Store)

	errRefRootAlreadyExists = errors.New("ref root already exists")
	errRefAlreadyExists     = errors.New("ref already exists in this ref root")
	ErrBlockNotFound        = errors.New("block does not exist")
)

// TODO: prefetch store
// TODO: validate block sizes

// Store is a reference-counted content-addressed KV store.
type Store interface {
	// Get returns a block reader for the specific hash.
	Get(ctx context.Context, hash Hash) (BlockCloser, error)

	// Put adds the block data for a specific hash, and inserts the hash into the RefRoot
	// (incrementing the refcount), only if the hash hasn't been added to the RefRoot yet.
	Put(ctx context.Context, refRootID ID, hash Hash, block Block) error

	// Has determines if the block data exists for a specific hash.
	Has(ctx context.Context, hash Hash) (bool, error)

	// Stats shows statistics for the store (allocated space, free space), if available.
	Stats(ctx context.Context) (*StoreStats, error)

	// NewRefRoot creates a new RefRoot into which hashes can be inserted.
	NewRefRoot(ctx context.Context, refRootID ID) error

	// CopyRefRoot creates the destination RefRoot if it doesn't exist, and inserts all the refs from
	// the source RefRoot, if needed (duplicates won't be added).
	CopyRefRoot(ctx context.Context, dstRefRootID ID, srcRefRootID ID) error

	// DeleteRefRoot deletes the specified RefRoot, and with it, all its associated refs (and
	// decrements the refcount for each ref).
	DeleteRefRoot(ctx context.Context, refRootID ID) error

	// RemoveRefs removes the hashes from this specific RefRoot (decrementing the refcount).
	RemoveRefs(ctx context.Context, refRootID ID, hashes []Hash) error
}

type Block interface {
	reader
	Size() int64
}

type reader interface {
	Read(ctx context.Context, p []byte) (n int, err error)
}

type readerCtx struct {
	ctx context.Context
	b   Block
}

func (r *readerCtx) Read(p []byte) (n int, err error) {
	return r.b.Read(r.ctx, p)
}

func Reader(ctx context.Context, b Block) io.Reader {
	return &readerCtx{ctx: ctx, b: b}
}

type BlockCloser interface {
	Block
	io.Closer
	Skip(ctx context.Context, n int64) error
}

type contextKey int

type Refs map[ID][]Hash

// ID is used to identify reference roots.
type ID [32]byte

// Hash is used to identify blocks.
type Hash [hashLength]byte

// StoreStats is a wrapper over the protobuf-generated StoreStats type, which is used to represent
// store stats.
type StoreStats struct {
	*pb.StoreStats
}

type refRootNotFoundErr struct {
	refRootID ID
}

type refNotFoundErr struct {
	hash      Hash
	refRootID ID
}

func (e refRootNotFoundErr) Error() string {
	return fmt.Sprintf("ref root '%v' does not exist", e.refRootID)
}

func (e refNotFoundErr) Error() string {
	return fmt.Sprintf("ref does not exist (hash '%v' not found within ref root '%v')", e.hash, e.refRootID)
}

// GenerateRefRootID generates a new random ID.
func GenerateRefRootID() ID {
	ref := ID{}
	uuid1, uuid2 := uuid.New(), uuid.New()
	for i := 0; i < 16; i++ {
		ref[i] = uuid1[i]
		ref[i+16] = uuid2[i]
	}
	return ref
}

// MarshalText returns the text value of ID so that it can be marshaled to other formats.
func (id ID) MarshalText() (text []byte, err error) {
	str := hex.EncodeToString(id[:])
	return []byte(str), nil
}

// UnmarshalText
func (id *ID) UnmarshalText(text []byte) error {
	decoded, err := hex.DecodeString(string(text))
	if err != nil {
		return err
	}
	*id = IDFromByteSlice(decoded)
	return nil
}

func (id ID) String() string {
	return hex.EncodeToString(id[:])
}

func (h Hash) MarshalText() (text []byte, err error) {
	str := hex.EncodeToString(h[:])
	return []byte(str), nil
}

func (h *Hash) UnmarshalText(text []byte) error {
	decoded, err := hex.DecodeString(string(text))
	if err != nil {
		return err
	}
	*h = HashFromSlice(decoded)
	return nil
}

func (h Hash) String() string {
	return hex.EncodeToString(h[:])
}

func (ss *StoreStats) String() string {
	used, free := "unavailable", "unavailable"
	if ss.Used.Available {
		used = fmt.Sprintf("%v", ss.Used.Size)
	}
	if ss.Free.Available {
		free = fmt.Sprintf("%v", ss.Free.Size)
	}
	return fmt.Sprintf("used space: %v, free space: %v", used, free)
}
