package blocks

import (
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"io"
)

type EncryptedStore struct {
	base Store
	// all hashes that pass through the store are prepended to hashKey and then hashed again.
	hashKey Hash
	// encryptionKey is used as the encryption key.
	encryptionKey []byte
}

func NewEncryptedStore(base Store, key string) *EncryptedStore {
	hashKey := HashData([]byte(key))
	encryptionKey := HashData(hashKey[:])
	// Our hashes are 33 bytes long, but crypto/aes doesn't work with
	// keys of size 33, so we leave the first byte out.
	encryptionKeySlice := encryptionKey[1:]

	return &EncryptedStore{
		base:          base,
		hashKey:       hashKey,
		encryptionKey: encryptionKeySlice,
	}
}

func (s *EncryptedStore) Get(ctx context.Context, hash Hash) (BlockCloser, error) {
	hash = s.rehash(hash)

	rc, err := s.base.Get(ctx, hash)
	if err != nil {
		return nil, err
	}
	encryptedData, err := ReadBlock(ctx, rc)
	if err != nil {
		return nil, err
	}
	err = rc.Close()
	if err != nil {
		return nil, err
	}

	decryptedData, err := s.decryptData(encryptedData)
	if err != nil {
		return nil, err
	}

	return NewMemoryBlock(decryptedData), nil
}

func (s *EncryptedStore) Has(ctx context.Context, hash Hash) (bool, error) {
	hash = s.rehash(hash)
	return s.base.Has(ctx, hash)
}

func (s *EncryptedStore) Put(ctx context.Context, refRootID ID, hash Hash, block Block) error {
	hash = s.rehash(hash)

	data, err := ReadBlock(ctx, block)
	if err != nil {
		return err
	}

	encryptedData, err := s.encryptData(data)
	if err != nil {
		return err
	}

	return s.base.Put(ctx, refRootID, hash, NewMemoryBlock(encryptedData))
}

func (s *EncryptedStore) RemoveRefs(ctx context.Context, refRootID ID, hashes []Hash) error {
	hashes = s.rehashMultiple(hashes)
	return s.base.RemoveRefs(ctx, refRootID, hashes)
}

func (s *EncryptedStore) NewRefRoot(ctx context.Context, refRootID ID) error {
	return s.base.NewRefRoot(ctx, refRootID)
}

func (s *EncryptedStore) DeleteRefRoot(ctx context.Context, refRootID ID) error {
	return s.base.DeleteRefRoot(ctx, refRootID)
}

func (s *EncryptedStore) CopyRefRoot(ctx context.Context, dstRefRootID ID, srcRefRootID ID) error {
	return s.base.CopyRefRoot(ctx, dstRefRootID, srcRefRootID)
}

func (s *EncryptedStore) Stats(ctx context.Context) (*StoreStats, error) {
	return s.base.Stats(ctx)
}

func (s *EncryptedStore) rehashMultiple(hashes []Hash) []Hash {
	rehashedHashes := make([]Hash, len(hashes))
	for i, hash := range hashes {
		rehashedHashes[i] = s.rehash(hash)
	}
	return rehashedHashes
}

func (s *EncryptedStore) rehash(hash Hash) Hash {
	concat := make([]byte, len(hash)+len(s.hashKey))
	for i := range hash {
		concat[i] = hash[i]
	}
	for i := range s.hashKey {
		concat[i+len(hash)] = s.hashKey[i]
	}
	return HashData(concat)
}

func (s *EncryptedStore) encryptData(data []byte) ([]byte, error) {
	block, err := aes.NewCipher(s.encryptionKey)
	if err != nil {
		return nil, err
	}

	// IV needs to be unique, but doesn't have to be secure.
	// It's common to put it at the beginning of the ciphertext.
	cipherText := make([]byte, aes.BlockSize+len(data))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], data)

	return cipherText, nil
}

func (s *EncryptedStore) decryptData(cipherText []byte) ([]byte, error) {
	block, err := aes.NewCipher(s.encryptionKey)
	if err != nil {
		return nil, err
	}

	if len(cipherText) < aes.BlockSize {
		return nil, errors.New("cipherText block size is too short")
	}

	// IV needs to be unique, but doesn't have to be secure.
	// It's common to put it at the beginning of the ciphertext.
	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(cipherText, cipherText)

	return cipherText, nil
}
