package blocks

import (
	"context"
	fmt "fmt"
	"io"
	"sync"

	pb "gitlab.com/muxro/muxfs/blocks/proto"
)

var (
	_ Store       = &MemoryStore{}
	_ BlockCloser = &MemoryBlock{}
)

type MemoryStore struct {
	blocks map[Hash][]byte
	refs   Refs
	count  map[Hash]int
	mu     sync.RWMutex
}

type MemoryBlock struct {
	data   []byte
	offset int64
}

func NewMemoryStore(sharedKey string) *MemoryStore {
	key := fmt.Sprintf("MemoryStore(%v)", sharedKey)
	if sharedKey != "" {
		// share store if we find it
		if store, ok := stores[key]; ok {
			if memoryStore, ok := store.(*MemoryStore); ok {
				return memoryStore
			}
		}
	}

	store := &MemoryStore{
		blocks: make(map[Hash][]byte),
		refs:   make(Refs),
		count:  make(map[Hash]int),
	}
	if sharedKey != "" {
		stores[key] = store
	}
	return store
}

func (s *MemoryStore) Get(ctx context.Context, hash Hash) (BlockCloser, error) {
	s.mu.RLock()
	defer s.mu.RUnlock()

	blockData, ok := s.blocks[hash]
	if !ok {
		return nil, ErrBlockNotFound
	}

	return NewMemoryBlock(blockData), nil
}

func (s *MemoryStore) NewRefRoot(ctx context.Context, refRootID ID) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.newRefRootNoLock(ctx, refRootID)
}

func (s *MemoryStore) Put(ctx context.Context, refRootID ID, hash Hash, block Block) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	existingHashes, ok := s.refs[refRootID]
	if !ok {
		return refRootNotFoundErr{refRootID: refRootID}
	}

	// If ref exists, stop
	for _, existingHash := range existingHashes {
		if existingHash == hash {
			return nil
		}
	}

	// If the block file already exists
	if exists, err := s.hasNoLock(ctx, hash); err == nil && exists {
		// If the block exists but the ref doesn't, (although weird and unlikely), we add the ref and stop
		s.addRef(refRootID, hash)
		return nil
	}

	data, err := ReadBlock(ctx, block)
	if err != nil {
		return err
	}

	s.blocks[hash] = data
	s.addRef(refRootID, hash)

	return nil
}

func (s *MemoryStore) Has(ctx context.Context, hash Hash) (bool, error) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	return s.hasNoLock(ctx, hash)
}

func (s *MemoryStore) hasNoLock(ctx context.Context, hash Hash) (bool, error) {
	_, ok := s.blocks[hash]
	return ok, nil
}

func (s *MemoryStore) RemoveRefs(ctx context.Context, refRootID ID, hashes []Hash) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	exHashes, ok := s.refs[refRootID]
	if !ok {
		return refRootNotFoundErr{refRootID: refRootID}
	}

	// Check for duplicates
	seen := make(map[Hash]bool)
	for _, hash := range hashes {
		_, ok := seen[hash]
		if ok {
			return fmt.Errorf("duplicate hash %v", hash)
		}
		seen[hash] = true
	}

	// Check that all hashes exist
	for _, hash := range hashes {
		var found bool
		// Find this hash (aka ref) in our refRoot
		for _, existingHash := range exHashes {
			if existingHash == hash {
				found = true
				break
			}
		}
		if !found {
			// Stop early if even one of the hashes doesn't exist
			return refNotFoundErr{hash: hash, refRootID: refRootID}
		}
	}

	var removed int
	for _, hash := range hashes {
		idx := -1
		// find this hash (aka ref) in our refRoot
		for i, eh := range exHashes {
			if eh == hash {
				idx = i
				break
			}
		}
		if idx == -1 {
			return refNotFoundErr{refRootID: refRootID, hash: hash}
		}

		// Remove hash at idx
		exHashes = append(exHashes[:idx], exHashes[idx+1:]...)
		s.refs[refRootID] = exHashes
		removed++

		// decrement refcount
		s.count[hash]--
		if s.count[hash] == 0 {
			// actually delete block
			delete(s.count, hash)
			err := s.deleteBlock(ctx, hash)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (s *MemoryStore) DeleteRefRoot(ctx context.Context, refRootID ID) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	exHashes, ok := s.refs[refRootID]
	if !ok {
		return refRootNotFoundErr{refRootID: refRootID}
	}

	for _, hash := range exHashes {
		s.count[hash]--
		if s.count[hash] > 0 {
			continue
		}

		delete(s.count, hash)
		err := s.deleteBlock(ctx, hash)
		if err != nil {
			return err
		}
	}
	delete(s.refs, refRootID)

	return nil
}

func (s *MemoryStore) CopyRefRoot(ctx context.Context, dstRefRootID ID, srcRefRootID ID) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	_, ok := s.refs[dstRefRootID]
	if ok {
		return errRefRootAlreadyExists
	}

	srcHashes, ok := s.refs[srcRefRootID]
	if !ok {
		return refRootNotFoundErr{refRootID: srcRefRootID}
	}

	err := s.newRefRootNoLock(ctx, dstRefRootID)
	if err != nil {
		return err
	}

	for _, srcHash := range srcHashes {
		// check if already exists
		var skip bool
		for _, dstHash := range s.refs[dstRefRootID] {
			if dstHash == srcHash {
				skip = true
				break
			}
		}
		if skip {
			continue
		}

		s.addRef(dstRefRootID, srcHash)
	}

	return nil
}

func (s *MemoryStore) newRefRootNoLock(ctx context.Context, refRootID ID) error {
	if _, ok := s.refs[refRootID]; ok {
		return errRefRootAlreadyExists
	}

	s.refs[refRootID] = []Hash{}

	return nil
}

func (s *MemoryStore) Stats(ctx context.Context) (*StoreStats, error) {
	s.mu.RLock()
	defer s.mu.RUnlock()

	var used uint64
	for _, block := range s.blocks {
		used += uint64(len(block))
	}

	return &StoreStats{
		StoreStats: &pb.StoreStats{
			Used: &pb.Space{Available: true, Size: used},
			Free: &pb.Space{Available: false},
		},
	}, nil
}

func (s *MemoryStore) deleteBlock(ctx context.Context, hash Hash) error {
	if _, ok := s.blocks[hash]; !ok {
		return ErrBlockNotFound
	}
	delete(s.blocks, hash)
	delete(s.count, hash)
	return nil
}

// Make sure to lock the mutex before calling this function
func (s *MemoryStore) addRef(refRootID ID, hash Hash) {
	s.refs[refRootID] = append(s.refs[refRootID], hash)
	s.count[hash]++
}

func NewMemoryBlock(data []byte) *MemoryBlock {
	return &MemoryBlock{data: data}
}

func (b *MemoryBlock) Read(ctx context.Context, p []byte) (n int, err error) {
	if b.offset >= b.Size() {
		return 0, io.EOF
	}
	n = copy(p, b.data[b.offset:])
	b.offset += int64(n)
	return
}

func (b *MemoryBlock) Skip(ctx context.Context, n int64) error {
	if b.offset >= b.Size() {
		return io.EOF
	}
	b.offset += n
	return nil
}

func (b *MemoryBlock) Size() int64 {
	return int64(len(b.data))
}

func (b *MemoryBlock) Close() error {
	return nil
}
