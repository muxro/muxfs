package blocks

import "context"

const StoreEncrypted StoreKind = "encrypted"

func init() {
	storeKinds[StoreEncrypted] = func() StoreConfig {
		return &EncryptedStoreConfig{}
	}
}

type EncryptedStoreConfig struct {
	Key  string              `yaml:"key,omitempty"`
	Base *GenericStoreConfig `yaml:"base,omitempty"`
}

func (c *EncryptedStoreConfig) Instance(ctx context.Context) (Store, error) {
	base, err := c.Base.Config.Instance(ctx)
	if err != nil {
		return nil, err
	}

	return NewEncryptedStore(base, c.Key), nil
}

func (c *EncryptedStoreConfig) Validate() error {
	if c.Key == "" {
		return storeUndefinedFieldErr("encrypted", "Key")
	}
	if c.Base == nil {
		return storeUndefinedFieldErr("encrypted", "Base")
	}

	return c.Base.Config.Validate()
}

func (c EncryptedStoreConfig) MarshalYAML() (interface{}, error) {
	return struct {
		Kind StoreKind
		Key  string
		Base *GenericStoreConfig
	}{
		Kind: StoreEncrypted,
		Key:  c.Key,
		Base: c.Base,
	}, nil
}
