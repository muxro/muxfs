package blocks

import (
	"context"
	"encoding/hex"
	"errors"
	"io"

	"github.com/restic/chunker"
	"golang.org/x/crypto/blake2b"
)

var (
	MinChunkSize uint = 2 * 1024 * 1024
	MaxChunkSize uint = 8 * 1024 * 1024
	pol               = chunker.Pol(0x3a41c64f9d0fdf)
)

type Chunker struct {
	rc *chunker.Chunker
}

type Chunk struct {
	chunker.Chunk
	Hash Hash
}

func NewChunker(r io.Reader) Chunker {
	return Chunker{chunker.NewWithBoundaries(r, pol, MinChunkSize, MaxChunkSize)}
}

func (c *Chunker) Reset(r io.Reader) {
	c.rc.ResetWithBoundaries(r, pol, MinChunkSize, MaxChunkSize)
}

func (c *Chunker) Next(data []byte) (chunk Chunk, err error) {
	resticchunk, err := c.rc.Next(data)
	if err != nil {
		return
	}

	chunk = Chunk{Chunk: resticchunk}
	chunk.Hash = HashData(chunk.Data)
	return
}

func HashData(data []byte) Hash {
	var hash Hash
	hash[0] = 0x1 // blake2b
	sum := blake2b.Sum256(data)
	for i, b := range sum {
		hash[i+1] = b
	}
	return hash
}

func ReadBlock(ctx context.Context, b Block) ([]byte, error) {
	size := b.Size()
	if size < 0 {
		return nil, errors.New("invalid size")
	}
	buf := make([]byte, size)
	_, err := io.ReadFull(Reader(ctx, b), buf)
	if err != nil {
		return nil, err
	}
	return buf, nil
}

func HashFromString(hashStr string) (Hash, error) {
	hash, err := hex.DecodeString(hashStr)
	if err != nil {
		return Hash{}, err
	}
	return HashFromSlice(hash), nil
}

func IDFromString(idStr string) (ID, error) {
	id, err := hex.DecodeString(idStr)
	if err != nil {
		return ID{}, err
	}
	return IDFromByteSlice(id), nil
}

func IDFromByteSlice(s []byte) ID {
	var id ID
	copy(id[:], s[:32])
	return id
}

func HashFromSlice(s []byte) Hash {
	var hash Hash
	copy(hash[:], s[:hashLength])
	return hash
}

// TODO: move this out of here
func TestData(name string, offset, size int64) []byte {
	if offset < 0 || size <= 0 {
		return nil
	}

	var key byte
	for i, b := range []byte(name) {
		key ^= byte(i) ^ b
	}

	data := make([]byte, size)
	for i := int64(0); i < size; i++ {
		where := (offset + i) * 179
		gen := byte(where) ^ byte(where>>8) ^ byte(where>>16) ^ byte(where>>24)
		data[i] = key ^ gen
	}

	return data
}
