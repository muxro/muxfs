package blocks

import "context"

const StoreMemory StoreKind = "memory"

func init() {
	storeKinds[StoreMemory] = func() StoreConfig {
		return &MemoryStoreConfig{}
	}
}

type MemoryStoreConfig struct {
	SharedKey string `yaml:"sharedKey,omitempty"`
}

func (c *MemoryStoreConfig) Validate() error {
	return nil
}

func (c *MemoryStoreConfig) Instance(ctx context.Context) (Store, error) {
	return NewMemoryStore(c.SharedKey), nil
}

func (c MemoryStoreConfig) MarshalYAML() (interface{}, error) {
	return struct {
		Kind      StoreKind `yaml:"kind,omitempty"`
		SharedKey string    `yaml:"sharedKey,omitempty"`
	}{
		Kind:      StoreMemory,
		SharedKey: c.SharedKey,
	}, nil
}
