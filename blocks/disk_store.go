package blocks

import (
	"context"
	"errors"
	fmt "fmt"
	"io"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"
	"sync"
	"syscall"

	pb "gitlab.com/muxro/muxfs/blocks/proto"

	"gopkg.in/yaml.v2"
)

var (
	_ Store       = &DiskStore{}
	_ BlockCloser = &DiskBlock{}
)

type DiskStore struct {
	BlocksPath string

	refsPath string
	refs     map[ID][]Hash
	count    map[Hash]int
	mu       sync.Mutex
}

type DiskBlock struct {
	file *os.File
	size int64
}

func NewDiskStore(blocksPath string) (*DiskStore, error) {
	if blocksPath == "" {
		return nil, errors.New("NewDiskStore: block directory path not specified")
	}
	// turn into absolute path
	blocksPath, err := expandTilde(blocksPath)
	if err != nil {
		return nil, err
	}
	symlink, err := isSymlink(blocksPath)
	if err != nil {
		return nil, err
	}
	if symlink {
		// resolve symlinks
		blocksPath, err = os.Readlink(blocksPath)
		if err != nil {
			return nil, err
		}
	}

	key := fmt.Sprintf("DiskStore(%v)", blocksPath)
	// share store if we find it
	if store, ok := stores[key]; ok {
		if diskStore, ok := store.(*DiskStore); ok {
			return diskStore, nil
		}
	}

	// create blocks dir
	err = os.MkdirAll(blocksPath, 0755)
	if err != nil {
		return nil, err
	}

	// add store to shared stores
	store := &DiskStore{
		BlocksPath: blocksPath,
		refsPath:   filepath.Join(blocksPath, ".refs"),
		refs:       make(map[ID][]Hash),
		count:      make(map[Hash]int),
	}
	err = store.loadRefs()
	if err != nil {
		return nil, err
	}

	stores[key] = store
	return store, nil
}

func (s *DiskStore) Get(ctx context.Context, hash Hash) (BlockCloser, error) {
	// Open file
	blockFile, err := os.Open(filepath.Join(s.BlocksPath, hash.String()))
	if err != nil {
		return nil, fmt.Errorf("cannot open block file: %w", err)
	}

	return NewDiskBlock(blockFile)
}

func (s *DiskStore) NewRefRoot(ctx context.Context, refRootID ID) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.newRefRootNoLock(ctx, refRootID)
}

func (s *DiskStore) newRefRootNoLock(ctx context.Context, refRootID ID) error {
	if _, ok := s.refs[refRootID]; ok {
		return errRefRootAlreadyExists
	}

	s.refs[refRootID] = []Hash{}

	return s.persistRefs()
}

func (s *DiskStore) DeleteRefRoot(ctx context.Context, refRootID ID) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	existingHashes, ok := s.refs[refRootID]
	if !ok {
		return refRootNotFoundErr{refRootID: refRootID}
	}

	for _, hash := range existingHashes {
		s.count[hash]--
		if s.count[hash] > 0 {
			continue
		}

		delete(s.count, hash)
		err := s.deleteBlock(ctx, hash)
		if err != nil {
			return err
		}
	}
	delete(s.refs, refRootID)

	return s.persistRefs()
}

func (s *DiskStore) CopyRefRoot(ctx context.Context, dstRefRootID ID, srcRefRootID ID) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	_, ok := s.refs[dstRefRootID]
	if ok {
		return errRefRootAlreadyExists
	}

	srcHashes, ok := s.refs[srcRefRootID]
	if !ok {
		return refRootNotFoundErr{refRootID: srcRefRootID}
	}

	err := s.newRefRootNoLock(ctx, dstRefRootID)
	if err != nil {
		return err
	}

	for _, srcHash := range srcHashes {
		// check if already exists
		var skip bool
		for _, dstHash := range s.refs[dstRefRootID] {
			if dstHash == srcHash {
				skip = true
				break
			}
		}
		if skip {
			continue
		}

		s.addRef(dstRefRootID, srcHash)
	}

	return s.persistRefs()
}

func (s *DiskStore) RemoveRefs(ctx context.Context, refRootID ID, hashes []Hash) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	exHashes, ok := s.refs[refRootID]
	if !ok {
		return refRootNotFoundErr{refRootID: refRootID}
	}

	seen := make(map[Hash]bool)
	for _, hash := range hashes {
		_, ok := seen[hash]
		if ok {
			return fmt.Errorf("duplicate hash %v", hash)
		}
		seen[hash] = true
	}

	// Check that all hashes exist
	for _, hash := range hashes {
		var found bool
		// Find this hash (aka ref) in our refRoot
		for _, existingHash := range exHashes {
			if existingHash == hash {
				found = true
				break
			}
		}
		if !found {
			// Stop early if even one of the hashes doesn't exist
			return refNotFoundErr{hash: hash, refRootID: refRootID}
		}
	}

	var removed int
	for _, hash := range hashes {
		idx := -1
		// find this hash (aka ref) in our refRoot
		for i, eh := range exHashes {
			if eh == hash {
				idx = i
				break
			}
		}
		if idx == -1 {
			return refNotFoundErr{refRootID: refRootID, hash: hash}
		}

		// Remove hash at idx
		exHashes = append(exHashes[:idx], exHashes[idx+1:]...)
		s.refs[refRootID] = exHashes
		removed++

		// decrement refcount
		s.count[hash]--
		if s.count[hash] == 0 {
			// actually delete block
			delete(s.count, hash)
			err := s.deleteBlock(ctx, hash)
			if err != nil {
				return err
			}
		}
	}

	return s.persistRefs()
}

func (s *DiskStore) Put(ctx context.Context, refRootID ID, hash Hash, block Block) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	existingHashes, ok := s.refs[refRootID]
	if !ok {
		return refRootNotFoundErr{refRootID: refRootID}
	}

	// If ref exists, stop
	for _, existingHash := range existingHashes {
		if existingHash == hash {
			return nil
		}
	}

	// If the block file already exists
	if exists, err := s.Has(ctx, hash); err == nil && exists {
		// If the block exists but the ref doesn't, (although weird and unlikely), we add the ref and stop
		s.addRef(refRootID, hash)
		return s.persistRefs()
	}

	blockFile, err := ioutil.TempFile(s.BlocksPath, "hash.tmp")
	if err != nil {
		return err
	}
	defer blockFile.Close()

	// Write block data to disk
	_, err = io.Copy(blockFile, Reader(ctx, block))
	if err != nil {
		return err
	}

	path := filepath.Join(s.BlocksPath, hash.String())
	err = os.Rename(blockFile.Name(), path)
	if err != nil {
		return err
	}

	s.addRef(refRootID, hash)
	return s.persistRefs()
}

// Make sure to lock the mutex before calling this function
func (s *DiskStore) addRef(refRootID ID, hash Hash) {
	s.refs[refRootID] = append(s.refs[refRootID], hash)
	s.count[hash]++
}

func (s *DiskStore) Has(ctx context.Context, hash Hash) (bool, error) {
	path := filepath.Join(s.BlocksPath, hash.String())

	_, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

func (s *DiskStore) Stats(ctx context.Context) (*StoreStats, error) {
	var used uint64
	files, err := ioutil.ReadDir(s.BlocksPath)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		used += uint64(file.Size())
	}

	var stat syscall.Statfs_t
	err = syscall.Statfs(s.BlocksPath, &stat)
	if err != nil {
		return nil, err
	}
	// Available blocks * size per block = available space in bytes
	free := stat.Bavail * uint64(stat.Bsize)

	return &StoreStats{
		StoreStats: &pb.StoreStats{
			Used: &pb.Space{Available: true, Size: used},
			Free: &pb.Space{Available: true, Size: free},
		},
	}, nil
}

func (s *DiskStore) loadRefs() error {
	data, err := ioutil.ReadFile(s.refsPath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}

	gc := struct {
		Hashes map[ID][]Hash
		Count  map[Hash]int
	}{
		Hashes: make(map[ID][]Hash),
		Count:  make(map[Hash]int),
	}
	err = yaml.Unmarshal(data, &gc)
	if err != nil {
		return err
	}
	s.refs = gc.Hashes
	s.count = gc.Count
	return nil
}

func (s *DiskStore) persistRefs() error {
	gc := struct {
		Hashes map[ID][]Hash
		Count  map[Hash]int
	}{
		Hashes: s.refs,
		Count:  s.count,
	}

	data, err := yaml.Marshal(gc)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(s.refsPath, data, 0755)
}

func (s *DiskStore) deleteBlock(ctx context.Context, hash Hash) error {
	exists, err := s.Has(ctx, hash)
	if err != nil {
		return err
	}
	if !exists {
		return ErrBlockNotFound
	}

	path := filepath.Join(s.BlocksPath, hash.String())

	return os.Remove(path)
}

func NewDiskBlock(file *os.File) (*DiskBlock, error) {
	fi, err := file.Stat()
	if err != nil {
		return nil, err
	}
	return &DiskBlock{file: file, size: fi.Size()}, nil
}

func (b *DiskBlock) Read(ctx context.Context, p []byte) (int, error) {
	return b.file.Read(p)
}

func (b *DiskBlock) Skip(ctx context.Context, n int64) error {
	_, err := b.file.Seek(n, io.SeekCurrent)
	return err
}

func (b *DiskBlock) Size() int64 {
	return b.size
}

func (b *DiskBlock) Close() error {
	return b.file.Close()
}

func expandTilde(path string) (string, error) {
	if !strings.HasPrefix(path, "~/") {
		return path, nil
	}

	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	return filepath.Join(usr.HomeDir, path[1:]), nil
}

func isSymlink(filename string) (bool, error) {
	fi, err := os.Lstat(filename)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	return fi.Mode()&os.ModeSymlink == os.ModeSymlink, nil
}
