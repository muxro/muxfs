package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"gitlab.com/muxro/muxfs/blocks"
	"gitlab.com/muxro/muxfs/daemon"
	pb "gitlab.com/muxro/muxfs/daemon/pb"
	"google.golang.org/grpc"
	"gopkg.in/urfave/cli.v2"
)

var clientTimeout = 10 * time.Second

type client struct {
	pb.DaemonClient
	conn *grpc.ClientConn
}

func newClient(sockAddr string) (*client, error) {
	conn, err := connectToServer(sockAddr)
	if err != nil {
		return nil, err
	}

	pbClient := pb.NewDaemonClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	resp, err := pbClient.Hello(ctx, &pb.HelloRequest{})
	if err != nil {
		return nil, err
	}
	if resp.Error != "" {
		return nil, errors.New(resp.Error)
	}

	return &client{DaemonClient: pbClient, conn: conn}, nil
}

func (c *client) Close() {
	c.conn.Close()
}

func connectToServer(sockAddr string) (*grpc.ClientConn, error) {
	// check if socket exists
	_, err := os.Stat(sockAddr)
	if err != nil {
		if os.IsNotExist(err) {
			return spawnAndConnect(sockAddr)
		}
		return nil, err
	}

	conn, err := connect(sockAddr)
	if err == nil {
		return conn, err
	}

	return spawnAndConnect(sockAddr)
}

func spawnAndConnect(sockAddr string) (*grpc.ClientConn, error) {
	exe, err := os.Readlink("/proc/self/exe")
	if err != nil {
		return nil, err
	}

	cmd := exec.Command(exe, "server")
	err = cmd.Start()
	if err != nil {
		return nil, err
	}

	return connect(sockAddr)
}

func connect(sockAddr string) (*grpc.ClientConn, error) {
	var err error

	attempts := 1000
	for i := 0; i < attempts; i++ {
		// Wait for server to start up and for the socket to be created.
		var fi os.FileInfo
		fi, err = os.Stat(sockAddr)
		if err != nil {
			time.Sleep(time.Millisecond)
			continue
		}

		if fi.Mode() != daemon.SockPerm {
			return nil, fmt.Errorf("unix socket perm is not %v, instead have: %v", daemon.SockPerm, fi.Mode())
		}

		ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
		defer cancel()

		var conn *grpc.ClientConn
		conn, err = grpc.DialContext(ctx, "unix://"+sockAddr, grpc.WithInsecure(), grpc.WithBlock())
		if err == nil {
			return conn, nil
		}
	}

	return nil, fmt.Errorf("couldn't connect to muxfs daemon after 1000 attempts: %w", err)
}

func fsAdd(c *cli.Context) error {
	if c.Args().Len() < 2 {
		cli.ShowCommandHelpAndExit(c, "add", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	fs := c.Args().Get(0)
	hash := c.Args().Get(1)
	fsStoreConfig := c.String("fs-store")
	snapStoreConfig := c.String("snapshot-store")

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.FSAdd(ctx, &pb.FSAddRequest{
		Name:                    fs,
		Hash:                    hash,
		FSStoreConfigName:       fsStoreConfig,
		SnapshotStoreConfigName: snapStoreConfig,
	})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func fsList(c *cli.Context) error {
	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.FSList(ctx, &pb.FSListRequest{})
	if err != nil {
		return cliErr(err)
	}

	var output string
	for i, fs := range r.FSes {
		output += fs.Name
		if i < len(r.FSes)-1 {
			output += "\n"
		}
	}
	fmt.Println(output)
	return nil
}

func fsDestroy(c *cli.Context) error {
	if c.Args().Len() < 1 {
		cli.ShowCommandHelpAndExit(c, "destroy", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	fs := c.Args().Get(0)

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.FSDestroy(ctx, &pb.FSDestroyRequest{Name: fs})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func fsClone(c *cli.Context) error {
	if c.Args().Len() < 2 {
		cli.ShowCommandHelpAndExit(c, "clone", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	sourceName := c.Args().Get(0)
	newName := c.Args().Get(1)

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.FSClone(ctx, &pb.FSCloneRequest{SourceName: sourceName, NewName: newName})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func fsChangeStore(c *cli.Context) error {
	if c.Args().Len() < 2 {
		cli.ShowCommandHelpAndExit(c, "migrate-store", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	fsName := c.Args().Get(0)
	newStoreConfig := c.Args().Get(1)

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.FSMigrateStore(ctx, &pb.FSMigrateStoreRequest{FSName: fsName, NewStoreConfig: newStoreConfig})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func fsVerify(c *cli.Context) error {
	if c.Args().Len() < 1 {
		cli.ShowCommandHelpAndExit(c, "verify", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	fs := c.Args().Get(0)
	checkHashes := c.Bool("check-hashes")
	allSnapshots := c.Bool("all-snapshots")

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	stream, err := client.FSVerify(ctx, &pb.FSVerifyRequest{
		Name:         fs,
		CheckHashes:  checkHashes,
		AllSnapshots: allSnapshots,
	})
	if err != nil {
		return cliErr(err)
	}

	for {
		reply, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}
			return cliErr(err)
		}
		if reply.Err == "" {
			fmt.Printf("%.2f%% - %v, ETA: %v\n", reply.Percent, reply.Message, reply.ETA)
		} else {
			fmt.Printf("error: %v\n", reply.Err)
		}
	}

	return nil
}

func mount(c *cli.Context) error {
	if c.Args().Len() < 2 {
		cli.ShowCommandHelpAndExit(c, "mount", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	fs := c.Args().Get(0)
	path := c.Args().Get(1)
	path, err = filepath.Abs(path)
	if err != nil {
		return cliErr(err)
	}
	readonly := c.Bool("readonly")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := client.Mount(ctx, &pb.MountRequest{FS: fs, Path: path, Readonly: readonly})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func unmount(c *cli.Context) error {
	if c.Args().Len() < 1 {
		cli.ShowCommandHelpAndExit(c, "unmount", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	arg := c.Args().Get(0)
	pathToUse := arg
	absolutePath, err := filepath.Abs(arg)
	if err == nil {
		pathToUse = absolutePath
	}

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.Unmount(ctx, &pb.UnmountRequest{FS: arg, Path: pathToUse})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func nukePath(c *cli.Context) error {
	if c.Args().Len() < 2 {
		cli.ShowCommandHelpAndExit(c, "nuke-path", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	fs := c.Args().Get(0)
	filePath := c.Args().Get(1)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := client.NukePath(ctx, &pb.NukePathRequest{FS: fs, FilePath: filePath})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func snapSave(c *cli.Context) error {
	if c.Args().Len() < 1 {
		cli.ShowCommandHelpAndExit(c, "save", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	// TODO: by FS name or path
	path := c.Args().Get(0)
	path, err = filepath.Abs(path)
	if err != nil {
		return cliErr(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.SnapSave(ctx, &pb.SnapSaveRequest{Path: path})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func snapMount(c *cli.Context) error {
	if c.Args().Len() < 2 {
		cli.ShowCommandHelpAndExit(c, "mount", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	hashStr := c.Args().Get(0)
	hash, err := blocks.HashFromString(hashStr)
	if err != nil {
		return cliErr(fmt.Errorf("invalid snapshot hash: %w", err))
	}
	path := c.Args().Get(1)
	path, err = filepath.Abs(path)
	if err != nil {
		return cliErr(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.SnapPreview(ctx, &pb.SnapPreviewRequest{Hash: hash[:], Path: path, StoreConfigName: storeConfig})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func snapList(c *cli.Context) error {
	if c.Args().Len() < 1 {
		cli.ShowCommandHelpAndExit(c, "list", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	fs := c.Args().Get(0)

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.SnapList(ctx, &pb.SnapListRequest{FS: fs})
	if err != nil {
		return cliErr(err)
	}

	for _, sd := range r.Snapshots {
		fmt.Println(sd)
	}

	return nil
}

func snapDelete(c *cli.Context) error {
	if c.Args().Len() < 1 {
		cli.ShowCommandHelpAndExit(c, "delete", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	hashStr := c.Args().Get(0)
	hash, err := blocks.HashFromString(hashStr)
	if err != nil {
		return cliErr(fmt.Errorf("invalid snapshot hash: %w", err))
	}

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.SnapDelete(ctx, &pb.SnapDeleteRequest{Hash: hash[:]})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func mountsetAdd(c *cli.Context) error {
	if c.Args().Len() < 2 {
		cli.ShowCommandHelpAndExit(c, "add", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	fs := c.Args().Get(0)
	path := c.Args().Get(1)
	path, err = filepath.Abs(path)
	if err != nil {
		return cliErr(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.MountsetAdd(ctx, &pb.MountsetAddRequest{Path: path, FS: fs})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func mountsetRemove(c *cli.Context) error {
	if c.Args().Len() < 1 {
		cli.ShowCommandHelpAndExit(c, "remove", 1)
	}

	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	arg := c.Args().Get(0)
	pathToUse := arg
	absolutePath, err := filepath.Abs(arg)
	if err == nil {
		pathToUse = absolutePath
	}

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.MountsetRemove(ctx, &pb.MountsetRemoveRequest{FS: arg, Path: pathToUse})
	if err != nil {
		return cliErr(err)
	}

	fmt.Println(r.Message)
	return nil
}

func mountsetList(c *cli.Context) error {
	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.MountsetList(ctx, &pb.MountsetListRequest{})
	if err != nil {
		return cliErr(err)
	}

	var output string
	for i, entry := range r.Entries {
		output += fmt.Sprintf("%v -> %v", entry.FS, entry.Path)
		if i < len(r.Entries)-1 {
			output += "\n"
		}
	}
	fmt.Println(output)
	return nil
}

func mountsetSave(c *cli.Context) error {
	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.MountsetSave(ctx, &pb.MountsetSaveRequest{})
	if err != nil {
		return cliErr(err)
	}

	var output string
	for i, entry := range r.Entries {
		output += fmt.Sprintf("%v -> %v", entry.FS, entry.Path)
		if i < len(r.Entries)-1 {
			output += "\n"
		}
	}
	fmt.Println(output)
	return nil
}

func mountsetMount(c *cli.Context) error {
	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.MountsetMount(ctx, &pb.MountsetMountRequest{})
	if err != nil {
		return cliErr(err)
	}

	var output string
	for i, entry := range r.Entries {
		output += fmt.Sprintf("%v -> %v", entry.FS, entry.Path)
		if i < len(r.Entries)-1 {
			output += "\n"
		}
	}
	fmt.Println(output)
	return nil
}

func mountsetUnmount(c *cli.Context) error {
	client, err := newClient(sockAddr)
	if err != nil {
		return cliErr(err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(context.Background(), clientTimeout)
	defer cancel()
	r, err := client.MountsetUnmount(ctx, &pb.MountsetUnmountRequest{})
	if err != nil {
		return cliErr(err)
	}

	var output string
	for i, entry := range r.Entries {
		output += fmt.Sprintf("%v -> %v", entry.FS, entry.Path)
		if i < len(r.Entries)-1 {
			output += "\n"
		}
	}
	fmt.Println(output)
	return nil
}
