package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"syscall"

	"github.com/adrg/xdg"

	"gopkg.in/urfave/cli.v2"
)

var (
	Version    = "0.0.2"
	CommitHash = "unknown"

	storeConfig string
	sockAddr    string
)

func main() {
	flags := extractFlags("store", "sock")

	if val, ok := flags["store"]; ok {
		storeConfig = val.(string)
	}
	// Default sockAddr value
	sockAddr = filepath.Join(xdg.RuntimeDir, "mfs.sock")
	if val, ok := flags["sock"]; ok {
		sockAddr = val.(string)
	}

	flag.Parse()

	app := &cli.App{
		Name:    "muxfs",
		Version: fmt.Sprintf("%v, git hash: %v", Version, CommitHash),
		Commands: []*cli.Command{
			&cli.Command{
				Name:    "server",
				Aliases: []string{"s"},
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "config",
						Aliases: []string{"c"},
						Value:   filepath.Join(xdg.DataHome, "mfs.yaml"),
					},
				},
				Hidden: true,
				Usage:  "Starts the muxfs server process which handles mounting and unmounting snapshots",
				Action: runServer,
			},

			&cli.Command{
				Name:      "add",
				Usage:     "Creates a FS for a particular snapshot hash and block store config. As new snapshots are created, the hash is updated automatically",
				UsageText: "mfs add <fs name> <snapshot hash>",
				ArgsUsage: "",
				Flags: []cli.Flag{
					&cli.StringFlag{Name: "snapshot-store", Value: "", Usage: "store from which to fetch the snapshot (if hash passed is not \"new\")"},
					&cli.StringFlag{Name: "fs-store", Value: "", Usage: "store to use for the new FS"},
				},
				Action: fsAdd,
			},
			&cli.Command{
				Name:   "list",
				Usage:  "Lists all filesystems",
				Action: fsList,
			},
			&cli.Command{
				Name:      "destroy",
				Usage:     "Removes the filesystem, all snapshots associated with it, and all file blocks",
				ArgsUsage: "<name>",
				Action:    fsDestroy,
			},
			&cli.Command{
				Name:      "clone",
				Usage:     "Clones the filesystem, all snapshots associated with it, and all file blocks",
				ArgsUsage: "<name> <new name>",
				Action:    fsClone,
			},
			&cli.Command{
				Name:      "change-store",
				Usage:     "Changes the filesystems' store",
				ArgsUsage: "<fs name> <store config>",
				Action:    fsChangeStore,
			},
			&cli.Command{
				Name:      "verify",
				Usage:     "verifies if the latest snapshot block and all of its file blocks exist",
				ArgsUsage: "<name>",
				Flags: []cli.Flag{
					&cli.BoolFlag{Name: "check-hashes", Value: false, Usage: "additionally verifies if the saved hashes match the actual hashes of the data"},
					&cli.BoolFlag{Name: "all-snapshots", Value: false, Usage: "additionally verifies all snapshots, not just the latest one"},
				},
				Action: fsVerify,
			},
			&cli.Command{
				Name:      "mount",
				Aliases:   []string{"m"},
				Usage:     "Mounts an FS or snapshot hash to a path",
				ArgsUsage: "<fs> <path>",
				Flags: []cli.Flag{
					&cli.BoolFlag{Name: "readonly", Value: false},
				},
				Action: mount,
			},
			&cli.Command{
				Name:      "unmount",
				Aliases:   []string{"u"},
				Usage:     "Unmounts the filesystem, by name or by path",
				ArgsUsage: "<fs>|<path>",
				Action:    unmount,
			},

			&cli.Command{
				Name:      "nuke-path",
				Aliases:   []string{"n"},
				Usage:     "Removes the specified file from the filesystem and from every snapshot",
				ArgsUsage: "<fs> <file-path>",
				Action:    nukePath,
			},

			&cli.Command{
				Name:    "snapshot",
				Aliases: []string{"s"},
				Usage:   "Snapshot management",
				Subcommands: []*cli.Command{
					&cli.Command{
						Name:      "save",
						Aliases:   []string{"s"},
						Usage:     "Creates a new snapshot of a currently mounted filesystem",
						ArgsUsage: "<fs>|<path>",
						Action:    snapSave,
					},
					&cli.Command{
						Name:      "mount",
						Usage:     "Mounts a particular snapshot hash as a readonly filesystem.",
						ArgsUsage: "<hash> <path>",
						Action:    snapMount,
					},
					&cli.Command{
						Name:      "list",
						Aliases:   []string{"l"},
						Usage:     "Lists all snapshots for a filesystem",
						ArgsUsage: "<fs>",
						Action:    snapList,
					},
					&cli.Command{
						Name:      "delete",
						Aliases:   []string{"d"},
						Usage:     "Deletes a snapshot",
						ArgsUsage: "<hash>",
						Action:    snapDelete,
					},
				},
			},

			&cli.Command{
				Name:      "mountset",
				Aliases:   []string{"ms"},
				Usage:     "Mountset management",
				ArgsUsage: "",
				Subcommands: []*cli.Command{
					&cli.Command{
						Name:      "add",
						Aliases:   []string{"a"},
						Usage:     "Add a filesystem to the mountset, to be mounted at the specified path",
						ArgsUsage: "<fs> <path>",
						Action:    mountsetAdd,
					},
					&cli.Command{
						Name:      "remove",
						Aliases:   []string{"r"},
						Usage:     "Removes a filesystem from the mountset",
						ArgsUsage: "<fs>|<path>",
						Action:    mountsetRemove,
					},
					&cli.Command{
						Name:      "list",
						Aliases:   []string{"l"},
						Usage:     "List all filesystems currently in the mountset",
						ArgsUsage: "",
						Action:    mountsetList,
					},
					&cli.Command{
						Name:      "save",
						Aliases:   []string{"s"},
						Usage:     "Save all currently mounted filesystems to the mountset",
						ArgsUsage: "",
						Action:    mountsetSave,
					},
					&cli.Command{
						Name:      "mount",
						Aliases:   []string{"m"},
						Usage:     "Mount all filesystems specified in the mountset",
						ArgsUsage: "",
						Action:    mountsetMount,
					},
					&cli.Command{
						Name:      "unmount",
						Aliases:   []string{"u"},
						Usage:     "Unmount all filesystems specified in the mountset",
						ArgsUsage: "",
						Action:    mountsetUnmount,
					},
				},
			},
		},
	}
	app.Run(os.Args)
}

func cliErr(err error) cli.ExitCoder {
	return cli.Exit(err.Error(), 1)
}

// remotefs -flag1 value -flag2 value ...rest
func extractFlags(names ...string) map[string]interface{} {
	flags := make(map[string]interface{})
	for {
		if len(os.Args) < 3 {
			return flags
		}
		curr := os.Args[1]
		var extracting string
		for _, name := range names {
			if "-"+name == curr {
				extracting = name
				break
			}
		}
		if extracting == "" {
			break
		}

		val := extractFlag(extracting)
		flags[extracting] = val
	}
	return flags
}

// TODO: Because urfave/cli v2 doesn't support global flags, we check if the
// os.Arg at i is equal to name, and if so, get the value from i+1, then remove both from os.Args
// `./remotefs -sock "value" ...rest of arguments`
func extractFlag(name string) interface{} {
	if os.Args[1] != "-"+name {
		return ""
	}

	val := os.Args[2]
	// remove name
	os.Args = removeAtIndex(os.Args, 1)
	// remove val
	os.Args = removeAtIndex(os.Args, 1) // i+1
	return val
}

func removeAtIndex(slice []string, i int) []string {
	return append(slice[:i], slice[i+1:]...)
}

// redirectStderr to the file passed in
// TODO: P0 Not cross-platform, only works on Linux: https://stackoverflow.com/a/34773942
func redirectStderr(f *os.File) error {
	return syscall.Dup2(int(f.Fd()), int(os.Stderr.Fd()))
}
