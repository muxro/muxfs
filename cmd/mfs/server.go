package main

import (
	"context"
	"errors"
	"os"
	"sync"
	"syscall"
	"time"

	"bazil.org/fuse"
	"gitlab.com/go-figure/instrum"
	"gitlab.com/go-figure/logr"
	journal "gitlab.com/go-figure/logr-journald"
	"gitlab.com/muxro/muxfs/daemon"
	"gopkg.in/urfave/cli.v2"
)

func runServer(c *cli.Context) (err error) {
	// On ^C
	ctx, cancel := instrum.CancelSignalContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	// Add WaitGroup to context
	var wg sync.WaitGroup
	ctx = instrum.WaitGroupContext(ctx, &wg)

	// Add sink to context
	sink := journal.NewJournalSink("muxfs")
	ctx = logr.SinkContext(ctx, sink)

	// Enable fuse error logging
	fuse.Debug = func(msg interface{}) {
		if err, ok := msg.(error); ok && err != nil {
			sink.Event("fuse_error", logr.KV{"error": err})
		}
	}

	configPath := c.String("config")
	d, err := daemon.New(ctx, configPath, sockAddr)
	if err != nil {
		return err
	}

	go func() {
		<-ctx.Done()
		d.Stop()

		// Forced shutdown after 5 seconds
		time.Sleep(5 * time.Second)
		sink.Event("shutdown_error", logr.KV{"error": errors.New("shutdown timed out")})
		os.Exit(1)
	}()

	err = d.Start()
	cancel()
	if err != nil {
		sink.Event("error", logr.KV{"error": err})
	}

	wg.Wait()

	return nil
}
