module gitlab.com/muxro/muxfs

go 1.13

replace gitlab.com/go-figure/logr => /home/ed/programming/go-figure/logr

replace gitlab.com/go-figure/logr-journald => /home/ed/programming/go-figure/logr-journald

replace gitlab.com/go-figure/grpc-middleware-common => /home/ed/programming/go-figure/grpc-middleware-common

replace gitlab.com/go-figure/instrum => /home/ed/programming/go-figure/instrum

require (
	bazil.org/fuse v0.0.0-20200117225306-7b5117fecadc
	github.com/aclements/go-rabin v0.0.0-20170911142644-d0b643ea1a4c
	github.com/adrg/xdg v0.2.1
	github.com/dustin/go-humanize v1.0.0
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/minio/highwayhash v1.0.0
	github.com/petermattis/goid v0.0.0-20180202154549-b0b1615b78e5 // indirect
	github.com/restic/chunker v0.2.0
	github.com/sasha-s/go-deadlock v0.2.0 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/y0ssar1an/q v1.0.8 // indirect
	gitlab.com/go-figure/grpc-middleware v0.0.0-20191216110303-7582a1a70751
	gitlab.com/go-figure/grpc-middleware-common v0.0.0-20200114171052-a2f4cf0290d1
	gitlab.com/go-figure/instrum v0.0.0-20200113094226-04f2c1f272d5
	gitlab.com/go-figure/logr v0.0.0-20200114171146-0285a5f33cd5
	gitlab.com/go-figure/logr-journald v0.0.0-00010101000000-000000000000
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	google.golang.org/grpc v1.27.1
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/urfave/cli.v2 v2.0.0-20190806201727-b62605953717
	gopkg.in/yaml.v2 v2.2.8
)
