package errors

import "fmt"

func Recover(err *error) {
	if err == nil {
		var nerr error
		err = &nerr
	}

	if r := recover(); r != nil {
		*err = &Error{
			Code:    Generic,
			Message: fmt.Sprintf("panic: %v", r),
			Where:   location(0, true),
			Cause:   *err,
			Masking: true,
		}
	}
}
