package errors

import (
	"fmt"
	"strings"
)

type ValidationError []FieldError

type FieldError struct {
	Field   string `json:"field"`
	Message string `json:"message"`
}

func (ve ValidationError) Error() string {
	var errors []string
	for _, fe := range ve {
		errors = append(errors, fmt.Sprintf("%s: %s", fe.Field, fe.Message))
	}

	return strings.Join(errors, ", ")
}

func Invalid(errors ...FieldError) *Error {
	return &Error{
		Code:    BadRequest,
		Message: "validationn error",
		Where:   location(1, false),
		Cause:   ValidationError(errors),
	}
}

func Field(field, message string) FieldError {
	return FieldError{
		Field:   field,
		Message: message,
	}
}
