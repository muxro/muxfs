package errors

import (
	"runtime"
	"strings"
)

type Frame struct {
	Function string `json:"function"`
	File     string `json:"file"`
	Line     int    `json:"line"`
}

func location(skip int, waitPanic bool) []Frame {
	pc := make([]uintptr, 100)
	n := runtime.Callers(skip+2, pc)
	if n == 0 {
		return nil
	}

	pc = pc[:n] // pass only valid pcs to runtime.CallersFrames
	frames := runtime.CallersFrames(pc)

	var res []Frame
	var frame runtime.Frame
	for more := true; more; {
		frame, more = frames.Next()
		if waitPanic {
			if frame.Function == "runtime.gopanic" {
				waitPanic = false
			}
			continue
		}

		if strings.HasPrefix(frame.Function, "runtime.") {
			break
		}

		res = append(res, Frame{
			Function: frame.Function,
			File:     frame.File,
			Line:     frame.Line,
		})
	}

	return res
}

func Stack(err error) []Frame {
	checkPanic := true

	var stack []Frame
	for err != nil {
		cerr, ok := err.(*Error)
		if !ok {
			break
		}

		// if the first error is a panic, return the stack trace of the panic
		if checkPanic && strings.HasPrefix(err.Error(), "panic: ") {
			return cerr.Where
		}
		checkPanic = false

		if len(cerr.Where) > 0 {
			stack = append(stack, cerr.Where[0])
		}

		err = cerr.Cause
	}

	return stack
}
