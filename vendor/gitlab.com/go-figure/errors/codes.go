package errors

type Code struct {
	UserError bool
	HTTPCode  int
}

var (
	Generic = &Code{UserError: true, HTTPCode: 500}

	BadRequest           = &Code{UserError: true, HTTPCode: 400} // RFC 7231, 6.5.1
	Unauthorized         = &Code{UserError: true, HTTPCode: 401} // RFC 7235, 3.1
	PaymentRequired      = &Code{UserError: true, HTTPCode: 402} // RFC 7231, 6.5.2
	Forbidden            = &Code{UserError: true, HTTPCode: 403} // RFC 7231, 6.5.3
	NotFound             = &Code{UserError: true, HTTPCode: 404} // RFC 7231, 6.5.4
	NotAcceptable        = &Code{UserError: true, HTTPCode: 406} // RFC 7231, 6.5.6
	Conflict             = &Code{UserError: true, HTTPCode: 409} // RFC 7231, 6.5.8
	PreconditionFailed   = &Code{UserError: true, HTTPCode: 412} // RFC 7232, 4.2
	ExpectationFailed    = &Code{UserError: true, HTTPCode: 417} // RFC 7231, 6.5.14
	UnprocessableEntity  = &Code{UserError: true, HTTPCode: 422} // RFC 4918, 11.2
	Locked               = &Code{UserError: true, HTTPCode: 423} // RFC 4918, 11.3
	FailedDependency     = &Code{UserError: true, HTTPCode: 424} // RFC 4918, 11.4
	PreconditionRequired = &Code{UserError: true, HTTPCode: 428} // RFC 6585, 3
	TooManyRequests      = &Code{UserError: true, HTTPCode: 429} // RFC 6585, 4
)

func IsUserError(err error) bool {
	cerr, ok := err.(*Error)
	if !ok {
		return false
	}

	return cerr.Code.UserError
}

func HTTPStatusCode(err error) int {
	cerr, ok := err.(*Error)
	if !ok {
		return 500
	}

	return cerr.Code.HTTPCode
}
