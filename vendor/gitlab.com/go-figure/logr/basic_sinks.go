package logr

import "time"

// check that all types implement the Sink interface
var _ Sink = DiscardSink{}
var _ Sink = TeeSink{}
var _ Sink = (*KVSink)(nil)

var Discard = DiscardSink{}

type DiscardSink struct{}

func (_ DiscardSink) Event(job, event string, kv KV)                                            {}
func (_ DiscardSink) Error(job, event string, err error, kv KV)                                 {}
func (_ DiscardSink) Timing(job, event string, timing time.Duration, kv KV)                     {}
func (_ DiscardSink) Gauge(job, event string, gauge float64, kv KV)                             {}
func (_ DiscardSink) Complete(job string, status CompletionStatus, timing time.Duration, kv KV) {}

type TeeSink []Sink

func (ts *TeeSink) Add(sink Sink) {
	*ts = append(*ts, sink)
}

func (ts TeeSink) Event(job, event string, kv KV) {
	for _, sink := range ts {
		sink.Event(job, event, kv)
	}
}

func (ts TeeSink) Error(job, event string, err error, kv KV) {
	for _, sink := range ts {
		sink.Error(job, event, err, kv)
	}
}

func (ts TeeSink) Timing(job, event string, timing time.Duration, kv KV) {
	for _, sink := range ts {
		sink.Timing(job, event, timing, kv)
	}
}

func (ts TeeSink) Gauge(job, event string, gauge float64, kv KV) {
	for _, sink := range ts {
		sink.Gauge(job, event, gauge, kv)
	}
}

func (ts TeeSink) Complete(job string, status CompletionStatus, timing time.Duration, kv KV) {
	for _, sink := range ts {
		sink.Complete(job, status, timing, kv)
	}
}

type KVSink struct {
	Sink Sink
	KV   KV
}

func (s *KVSink) KeyValue(key string, val interface{}) {
	if s.KV == nil {
		s.KV = make(KV)
	}

	s.KV[key] = val
}

func mergedKV(kv1, kv2 KV) KV {
	if kv1 == nil {
		return kv2
	}
	if kv2 == nil {
		return kv1
	}

	merged := make(KV)

	for k, v := range kv1 {
		merged[k] = v
	}

	for k, v := range kv2 {
		merged[k] = v
	}

	return merged
}

func (s KVSink) Event(job, event string, kv KV) {
	s.Sink.Event(job, event, mergedKV(s.KV, kv))
}

func (s KVSink) Error(job, event string, err error, kv KV) {
	s.Sink.Error(job, event, err, mergedKV(s.KV, kv))
}

func (s KVSink) Timing(job, event string, timing time.Duration, kv KV) {
	s.Sink.Timing(job, event, timing, mergedKV(s.KV, kv))
}

func (s KVSink) Gauge(job, event string, gauge float64, kv KV) {
	s.Sink.Gauge(job, event, gauge, mergedKV(s.KV, kv))
}

func (s KVSink) Complete(job string, status CompletionStatus, timing time.Duration, kv KV) {
	s.Sink.Complete(job, status, timing, mergedKV(s.KV, kv))
}
