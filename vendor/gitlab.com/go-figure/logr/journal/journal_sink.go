package journal

import (
	"fmt"
	"gitlab.com/go-figure/logr"
	"strings"
	"time"
)

var _ logr.Sink = JournalSink{}

type JournalSink struct {
	identifier string
}

func NewJournalSink(identifier string) JournalSink {
	return JournalSink{identifier: identifier}
}

func (s JournalSink) Event(job, event string, kv logr.KV) {
	message := fmt.Sprintf("job:%s event:%s kv:[%s]\n",
		job, event, logr.FormattedKV(kv))
	Send(message, PriorityInfo, journalFields(s.identifier, job, event, kv))
}

func (s JournalSink) Error(job, event string, err error, kv logr.KV) {
	message := fmt.Sprintf("job:%s event:%s error:%s kv:[%s]\n",
		job, event, err.Error(), logr.FormattedKV(kv))
	fields := journalFields(s.identifier, job, event, kv)
	fields["ERROR"] = err.Error()
	Send(message, PriorityErr, fields)
}

func (s JournalSink) Timing(job, event string, timing time.Duration, kv logr.KV) {
	message := fmt.Sprintf("job:%s event:%s timing:%s kv:[%s]\n",
		job, event, timing.String(), logr.FormattedKV(kv))
	fields := journalFields(s.identifier, job, event, kv)
	fields["TIMING"] = timing.String()
	Send(message, PriorityInfo, fields)
}

func (s JournalSink) Gauge(job, event string, value float64, kv logr.KV) {
	message := fmt.Sprintf("job:%s event:%s gauge:%g kv:[%s]\n",
		job, event, value, logr.FormattedKV(kv))
	fields := journalFields(s.identifier, job, event, kv)
	fields["GAUGE"] = fmt.Sprintf("%g", value)
	Send(message, PriorityInfo, fields)
}

func (s JournalSink) Complete(job string, status logr.CompletionStatus, timing time.Duration, kv logr.KV) {
	message := fmt.Sprintf("job:%s status:%s timing:%s kv:[%s]\n",
		job, status, timing.String(), logr.FormattedKV(kv))
	fields := journalFields(s.identifier, job, fmt.Sprintf("%v", status), kv)
	fields["TIMING"] = timing.String()
	prio := PriorityInfo
	if status == logr.Failed {
		prio = PriorityErr
	}
	Send(message, prio, fields)
}

func journalFields(identifier, job, event string, kv logr.KV) map[string]string {
	fields := map[string]string{
		"JOB":   job,
		"EVENT": event,
	}
	if identifier != "" {
		fields["SYSLOG_IDENTIFIER"] = identifier
	}
	for key, val := range kv {
		key = strings.ReplaceAll(key, ".", "_")
		key = strings.ToUpper(key)
		key = fmt.Sprintf("KV_%v", key)
		fields[key] = fmt.Sprintf("%v", val)
	}
	return fields
}
