module gitlab.com/go-figure/logr

require (
	github.com/golang/mock v1.1.1
	github.com/stretchr/testify v1.4.0
	golang.org/x/net v0.0.0-20180808004115-f9ce57c11b24 // indirect
)

go 1.13
