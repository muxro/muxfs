package instrum

import (
	"context"
	"fmt"
	"strings"
	"sync"

	"gitlab.com/go-figure/logr"
)

type Service struct {
	Name  string
	Units []*Unit
}

type Unit struct {
	Name string
	Deps []string
	Fn   func(context.Context) error
}

func NewService(name string) *Service {
	return &Service{
		Name: name,
	}
}

func (s *Service) Unit(name string, fn func(context.Context) error, deps ...string) {
	name = strings.ToLower(name)

	// check if unit was already defined
	for _, unit := range s.Units {
		if unit.Name == name {
			panic(fmt.Sprintf("unit %s already defined", name))
		}
	}

	// check if deps can be satisfied
	for i, dep := range deps {
		dep = strings.ToLower(dep)
		deps[i] = dep

		found := false
		for _, unit := range s.Units {
			if unit.Name == dep {
				found = true
				break
			}
		}

		if !found {
			panic(fmt.Sprintf("dependency %s not found", dep))
		}
	}

	s.Units = append(s.Units, &Unit{
		Name: name,
		Deps: deps,
		Fn:   fn,
	})
}

func (s *Service) Start(ctx context.Context) error {
	sink := logr.SinkFromContext(ctx)
	ctx, cancel := context.WithCancel(ctx)

	tr := &unitRunner{
		name:     s.Name,
		sink:     sink,
		cancel:   cancel,
		finished: make(map[string]bool),
		c:        sync.NewCond(&sync.Mutex{}),
	}

	waiting := s.Units
	for len(waiting) > 0 && ctx.Err() == nil {
		oldWaiting := waiting
		waiting = make([]*Unit, 0)

		tr.c.L.Lock()
		for _, unit := range oldWaiting {
			if !checkDeps(tr.finished, unit.Deps) {
				waiting = append(waiting, unit)
				continue
			}

			tr.Start(ctx, unit)
		}

		tr.c.Wait()
		tr.c.L.Unlock()
	}

	tr.wg.Wait()
	return tr.err
}

type unitRunner struct {
	name string

	sink logr.Sink

	cancel func()
	wg     sync.WaitGroup

	finished map[string]bool
	c        *sync.Cond

	errOnce sync.Once
	err     error
}

func (tr *unitRunner) Start(ctx context.Context, unit *Unit) {
	tr.wg.Add(1)
	go func() {
		defer tr.wg.Done()

		jobName := strings.ToLower(fmt.Sprintf("%s_%s", tr.name, unit.Name))
		log := logr.NewJob(tr.sink, jobName, nil)
		ctx := logr.ReceiverContext(ctx, log)

		log.Event("start", nil)

		err := unit.Fn(ctx)
		if err == nil {
			log.Complete(logr.Success, nil)

			tr.c.L.Lock()
			tr.finished[unit.Name] = true
			tr.c.L.Unlock()
			tr.c.Signal()
			return
		}

		log.Error("error", err, nil)
		log.Complete(logr.Failed, nil)
		tr.errOnce.Do(func() {
			tr.err = err
			tr.cancel()
			tr.c.Signal()
		})
	}()
}

func checkDeps(fin map[string]bool, deps []string) bool {
	for _, dep := range deps {
		_, ok := fin[dep]
		if !ok {
			return false
		}
	}

	return true
}
