package middleware

import "context"

type contextKey struct {
	name string
}

func (k *contextKey) String() string { return "grpc middleware " + k.name }

var (
	ctxServerInfo = &contextKey{"server info"}
)

func ServerInfoContext(ctx context.Context, info *ServerInfo) context.Context {
	if info == nil {
		panic("info is nil")
	}

	return context.WithValue(ctx, ctxServerInfo, info)
}

func ServerInfoFromContext(ctx context.Context) *ServerInfo {
	info, _ := ctx.Value(ctxServerInfo).(*ServerInfo)
	return info
}
