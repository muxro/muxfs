package middleware

import "context"

type Middleware func(next Handler) Handler

type Handler func(ctx context.Context) error
