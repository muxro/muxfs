package middleware

import (
	"context"

	"google.golang.org/grpc"
)

type ServerInfo struct {
	// Server is the service implementation the user provides. This is read-only.
	Server interface{}
	// FullMethod is the full RPC method string, i.e., /package.service/method.
	FullMethod string
	// IsClientStream indicates whether the RPC is a client streaming RPC.
	IsClientStream bool
	// IsServerStream indicates whether the RPC is a server streaming RPC.
	IsServerStream bool
}

func UnaryServerInterceptor(mid ...Middleware) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, ghandler grpc.UnaryHandler) (interface{}, error) {
		var handler Handler

		// the last handler in the chain should call the grpc handler
		var resp interface{}
		handler = func(ctx context.Context) error {
			var err error
			resp, err = ghandler(ctx, req)
			return err
		}

		for i := len(mid) - 1; i >= 0; i-- {
			handler = mid[i](handler)
		}

		ctx = ServerInfoContext(ctx, &ServerInfo{
			Server:     info.Server,
			FullMethod: info.FullMethod,
		})
		return resp, handler(ctx)
	}
}

// wrappedServerStream is a thin wrapper around grpc.ServerStream that allows modifying context.
type wrappedServerStream struct {
	grpc.ServerStream
	// WrappedContext is the wrapper's own Context. You can assign it.
	WrappedContext context.Context
}

// Context returns the wrapper's WrappedContext, overwriting the nested grpc.ServerStream.Context()
func (w *wrappedServerStream) Context() context.Context {
	return w.WrappedContext
}

func StreamServerInterceptor(mid ...Middleware) grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, ghandler grpc.StreamHandler) error {
		var handler Handler

		// the last handler in the chain should call the grpc handler
		handler = func(ctx context.Context) error {
			err := ghandler(srv, &wrappedServerStream{
				ServerStream:   stream,
				WrappedContext: ctx,
			})
			if err == context.Canceled {
				return nil
			}
			return err
		}

		for i := len(mid) - 1; i >= 0; i-- {
			handler = mid[i](handler)
		}

		ctx := ServerInfoContext(stream.Context(), &ServerInfo{
			Server:         srv,
			FullMethod:     info.FullMethod,
			IsClientStream: info.IsClientStream,
			IsServerStream: info.IsServerStream,
		})
		return handler(ctx)
	}
}
