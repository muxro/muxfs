with import <nixos-unstable> {};

buildGoPackage rec {
  name = "muxfs";
  src = ./.;
  goPackagePath = "gitlab.com/muxro/muxfs";
}