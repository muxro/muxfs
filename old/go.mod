module remotefs

go 1.12

require (
	bazil.org/fuse v0.0.0-20180421153158-65cc252bf669
	github.com/aclements/go-rabin v0.0.0-20170911142644-d0b643ea1a4c
	github.com/adrg/xdg v0.2.1
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/fatih/color v1.7.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/petermattis/goid v0.0.0-20180202154549-b0b1615b78e5 // indirect
	github.com/restic/chunker v0.2.0
	github.com/sasha-s/go-deadlock v0.2.0
	github.com/senseyeio/duration v0.0.0-20180430131211-7c2a214ada46
	github.com/stretchr/testify v1.4.0
	github.com/y0ssar1an/q v1.0.9
	gitlab.com/go-figure/grpc-common-middleware v0.0.0-20200114171052-a2f4cf0290d1
	gitlab.com/go-figure/grpc-middleware v0.0.0-20191216110303-7582a1a70751
	gitlab.com/go-figure/instrum v0.0.0-20200113094226-04f2c1f272d5
	gitlab.com/go-figure/logr v0.0.0-20200114170854-fd8903b9e28b
	gitlab.com/go-figure/logr/journal v0.0.0-20200114171146-0285a5f33cd5
	go.uber.org/goleak v0.10.0
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/net v0.0.0-20190926025831-c00fd9afed17
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/grpc v1.25.1
	gopkg.in/urfave/cli.v2 v2.0.0-20190806201727-b62605953717
	gopkg.in/yaml.v2 v2.2.7
)
