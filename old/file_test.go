package remotefs

import (
	"bytes"
	fmt "fmt"
	"io"
	"os"
	"remotefs/blocks"
	"testing"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
	"golang.org/x/net/context"
)

func TestCommit(t *testing.T) {
	// TODO: sometimes hangs forever
	runTest(t, "EmptyFile", 0, func(t *testing.T, fs *TestFS, name, path string) {
		f, err := fs.OpenFile(path, os.O_RDONLY, 0755)
		require.NoError(t, err)
		defer f.Close()

		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		require.Equal(t, 0, len(n.blocks))
	})
	runTest(t, "EmptySparseFile", 0, func(t *testing.T, fs *TestFS, name, path string) {
		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		// create a new node, and create its blocks from 500 zeroes
		nodeExpected, nodeExpectedFS, err := getNode(name, 500)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()
		blocksExpected, _, err := nodeExpected.FS.putBlocksFromReader(io.LimitReader(repeatZeroes{}, 500))
		require.NoError(t, err)
		nodeExpected.blocks = derefBlocks(blocksExpected)

		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		err = f.Truncate(500)
		require.NoError(t, err)

		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		require.Equal(t, nodeExpected.blocks, n.blocks)
		require.Equal(t, nodeExpected.blockDataEnd, n.blockDataEnd)

		actual, err := fs.ReadFile(path)
		require.NoError(t, err)
		expected := repeatByte(0, 500)
		require.Equal(t, expected, actual)
	})
	runTest(t, "JustBlocks", 10, func(t *testing.T, fs *TestFS, name, path string) {
		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		nodeExpected, nodeExpectedFS, err := getNode(name, 10*1024*1024)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		// commit just blocks with no changes - should be a noop
		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		require.Equal(t, nodeExpected.blocks, n.blocks)

		// check that file data matches
		expected := blocks.TestData(path, 0, int64(n.meta.Size))
		actual, err := fs.ReadFile(path)
		require.NoError(t, err)
		require.Equal(t, expected, actual)
	})
	runTest(t, "JustCache", 0, func(t *testing.T, fs *TestFS, name, path string) {
		const cacheSize = 10 * 1024 * 1024
		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		nodeExpected, nodeExpectedFS, err := getNode(name, 10*1024*1024)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		expected := blocks.TestData(path, 0, cacheSize)
		wrote, err := f.Write(expected)
		require.NoError(t, err)
		require.Equal(t, cacheSize, wrote)

		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		require.Equal(t, nodeExpected.blocks, n.blocks)

		actual, err := fs.ReadFile(path)
		require.NoError(t, err)
		require.True(t, n.cache.IsEmpty())
		require.Equal(t, expected, actual)
	})
	runTest(t, "JustCacheVariableSize", 0, func(t *testing.T, fs *TestFS, name, path string) {
		const cacheSize = 19 * 1024 * 1024
		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		nodeExpected, nodeExpectedFS, err := getNode(name, 19*1024*1024)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		expected := blocks.TestData(path, 0, cacheSize)
		wrote, err := f.Write(expected)
		require.NoError(t, err)
		require.Equal(t, cacheSize, wrote)

		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		require.Equal(t, nodeExpected.blocks, n.blocks)

		actual, err := fs.ReadFile(path)
		require.NoError(t, err)
		require.True(t, n.cache.IsEmpty())
		require.Equal(t, expected, actual)
	})
	runTest(t, "BlocksAndCache", 10, func(t *testing.T, fs *TestFS, name, path string) {
		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		nodeExpected, nodeExpectedFS, err := getNode(name, 10*1024*1024)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		fileDataBefore, err := fs.ReadFile(path)
		require.NoError(t, err)

		// fi, err := fs.Stat(path)
		// require.NoError(t, err)
		// fileSize := fi.Size()

		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		writeOffset := int64(5 * 1024 * 1024)
		writeSize := int64(5 * 1024 * 1024)

		wrote, err := f.WriteAt(blocks.TestData(path, writeOffset, writeSize), writeOffset)
		require.NoError(t, err)
		require.Equal(t, writeSize, int64(wrote))

		fi, err := fs.Stat(path)
		require.NoError(t, err)
		size := fi.Size()
		require.Equal(t, writeOffset+writeSize, size)

		fileDataAfter, err := fs.ReadFile(path)
		require.NoError(t, err)
		require.Equal(t, fileDataBefore, fileDataAfter)

		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		require.Equal(t, nodeExpected.blocks, n.blocks)
	})
	runTest(t, "BlocksAndCacheOverwrite", 25, func(t *testing.T, fs *TestFS, name, path string) {
		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		nodeExpected, nodeExpectedFS, err := getNode(name, 25*1024*1024)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		writeOffset := int64(0 * 1024 * 1024)
		writeSize := int64(25 * 1024 * 1024)

		wrote, err := f.WriteAt(blocks.TestData(path, writeOffset, writeSize), writeOffset)
		require.NoError(t, err)
		require.Equal(t, writeSize, int64(wrote))

		fi, err := fs.Stat(path)
		require.NoError(t, err)
		size := fi.Size()
		require.Equal(t, int64(writeSize), size)

		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		require.Equal(t, nodeExpected.blocks, n.blocks)
	})
	runTest(t, "BlocksAndCacheOverwriteDifferentExtend", 25, func(t *testing.T, fs *TestFS, name, path string) {
		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		writeOffset := int64(5 * 1024 * 1024)
		writeSize := int64(25 * 1024 * 1024)

		// regular file data from [0, writeOffset]
		expectedData := blocks.TestData(path, 0, writeOffset)
		expectedData = append(expectedData, bytes.Repeat([]byte{'f'}, int(writeSize))...)
		nodeExpected, nodeExpectedFS, err := getNodeWithData(name, expectedData)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		wrote, err := f.WriteAt(bytes.Repeat([]byte{'f'}, int(writeSize)), writeOffset)
		require.NoError(t, err)
		require.Equal(t, writeSize, int64(wrote))

		fi, err := fs.Stat(path)
		require.NoError(t, err)
		size := fi.Size()
		require.Equal(t, int64(len(expectedData)), size)

		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		require.Equal(t, nodeExpected.blocks, n.blocks)
	})
	runTest(t, "BlocksAndCacheTruncate", 10, func(t *testing.T, fs *TestFS, name, path string) {
		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		nodeExpected, nodeExpectedFS, err := getNode(name, 500)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		const cacheSize = 500
		cacheData := blocks.TestData(path, 0, cacheSize)

		require.NoError(t, err)
		err = f.Truncate(0)
		require.NoError(t, err)

		// write cache data to the start of the file
		wrote, err := f.Write(cacheData)
		require.NoError(t, err)
		require.Equal(t, cacheSize, wrote)

		// get node and commit
		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)
		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		require.Equal(t, nodeExpected.blocks, n.blocks)

		actual, err := fs.ReadFile(path)
		require.NoError(t, err)
		require.True(t, n.cache.IsEmpty())
		require.Equal(t, cacheData, actual)
	})
	runTest(t, "BlocksAndCacheExtend", 10, func(t *testing.T, fs *TestFS, name, path string) {
		const cacheSize = 4 * 1024 * 1024

		nodeExpected, nodeExpectedFS, err := getNode(name, 14*1024*1024)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		// get node and commit
		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		blockData, err := fs.ReadFile(path)
		require.NoError(t, err)

		fi, err := fs.Stat(path)
		require.NoError(t, err)
		size := fi.Size()

		cacheData := blocks.TestData(path, size, cacheSize)

		_, err = f.Seek(0, io.SeekEnd)
		require.NoError(t, err)

		// write cache data to the end of the file
		wrote, err := f.Write(cacheData)
		require.NoError(t, err)
		require.Equal(t, cacheSize, wrote)

		err = n.Commit(defaultCtx)
		require.NoError(t, err)

		// check that the data we read is blockData + cacheData
		expected := append(blockData, cacheData...)
		actual, err := fs.ReadFile(path)
		require.NoError(t, err)
		require.True(t, n.cache.IsEmpty())
		require.Equal(t, expected, actual)

		require.Equal(t, nodeExpected.blocks, n.blocks)
	})
	runTest(t, "HalfBlock", 20, func(t *testing.T, fs *TestFS, name, path string) {
		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		nodeExpected, nodeExpectedFS, err := getNode(name, 50)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		n.mu.RLock()
		trunc := n.blocks[0].Offset + 50
		firstBlockSize := n.blocks[1].Offset
		n.mu.RUnlock()

		f.Truncate(trunc)

		n.mu.RLock()
		require.Equal(t, trunc, n.blockDataEnd)
		require.Equal(t, trunc, n.meta.Size)
		require.NotEqual(t, n.blocksSize, n.blockDataEnd)
		require.Equal(t, firstBlockSize, n.blocksSize)
		n.mu.RUnlock()

		err = n.Commit(defaultCtx)
		require.NoError(t, err)
		require.True(t, n.cache.IsEmpty())
		require.Equal(t, trunc, n.blocksSize)

		require.Equal(t, nodeExpected.blocks, n.blocks)

		expected := blocks.TestData(path, 0, trunc)
		actual, err := fs.ReadFile(path)
		require.NoError(t, err)
		require.Equal(t, expected, actual)
	})
	runTest(t, "BlocksAndSparse", 10, func(t *testing.T, fs *TestFS, name, path string) {
		f, err := fs.OpenFile(path, os.O_RDWR, 0755)
		require.NoError(t, err)
		defer f.Close()

		// create node and add 500 zeroes to the end
		nodeExpected, nodeExpectedFS, err := getNode(name, 10*1024*1024+500)
		require.NoError(t, err)
		defer nodeExpectedFS.Done()

		// reader that contains file data up to file size, and then zeroes
		readerExpected := io.MultiReader(bytes.NewReader(blocks.TestData(path, 0, 10*1024*1024)), io.LimitReader(repeatZeroes{}, 500))
		blocksExpected, _, err := nodeExpected.FS.putBlocksFromReader(readerExpected)
		require.NoError(t, err)
		nodeExpected.blocks = derefBlocks(blocksExpected)

		n, ok := fs.FS.root.children[name].(*FileNode)
		require.True(t, ok)

		n.mu.RLock()
		filesize := n.meta.Size
		extendBy := 500
		trunc := int64(filesize) + int64(extendBy)
		n.mu.RUnlock()

		err = f.Truncate(trunc)
		require.NoError(t, err)

		n.mu.RLock()
		initialBlocksSize := n.blocksSize
		n.mu.RUnlock()

		err = n.Commit(defaultCtx)
		require.NoError(t, err)
		require.True(t, n.cache.IsEmpty())
		require.Equal(t, trunc, int64(n.blocksSize))

		require.Equal(t, nodeExpected.blocks, n.blocks)

		expected := append(blocks.TestData(path, 0, int64(initialBlocksSize)), repeatByte(0, extendBy)...)
		actual, err := fs.ReadFile(path)
		require.NoError(t, err)
		require.Equal(t, expected, actual)
	})
}

func getNode(name string, size int64) (*FileNode, *TestFS, error) {
	path := "/" + name

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: path, Meta: &EntryMeta{Size: size}},
		},
	}))
	if err != nil {
		return nil, nil, err
	}

	node, _ := fs.FS.root.children[name].(*FileNode)
	return node, fs, nil
}

func getNodeWithData(name string, data []byte) (*FileNode, *TestFS, error) {
	path := "/" + name

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: path, Meta: &EntryMeta{Size: 0}},
		},
	}))
	if err != nil {
		return nil, nil, err
	}

	req := &fuse.WriteRequest{Offset: 0, Data: data}
	resp := &fuse.WriteResponse{}
	node, _ := fs.FS.root.children[name].(*FileNode)
	node.Write(context.Background(), req, resp)
	err = node.Commit(defaultCtx)
	if err != nil {
		return nil, nil, err
	}

	return node, fs, nil
}

func printBlocks(node *FileNode, s string) {
	fmt.Printf("Blocks %v:\n", s)
	for i, block := range node.blocks {
		size := node.blocksSize - block.Offset
		if i < len(node.blocks)-1 {
			size = node.blocks[i+1].Offset - block.Offset
		}
		fmt.Printf("- %v - Offset: %v, Size: %v\n", blocks.HashToStr(blocks.ByteSliceToArr(block.Hash))[:6], block.Offset, size)
	}
}

// runTest creates a new test FS, and makes the specified file available with the specified size.
// it then runs a subtest with the same name, and exposes the fs, file name, and file path in a callback function
func runTest(t *testing.T, name string, size int64, f func(t *testing.T, fs *TestFS, name, path string)) {
	path := "/" + name
	// size is in MB
	size *= 1024 * 1024

	t.Run(name, func(t *testing.T) {
		fs, err := NewTestFS(FromSnapshot(&Snapshot{
			Entries: []*Entry{
				&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
				&Entry{Path: path, Meta: &EntryMeta{Size: size}},
			},
		}))
		defer fs.Done()
		require.NoError(t, err)

		f(t, fs, name, path)
	})
}

func TestCommitBenchmarkCorrectness(t *testing.T) {
	const fileSize = 10 * 1024 * 1024
	fs, _ := NewTestFS(FromPaths("/"))
	defer fs.Done()

	data := make([]byte, 4096)

	root := fs.FS.root

	// create new node
	createReq := &fuse.CreateRequest{
		Name: "file",
		Mode: 0644,
	}
	createResp := &fuse.CreateResponse{}
	root.Create(context.Background(), createReq, createResp)

	node := fs.FS.root.children["file"].(*FileNode)

	// write data to node
	writeReq := &fuse.WriteRequest{}
	writeResp := &fuse.WriteResponse{}
	for o := int64(0); o <= fileSize; o += 4096 {
		writeReq.Offset = o
		writeReq.Data = data

		node.Write(context.Background(), writeReq, writeResp)
	}

	// turn that data into blocks
	node.Commit(defaultCtx)

	// truncate file to half size
	setAttrReq := &fuse.SetattrRequest{}
	setAttrReq.Size = uint64(fileSize / 2)
	setAttrResp := &fuse.SetattrResponse{}
	setAttrReq.Valid |= fuse.SetattrSize
	node.Setattr(context.Background(), setAttrReq, setAttrResp)

	expected := make([]byte, fileSize/2)
	n, _ := node.ReadAt(expected, 0)
	require.Equal(t, fileSize/2, n)

	// save node state
	nodeBefore := *fs.FS.root.children["file"].(*FileNode)
	metaBefore := *nodeBefore.meta

	node.Commit(defaultCtx)

	nodeAfter := nodeBefore
	nodeAfter.meta = &metaBefore

	actual := make([]byte, fileSize/2)
	n, _ = nodeAfter.ReadAt(actual, 0)
	require.Equal(t, fileSize/2, n)

	require.Equal(t, expected, actual)

	require.True(t, nodeAfter.cache.IsEmpty())
}

func BenchmarkCommitBlocks(b *testing.B) {
	benchCases := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "25MB", size: 25 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024, noskip: true},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			fs, _ := NewTestFS(FromSnapshot(&Snapshot{
				Entries: []*Entry{
					&Entry{Path: "/"},
					&Entry{Path: "/file", Meta: &EntryMeta{Size: bench.size}},
				},
			}))
			defer fs.Done()

			node := fs.FS.root.children["file"].(*FileNode)

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				node.Commit(defaultCtx)
			}
		})
	}
}

func BenchmarkCommitCache(b *testing.B) {
	benchCases := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "25MB", size: 25 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024, noskip: true},
		{desc: "400MB", size: 400 * 1024 * 1024},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			fs, _ := NewTestFS(FromSnapshot(&Snapshot{
				Entries: []*Entry{
					&Entry{Path: "/"},
					&Entry{Path: "/file", Meta: &EntryMeta{Size: 0}},
				},
			}))
			defer fs.Done()

			node := fs.FS.root.children["file"].(*FileNode)

			data := make([]byte, 4096)
			// write data to node
			writeReq := &fuse.WriteRequest{}
			writeResp := &fuse.WriteResponse{}
			for o := int64(0); o <= bench.size; o += 4096 {
				writeReq.Offset = o
				writeReq.Data = data

				node.Write(context.Background(), writeReq, writeResp)
			}

			nodeBefore := *fs.FS.root.children["file"].(*FileNode)
			metaBefore := *nodeBefore.meta

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				benchNode := nodeBefore
				benchNode.meta = &metaBefore
				benchNode.Commit(defaultCtx)
			}
		})
	}
}

func BenchmarkCommitMixed(b *testing.B) {
	benchCases := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "25MB", size: 25 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024, noskip: true},
		{desc: "400MB", size: 400 * 1024 * 1024},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			fs, _ := NewTestFS(FromSnapshot(&Snapshot{
				Entries: []*Entry{
					&Entry{Path: "/"},
					&Entry{Path: "/file", Meta: &EntryMeta{Size: bench.size / 2}},
				},
			}))
			defer fs.Done()

			node := fs.FS.root.children["file"].(*FileNode)

			data := make([]byte, 4096)
			// write data to node
			writeReq := &fuse.WriteRequest{}
			writeResp := &fuse.WriteResponse{}
			for o := int64(bench.size / 2); o <= bench.size; o += 4096 {
				writeReq.Offset = o
				writeReq.Data = data

				node.Write(context.Background(), writeReq, writeResp)
			}

			nodeBefore := *fs.FS.root.children["file"].(*FileNode)
			metaBefore := *nodeBefore.meta

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				benchNode := nodeBefore
				benchNode.meta = &metaBefore
				benchNode.Commit(defaultCtx)
			}
		})
	}
}

func BenchmarkCommitOverwriteSame(b *testing.B) {
	benchCases := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "25MB", size: 25 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024, noskip: true},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			fs, _ := NewTestFS(FromSnapshot(&Snapshot{
				Entries: []*Entry{
					&Entry{Path: "/"},
					&Entry{Path: "/file", Meta: &EntryMeta{Size: bench.size}},
				},
			}))
			defer fs.Done()

			node := fs.FS.root.children["file"].(*FileNode)

			data := blocks.TestData("/file", 0, bench.size)
			// we just write the whole file in one call
			writeReq := &fuse.WriteRequest{}
			writeResp := &fuse.WriteResponse{}
			writeReq.Offset = 0
			writeReq.Data = data
			node.Write(context.Background(), writeReq, writeResp)

			nodeBefore := *fs.FS.root.children["file"].(*FileNode)
			metaBefore := *nodeBefore.meta

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				benchNode := nodeBefore
				benchNode.meta = &metaBefore
				benchNode.Commit(defaultCtx)
			}
		})
	}
}
func BenchmarkCommitOverwriteDifferent(b *testing.B) {
	benchCases := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "25MB", size: 25 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024, noskip: true},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			fs, _ := NewTestFS(FromSnapshot(&Snapshot{
				Entries: []*Entry{
					&Entry{Path: "/"},
					&Entry{Path: "/file", Meta: &EntryMeta{Size: bench.size}},
				},
			}))
			defer fs.Done()

			node := fs.FS.root.children["file"].(*FileNode)

			data := blocks.TestData("DIFFERENT", 0, bench.size)
			// we just write the whole file in one call
			writeReq := &fuse.WriteRequest{}
			writeResp := &fuse.WriteResponse{}
			writeReq.Offset = 0
			writeReq.Data = data
			node.Write(context.Background(), writeReq, writeResp)

			nodeBefore := *fs.FS.root.children["file"].(*FileNode)
			metaBefore := *nodeBefore.meta

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				benchNode := nodeBefore
				benchNode.meta = &metaBefore
				benchNode.Commit(defaultCtx)
			}
		})
	}
}
