//+build race

package remotefs

import (
	"context"
	"testing"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
)

func TestRaceDirAttr(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/dir/"))
	defer fs.Done()
	require.NoError(t, err)

	n, ok := fs.FS.root.children["dir"].(*DirNode)
	require.True(t, ok)

	testRace(func() {
		req := &fuse.SetattrRequest{}
		req.Valid = fuse.SetattrMode
		req.Mode = 0755
		resp := &fuse.SetattrResponse{}
		n.Setattr(context.Background(), req, resp)
	}, func() {
		var attr fuse.Attr
		n.Attr(context.Background(), &attr)
	})
}

func TestRaceCreate(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/"))
	defer fs.Done()
	require.NoError(t, err)

	root := fs.FS.root

	testRace(func() {
		req := &fuse.CreateRequest{Name: "file", Flags: fuse.OpenReadOnly, Mode: 0777}
		resp := &fuse.CreateResponse{}
		root.Create(context.Background(), req, resp)
	}, func() {
		root.Lookup(context.Background(), "file")
	})
}

func TestRaceMkdir(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/"))
	defer fs.Done()
	require.NoError(t, err)

	root := fs.FS.root

	testRace(func() {
		req := &fuse.MkdirRequest{Name: "dir", Mode: 0777}
		root.Mkdir(context.Background(), req)
	}, func() {
		root.Lookup(context.Background(), "dir")
	})
}

func TestRaceRemove(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()
	require.NoError(t, err)

	root := fs.FS.root

	testRace(func() {
		req := &fuse.RemoveRequest{Name: "file", Dir: false}
		root.Remove(context.Background(), req)
	}, func() {
		root.Lookup(context.Background(), "file")
	})
}

func TestRaceRename(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/oldname", "/newdir/"))
	defer fs.Done()
	require.NoError(t, err)

	root := fs.FS.root

	newDir := root.children["newdir"]

	testRace(func() {
		req := &fuse.RenameRequest{OldName: "oldname", NewName: "newname"}
		root.Rename(context.Background(), req, newDir)
	}, func() {
		root.Lookup(context.Background(), "newdir")
	})
}

func TestRaceSymlink(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()
	require.NoError(t, err)

	root := fs.FS.root

	testRace(func() {
		req := &fuse.SymlinkRequest{NewName: "symlink", Target: "file"}
		root.Symlink(context.Background(), req)
	}, func() {
		root.Lookup(context.Background(), "symlink")
	})
}

func TestRaceReadDirAll(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/"))
	defer fs.Done()
	require.NoError(t, err)

	root := fs.FS.root

	testRace(func() {
		req := &fuse.MkdirRequest{Name: "dir", Mode: 0777}
		root.Mkdir(context.Background(), req)
	}, func() {
		root.ReadDirAll(context.Background())
	})
}

func testRace(writeOp, readOp func()) {
	testRaceIterations(1000, writeOp, readOp)
}

func testRaceShort(writeOp, readOp func()) {
	testRaceIterations(1, writeOp, readOp)
}

func testRaceIterations(iterations int, writeOp, readOp func()) {
	done := make(chan bool, 3)

	writeFn := func() {
		for i := 0; i < iterations; i++ {
			writeOp()
		}

		done <- true
	}
	readFn := func() {
		for i := 0; i < iterations; i++ {
			readOp()
		}

		done <- true
	}

	go readFn()
	go writeFn()
	go writeFn()

	for i := 0; i < 3; i++ {
		<-done
	}
}
