package remotefs

import (
	fmt "fmt"
	"os"
	"remotefs/blocks"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"github.com/y0ssar1an/q"
)

func TestSnapshotCreation(t *testing.T) {
	require := require.New(t)

	entries := testEntries(
		"/",
		"/foo",
		"/diz/bar/baz",
		"/diz/",
		"/diz/bar/",
		"/diz/bar/kix",
		"/diz/bar/qux/",
		"/diz/bar/qux/foobie",
	)
	links := []*Link{
		&Link{Path: "/symlink", Target: "/diz/bar/kix", Meta: &EntryMeta{Size: 12, Mode: uint32(0777 | os.ModeSymlink)}},
		&Link{Path: "/another_symlink", Target: "../../this_doesnt_exist", Meta: &EntryMeta{Size: 23, Mode: uint32(0777 | os.ModeSymlink)}},
	}
	fs, err := NewTestFS(FromSnapshot(&Snapshot{Entries: entries, Links: links}))
	require.Nil(err)
	defer fs.Done()

	snap, err := fs.FS.makeSnapshot(defaultCtx)
	require.Nil(err)

	for _, entry := range entries {
		var snapEntry *Entry
		for _, se := range snap.Entries {
			if se.Path == entry.Path {
				snapEntry = se
				break
			}
		}
		require.NotNil(snapEntry, "cannot find entry: "+entry.Path)
	}
	for _, link := range links {
		var snapLink *Link
		for _, sl := range snap.Links {
			if sl.Path == link.Path {
				snapLink = sl
				break
			}
		}
		require.NotNil(snapLink, "cannot find linkw: "+link.Path)
	}
}

func TestSnapshotValidation(t *testing.T) {
	// no root entry
	_, err := NewTestFS(FromSnapshot(&Snapshot{}))
	require.NotNil(t, err)

	// duplicates
	_, err = NewTestFS(FromPaths("/", "/file", "/file"))
	require.NotNil(t, err)
	_, err = NewTestFS(FromSnapshot(testLinksSnapshot("/symlink", "/symlink")))
	require.NotNil(t, err)

	// file name < 256
	_, err = NewTestFS(FromPaths("/", "/"+strings.Repeat("a", 256)))
	require.NotNil(t, err)
	_, err = NewTestFS(FromSnapshot(testLinksSnapshot("/" + strings.Repeat("a", 256))))
	require.NotNil(t, err)

	// path < 4096
	paths := []string{"/"}
	const repeatPath = 17 // 17 * 255 = 4335, which is > 4096
	for i := 1; i <= repeatPath; i++ {
		name := strings.Repeat("a", 255)
		paths = append(paths, strings.Repeat("/"+name, i))
	}
	_, err = NewTestFS(FromPaths(paths...))
	require.NotNil(t, err)
	_, err = NewTestFS(FromSnapshot(testLinksSnapshot(paths[1:]...)))
	require.NotNil(t, err)

	// parent must exist
	_, err = NewTestFS(FromPaths("/", strings.Repeat("/a", 2)))
	require.NotNil(t, err)
	_, err = NewTestFS(FromSnapshot(testLinksSnapshot(strings.Repeat("/a", 2))))
	require.NotNil(t, err)

	// enough users exist
	_, err = NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/uid", Meta: &EntryMeta{Uid: 1}},
		},
	}))
	require.NotNil(t, err)
	_, err = NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
		},
		Links: []*Link{
			&Link{Path: "/uid", Meta: &EntryMeta{Uid: 1}},
		},
	}))
	require.NotNil(t, err)

	// enough groups exist
	_, err = NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/gid", Meta: &EntryMeta{Gid: 1}},
		},
	}))
	require.NotNil(t, err)
	_, err = NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
		},
		Links: []*Link{
			&Link{Path: "/gid", Meta: &EntryMeta{Gid: 1}},
		},
	}))
	require.NotNil(t, err)

	// sorted
	_, err = NewTestFS(
		FromSnapshot(&Snapshot{
			Entries: []*Entry{
				&Entry{Path: "/"},
				&Entry{Path: "/b"},
				&Entry{Path: "/a"},
				&Entry{Path: "/c"},
			},
		}),
		Sort(false),
	)
	require.NotNil(t, err)
	_, err = NewTestFS(
		FromSnapshot(&Snapshot{
			Entries: []*Entry{
				&Entry{Path: "/"},
			},
			Links: []*Link{
				&Link{Path: "/b", Target: "/target"},
				&Link{Path: "/a", Target: "/target"},
				&Link{Path: "/c", Target: "/target"},
			},
		}),
		Sort(false),
	)
	require.NotNil(t, err)
}

func testLinksSnapshot(paths ...string) *Snapshot {
	var links []*Link
	for _, path := range paths {
		links = append(links, &Link{Path: path, Target: "/target"})
	}
	return &Snapshot{
		Entries: []*Entry{&Entry{Path: "/"}},
		Links:   links,
	}
}

func TestSnapshotSorted(t *testing.T) {
	t.Run("EntriesAndLinks", func(t *testing.T) {
		require := require.New(t)

		// FIXME: needs to test sorting uppercase mixed with lowercase too
		entryPaths := []string{
			"/",
			"/5",
			"/diz/bar/kix",
			"/19",
			"/cd",
			"/diz/bar/qux/foobie",
			"/abc",
			"/diz/",
			"/diz/bar/baz",
			"/diz/bar/",
			"/diz/bar/qux/",
		}
		links := testLinks(
			"/symlinkb", "/targeta",
			"/symlinkc", "/targetb",
			"/symlinkd", "/targeta",
		)
		entries := testEntries(entryPaths...)
		fs, err := NewTestFS(FromSnapshot(&Snapshot{Entries: entries, Links: links}))
		require.Nil(err)
		defer fs.Done()

		// create some new files that should be at the start
		f, err := fs.Create("/a")
		defer f.Close()
		require.Nil(err)

		err = fs.Symlink("/symlinka", "/targetc")
		require.Nil(err)

		snap, err := fs.FS.makeSnapshot(defaultCtx)
		require.Nil(err)

		for i := range snap.Entries {
			if i == 0 {
				continue
			}
			require.False(snap.Entries[i-1].Path >= snap.Entries[i].Path)
		}

		for i := range snap.Links {
			if i == 0 {
				continue
			}
			require.False(snap.Links[i-1].Path >= snap.Links[i].Path)
		}
	})
	t.Run("UsersAndGroups", func(t *testing.T) {
		fs1, err := NewTestFS(FromSnapshot(&Snapshot{
			Entries: []*Entry{
				&Entry{Path: "/", Meta: &EntryMeta{Uid: 0, Gid: 0}},
				&Entry{Path: "/fuser0", Meta: &EntryMeta{Uid: 0, Gid: 0}},
				&Entry{Path: "/fuser1", Meta: &EntryMeta{Uid: 1, Gid: 1}},
				&Entry{Path: "/fuser2", Meta: &EntryMeta{Uid: 2, Gid: 2}},
			},
			Links: []*Link{},
			Users: []*Owner{
				&Owner{Name: "user1", Id: 1001},
				&Owner{Name: "user2", Id: 1002},
			},
			Groups: []*Owner{
				&Owner{Name: "group1", Id: 2001},
				&Owner{Name: "group2", Id: 2002},
			},
		}))
		require.NoError(t, err)
		defer fs1.Done()

		q.Q(fs1.FS.users)

		snap1, err := fs1.FS.makeSnapshot(defaultCtx)
		require.NoError(t, err)

		snapEntryUserID := func(snap *Snapshot, entryIdx int) int {
			if snap.Entries[entryIdx].Meta.Uid == 0 {
				return -1
			}
			return int(snap.Users[snap.Entries[entryIdx].Meta.Uid-1].Id)
		}

		// -1 == current user
		require.Equal(t, -1, snapEntryUserID(snap1, 1))
		require.Equal(t, 1001, snapEntryUserID(snap1, 2))
		require.Equal(t, 1002, snapEntryUserID(snap1, 3))

		snap1.Meta.CreatedAt = 0

		snap1Hash, err := snap1.hash()
		require.NoError(t, err)
		// q.Q(snap1)
		q.Q(snap1Hash.String())

		fs2, err := NewTestFS(FromSnapshot(&Snapshot{
			Entries: []*Entry{
				&Entry{Path: "/", Meta: &EntryMeta{Uid: 0, Gid: 0}},
				&Entry{Path: "/fuser0", Meta: &EntryMeta{Uid: 0, Gid: 0}},
				&Entry{Path: "/fuser1", Meta: &EntryMeta{Uid: 2, Gid: 2}},
				&Entry{Path: "/fuser2", Meta: &EntryMeta{Uid: 1, Gid: 1}},
			},
			Links: []*Link{},
			Users: []*Owner{
				&Owner{Name: "user2", Id: 1002},
				&Owner{Name: "user1", Id: 1001},
			},
			Groups: []*Owner{
				&Owner{Name: "group2", Id: 2002},
				&Owner{Name: "group1", Id: 2001},
			},
		}))
		require.NoError(t, err)
		defer fs2.Done()

		snap2, err := fs2.FS.makeSnapshot(defaultCtx)
		require.NoError(t, err)

		q.Q(snap2)

		// -1 == current user
		require.Equal(t, -1, snapEntryUserID(snap2, 1))
		require.Equal(t, 1001, snapEntryUserID(snap2, 2))
		require.Equal(t, 1002, snapEntryUserID(snap2, 3))

		snap2.Meta.CreatedAt = 0

		snap2Hash, err := snap2.hash()
		require.NoError(t, err)
		q.Q(snap2Hash.String())

		require.Equal(t, snap1Hash, snap2Hash)
	})
}

func printRefs(fses ...*FS) {
	if len(fses) == 0 {
		return
	}

	store := fses[0].store
	refs, err := store.(blocks.StoreTest).Refs()
	if err != nil {
		panic(err)
	}

	fmt.Println("Hashes: {")
	for ref, hashes := range refs {
		fmt.Printf("\t")

		for i, fs := range fses {
			i++
			if ref == fs.ref {
				fmt.Printf("[FS %v commit ref] ", i)
			}
			if ref == fs.snapshotUpdater.updater.Ref {
				fmt.Printf("[FS %v snapshotUpdater ref] ", i)
			}

			infos, err := fs.snapshotUpdater.Get(fs.ctx)
			if err != nil {
				panic(err)
			}
			for j, info := range infos {
				j++
				if ref == blocks.IDFromByteSlice(info.Meta.Ref) {
					fmt.Printf("[FS %v snapshot %v, tag '%v'] ", i, j, info.Meta.Tag)
				}
			}
		}

		fmt.Printf("%v: [", ref)
		if len(hashes) == 0 {
			fmt.Printf("],\n")
			continue
		}
		fmt.Printf("\n")
		for _, hash := range hashes {
			fmt.Printf("\t\t%v\n", hash)
		}
		fmt.Printf("\t],\n")
	}
	fmt.Println("}")
}

func TestScheduleSnapshots(t *testing.T) {
	// FIXME: duration can't be under 1 second
	next, err := nextSnapshotterFromString("PT1S")
	require.NoError(t, err)

	t.Run("SingleGroup", func(t *testing.T) {
		fs, err := NewTestFS(
			FromPaths("/", "/file"),
			SnapshotRetentionGroups([]*snapshotRetentionGroup{
				&snapshotRetentionGroup{
					name:     "one second",
					capacity: 2,
					next:     next,
				},
			}),
		)
		require.NoError(t, err)
		defer fs.Done()

		infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
		require.NoError(t, err)
		require.Equal(t, 1, len(infos))

		// wait for timer
		time.Sleep(time.Second + 80*time.Millisecond)
		// trigger change
		fs.FS.changed()
		// wait for first snapshot
		time.Sleep(50 * time.Millisecond)

		infos, err = fs.FS.snapshotUpdater.Get(fs.FS.ctx)
		require.NoError(t, err)
		require.Equal(t, 2, len(infos))
		firstInfo := infos[0]

		// wait for timer
		time.Sleep(time.Second + 80*time.Millisecond)
		// trigger change
		fs.FS.changed()
		// wait for second snapshot
		time.Sleep(50 * time.Millisecond)
		infos, err = fs.FS.snapshotUpdater.Get(fs.FS.ctx)
		require.NoError(t, err)
		require.Equal(t, 3, len(infos))
		secondInfo := infos[0]

		// wait for timer
		time.Sleep(time.Second + 80*time.Millisecond)
		// trigger change
		fs.FS.changed()
		// wait for third snapshot
		time.Sleep(50 * time.Millisecond)
		// capacity filled, the oldest snapshot must have been removed
		infos, err = fs.FS.snapshotUpdater.Get(fs.FS.ctx)
		require.NoError(t, err)
		require.Equal(t, 3, len(infos))

		// check that the secondInfo was moved down a position
		require.NotEqual(t, infos[0], secondInfo)
		require.Equal(t, infos[1], secondInfo)

		// check that the first snapshot was removed
		exists, err := fs.FS.store.Has(defaultCtx, blocks.ByteSliceToArr(firstInfo.Hash))
		require.NoError(t, err)
		require.False(t, exists)
	})
	t.Run("MultipleGroups", func(t *testing.T) {
		fs, err := NewTestFS(
			FromPaths("/", "/file"),
			SnapshotRetentionGroups([]*snapshotRetentionGroup{
				&snapshotRetentionGroup{
					name:     "1",
					capacity: 1,
					next:     next,
				},
				&snapshotRetentionGroup{
					name:     "2",
					capacity: 1,
					next:     next,
				},
			}),
		)
		require.NoError(t, err)
		defer fs.Done()

		var (
			g1hash, g2hash blocks.Hash
			emptyHash      blocks.Hash
		)

		for i := 0; i < 2; i++ {
			// trigger change
			fs.FS.changed()
			time.Sleep(time.Second + 80*time.Millisecond)

			// Check that previous group hashes don't exist anymore (both groups have cap 1)
			if g1hash != emptyHash && g2hash != emptyHash {
				infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
				require.NoError(t, err)

				for _, info := range infos {
					hash := blocks.HashFromSlice(info.Hash)
					if info.Meta.Tag == "1" {
						require.NotEqual(t, hash, g1hash)
					}
					if info.Meta.Tag == "2" {
						require.NotEqual(t, hash, g2hash)
					}
				}
			}

			infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
			require.NoError(t, err)
			for _, info := range infos {
				hash := blocks.HashFromSlice(info.Hash)
				if info.Meta.Tag == "1" {
					g1hash = hash
				}
				if info.Meta.Tag == "2" {
					g2hash = hash
				}
			}
		}

		var foundG1, foundG2 bool
		infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
		require.NoError(t, err)
		for _, info := range infos {
			hash := blocks.HashFromSlice(info.Hash)
			if info.Meta.Tag == "1" {
				require.False(t, foundG1)
				require.Equal(t, hash, g1hash)
				foundG1 = true
			}
			if info.Meta.Tag == "2" {
				require.False(t, foundG2)
				require.Equal(t, hash, g2hash)
				foundG2 = true
			}
		}
	})
}

func TestSnapshotRetentionGroups(t *testing.T) {
	testCases := []struct {
		desc    string
		groups  []*snapshotRetentionGroup
		snapMap map[string][]string
		remove  []string
		err     error
	}{
		{
			desc: "NotSorted",
			groups: []*snapshotRetentionGroup{
				newGroup("daily", 3, "daily"),
			},
			snapMap: map[string][]string{
				"daily": {
					"2019-04-01 05:52",
					"2019-04-02 04:31",
					"2019-04-02 09:20",
					"2019-04-03 03:41",
				},
			},
			remove: nil,
			err:    errSnapshotInfosNotSorted,
		},
		{
			desc: "Hourly",
			groups: []*snapshotRetentionGroup{
				newGroup("hourly", 3, "hourly"),
			},
			snapMap: map[string][]string{
				"hourly": {
					"2019-04-01 05:52",
					"2019-04-01 04:31",
					"2019-04-01 04:20",
					"2019-04-01 03:41",
				},
			},
			remove: []string{
				"2019-04-01 04:20",
			},
			err: nil,
		},
		{
			desc: "Daily",
			groups: []*snapshotRetentionGroup{
				newGroup("daily", 3, "daily"),
			},
			snapMap: map[string][]string{
				"daily": {
					"2019-04-03 03:41",
					"2019-04-02 09:20",
					"2019-04-02 04:31",
					"2019-04-01 05:52",
				},
			},
			remove: []string{
				"2019-04-02 04:31",
			},
			err: nil,
		},
		{
			desc: "Weekly",
			groups: []*snapshotRetentionGroup{
				newGroup("weekly", 3, "weekly"),
			},
			snapMap: map[string][]string{
				"weekly": {
					"2019-04-20 00:00",
					"2019-04-18 00:00",
					"2019-04-13 00:00",
					"2019-04-06 00:00",
				},
			},
			remove: []string{
				"2019-04-18 00:00",
			},
			err: nil,
		},
		{
			desc: "Monthly",
			groups: []*snapshotRetentionGroup{
				newGroup("monthly", 3, "monthly"),
			},
			snapMap: map[string][]string{
				"monthly": {
					"2019-04-01 00:00",
					"2019-03-28 00:00",
					"2019-03-27 00:00",
					"2019-02-01 00:00",
				},
			},
			remove: []string{
				"2019-03-27 00:00",
			},
			err: nil,
		},
		{
			desc: "Yearly",
			groups: []*snapshotRetentionGroup{
				newGroup("yearly", 3, "yearly"),
			},
			snapMap: map[string][]string{
				"yearly": {
					"2022-04-01 00:00",
					"2021-03-28 00:00",
					"2021-02-27 00:00",
					"2019-02-01 00:00",
				},
			},
			remove: []string{
				"2021-02-27 00:00",
			},
			err: nil,
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			for _, group := range tC.groups {
				infos := getTestSnapshotInfos(group.name, tC.snapMap[group.name])

				remove, err := group.GetSnapshotsToRemove(infos)
				require.Equal(t, tC.err, err)

				var removeStrings []string
				for _, rm := range remove {
					removeStrings = append(removeStrings, rm.timestamp().Format("2006-01-02 15:04"))
				}

				require.Equal(t, tC.remove, removeStrings)
			}
		})
	}
}

func TestGroupPeriodNext(t *testing.T) {
	testCases := []struct {
		desc    string
		snapMap map[string][]string
		groups  []*snapshotRetentionGroup
		expect  map[string]string
	}{
		{
			desc: "Hourly",
			groups: []*snapshotRetentionGroup{
				newGroup("hourly", 24, "hourly"),
			},
			snapMap: map[string][]string{
				"hourly": {
					"2019-10-29 00:00",
				},
			},
			expect: map[string]string{
				"hourly": "2019-10-29 01:00",
			},
		},
		{
			desc: "Daily",
			groups: []*snapshotRetentionGroup{
				newGroup("daily", 7, "daily"),
			},
			snapMap: map[string][]string{
				"daily": {
					"2019-10-29 00:00",
				},
			},
			expect: map[string]string{
				"daily": "2019-10-30 00:00",
			},
		},
		{
			desc: "Weekly",
			groups: []*snapshotRetentionGroup{
				newGroup("weekly", 4, "weekly"),
			},
			snapMap: map[string][]string{
				"weekly": {
					"2019-10-29 00:00",
				},
			},
			expect: map[string]string{
				"weekly": "2019-11-04 00:00",
			},
		},
		{
			desc: "Monthly",
			groups: []*snapshotRetentionGroup{
				newGroup("monthly", 7, "monthly"),
			},
			snapMap: map[string][]string{
				"monthly": {
					"2019-10-29 00:00",
				},
			},
			expect: map[string]string{
				"monthly": "2019-11-01 00:00",
			},
		},
		{
			desc: "Yearly",
			groups: []*snapshotRetentionGroup{
				newGroup("yearly", 10, "yearly"),
			},
			snapMap: map[string][]string{
				"yearly": {
					"2019-05-22 00:00",
				},
			},
			expect: map[string]string{
				"yearly": "2020-01-01 00:00",
			},
		},
		{
			desc: "Period",
			groups: []*snapshotRetentionGroup{
				newGroup("hourly", 1, "PT1H"),
				newGroup("daily", 1, "P1D"),
				newGroup("weekly", 1, "P7D"),
				newGroup("monthly", 1, "P1M"),
				newGroup("yearly", 1, "P1Y"),
			},
			snapMap: map[string][]string{
				"hourly": {
					"2019-05-22 14:22",
				},
				"daily": {
					"2019-05-22 14:22",
				},
				"weekly": {
					"2019-05-22 14:22",
				},
				"monthly": {
					"2019-05-22 14:22",
				},
				"yearly": {
					"2019-05-22 14:22",
				},
			},
			expect: map[string]string{
				"hourly":  "2019-05-22 15:22",
				"daily":   "2019-05-23 14:22",
				"weekly":  "2019-05-29 14:22",
				"monthly": "2019-06-21 14:22",
				"yearly":  "2020-05-21 20:11",
			},
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			for _, group := range tC.groups {
				infos := getTestSnapshotInfos(group.name, tC.snapMap[group.name])

				expected, err := time.ParseInLocation("2006-01-02 15:04", tC.expect[group.name], time.UTC)
				require.NoError(t, err)
				next := group.NextSnapshot(infos)
				// don't care about seconds
				next = time.Date(next.Year(), next.Month(), next.Day(), next.Hour(), next.Minute(), 0, 0, next.Location())
				require.Equal(t, expected, next)
			}
		})
	}
}

// func TestSnapshotRetentionGroups(t *testing.T) {
// 	fs, err := NewTestFS(
// 		FromPaths("/"),
// 		SnapshotRetentionGroups([]*snapshotRetentionGroup{
// 			&snapshotRetentionGroup{
// 				name:     "second",
// 				capacity: 5,
// 				next:     snapshotPeriod(50 * time.Millisecond),
// 			},
// 		}),
// 	)
// 	require.NoError(t, err)

// 	time.Sleep(10 * time.Second)

// 	infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
// 	require.NoError(t, err)

// 	panic("unimplemented")
// 	fmt.Println("infos:", infos)

// 	fs.Done()
// }

func getTestSnapshotInfos(group string, lines []string) (infos []*SnapshotInfo) {
	for _, line := range lines {
		dur, err := time.ParseInLocation("2006-01-02 15:04", line, time.UTC)
		if err != nil {
			panic(err)
		}
		infos = append(infos, &SnapshotInfo{Meta: &SnapshotMeta{Tag: group, CreatedAt: dur.Unix()}})
		// add same info but in a different group
		infos = append(infos, &SnapshotInfo{Meta: &SnapshotMeta{Tag: "NOT_" + group, CreatedAt: dur.Unix()}})
	}
	return
}
