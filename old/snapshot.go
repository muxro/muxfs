package remotefs

import (
	"errors"
	fmt "fmt"
	"io/ioutil"
	"os/user"
	"path/filepath"
	"remotefs/blocks"
	"sort"
	"strconv"
	"time"

	proto "github.com/golang/protobuf/proto"
	"golang.org/x/net/context"
)

// GetSnapshot reads a snapshot block from the store, and returns it as an unmarshaled Snapshot object.
func GetSnapshot(ctx context.Context, store blocks.Store, hash blocks.Hash) (*Snapshot, error) {
	snapshot := &Snapshot{}

	// get Snapshot from store
	snapReader, err := store.Get(ctx, hash)
	if err != nil {
		return nil, fmt.Errorf("snapshot block does not exist: %w", err)
	}
	defer snapReader.Close()

	snapData, err := ioutil.ReadAll(snapReader)
	if err != nil {
		return nil, err
	}

	err = proto.Unmarshal(snapData, snapshot)
	if err != nil {
		return nil, err
	}

	return snapshot, nil
}

// Valid determines if the snapshot is valid
func (s *Snapshot) Valid() error {
	if !s.containsRootEntry() {
		return errors.New("root entry doesn't exist")
	}

	// keep track of all paths (entries + symlinks)
	paths := make(map[string]bool)

	// check if sorted
	var sortedEntries []*Entry
	for _, entry := range s.Entries {
		sortedEntries = append(sortedEntries, entry)
	}

	sort.Sort(sortEntryAlphabetic(sortedEntries))
	for i, entry := range s.Entries {
		if entry.Path != sortedEntries[i].Path {
			return fmt.Errorf("not sorted: %v > %v", entry.Path, sortedEntries[i].Path)
		}
	}

	for _, e := range s.Entries {
		if _, exists := paths[e.Path]; exists {
			return fmt.Errorf("duplicate: %v", e.Path)
		}
		paths[e.Path] = true

		if len(filepath.Base(e.Path)) > 255 {
			return fmt.Errorf("file name longer than 255 characters: %v", e.Path)
		}
		if len(e.Path) > 4096 {
			return fmt.Errorf("total file path length longer than 4096 characters: %v", e.Path)
		}

		if !s.pathHasParent(e.Path) {
			return fmt.Errorf("parent dir doesn't exist: %v", e.Path)
		}

		if len(s.Users) <= int(e.Meta.Uid) {
			return fmt.Errorf("not enough users in snapshot for: %v. wanted: %v, have: %v", e.Path, e.Meta.Uid+1, len(s.Users))
		}
		if len(s.Groups) <= int(e.Meta.Gid) {
			return fmt.Errorf("not enough groups in snapshot for: %v. wanted: %v, have: %v", e.Path, e.Meta.Gid+1, len(s.Groups))
		}
	}
	for i, l := range s.Links {
		if _, exists := paths[l.Path]; exists {
			return fmt.Errorf("duplicate: %v", l.Path)
		}
		paths[l.Path] = true

		// check if sorted
		if i > 0 && s.Links[i-1].Path > s.Links[i].Path {
			return fmt.Errorf("not sorted: %v > %v", s.Links[i-1].Path, s.Links[i].Path)
		}

		if len(filepath.Base(l.Path)) > 255 {
			return fmt.Errorf("file name longer than 255 characters: %v", l.Path)
		}
		if len(l.Path) > 4096 {
			return fmt.Errorf("total file path length longer than 4096 characters: %v", l.Path)
		}

		if !s.pathHasParent(l.Path) {
			return fmt.Errorf("parent dir doesn't exist: %v", l.Path)
		}

		if len(s.Users) <= int(l.Meta.Uid) {
			return fmt.Errorf("not enough users in snapshot for: %v. wanted: %v, have: %v", l.Path, l.Meta.Uid+1, len(s.Users))
		}
		if len(s.Groups) <= int(l.Meta.Gid) {
			return fmt.Errorf("not enough groups in snapshot for: %v. wanted: %v, have: %v", l.Path, l.Meta.Gid+1, len(s.Groups))
		}
	}

	return nil
}

func (s *Snapshot) data() ([]byte, error) {
	return proto.Marshal(s)
}

func (s *Snapshot) hash() (blocks.Hash, error) {
	snapData, err := s.data()
	if err != nil {
		return blocks.Hash{}, err
	}

	return s.hashFromData(snapData), nil
}

func (s *Snapshot) hashFromData(snapData []byte) blocks.Hash {
	return blocks.HashData(snapData)
}

func (s *Snapshot) pathHasParent(path string) bool {
	if path == "/" {
		return true
	}

	for _, e := range s.Entries {
		if isDirectChild(e.Path, path) {
			return true
		}
	}
	for _, l := range s.Links {
		if isDirectChild(l.Path, path) {
			return true
		}
	}
	return false
}

func (s *Snapshot) containsRootEntry() bool {
	for _, e := range s.Entries {
		if e.Path == "/" {
			return true
		}
	}
	return false
}

// usersAndGroupsFromSystem goes through the users and groups stored in the snapshot,
// and looks them up (by name) in the system. If found, their IDs are replaced with the ones in
// the system, otherwise the IDs from the snapshot are used.
// Returns the new users and groups
func (s *Snapshot) usersAndGroupsFromSystem() (users, groups []*Owner, err error) {
	currUser, err := user.Current()
	if err != nil {
		return
	}
	currUid, err := parseUnixID(currUser.Uid)
	if err != nil {
		return
	}
	users = append(users, &Owner{
		Name: currUser.Username,
		Id:   currUid,
	})

	for _, sUser := range s.Users {
		newUser := sUser

		u, err := user.Lookup(sUser.Name)
		if err == nil {
			uid, err := parseUnixID(u.Uid)
			if err != nil {
				return users, groups, err
			}

			newUser = &Owner{Name: sUser.Name, Id: uid}
		}

		users = append(users, newUser)
	}

	// add groups
	currGroup, err := user.LookupGroupId(currUser.Gid)
	if err != nil {
		return
	}
	currGid, err := parseUnixID(currGroup.Gid)
	if err != nil {
		return
	}
	groups = append(groups, &Owner{
		Name: currGroup.Name,
		Id:   currGid,
	})

	for _, sGroup := range s.Groups {
		newGroup := sGroup

		g, err := user.LookupGroup(sGroup.Name)
		if err == nil {
			gid, err := parseUnixID(g.Gid)
			if err != nil {
				return users, groups, err
			}

			newGroup = &Owner{Name: sGroup.Name, Id: gid}
		}

		groups = append(groups, newGroup)
	}

	return
}

func (s *Snapshot) uniqueFileHashes() (hashes []blocks.Hash) {
	seen := make(map[blocks.Hash]bool)
	for _, entry := range s.Entries {
		for _, block := range entry.Blocks {
			hash := blocks.Hash(blocks.ByteSliceToArr(block.Hash))
			_, ok := seen[hash]
			if !ok {
				hashes = append(hashes, hash)
				seen[hash] = true
			}
		}
	}
	return
}

func (i *SnapshotInfo) timestamp() time.Time {
	return time.Unix(i.Meta.CreatedAt, 0).UTC()
}

func parseUnixID(idStr string) (uint32, error) {
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		return 0, err
	}

	return uint32(id), nil
}
