package remotefs

import (
	"bytes"
	"errors"
	fmt "fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"remotefs/blocks"
	"runtime"
	"sort"
	"strings"
	"sync"
	"time"

	"bazil.org/fuse"
	bazilFS "bazil.org/fuse/fs"
	"github.com/google/uuid"
	"github.com/sasha-s/go-deadlock"
	"gitlab.com/go-figure/instrum"
	"gitlab.com/go-figure/logr"
	"golang.org/x/net/context"
)

var snapshotUpdaterLimit int = 10 // compress every 10
var errVerifyEmptyFS = errors.New("empty FS, nothing to verify")

// FS is the run-time representation of a filesystem.
type FS struct {
	conn                  *fuse.Conn
	writable              bool
	hasActivityCommit     bool
	hasCacheMeasureCommit bool
	maxCacheSize          int64
	store                 blocks.Store
	ref                   blocks.ID
	blockHashes           []blocks.Hash
	blockHashesLock       sync.Mutex
	uuid                  fsUUID
	mountpoint            string
	snapshot              *Snapshot
	snapshotHash          blocks.Hash
	root                  *DirNode
	users, groups         []*Owner

	notify                  notifier
	cacheMeasure            *CacheSizeMeasure
	activityTimeout         *activityTimeout
	orphanedFiles           []*FileNode
	snapshotRetentionGroups []*snapshotRetentionGroup
	snapshotUpdater         *snapshotUpdater

	waitForChange chan bool
	needsSave     bool
	changeLock    deadlock.RWMutex

	// Used for cleanup, when killing FS
	cancelScheduleSnapshots func()

	ctx        context.Context
	commitLock sync.RWMutex
}

// FSOptions is the FS options object that needs to be passed to NewFS when instancing a filesystem.
type FSOptions struct {
	Context                 context.Context
	UUID                    fsUUID
	Snapshot                *Snapshot
	SnapshotHash            blocks.Hash
	SnapshotUpdater         *snapshotUpdater
	Store                   blocks.Store
	SnapshotRetentionGroups []*snapshotRetentionGroup
	Notify                  notifier
	HasCacheMeasureCommit   bool
	MaxCacheSize            int64
	HasActivityCommit       bool
}

// Mount is the representation of a mounted FS. It is returned by the filesystem Mount method, and
// used by the daemon to keep track of mounted filesystems. Any errors that occur in the FS are
// passed through the Err error channel.
type Mount struct {
	Err chan error
	FS  *FS
}

type hashMismatchError struct {
	wanted, have blocks.Hash
}

func (e hashMismatchError) Error() string {
	return fmt.Sprintf("hash doesn't match: wanted: %v, have: %v", e.wanted, e.have)
}

// NewFS instances a new filesystem.
func NewFS(opts *FSOptions) (*FS, error) {
	snap := opts.Snapshot
	// Get a list of all the file block hashes in the snapshot
	blockHashes := snap.uniqueFileHashes()

	// Override users and groups with data from the system
	users, groups, err := snap.usersAndGroupsFromSystem()
	if err != nil {
		return nil, err
	}
	snap.Users = users
	snap.Groups = groups

	if err := snap.Valid(); err != nil {
		return nil, fmt.Errorf("snapshot is invalid: %w", err)
	}

	var snapshotUpdater *snapshotUpdater
	if opts.SnapshotUpdater != nil {
		snapshotUpdater = opts.SnapshotUpdater
	} else {
		var err error
		snapshotUpdater, err = NewSnapshotUpdater(opts.Store, opts.UUID)
		if err != nil {
			return nil, err
		}
	}

	fs := &FS{
		ctx:                     opts.Context,
		writable:                false,
		hasActivityCommit:       opts.HasActivityCommit,
		hasCacheMeasureCommit:   opts.HasCacheMeasureCommit,
		maxCacheSize:            opts.MaxCacheSize,
		store:                   opts.Store,
		snapshot:                snap,
		snapshotHash:            opts.SnapshotHash,
		snapshotUpdater:         snapshotUpdater,
		snapshotRetentionGroups: opts.SnapshotRetentionGroups,
		blockHashes:             blockHashes,
		uuid:                    opts.UUID,
		users:                   users,
		groups:                  groups,
		waitForChange:           make(chan bool),
	}

	fs.makeNodes(snap)

	if opts.Notify != nil {
		opts.Notify.Register(fs)
		fs.notify = opts.Notify
	}

	return fs, nil
}

func (fs *FS) enableWrites() error {
	fs.ref = blocks.GenerateRefRootID()

	if fs.hasActivityCommit {
		fs.activityTimeout = newActivityTimeout(fs)
		fs.setupActivity()
	}

	if fs.hasCacheMeasureCommit {
		cacheMeasure := NewCacheSizeMeasure()
		cacheMeasure.OnSizeChange(func(oldTotalSize, newTotalSize int64) {
			if oldTotalSize < fs.maxCacheSize && newTotalSize >= fs.maxCacheSize {
				retry(fs.ctx, "cache_measure_commit", func() error {
					return fs.lockAndCommitAll(fs.ctx)
				}, 5)
			}
		})
		fs.cacheMeasure = cacheMeasure

		// walk file nodes and subscribe to cache measure
		err := walk(nil, "", fs.root, "/", func(node bazilFS.Node, name, fullpath string) error {
			// Must be file node
			if f, ok := node.(*FileNode); ok {
				fs.cacheMeasure.Subscribe(f.cache)
			}
			return nil
		})
		if err != nil {
			return err
		}
	}

	if len(fs.snapshot.Meta.Ref) > 0 {
		// Copy snapshot refRoot into fs.refRoot Except the snapshot refRoot also contains the snapshot
		// block, which we do not want to have in our fs.refRoot, so we remove it.
		err := fs.store.CopyRefRoot(fs.ctx, fs.ref, blocks.IDFromByteSlice(fs.snapshot.Meta.Ref))
		if err != nil {
			return err
		}
		err = fs.store.RemoveRefs(fs.ctx, fs.ref, []blocks.Hash{fs.snapshotHash})
		if err != nil {
			return err
		}
	} else {
		err := fs.store.NewRefRoot(fs.ctx, fs.ref)
		if err != nil {
			return err
		}
	}

	go fs.scheduleSnapshots()

	fs.writable = true

	return nil
}

func (fs *FS) removeRefsNotInSnapshot(snap *Snapshot) error {
	// Find hashes that are in the old fs.blockHashes slice, but not in the newBlockHashes slice, and
	// remove them.
	newBlockHashes := snap.uniqueFileHashes()

	var hashesDiff []blocks.Hash
	fs.blockHashesLock.Lock()
	defer fs.blockHashesLock.Unlock()
	for _, hash := range fs.blockHashes {
		var found bool
		for _, newHash := range newBlockHashes {
			if hash == newHash {
				found = true
				break
			}
		}
		if !found {
			var alreadyExists bool
			for _, hd := range hashesDiff {
				if hd == hash {
					alreadyExists = true
					break
				}
			}
			if !alreadyExists {
				hashesDiff = append(hashesDiff, hash)
			}
		}
	}

	err := fs.store.RemoveRefs(fs.ctx, fs.ref, hashesDiff)
	if err != nil {
		return err
	}

	// Update fs block hashes
	fs.blockHashes = newBlockHashes

	return err
}

func (fs *FS) newRefRootFromSnapshot(snap *Snapshot) error {
	// create new FS commit ref
	newRef := blocks.GenerateRefRootID()
	fs.ref = newRef
	return fs.store.CopyRefRoot(fs.ctx, fs.ref, blocks.ByteSliceToArr(snap.Meta.Ref))
}

func (fs *FS) transferRefRootToSnapshot(snap *Snapshot) {
	fsRefSlice := fs.ref[:]
	snap.Meta.Ref = make([]byte, len(fsRefSlice))
	copy(snap.Meta.Ref, fsRefSlice)
}

// scheduleSnapshots iterates through all the snapshot retention groups and figures out when the
// next snapshot should occur, and schedules it. When the snapshot is made, it determines which (if
// any) snapshots need to be removed (specified by the retention group), and removes them.
func (fs *FS) scheduleSnapshots() {
	// TODO: cancel context
	if len(fs.snapshotRetentionGroups) == 0 {
		return
	}
	const retries = 5

	cancelChan := make(chan bool)
	fs.cancelScheduleSnapshots = func() {
		cancelChan <- true
		close(cancelChan)
	}

	for {
		nextDates := make([]time.Time, len(fs.snapshotRetentionGroups))

		var infos []*SnapshotInfo
		failed := retry(fs.ctx, "get_snaplist", func() error {
			var err error
			infos, err = fs.snapshotUpdater.Get(fs.ctx)
			return err
		}, retries)
		if failed {
			continue
		}

		var untilNextSnapshot time.Duration
		for i, group := range fs.snapshotRetentionGroups {
			nextDate := group.NextSnapshot(infos)
			nextDates[i] = nextDate
			dur := nextDate.Sub(time.Now())

			if untilNextSnapshot == time.Duration(0) || untilNextSnapshot > dur {
				untilNextSnapshot = dur
			}
		}
		timer := time.NewTimer(untilNextSnapshot)
		select {
		case <-cancelChan:
			return
		case <-timer.C:
			select {
			case <-cancelChan:
				return
			case <-fs.onChange():
			}
		}

		// Get groups which need to be saved
		var expiredGroups []*snapshotRetentionGroup
		for i, group := range fs.snapshotRetentionGroups {
			nextDate := nextDates[i]
			if nextDate.After(time.Now()) {
				continue
			}
			expiredGroups = append(expiredGroups, group)
		}

		// TODO: if this function fails and we retry it, it can do something different on every retry
		err := fs.save(expiredGroups...)
		if err != nil {
			logr.FromContext(fs.ctx).Error("fs_save_error", err, logr.KV{"fs.uuid": fs.uuid})
		}
	}
}

func (fs *FS) save(groups ...*snapshotRetentionGroup) error {
	// Read lock to prevent data race when reading fs.needsSave
	fs.changeLock.RLock()

	// There is no point in creating any snapshots if the FS is not writable, or there were no changes
	// TODO: P0 don't call save() instead?
	if !fs.writable || !fs.needsSave {
		fs.changeLock.RUnlock()
		return nil
	}
	fs.changeLock.RUnlock()

	// Make snapshot only once, because the snapshot is the same for every group, except:
	// - snap.Meta.Tag, which is set to the name of the group
	// - snap.Meta.Ref, which must be a new refRoot for each group, which is cloned from fs.ref
	// We set both of these fields in the loop below before saving each snapshot to the store
	snap, err := fs.makeSnapshot(fs.ctx)
	if err != nil {
		return fmt.Errorf("snapshotting: %w", err)
	}

	// fs.blockHashes contains all of the file hashes from when the previous snapshot was made.
	// We can determine if a file block was deleted by checking which file hashes are present in
	// this new snapshot, but not in our old fs.blockHashes.
	// We can then remove the refs for the deleted file blocks.
	// After this, fs.blockHashes will be updated to this snapshot's file blocks.
	// fs.blockHashes does not include the snapshot block.
	err = fs.removeRefsNotInSnapshot(snap)
	if err != nil {
		return err
	}

	// Get the current SnapshotInfo list
	infos, err := fs.snapshotUpdater.Get(fs.ctx)
	if err != nil {
		return err
	}

	for _, group := range groups {
		snap.Meta.Tag = group.name

		// Create new refRoot and set it on snap.Meta
		snapRef := blocks.GenerateRefRootID()
		snap.Meta.Ref = snapRef[:]
		// snap.Meta.Ref is cloned from fs.ref, which contains all file block refs
		err := fs.store.CopyRefRoot(fs.ctx, snapRef, fs.ref)
		if err != nil {
			return err
		}

		// Returns the new SnapshotInfo list, which we use to update our own list without having to call
		// updater.Get() again.
		infos, err = fs.saveSnapshotToStore(infos, snap)
		if err != nil {
			return err
		}

		// Remove expired snapshots, and update the infos list with a new list that doesn't contain
		// the expired snapshots. If no snapshots were removed, then newInfos == oldInfos.
		infos, err = fs.maybeRemoveSnapshots(infos, group)
		if err != nil {
			return err
		}
	}

	// fs.refRoot remains unchanged!

	// The FS current snapshot will be set to the last snapshot in the group list
	hash, err := snap.hash()
	if err != nil {
		return err
	}
	fs.snapshot = snap
	fs.snapshotHash = hash

	return fs.snapshotUpdater.Set(fs.ctx, infos)
}

// Create new copies of all old snapshots, except with new refRoots.
// Copies file blocks too.
func (fs *FS) copyAllSnapshotsToStore(dstStore blocks.Store) (newSnapInfos, oldSnapInfos []*SnapshotInfo, err error) {
	oldSnapInfos, err = fs.snapshotUpdater.Get(fs.ctx)
	if err != nil {
		return
	}
	if len(oldSnapInfos) == 0 {
		return nil, nil, errors.New("cannot copy empty list of snapshots")
	}

	for _, oldInfo := range oldSnapInfos {
		hash := blocks.HashFromSlice(oldInfo.Hash)
		newInfo, err := copySnapshotToStore(fs.ctx, hash, dstStore, fs.store)
		if err != nil {
			return nil, nil, err
		}
		newSnapInfos = append(newSnapInfos, newInfo)
	}
	return
}

func copySnapshotToStore(ctx context.Context, snapHash blocks.Hash, dstStore, srcStore blocks.Store) (*SnapshotInfo, error) {
	oldSnap, err := GetSnapshot(ctx, srcStore, snapHash)
	if err != nil {
		return nil, err
	}
	// Copy to new snapshot
	newMeta := *oldSnap.Meta
	newSnap := *oldSnap
	newSnap.Meta = &newMeta
	// Set new refRoot
	newRefRoot := blocks.GenerateRefRootID()
	newSnap.Meta.Ref = newRefRoot[:]

	// Create new refRoot
	err = dstStore.NewRefRoot(ctx, newRefRoot)
	if err != nil {
		return nil, err
	}

	// Add new snapshot block data to dest store
	newData, err := newSnap.data()
	if err != nil {
		return nil, err
	}
	// Get new hash
	newSnapHash := newSnap.hashFromData(newData)

	err = dstStore.Put(ctx, newRefRoot, newSnapHash, bytes.NewReader(newData))
	if err != nil {
		return nil, err
	}

	// Copy all file blocks
	for _, entry := range oldSnap.Entries {
		for _, block := range entry.Blocks {
			hash := blocks.HashFromSlice(block.Hash)
			br, err := srcStore.Get(ctx, hash)
			if err != nil {
				return nil, err
			}
			err = dstStore.Put(ctx, newRefRoot, hash, br)
			if err != nil {
				br.Close()
				return nil, err
			}
			br.Close()
		}
	}

	return &SnapshotInfo{
		Hash: newSnapHash[:],
		Meta: newSnap.Meta,
	}, nil
}

// Clones receiver into newFS. if store == nil, it will be cloned into the same store.
func (fs *FS) clone(store blocks.Store) (*FS, error) {
	if store == nil {
		store = fs.store
	}
	newSnapInfos, _, err := fs.copyAllSnapshotsToStore(store)
	if err != nil {
		return nil, err
	}
	snapshot, err := GetSnapshot(fs.ctx, store, blocks.HashFromSlice(newSnapInfos[0].Hash))
	if err != nil {
		return nil, err
	}
	newFS, err := NewFS(&FSOptions{
		Context:                 fs.ctx,
		UUID:                    fsUUID(uuid.New().String()),
		Store:                   store,
		Snapshot:                snapshot,
		SnapshotRetentionGroups: fs.snapshotRetentionGroups,
		Notify:                  fs.notify,
		HasCacheMeasureCommit:   fs.hasCacheMeasureCommit,
		HasActivityCommit:       fs.hasActivityCommit,
	})
	if err != nil {
		return nil, err
	}

	err = newFS.snapshotUpdater.Set(fs.ctx, newSnapInfos)
	if err != nil {
		return nil, err
	}

	return newFS, nil
}

func (fs *FS) migrateStore(store blocks.Store) error {
	newSnapInfos, oldSnapInfos, err := fs.copyAllSnapshotsToStore(store)
	if err != nil {
		return err
	}

	err = deleteSnapshotRoots(fs.ctx, fs.store, oldSnapInfos)
	if err != nil {
		return err
	}

	// Delete old snapshot lists
	err = fs.snapshotUpdater.Delete(fs.ctx)
	if err != nil {
		return err
	}

	// Swap store
	fs.store = store

	newHash := blocks.HashFromSlice(newSnapInfos[0].Hash)
	newSnap, err := GetSnapshot(fs.ctx, fs.store, newHash)
	if err != nil {
		return err
	}
	fs.snapshot = newSnap
	fs.snapshotHash = newHash

	// Create new snapshotUpdater with new store
	fs.snapshotUpdater, err = NewSnapshotUpdater(fs.store, fs.uuid)
	if err != nil {
		return err
	}

	return fs.snapshotUpdater.Set(fs.ctx, newSnapInfos)
}

func (fs *FS) destroy() error {
	// Delete all snapshot refRoots
	snapinfos, err := fs.snapshotUpdater.Get(fs.ctx)
	if err != nil {
		return err
	}

	err = deleteSnapshotRoots(fs.ctx, fs.store, snapinfos)
	if err != nil {
		return err
	}

	// Delete updater refRoot
	return fs.snapshotUpdater.Delete(fs.ctx)
}

func deleteSnapshotRoots(ctx context.Context, store blocks.Store, infos []*SnapshotInfo) error {
	for _, info := range infos {
		root := blocks.IDFromByteSlice(info.Meta.Ref)
		err := store.DeleteRefRoot(ctx, root)
		if err != nil {
			return err
		}
	}
	return nil
}

// saveSnapshotToStore adds the snapshot data block to the store.
func (fs *FS) saveSnapshotToStore(infos []*SnapshotInfo, snap *Snapshot) ([]*SnapshotInfo, error) {
	snapData, err := snap.data()
	if err != nil {
		return nil, err
	}
	hash := snap.hashFromData(snapData)

	// put snapshot data to snap ref
	err = fs.store.Put(fs.ctx, blocks.IDFromByteSlice(snap.Meta.Ref), hash, bytes.NewReader(snapData))
	if err != nil {
		return nil, fmt.Errorf("putting snapshot data block: %w", err)
	}

	// prepend new snap info
	snapMetaClone := *snap.Meta
	infos = append([]*SnapshotInfo{
		&SnapshotInfo{
			Hash: hash[:],
			Meta: &snapMetaClone,
		},
	}, infos...)

	return infos, nil
}

// Mount mounts a filesystem.
func (fs *FS) Mount(mountpoint string) (*Mount, error) {
	if len(mountpoint) == 0 {
		return nil, errors.New("mountpoint string is empty")
	}
	readonly := !fs.writable

	if _, err := os.Stat(mountpoint); os.IsNotExist(err) {
		err := os.Mkdir(mountpoint, 0755)
		if err != nil {
			return nil, fmt.Errorf("while creating mountpoint that doesn't exist: %w", err)
		}
	}

	// TODO: @performance before release, investigate all options
	// https://godoc.org/bazil.org/fuse#MountOption
	// AsyncRead. MaxReadahead, WritebackCache, FSName/Volumename/Subtype etc
	// especially Allow*
	fuseMountOpts := []fuse.MountOption{
		fuse.AsyncRead(),
		fuse.MaxReadahead(1 * 1024 * 1024),
		fuse.DefaultPermissions(),
		fuse.FSName("remotefs"),
		fuse.Subtype("remotefs"),
		fuse.LocalVolume(),
		fuse.VolumeName("Remote FS"),
	}
	if readonly {
		fuseMountOpts = append(fuseMountOpts, fuse.ReadOnly())
	}
	conn, err := fuse.Mount(mountpoint, fuseMountOpts...)
	if err != nil {
		return nil, err
	}

	<-conn.Ready
	// check if the mount process has an error to report
	if err := conn.MountError; err != nil {
		return nil, err
	}
	fs.conn = conn
	fs.mountpoint = mountpoint

	serveErr := make(chan error)
	// TODO: pass context here?
	go func() {
		err := bazilFS.Serve(conn, fs)
		if err != nil {
			serveErr <- err
			return
		}

		err = fs.save(latestRetentionGroup)
		if err != nil {
			serveErr <- err
			return
		}

		err = fs.close()
		if err != nil {
			serveErr <- err
			return
		}

		serveErr <- nil

		fs.notify.FSUnmounted()
	}()

	mounted, err := waitUntilMounted(mountpoint, serveErr)
	if err != nil {
		return nil, err
	}
	if !mounted {
		return nil, errors.New("cannot mount FS")
	}

	return &Mount{FS: fs, Err: serveErr}, nil
}

func (fs *FS) close() error {
	if fs.writable {
		err := fs.store.DeleteRefRoot(fs.ctx, fs.ref)
		if err != nil {
			return err
		}
	}

	if fs.cancelScheduleSnapshots != nil {
		fs.cancelScheduleSnapshots()
	}

	if fs.activityTimeout != nil {
		fs.activityTimeout.stop()
	}

	if fs.conn != nil {
		return fs.conn.Close()
	}

	return nil
}

// Unmount unmounts a mounted filesystem.
func (fs *FS) Unmount() error {
	err := fuse.Unmount(fs.mountpoint)
	if err != nil {
		return fmt.Errorf("can't unmount: %w", err)
	}
	return nil
}

// makeNodes creates the filesystem node tree.
// Calling with `nil, ""` will create the root node and all of its children, and set the root node.
func (fs *FS) makeNodes(snapshot *Snapshot) {
	fs.newNodeChildren(nil, "", snapshot)
}

func (fs *FS) newNodeChildren(parent *DirNode, parentPath string, snapshot *Snapshot) {
	isRoot := parent == nil
	if !isRoot && parent.children == nil {
		parent.children = make(map[string]bazilFS.Node)
	}

	// Add symlinks
	for _, l := range snapshot.Links {
		if !isDirectChild(parentPath, l.Path) {
			continue
		}

		name := filepath.Base(l.Path)

		l.Meta.MapIdentsToSystem(fs.users, fs.groups)

		newSymlink := &SymlinkNode{
			parent: parent,
			meta:   l.Meta,
			target: l.Target,
			FS:     fs,
		}

		parent.children[name] = newSymlink
	}

	// Add entries
	for _, e := range snapshot.Entries {
		if !isRoot && !isDirectChild(parentPath, e.Path) {
			continue
		}

		name := filepath.Base(e.Path)
		isDir := strings.HasSuffix(e.Path, "/")

		// Convert to sys uid
		e.Meta.MapIdentsToSystem(fs.users, fs.groups)

		var newNode bazilFS.Node
		if isDir {
			// is dir
			newNode = &DirNode{
				parent:   parent,
				meta:     e.Meta,
				children: nil,
				FS:       fs,
			}

			// populate new dir node's children
			fs.newNodeChildren(newNode.(*DirNode), e.Path, snapshot)
		} else {
			newNode = &FileNode{
				parent:       parent,
				meta:         e.Meta,
				blocks:       derefBlocks(e.Blocks),
				blockDataEnd: e.Meta.Size,
				blocksSize:   e.Meta.Size,
				cache:        NewCache(),
				FS:           fs,
			}
		}

		// set FS root node
		if isRoot {
			fs.root = newNode.(*DirNode)
			// we don't have any parent for the root node, so return early
			return
		}

		parent.children[name] = newNode
	}
}

func (fs *FS) maybeRemoveSnapshots(infos []*SnapshotInfo, group *snapshotRetentionGroup) ([]*SnapshotInfo, error) {
	remove, err := group.GetSnapshotsToRemove(infos)
	if err != nil {
		return nil, err
	}

	if len(remove) == 0 {
		return infos, nil
	}

	// remove
	for _, rm := range remove {
		err := fs.store.DeleteRefRoot(fs.ctx, blocks.IDFromByteSlice(rm.Meta.Ref))
		if err != nil {
			return nil, err
		}
	}

	var newInfos []*SnapshotInfo
	for _, info := range infos {
		keep := true
		for _, rm := range remove {
			if bytes.Equal(info.Hash, rm.Hash) {
				keep = false
				break
			}
		}
		if keep {
			newInfos = append(newInfos, info)
		}
	}

	return newInfos, nil
}

func (fs *FS) onChange() chan bool {
	fs.changeLock.RLock()
	defer fs.changeLock.RUnlock()

	fs.needsSave = false
	return fs.waitForChange
}

func (fs *FS) changed() {
	fs.changeLock.Lock()
	defer fs.changeLock.Unlock()

	fs.needsSave = true
	close(fs.waitForChange)
	fs.waitForChange = make(chan bool)
}

// Root returns the root node, used by fuse.
func (fs *FS) Root() (bazilFS.Node, error) {
	return fs.root, nil
}

// lockAndCommitAll first commits all files without locking, and then locks and commits all files
// again. Commits for all files except the ones that depended on the lock will be no-ops, so very
// fast.
func (fs *FS) lockAndCommitAll(ctx context.Context) error {
	wg := instrum.WaitGroupFromContext(ctx)
	wg.Add(1)
	defer wg.Done()

	err := fs.commitAll(ctx)
	if err != nil {
		return err
	}

	fs.commitLock.Lock()
	defer fs.commitLock.Unlock()

	return fs.commitAll(ctx)
}

func (fs *FS) commitAll(ctx context.Context) error {
	numParallelCommits := runtime.NumCPU()
	c := make(chan bool, numParallelCommits)
	for i := 0; i < numParallelCommits; i++ {
		c <- true
	}

	err := walk(nil, "", fs.root, "/", func(node bazilFS.Node, name, fullpath string) error {
		// Must be file node
		if n, ok := node.(*FileNode); ok {
			parallelCommit(ctx, c, n)
		}
		return nil
	})
	if err != nil {
		return err
	}

	// Remove any files that aren't orphans anymore
	for i := len(fs.orphanedFiles) - 1; i >= 0; i-- {
		n := fs.orphanedFiles[i]
		if n.openFds == 0 {
			fs.orphanedFiles = fs.orphanedFiles[:i]
			continue
		}

		parallelCommit(ctx, c, n)
	}

	// Wait for all Commits to finish
	for i := 0; i < numParallelCommits; i++ {
		<-c
	}

	// TODO: should this be here?
	// runtime.GC()

	return nil
}

func parallelCommit(ctx context.Context, c chan bool, n *FileNode) {
	<-c

	go func(n *FileNode) {
		// TODO: cancel context
		retry(ctx, "node_commit", func() error {
			return n.Commit(ctx)
		}, 5)
		c <- true
	}(n)
}

// Returns the new blocks, and the last block size, which we use in commitBlocks
func (fs *FS) putBlocksFromReader(r io.Reader) ([]*Block, int64, error) {
	chunker := blocks.NewChunker(r)

	buf := make([]byte, blocks.MaxChunkSize)

	var lastBlockSize int64

	var newBlocks []*Block
	for {
		chunk, err := chunker.Next(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, 0, err
		}

		err = fs.store.Put(fs.ctx, fs.ref, chunk.Hash, bytes.NewReader(chunk.Data))
		if err != nil {
			return nil, 0, err
		}

		fs.blockHashesLock.Lock()
		fs.blockHashes = append(fs.blockHashes, chunk.Hash)
		fs.blockHashesLock.Unlock()

		block := &Block{
			Offset: int64(chunk.Start),
			Hash:   chunk.Hash[:],
		}

		newBlocks = append(newBlocks, block)
		lastBlockSize = int64(chunk.Length)
	}

	return newBlocks, lastBlockSize, nil
}

func (fs *FS) makeSnapshot(ctx context.Context) (*Snapshot, error) {
	snapshot := &Snapshot{
		Meta: &SnapshotMeta{
			CreatedAt: time.Now().Unix(),
			Tag:       "",  // tag should be filled in before saving
			Ref:       nil, // ref should be filled in before saving
		},
	}

	err := fs.commitAll(ctx)
	if err != nil {
		return nil, err
	}

	fs.commitLock.Lock()
	defer fs.commitLock.Unlock()

	err = fs.commitAll(ctx)
	if err != nil {
		return nil, err
	}

	err = walk(nil, "", fs.root, "/", func(node bazilFS.Node, name, fullpath string) error {
		meta := &EntryMeta{}
		switch n := node.(type) {
		case *DirNode:
			*meta = *n.meta
		case *FileNode:
			*meta = *n.meta
		case *SymlinkNode:
			*meta = *n.meta
		}

		// convert snapshot owner IDs (UID, GID) from system UID and GID to snapshot IDs
		suid := getOwnerSnapshotID(meta.Uid, fs.users)
		if suid == -1 {
			// this user doesn't exist in the user list, we have to add him
			fs.users = addUserFromSystem(fs.users, meta.Uid)
			// the new UID is the last index in the new slice
			suid = len(fs.users) - 1
		}
		meta.Uid = uint32(suid)

		sgid := getOwnerSnapshotID(meta.Gid, fs.groups)
		if sgid == -1 {
			// this group doesn't exist in the group list, we have to add it
			fs.groups = addGroupFromSystem(fs.groups, meta.Gid)
			// the new GID is the last index in the new slice
			sgid = len(fs.groups) - 1
		}
		meta.Gid = uint32(sgid)

		switch n := node.(type) {
		case *DirNode:
			// In Entry paths, directories must end with '/'
			// don't append slash to root node
			if name != "/" && !strings.HasSuffix(name, "/") {
				fullpath += "/"
			}

			snapshot.Entries = append(snapshot.Entries, &Entry{
				Path:   fullpath,
				Meta:   meta,
				Blocks: nil,
			})
		case *FileNode:
			// TODO: race?
			var ptrBlocks []*Block
			for i := range n.blocks {
				ptrBlocks = append(ptrBlocks, &n.blocks[i])
			}
			entry := &Entry{
				Path:   fullpath,
				Meta:   meta,
				Blocks: ptrBlocks,
			}
			snapshot.Entries = append(snapshot.Entries, entry)
		case *SymlinkNode:
			snapshot.Links = append(snapshot.Links, &Link{
				Path:   fullpath,
				Meta:   meta,
				Target: n.target,
			})
		default:
			return fmt.Errorf("encountered invalid node type while snapshotting: %v", reflect.TypeOf(n))
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	// Add runtime users and groups, except the first entry which is always the
	// current user/group, so there's no point in saving it in the snapshot.
	snapshot.Users = fs.users[1:]
	snapshot.Groups = fs.groups[1:]

	// Sort snapshot.Users and snapshot.Groups alphabetically so that the snapshots always have the
	// same hash no matter what order the users were added in. When we change the position of users or
	// groups in the slice, we need to update the Uid and Gid of each Entry and Link as well, because
	// they are just indices referencing the position in the slice.
	// Example:
	// entry.Meta.Uid == 0 doesn't exist in snapshot.Users; it is always the current user
	// entry.Meta.Uid == 1 corresponds to snapshot.Users[0]
	// entry.Meta.Uid == 2 corresponds to snapshot.Users[1]
	// Same thing for Gid.
	// When we swap the order of snapshot.Users, all entries that have Uid == 1 must have Uid == 2,
	// and all entries that have Uid == 2 must have Uid == 1
	sort.Slice(snapshot.Users, func(i, j int) bool {
		var si string = snapshot.Users[i].Name
		var sj string = snapshot.Users[j].Name
		var siLower = strings.ToLower(si)
		var sjLower = strings.ToLower(sj)
		if siLower == sjLower {
			return si < sj
		}

		// No swap
		if !(siLower < sjLower) {
			return false
			// for
			// e.Meta.Uid = uint32(i + 1)
		}

		// Swap:
		// First we search for which indices we have to swap the Uid for and
		// we remember them in two different slices.
		var replaceFirst, replaceSecond []int
		for ei, entry := range snapshot.Entries {
			if entry.Meta.Uid == uint32(i+1) {
				fmt.Println(entry.Path, "should swap", entry.Meta.Uid, "with", j+1)
				replaceFirst = append(replaceFirst, ei)
			} else if entry.Meta.Uid == uint32(j+1) {
				fmt.Println(entry.Path, "should swap", entry.Meta.Uid, "with", i+1)
				replaceSecond = append(replaceSecond, ei)
			}
		}
		for ri := range replaceFirst {
			snapshot.Entries[ri].Meta.Uid = uint32(j + 1)
		}
		for ri := range replaceSecond {
			snapshot.Entries[ri].Meta.Uid = uint32(i + 1)
		}
		return true
	})

	sort.Sort(sortEntryAlphabetic(snapshot.Entries))
	sort.Sort(sortLinkAlphabetic(snapshot.Links))

	return snapshot, nil
}

type verifyResult struct {
	Message string
	Percent float32
	ETA     string
	Err     error
}

func (fs *FS) verify(checkHashes, allSnapshots bool) chan verifyResult {
	results := make(chan verifyResult)

	go func() {
		defer close(results)

		infos, err := fs.snapshotUpdater.Get(fs.ctx)
		if err != nil {
			results <- verifyResult{Err: fmt.Errorf("getting snapshot list: %w", err)}
			return
		}
		if len(infos) == 0 {
			results <- verifyResult{Err: errVerifyEmptyFS}
			return
		}

		// Figure out total number of blocks we have to check
		totalBlocks := len(infos)

		var (
			totalSnapBlockDur time.Duration
			blockCount        int
			snapshots         []*Snapshot
		)
		for i, info := range infos {
			blockCount++

			// Start measuring how much time it takes to verify one snapshot block.
			start := time.Now()

			// Check that the snapshot block exists
			hash := blocks.HashFromSlice(info.Hash)
			snap, err := GetSnapshot(fs.ctx, fs.store, hash)
			if err != nil {
				results <- verifyResult{Err: fmt.Errorf("fetching snapshot block: %w", err)}
				continue
			}
			// Add to snapshot list so we can iterate through file blocks below, without fetching the
			// snapshot again.
			snapshots = append(snapshots, snap)

			// Calculate total number of blocks.
			for _, entry := range snap.Entries {
				totalBlocks += len(entry.Blocks)
			}

			if checkHashes {
				calculatedHash, err := snap.hash()
				if err != nil {
					results <- verifyResult{Err: fmt.Errorf("calculating snapshot block hash: %w", err)}
					continue
				}

				if hash != calculatedHash {
					results <- verifyResult{Err: hashMismatchError{wanted: hash, have: calculatedHash}}
					continue
				}
			}

			dur := time.Now().Sub(start)
			totalSnapBlockDur += dur

			// ETA during snapshot block verification will be a rough estimate, because they are usually
			// smaller than file blocks. The estimation will get more accurate once we start verifying a few
			// file blocks.
			// How many snapshot blocks there are, minus how many we've verified so far
			eta := time.Duration(len(infos)-i) * totalSnapBlockDur / time.Duration(blockCount)

			results <- verifyResult{
				Message: fmt.Sprintf("verified snapshot block %v", i),
				Percent: float32(blockCount) * 100.0 / float32(totalBlocks),
				ETA:     eta.String(),
				Err:     nil,
			}

			// Only check first snapshot's block
			if !allSnapshots {
				break
			}
		}

		var averageCount int
		var totalFileBlockDur time.Duration
		// TODO: Don't process same block multiple times
		for i, snap := range snapshots {
			// Verify file blocks
			for _, entry := range snap.Entries {
				if len(entry.Blocks) == 0 {
					continue
				}

				for _, block := range entry.Blocks {
					blockCount++

					// Start measuring how much time it takes to verify one file block.
					var start time.Time
					if averageCount < 10 {
						start = time.Now()
					}

					hash := blocks.HashFromSlice(block.Hash)
					br, err := fs.store.Get(fs.ctx, hash)
					if err != nil {
						results <- verifyResult{Err: fmt.Errorf("fetching file block: %w", err)}
						continue
					}

					if checkHashes {
						blockData, err := blocks.ReadBlock(br)
						br.Close()
						if err != nil {
							results <- verifyResult{Err: fmt.Errorf("reading file block data: %w", err)}
							continue
						}

						calculatedHash := blocks.HashData(blockData)
						if hash != calculatedHash {
							results <- verifyResult{Err: hashMismatchError{wanted: hash, have: calculatedHash}}
							continue
						}
					} else {
						// Don't forget to close block
						br.Close()
					}

					if averageCount < 10 {
						dur := time.Now().Sub(start)
						totalFileBlockDur += dur
						averageCount++
					}
				}

				eta := time.Duration(totalBlocks-blockCount) * totalFileBlockDur / time.Duration(averageCount)

				results <- verifyResult{
					Message: fmt.Sprintf("(snapshot %v): verified blocks from file '%v'", i, entry.Path),
					Percent: float32(blockCount) * 100.0 / float32(totalBlocks),
					ETA:     eta.String(),
				}
			}
			// Only check first snapshot's file blocks
			if !allSnapshots {
				break
			}
		}
	}()

	return results
}

func walk(parent *DirNode, parentpath string, node bazilFS.Node, nodename string, fn func(bazilFS.Node, string, string) error) error {
	// Join name with parent's name to get the full path
	fullpath := filepath.Join(parentpath, nodename)

	err := fn(node, nodename, fullpath)
	if err != nil {
		return err
	}

	switch n := node.(type) {
	case *DirNode:
		type nodeWithName struct {
			Name string
			Node bazilFS.Node
		}
		var children []nodeWithName
		n.mu.RLock()
		for name, child := range n.children {
			children = append(children, nodeWithName{Name: name, Node: child})
		}
		n.mu.RUnlock()

		for _, child := range children {
			err := walk(n, fullpath, child.Node, child.Name, fn)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func isDirectChild(parent, child string) bool {
	if len(parent) == 0 || parent == child || len(parent) > len(child) {
		return false
	}
	if !strings.HasPrefix(child, parent) {
		return false
	}

	diff := child[len(parent):]
	if diff[0] == '/' {
		diff = diff[1:]
	}
	if diff[len(diff)-1] == '/' {
		diff = diff[:len(diff)-1]
	}

	paths := strings.Split(diff, string(os.PathSeparator))
	return len(paths) == 1
}

func getNodeName(node bazilFS.Node) string {
	var parent *DirNode
	switch n := node.(type) {
	case *DirNode:
		parent = n.parent
	case *FileNode:
		parent = n.parent
	case *SymlinkNode:
		parent = n.parent
	}

	if parent == nil {
		// must be root node
		return "/"
	}

	for name, child := range parent.children {
		if node == child {
			return name
		}
	}

	panic("can't find name for node")
}

func getFullNodePath(node bazilFS.Node) string {
	var parent *DirNode
	var name string
	switch n := node.(type) {
	case *DirNode:
		parent = n.parent
		name = getNodeName(n)
	case *FileNode:
		parent = n.parent
		name = getNodeName(n)
	case *SymlinkNode:
		parent = n.parent
		name = getNodeName(n)
	}

	var fullpath string
	for parent != nil {
		fullpath = filepath.Join(name, fullpath)
		parent = parent.parent
	}

	// add root
	fullpath = filepath.Join("/", fullpath)

	return fullpath
}

func derefBlocks(blocks []*Block) []Block {
	if len(blocks) == 0 {
		return nil
	}

	deref := make([]Block, len(blocks))
	for i, b := range blocks {
		deref[i] = *b
	}

	return deref
}

// sortEntryAlphabetic implements sort.Interface for sorting []*Entry by Path alphabetically
type sortEntryAlphabetic []*Entry

func (a sortEntryAlphabetic) Len() int { return len(a) }

func (a sortEntryAlphabetic) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

func (a sortEntryAlphabetic) Less(i, j int) bool {
	var si string = a[i].Path
	var sj string = a[j].Path
	var siLower = strings.ToLower(si)
	var sjLower = strings.ToLower(sj)
	if siLower == sjLower {
		return si < sj
	}
	return siLower < sjLower
}

// sortLinkAlphabetic implements sort.Interface for sorting []*Link by Path alphabetically
type sortLinkAlphabetic []*Link

func (a sortLinkAlphabetic) Len() int { return len(a) }

func (a sortLinkAlphabetic) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

func (a sortLinkAlphabetic) Less(i, j int) bool {
	var si string = a[i].Path
	var sj string = a[j].Path
	var siLower = strings.ToLower(si)
	var sjLower = strings.ToLower(sj)
	if siLower == sjLower {
		return si < sj
	}
	return siLower < sjLower
}

func waitUntilMounted(path string, earlyError chan error) (bool, error) {
	attempts := 1000
	for i := 0; i < attempts; i++ {
		mounted, err := isMounted(path)
		if err != nil {
			return false, err
		}

		if mounted {
			return true, nil
		}

		err, ok := <-earlyError
		if ok && err != nil {
			return false, err
		}

		time.Sleep(time.Millisecond)
	}

	return false, nil
}

func unmountAndWaitUntilUnmounted(path string) error {
	unmountAttempts := 1000
	for i := 0; i < unmountAttempts; i++ {
		err := fuse.Unmount(path)
		if err == nil {
			break
		}
		time.Sleep(time.Millisecond)
	}

	checkAttempts := 1000
	for i := 0; i < checkAttempts; i++ {
		mounted, err := isMounted(path)
		if err != nil {
			return err
		}

		if !mounted {
			return nil
		}

		time.Sleep(time.Millisecond)
	}

	return fmt.Errorf("could not unmount after %v attempts", unmountAttempts)
}

func isMounted(checkPath string) (bool, error) {
	data, err := ioutil.ReadFile("/etc/mtab")
	if err != nil {
		return false, err
	}

	lines := strings.Split(string(data), "\n")

	for _, line := range lines {
		words := strings.Split(line, " ")
		if len(words) < 3 {
			continue
		}

		name, path, fusename := words[0], words[1], words[2]
		if name == "remotefs" && path == checkPath && fusename == "fuse.remotefs" {
			return true, nil
		}
	}

	return false, nil
}
