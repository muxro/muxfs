package remotefs

import (
	"os/user"
	"strconv"
)

// MapIdentsToSystem maps an EntryMeta's UID and GID from the Snapshot format, which is
// an array index (like 0, 1) to the system IDs (like 1000, 1001)
// It takes the users and groups currently available in the system
func (m *EntryMeta) MapIdentsToSystem(users, groups []*Owner) {
	m.Uid = users[m.Uid].Id
	m.Gid = groups[m.Gid].Id
}

// getOwnerSnapshotID returns the snapshot ID (index in the slice) of the owner with the specified system ID.
// Returns -1 if that ID is not found
func getOwnerSnapshotID(sysID uint32, owners []*Owner) int {
	for i, o := range owners {
		if o.Id == sysID {
			return i
		}
	}
	return -1
}

// addUserFromSystem looks up the user by ID, and returns a new slice with the user appended
// If the user is not found in the system a blank name will be used
func addUserFromSystem(users []*Owner, sysUID uint32) []*Owner {
	newUser := &Owner{
		Id:   sysUID,
		Name: "",
	}

	sysUser, err := user.LookupId(strconv.Itoa(int(sysUID)))
	if err == nil {
		newUser.Name = sysUser.Username
	}

	return append(users, newUser)
}

// addGroupFromSystem looks up the group by ID, and returns a new slice with the group appended
// If the group is not found in the system a blank name will be used
func addGroupFromSystem(groups []*Owner, sysGID uint32) []*Owner {
	newGroup := &Owner{
		Id:   sysGID,
		Name: "",
	}

	sysGroup, err := user.LookupGroupId(strconv.Itoa(int(sysGID)))
	if err == nil {
		newGroup.Name = sysGroup.Name
	}

	return append(groups, newGroup)
}
