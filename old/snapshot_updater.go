package remotefs

import (
	"bytes"
	"io/ioutil"
	"remotefs/blocks"

	proto "github.com/golang/protobuf/proto"
	"golang.org/x/net/context"
)

type snapshotUpdater struct {
	updater *blocks.Updater
}

func NewSnapshotUpdater(store blocks.Store, uuid fsUUID) (*snapshotUpdater, error) {
	updater, err := blocks.NewUpdater(store, NewSnapshotUpdaterKey(uuid), snapshotUpdaterLimit)
	if err != nil {
		return nil, err
	}
	return &snapshotUpdater{updater: updater}, nil
}

func NewSnapshotUpdaterKey(uuid fsUUID) blocks.UpdaterKey {
	return blocks.UpdaterKey(string(uuid) + "-snapshots-")
}

func (su *snapshotUpdater) Get(ctx context.Context) ([]*SnapshotInfo, error) {
	rc, err := su.updater.Get(ctx)
	if err != nil {
		if err == blocks.ErrUpdaterEmpty {
			return []*SnapshotInfo{}, nil
		}
		return nil, err
	}
	defer rc.Close()

	infosData, err := ioutil.ReadAll(rc)
	if err != nil {
		return nil, err
	}
	var infos SnapshotInfos
	err = proto.Unmarshal(infosData, &infos)
	if err != nil {
		return nil, err
	}
	return infos.Infos, nil
}

func (su *snapshotUpdater) Set(ctx context.Context, infos []*SnapshotInfo) error {
	protoInfos := &SnapshotInfos{Infos: infos}
	newInfosData, err := proto.Marshal(protoInfos)
	if err != nil {
		return err
	}
	err = su.updater.Set(ctx, bytes.NewReader(newInfosData))
	if err != nil {
		return err
	}
	return nil
}

func (su *snapshotUpdater) Delete(ctx context.Context) error {
	return su.updater.Delete(ctx)
}
