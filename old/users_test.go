package remotefs

import (
	"bufio"
	"context"
	"os"
	"os/user"
	"strconv"
	"strings"
	"testing"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
)

var (
	ownerCurrent *Owner
	ownerNobody  *Owner
	ownerNogroup *Owner
)

func init() {
	userCurrent, err := user.Current()
	if err != nil {
		panic("current user doesn't exist on this system")
	}
	currentUID, err := parseUnixID(userCurrent.Uid)
	if err != nil {
		panic("can't parse current user uid")
	}
	ownerCurrent = &Owner{
		Name: userCurrent.Username,
		Id:   currentUID,
	}

	userNobody, err := user.Lookup("nobody")
	if err != nil {
		panic("user nobody doesn't exist on this system")
	}
	nobodyUID, err := parseUnixID(userNobody.Uid)
	if err != nil {
		panic("can't parse nobody user uid")
	}
	ownerNobody = &Owner{
		Name: userNobody.Username,
		Id:   nobodyUID,
	}

	groupNogroup, err := user.LookupGroup("nogroup")
	if err != nil {
		panic("group nogroup doesn't exist on this system")
	}
	nogroupGID, err := parseUnixID(groupNogroup.Gid)
	if err != nil {
		panic("can't parse nogroup group gid")
	}
	ownerNogroup = &Owner{
		Name: groupNogroup.Name,
		Id:   nogroupGID,
	}
}

func TestChown(t *testing.T) {
	require := require.New(t)

	sysGroups, err := getGroupList()
	require.Nil(err)

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Uid: 0, Gid: 0}},
			&Entry{Path: "/dir/", Meta: &EntryMeta{Uid: 0, Gid: 0}},
			&Entry{Path: "/foo", Meta: &EntryMeta{Uid: 0, Gid: 0}},
			&Entry{Path: "/bar", Meta: &EntryMeta{Uid: 1, Gid: 1}},
			&Entry{Path: "/baz", Meta: &EntryMeta{Uid: 0, Gid: 1}},
		},
		Users: []*Owner{
			&Owner{Name: "zak", Id: 3002},
		},
		Groups: []*Owner{
			&Owner{Name: sysGroups[0].Name, Id: 123}, // id other than 0
		},
	}))
	require.Nil(err)
	defer fs.Done()

	require.True(userHasID(fs.FS.users, ownerCurrent.Name, ownerCurrent.Id))
	require.True(userHasID(fs.FS.users, "zak", 3002))

	node := fs.FS.root.children["foo"].(*FileNode)

	// This Chown call should add a new user from the system (nobody)
	// and the 'nogroup' group
	resp, err := chown(node, ownerNobody.Id, ownerNogroup.Id)
	require.Nil(err)

	// check response Attr
	require.Equal(uint32(ownerNobody.Id), resp.Attr.Uid)
	require.Equal(uint32(ownerNogroup.Id), resp.Attr.Gid)

	// check the node meta
	require.Equal(uint32(ownerNobody.Id), node.meta.Uid)
	require.Equal(uint32(ownerNogroup.Id), node.meta.Gid)

	// check the file ownership matches using file.Stat()
	// SnapGID = 0 maps to SysGID = 100 (group 'users')
	// SnapGID = 1 maps to SysGID = 0 (group 'root') usually
	require.True(fs.isOwnershipEqual("/", 1000, 100))
	require.True(fs.isOwnershipEqual("/dir/", 1000, 100))
	require.True(fs.isOwnershipEqual("/foo", ownerNobody.Id, ownerNobody.Id))
	require.True(fs.isOwnershipEqual("/bar", 3002, sysGroups[0].Id))

	snap, err := fs.FS.makeSnapshot(defaultCtx)
	require.Nil(err)
	snap, err = fs.FS.makeSnapshot(defaultCtx)
	require.Nil(err)
	snap, err = fs.FS.makeSnapshot(defaultCtx)
	require.Nil(err)
	snap, err = fs.FS.makeSnapshot(defaultCtx)
	require.Nil(err)

	// users look like this now:
	// [x] = ed:1000 (current user not saved)
	// [0] = zak:3002
	// [1] = nobody:65534
	require.Equal(2, len(snap.Users))
	require.Equal("zak", snap.Users[0].Name)
	require.Equal(uint32(3002), snap.Users[0].Id)
	require.Equal(ownerNobody.Name, snap.Users[1].Name)
	require.Equal(ownerNobody.Id, snap.Users[1].Id)

	// groups look like this now:
	// [x] = users:100 (current group not saved)
	// [0] = osGroups[0].Name:osGroups[0].Id
	// [1] = nogroup:65534 (or whatever nogroup has)
	require.Equal(2, len(snap.Groups))
	require.Equal(sysGroups[0].Name, snap.Groups[0].Name)
	require.Equal(sysGroups[0].Id, snap.Groups[0].Id)
	require.Equal(ownerNogroup.Name, snap.Groups[1].Name)
	require.Equal(ownerNogroup.Id, snap.Groups[1].Id)

	// check that we mapped the SysIDs to SnapIDs correctly in the entries
	for _, e := range snap.Entries {
		switch e.Path {
		case "/foo":
			require.Equal(uint32(2), e.Meta.Uid)
			require.Equal(uint32(2), e.Meta.Gid)
		case "/baz":
			require.Equal(uint32(0), e.Meta.Uid)
			require.Equal(uint32(1), e.Meta.Gid)
		case "/bar":
			require.Equal(uint32(1), e.Meta.Uid)
			require.Equal(uint32(1), e.Meta.Gid)
		}
	}
}

func TestUsersSnapshot(t *testing.T) {
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Uid: 0, Gid: 0}},
		},
		Users:  []*Owner{},
		Groups: []*Owner{},
	}))
	require.NoError(t, err)
	defer fs.Done()

	snap, err := fs.FS.makeSnapshot(defaultCtx)
	require.NoError(t, err)
	require.Equal(t, 0, len(snap.Users))
	snap, err = fs.FS.makeSnapshot(defaultCtx)
	require.NoError(t, err)
	require.Equal(t, 0, len(snap.Users))
	snap, err = fs.FS.makeSnapshot(defaultCtx)
	require.NoError(t, err)
	require.Equal(t, 0, len(snap.Users))
}

func chown(node *FileNode, uid, gid uint32) (fuse.SetattrResponse, error) {
	req := fuse.SetattrRequest{
		Valid: fuse.SetattrUid | fuse.SetattrGid,
		Uid:   uid,
		Gid:   gid,
	}

	var resp fuse.SetattrResponse
	err := node.Setattr(context.Background(), &req, &resp)
	return resp, err
}

func getGroupList() (groups []*Owner, err error) {
	f, err := os.Open("/etc/passwd")
	if err != nil {
		return
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := scanner.Text()

		parts := strings.Split(line, ":")

		name := parts[0]
		var gid int
		gid, err = strconv.Atoi(parts[3])
		if err != nil {
			return
		}

		groups = append(groups, &Owner{
			Name: name,
			Id:   uint32(gid),
		})
	}

	return
}

func userHasID(users []*Owner, name string, id uint32) bool {
	for _, u := range users {
		if u.Name == name && u.Id == id {
			return true
		}
	}
	return false
}

func TestChmod(t *testing.T) {
	require := require.New(t)

	// files only have relevant permissions for the current user
	// user/others are 0
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Mode: uint32(0777 | os.ModeDir), Size: 4096}},
			&Entry{Path: "/read", Meta: &EntryMeta{Mode: 0400}},
			&Entry{Path: "/write", Meta: &EntryMeta{Mode: 0200}},
			&Entry{Path: "/execute", Meta: &EntryMeta{Mode: 0500}}, // must be readable too
		},
	}))
	require.Nil(err)
	defer fs.Done()

	// write minimal executable script
	err = fs.Chmod("/execute", 0700)
	require.Nil(err)
	execf, err := fs.OpenFile("/execute", os.O_RDWR, 0000)
	require.Nil(err)
	execf.Write([]byte("#!/bin/sh"))
	execf.Close()
	err = fs.Chmod("/execute", 0500)
	require.Nil(err)

	// read-only
	f, err := fs.OpenFile("/read", os.O_RDONLY, 0000)
	defer f.Close()
	require.Nil(err)
	f, err = fs.OpenFile("/read", os.O_WRONLY, 0000)
	errPerm(t, err)
	defer f.Close()
	err = fs.Exec("/read")
	errPerm(t, err)

	// write-only
	f, err = fs.OpenFile("/write", os.O_RDONLY, 0000)
	defer f.Close()
	errPerm(t, err)
	f, err = fs.OpenFile("/write", os.O_WRONLY, 0000)
	defer f.Close()
	require.Nil(err)
	err = fs.Exec("/write")
	errPerm(t, err)

	// exec-only (has to have read too)
	f, err = fs.OpenFile("/execute", os.O_RDONLY, 0000)
	defer f.Close()
	require.Nil(err)
	f, err = fs.OpenFile("/execute", os.O_WRONLY, 0000)
	defer f.Close()
	errPerm(t, err)
	err = fs.Exec("/execute")
	require.Nil(err)
}

func errPerm(t *testing.T, err error) {
	require.True(t, os.IsPermission(err))
}
