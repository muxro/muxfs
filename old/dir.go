package remotefs

import (
	fmt "fmt"
	"os"
	"path/filepath"
	"sync"
	"sync/atomic"
	"syscall"
	"time"

	"bazil.org/fuse"
	bazilFS "bazil.org/fuse/fs"
	"golang.org/x/net/context"
)

type DirNode struct {
	parent   *DirNode
	meta     *EntryMeta
	children map[string]bazilFS.Node

	mu sync.RWMutex

	FS *FS
}

var _ bazilFS.NodeMkdirer = &DirNode{}   // mkdir
var _ bazilFS.NodeCreater = &DirNode{}   // touch
var _ bazilFS.NodeRemover = &DirNode{}   // rm
var _ bazilFS.NodeRenamer = &DirNode{}   // mv
var _ bazilFS.NodeSymlinker = &DirNode{} // Create symlink

const ENOTEMPTY = fuse.Errno(syscall.ENOTEMPTY)

func (n *DirNode) Attr(ctx context.Context, a *fuse.Attr) error {
	n.mu.RLock()
	defer n.mu.RUnlock()

	meta := n.meta

	a.Size = uint64(meta.Size)
	a.Mode = os.FileMode(meta.Mode)
	a.Mtime = time.Unix(meta.UpdatedAt, 0)
	a.Uid = meta.Uid
	a.Gid = meta.Gid

	return nil
}

func (n *DirNode) Setattr(ctx context.Context, req *fuse.SetattrRequest, resp *fuse.SetattrResponse) error {
	defer n.FS.changed()

	n.mu.Lock()
	defer n.mu.Unlock()

	if req.Valid.Size() {
		// cant change size of a dir
		return fuse.EIO
	}
	if req.Valid.Mode() {
		n.meta.Mode = uint32(req.Mode)
		resp.Attr.Mode = req.Mode
	}
	if req.Valid.Uid() {
		n.meta.Uid = req.Uid
		resp.Attr.Uid = req.Uid
	}
	if req.Valid.Gid() {
		n.meta.Gid = req.Gid
		resp.Attr.Gid = req.Gid
	}

	return nil
}

func (n *DirNode) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	n.mu.RLock()
	defer n.mu.RUnlock()

	dirents := make([]fuse.Dirent, len(n.children))

	i := 0
	for name, c := range n.children {
		switch c.(type) {
		case *DirNode:
			dirents[i] = fuse.Dirent{
				Name: name,
				Type: fuse.DT_Dir,
			}
		case *FileNode:
			dirents[i] = fuse.Dirent{
				Name: name,
				Type: fuse.DT_File,
			}
		case *SymlinkNode:
			dirents[i] = fuse.Dirent{
				Name: name,
				Type: fuse.DT_Link,
			}
		default:
			panic("ReadDirAll: unknown node type")
		}

		i++
	}

	return dirents, nil
}

func (n *DirNode) Lookup(ctx context.Context, name string) (bazilFS.Node, error) {
	n.mu.RLock()
	defer n.mu.RUnlock()

	found, ok := n.children[name]
	if !ok {
		return nil, fuse.ENOENT
	}

	return found, nil
}

func (n *DirNode) Mkdir(ctx context.Context, req *fuse.MkdirRequest) (bazilFS.Node, error) {
	defer n.FS.changed()

	// rlock FS
	n.FS.commitLock.RLock()
	defer n.FS.commitLock.RUnlock()

	newDir := &DirNode{
		parent: n,
		meta: &EntryMeta{
			Size:      4096,
			Mode:      uint32(req.Mode),
			Uid:       req.Uid,
			Gid:       req.Gid,
			UpdatedAt: time.Now().Unix(),
		},
		children: make(map[string]bazilFS.Node),
		FS:       n.FS,
	}

	n.mu.Lock()
	n.children[req.Name] = newDir
	n.mu.Unlock()

	return newDir, nil
}

func (n *DirNode) Forget() {
	// fmt.Println("<<<< FORGET DIR with children:", n.children)
}

func (n *DirNode) Create(ctx context.Context, req *fuse.CreateRequest, resp *fuse.CreateResponse) (bazilFS.Node, bazilFS.Handle, error) {
	defer n.FS.changed()

	// rlock FS
	n.FS.commitLock.RLock()
	defer n.FS.commitLock.RUnlock()

	if len(req.Name) > 255 {
		return nil, nil, fuse.EIO
	}

	// check full path < 4096
	dirPath := getFullNodePath(n)
	fullpath := filepath.Join(dirPath, req.Name)
	if len(fullpath) > 4095 {
		return nil, nil, fuse.EIO
	}

	writeCache := NewCache()
	if n.FS.hasCacheMeasureCommit {
		n.FS.cacheMeasure.Subscribe(writeCache)
	}

	newFile := &FileNode{
		parent:       n,
		blocks:       nil,
		blockDataEnd: 0,
		blocksSize:   0,
		cache:        writeCache,
		meta: &EntryMeta{
			Size:      0,
			Mode:      uint32(req.Mode),
			Uid:       req.Uid,
			Gid:       req.Gid,
			UpdatedAt: time.Now().Unix(),
		},
		FS: n.FS,
	}

	atomic.AddInt64(&newFile.openFds, 1)

	n.mu.Lock()
	n.children[req.Name] = newFile
	n.mu.Unlock()

	return newFile, newFile, nil
}

func (n *DirNode) Release(ctx context.Context, req *fuse.ReleaseRequest) error {
	return nil
}

// Remove removes the entry with the given name from
// the receiver, which must be a directory.  The entry to be removed
// may correspond to a file (unlink) or to a directory (rmdir).
func (n *DirNode) Remove(ctx context.Context, req *fuse.RemoveRequest) error {
	defer n.FS.changed()

	// rlock FS
	n.FS.commitLock.RLock()
	defer n.FS.commitLock.RUnlock()
	n.mu.Lock()
	defer n.mu.Unlock()

	child, ok := n.children[req.Name]
	if !ok {
		return fuse.ENOENT
	}

	switch c := child.(type) {
	case *FileNode:
		val := atomic.LoadInt64(&c.openFds)
		if val != 0 {
			c.FS.orphanedFiles = append(c.FS.orphanedFiles, c)
		}
	case *DirNode: // can only rmdir if dir is empty
		if req.Dir && len(c.children) != 0 {
			return ENOTEMPTY
		}
	case *SymlinkNode:
		// nothing
	}

	delete(n.children, req.Name)

	return nil
}

func (n *DirNode) Rename(ctx context.Context, req *fuse.RenameRequest, newDir bazilFS.Node) error {
	defer n.FS.changed()

	// rlock FS
	n.FS.commitLock.RLock()
	defer n.FS.commitLock.RUnlock()

	newDirNode, ok := newDir.(*DirNode)
	if !ok {
		return fuse.ENOSYS
	}

	// check filename < 256
	if len(req.NewName) > 255 {
		return fuse.EIO
	}

	// check full path < 4096
	n.mu.RLock()
	newDirPath := getFullNodePath(newDirNode)
	n.mu.RUnlock()
	fullpath := filepath.Join(newDirPath, req.NewName)
	if len(fullpath) > 4095 {
		return fuse.EIO
	}

	n.mu.Lock()
	defer n.mu.Unlock()
	oldNode, ok := n.children[req.OldName]
	if !ok {
		return fuse.EEXIST
	}
	newDirNode.children[req.NewName] = oldNode
	delete(n.children, req.OldName)
	return nil
}

// Symlink creates a new symbolic link in the receiver, which must be a directory.
func (n *DirNode) Symlink(ctx context.Context, req *fuse.SymlinkRequest) (bazilFS.Node, error) {
	defer n.FS.changed()

	// rlock FS
	n.FS.commitLock.RLock()
	defer n.FS.commitLock.RUnlock()

	newSymlink := &SymlinkNode{
		parent: n,
		meta: &EntryMeta{
			Size:      int64(len(req.Target)),
			Mode:      uint32(0777 | os.ModeSymlink),
			UpdatedAt: time.Now().Unix(),
		},
		target: req.Target,
		FS:     n.FS,
	}

	n.mu.Lock()
	n.children[req.NewName] = newSymlink
	n.mu.Unlock()

	return newSymlink, nil
}

// TODO: Commit all files and snapshot
func (n *DirNode) Fsync(ctx context.Context, req *fuse.FsyncRequest) error {
	fmt.Println("fsync dir")
	return nil
}
