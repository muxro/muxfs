#! /bin/sh

go test -short -benchmem -bench . -run "^Benchmark" 2>&1 | tee bench/results_short_$(date +'%Y%m%d_%H%M')
