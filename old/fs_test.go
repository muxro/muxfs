package remotefs

import (
	"bytes"
	"context"
	"errors"
	fmt "fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"remotefs/blocks"
	"testing"
	"time"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
)

func testIsDir(t *testing.T, fs *TestFS, filename string) {
	fi, err := fs.Stat(filename)
	require.NoError(t, err)
	require.True(t, fi.IsDir())
}

func TestTreeCreation(t *testing.T) {
	require := require.New(t)

	fs, err := NewTestFS(
		FromPaths(
			"/",
			"/foo",
			"/diz/bar/baz",
			"/diz/",
			"/diz/bar/",
			"/diz/bar/kix",
			"/diz/bar/qux/",
			"/diz/bar/qux/foobie",
		),
	)
	require.Nil(err)
	defer fs.Done()

	root := fs.FS.root
	require.NotNil(root)
	testIsDir(t, fs, "/")

	foo, ok := root.children["foo"].(*FileNode)
	require.NotNil(foo)
	require.True(ok)

	diz, ok := root.children["diz"].(*DirNode)
	require.NotNil(diz)
	require.True(ok)

	bar, ok := diz.children["bar"].(*DirNode)
	require.NotNil(bar)
	require.True(ok)

	baz, ok := bar.children["baz"].(*FileNode)
	require.NotNil(baz)
	require.True(ok)

	kix, ok := bar.children["kix"].(*FileNode)
	require.NotNil(kix)
	require.True(ok)

	qux, ok := bar.children["qux"].(*DirNode)
	require.NotNil(qux)
	require.True(ok)

	foobie, ok := qux.children["foobie"].(*FileNode)
	require.NotNil(foobie)
	require.True(ok)
}

func TestSymlink(t *testing.T) {
	require := require.New(t)
	fs, err := NewTestFS(FromPaths("/", "/file"))
	require.Nil(err)
	defer fs.Done()

	// make a new symlink pointing to /file
	err = fs.Symlink("/file", "/symlink")
	require.Nil(err)

	// os.Lstat the symlink and make sure it's a symlink
	info, err := fs.Lstat("/symlink")
	require.Nil(err)

	require.Equal("symlink", info.Name())
	require.False(info.IsDir())
	require.NotEqual(os.FileMode(0), info.Mode()&os.ModeSymlink)

	// os.Stat the symlink and make sure it's NOT a symlink
	info, err = fs.Stat("/symlink")
	require.Nil(err)

	// os.Stat should follow symlinks, but the file name is of the symlink instead of the file ???
	require.Equal("symlink", info.Name())
	require.False(info.IsDir())
	require.Equal(os.FileMode(0), info.Mode()&os.ModeSymlink)

	target, err := fs.Readlink("/symlink")
	require.Nil(err)
	require.Equal(filepath.Join(fs.Path, "/file"), target)

	// now we create a symlink with a broken target
	err = fs.SymlinkAbsolute("/some/bogus/path", "/brokensymlink")
	require.Nil(err)

	target, err = fs.Readlink("/brokensymlink")
	require.Nil(err)
	require.Equal("/some/bogus/path", target)

	info, err = fs.Lstat("/brokensymlink")
	require.Nil(err)
	require.Equal("brokensymlink", info.Name())

	// stat on a broken symlink should not work
	info, err = fs.Stat("/brokensymlink")
	require.NotNil(err)

	// make sure you can `ls` a dir containing a symlink
	files, err := fs.ReadDir("/")
	require.Nil(err)

	var foundSymlink bool
	for _, file := range files {
		if file.Name() == "symlink" {
			foundSymlink = true
			break
		}
	}
	require.True(foundSymlink)
}

func TestSetattr(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/file", "/dir/"))
	require.NoError(t, err)
	defer fs.Done()
	err = fs.Symlink("/file", "/symlink")
	require.NoError(t, err)

	// symlink
	symlinkNode := fs.FS.root.children["symlink"].(*SymlinkNode)
	// size
	ctx := context.Background()
	req := &fuse.SetattrRequest{
		Valid: fuse.SetattrValid(fuse.SetattrSize),
		Size:  20,
	}
	var resp fuse.SetattrResponse
	err = symlinkNode.Setattr(ctx, req, &resp)
	require.NotNil(t, err)

	// mode
	var attrBefore fuse.Attr
	err = symlinkNode.Attr(ctx, &attrBefore)
	modeBefore := attrBefore.Mode
	req = &fuse.SetattrRequest{
		Valid: fuse.SetattrValid(fuse.SetattrMode),
		Mode:  0456,
	}
	err = symlinkNode.Setattr(ctx, req, &resp)
	require.NoError(t, err)
	var attrAfter fuse.Attr
	err = symlinkNode.Attr(ctx, &attrAfter)
	modeAfter := attrAfter.Mode
	require.Equal(t, modeAfter, modeBefore)

	// uid
	req = &fuse.SetattrRequest{
		Valid: fuse.SetattrValid(fuse.SetattrUid),
		Uid:   1234,
	}
	err = symlinkNode.Setattr(ctx, req, &resp)
	require.NoError(t, err)
	require.Equal(t, uint32(1234), resp.Attr.Uid)

	// gid
	req = &fuse.SetattrRequest{
		Valid: fuse.SetattrValid(fuse.SetattrGid),
		Gid:   1234,
	}
	err = symlinkNode.Setattr(ctx, req, &resp)
	require.NoError(t, err)
	require.Equal(t, uint32(1234), resp.Attr.Gid)

	// dir
	dirNode := fs.FS.root.children["dir"].(*DirNode)
	// size
	ctx = context.Background()
	req = &fuse.SetattrRequest{
		Valid: fuse.SetattrValid(fuse.SetattrSize),
		Size:  20,
	}
	err = dirNode.Setattr(ctx, req, &resp)
	require.NotNil(t, err)

	// mode
	err = symlinkNode.Attr(ctx, &attrBefore)
	modeBefore = attrBefore.Mode
	req = &fuse.SetattrRequest{
		Valid: fuse.SetattrValid(fuse.SetattrMode),
		Mode:  0456,
	}
	err = symlinkNode.Setattr(ctx, req, &resp)
	require.NoError(t, err)
	require.NotEqual(t, resp.Attr.Mode, modeBefore)

	// uid
	req = &fuse.SetattrRequest{
		Valid: fuse.SetattrValid(fuse.SetattrUid),
		Uid:   1234,
	}
	err = symlinkNode.Setattr(ctx, req, &resp)
	require.NoError(t, err)
	require.Equal(t, uint32(1234), resp.Attr.Uid)

	// gid
	req = &fuse.SetattrRequest{
		Valid: fuse.SetattrValid(fuse.SetattrGid),
		Gid:   1234,
	}
	err = symlinkNode.Setattr(ctx, req, &resp)
	require.NoError(t, err)
	require.Equal(t, uint32(1234), resp.Attr.Gid)
}

func verifyMode(t *testing.T, fs *TestFS, file string, mode os.FileMode) {
	fi, err := fs.Stat(file)
	require.NoError(t, err)
	require.Equal(t, mode, fi.Mode())
}

func TestOpenTruncate(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/file"))
	require.NoError(t, err)
	defer fs.Done()

	f, err := fs.OpenFile("/file", os.O_RDONLY|os.O_TRUNC, 0000)
	require.NoError(t, err)
	defer f.Close()

	fi, err := f.Stat()
	require.NoError(t, err)
	size := fi.Size()

	require.Equal(t, int64(0), size)
}

func TestCacheMeasureSize(t *testing.T) {
	fs, err := NewTestFS(
		FromPaths("/", "/f1", "/f2"),
		CacheMeasureCommit(500),
	)
	require.NoError(t, err)
	defer fs.Done()

	csm := fs.FS.cacheMeasure
	var timesChanged int
	fs.FS.cacheMeasure.OnSizeChange(func(oldtotalsize, newtotalsize int64) {
		timesChanged++

		switch timesChanged {
		case 1:
			require.Equal(t, int64(cacheBlockSize), newtotalsize)
		case 2:
			require.Equal(t, int64(2*cacheBlockSize), newtotalsize)
		case 3:
			require.Equal(t, int64(3*cacheBlockSize), newtotalsize)
		}
	})

	// nothing in the cache yet
	require.Equal(t, int64(0), csm.TotalSize())

	f1, err := fs.OpenFile("/f1", os.O_RDWR, 0000)
	defer f1.Close()
	require.NoError(t, err)
	n, err := f1.Write(make([]byte, cacheBlockSize))
	require.NoError(t, err)
	require.Equal(t, cacheBlockSize, n)

	time.Sleep(50 * time.Millisecond)

	// should be 1 cacheBlockSize
	require.Equal(t, 1, timesChanged)
	require.Equal(t, int64(cacheBlockSize), csm.TotalSize())

	f2, err := fs.OpenFile("/f2", os.O_RDWR, 0000)
	defer f2.Close()
	require.NoError(t, err)
	n, err = f2.Write(make([]byte, cacheBlockSize))
	require.NoError(t, err)
	require.Equal(t, cacheBlockSize, n)

	time.Sleep(50 * time.Millisecond)

	// should be 2 cacheBlockSize
	require.Equal(t, 2, timesChanged)
	require.Equal(t, int64(2*cacheBlockSize), csm.TotalSize())

	// write to some other cache block
	n, err = f1.WriteAt(make([]byte, cacheBlockSize), cacheBlockSize*4)
	require.NoError(t, err)
	require.Equal(t, cacheBlockSize, n)

	time.Sleep(50 * time.Millisecond)

	// should be 3 cacheBlockSize
	require.Equal(t, 3, timesChanged)
	require.Equal(t, int64(3*cacheBlockSize), csm.TotalSize())
}

func TestFSSave(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/"))
	require.NoError(t, err)
	defer fs.Done()

	err = fs.FS.save(manualRetentionGroup)
	require.NoError(t, err)

	snaps, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
	require.NoError(t, err)
	require.Empty(t, snaps)

	err = fs.WriteFile("/file", blocks.TestData("/file", 0, 5*1024*1024), 0755)
	require.NoError(t, err)
	err = fs.FS.save(manualRetentionGroup)
	require.NoError(t, err)

	snaps, err = fs.FS.snapshotUpdater.Get(fs.FS.ctx)
	require.NoError(t, err)
	require.Len(t, snaps, 1)
}

func TestCacheMeasureCommit(t *testing.T) {
	fs, err := NewTestFS(
		FromPaths("/", "/file"),
		CacheMeasureCommit(10),
	)
	require.NoError(t, err)
	defer fs.Done()

	f, err := fs.OpenFile("/file", os.O_WRONLY|os.O_TRUNC, 0000)
	require.NoError(t, err)
	expected := make([]byte, 11)
	n, err := f.Write(expected)
	require.NoError(t, err)
	require.Equal(t, 11, n)
	f.Close()

	// wait for commit to finish
	time.Sleep(10 * time.Millisecond)

	// clear cache to make sure we're not reading from it
	fs.FS.root.children["file"].(*FileNode).cache.Clear()

	f, err = fs.OpenFile("/file", os.O_RDONLY, 0000)
	require.NoError(t, err)
	defer f.Close()

	actual, err := ioutil.ReadAll(f)
	require.NoError(t, err)

	require.Equal(t, len(expected), len(actual))
	require.Equal(t, expected, actual)
}

func TestWriteLock(t *testing.T) {
	const sleepTime = 10 * time.Millisecond

	fs, err := NewTestFS(FromPaths("/", "/file"))
	require.NoError(t, err)
	defer fs.Done()

	done := make(chan bool)
	startWrite := make(chan bool)

	n, ok := fs.FS.root.children["file"].(*FileNode)
	require.True(t, ok)

	go func() {
		fs.FS.commitLock.Lock()
		startWrite <- true
		time.Sleep(sleepTime)
		fs.FS.commitLock.Unlock()
	}()

	var dur time.Duration
	go func() {
		<-startWrite
		start := time.Now()

		// write
		req := &fuse.WriteRequest{Offset: 0, Data: make([]byte, 1000)}
		resp := &fuse.WriteResponse{}
		n.Write(context.Background(), req, resp)

		// see how long the write took
		dur = time.Now().Sub(start)
		done <- true
	}()

	<-done

	require.True(t, dur >= sleepTime)
}
func TestCommitWriteDeadlock(t *testing.T) {
	const (
		sizeStart    = 5 * 1024 * 1024
		sizeUpTo     = 10 * 1024 * 1024
		maxCacheSize = 5 * 1024 * 1024
	)
	for size := int64(sizeStart); size < sizeUpTo; size += 1024 * 1024 {
		desc := fmt.Sprintf("Size%vMB_CacheSize%vMB", size/1024/1024, maxCacheSize/1024/1024)
		t.Run(desc, func(t *testing.T) {
			timeout := time.After(4000 * time.Millisecond)
			done := make(chan bool)

			go func() {
				fs, _ := NewTestFS(FromSnapshot(&Snapshot{
					Entries: []*Entry{
						&Entry{Path: "/"},
						&Entry{Path: "/file", Meta: &EntryMeta{Size: size}},
					},
				}), CacheMeasureCommit(maxCacheSize))
				defer fs.Done()

				node := fs.FS.root.children["file"].(*FileNode)

				data := bytes.Repeat([]byte{'f'}, 4096)

				req := &fuse.WriteRequest{}
				resp := &fuse.WriteResponse{}
				for o := int64(0); o <= size; o += 4096 {
					req.Offset = o
					req.Data = data

					node.Write(context.Background(), req, resp)
				}
				done <- true
			}()

			select {
			case <-timeout:
				t.Fatal("Test didn't finish in time (deadlock)")
			case <-done:
			}
		})
	}
}

func TestOrphanedFiles(t *testing.T) {
	fs, err := NewTestFS()
	require.NoError(t, err)
	defer fs.Done()

	f, err := fs.Create("/orphan")
	defer f.Close()
	require.NoError(t, err)

	require.Equal(t, 0, len(fs.FS.orphanedFiles))

	err = fs.Remove("/orphan")
	require.NoError(t, err)

	require.Equal(t, 1, len(fs.FS.orphanedFiles))
}

func TestUnmount(t *testing.T) {
	fs, err := NewTestFS()
	require.NoError(t, err)

	fs.Done()
	err = unmountAndWaitUntilUnmounted(fs.Path)
	require.NoError(t, err)
}

func TestActivityCommit(t *testing.T) {
	revert := setActivityCommitTime(10 * time.Millisecond)
	defer revert()

	fs, err := NewTestFS(
		FromPaths("/", "/file"),
		ActivityCommit(true),
	)
	require.NoError(t, err)
	defer fs.Done()

	f, err := fs.OpenFile("/file", os.O_WRONLY|os.O_TRUNC, 0000)
	require.NoError(t, err)
	expected := blocks.TestData("test", 0, 10)
	n, err := f.Write(expected)
	require.NoError(t, err)
	require.Equal(t, 10, n)
	f.Close()

	time.Sleep(activityCommitTime / 4)
	fs.FS.root.children["file"].(*FileNode).cache.Clear()

	f, err = fs.OpenFile("/file", os.O_RDONLY, 0000)
	require.NoError(t, err)
	defer f.Close()

	actual, err := ioutil.ReadAll(f)
	require.NoError(t, err)
	require.NotEqual(t, expected, actual)

	f, err = fs.OpenFile("/file", os.O_WRONLY|os.O_TRUNC, 0000)
	require.NoError(t, err)
	n, err = f.Write(expected)
	require.NoError(t, err)
	require.Equal(t, 10, n)
	f.Close()

	time.Sleep(activityCommitTime + 5*time.Millisecond)
	fs.FS.root.children["file"].(*FileNode).cache.Clear()

	f, err = fs.OpenFile("/file", os.O_RDONLY, 0000)
	require.NoError(t, err)
	defer f.Close()

	actual, err = ioutil.ReadAll(f)
	require.NoError(t, err)
	require.Equal(t, expected, actual)
}

func TestFSNeedsSave(t *testing.T) {
	ctx := context.Background()
	testCases := []struct {
		desc       string
		changeFunc func(*testing.T, *TestFS)
		needsSave  bool
	}{
		{
			desc: "FileAttr",
			changeFunc: func(t *testing.T, fs *TestFS) {
				var attr fuse.Attr
				err := fs.childFile("file").Attr(ctx, &attr)
				require.NoError(t, err)
			},
			needsSave: false,
		},
		{
			desc: "FileRead",
			changeFunc: func(t *testing.T, fs *TestFS) {
				err := fs.childFile("file").Read(ctx, &fuse.ReadRequest{
					Dir:    false,
					Offset: 0,
					Size:   10,
				}, &fuse.ReadResponse{})
				require.NoError(t, err)
			},
			needsSave: false,
		},
		{
			desc: "FileSetAttr",
			changeFunc: func(t *testing.T, fs *TestFS) {
				err := fs.childFile("file").Setattr(ctx, &fuse.SetattrRequest{}, &fuse.SetattrResponse{})
				require.NoError(t, err)
			},
			needsSave: true,
		},
		{
			desc: "FileWrite",
			changeFunc: func(t *testing.T, fs *TestFS) {
				err := fs.childFile("file").Write(ctx, &fuse.WriteRequest{}, &fuse.WriteResponse{})
				require.NoError(t, err)
			},
			needsSave: true,
		},
		{
			desc: "FileCommit",
			changeFunc: func(t *testing.T, fs *TestFS) {
				err := fs.childFile("file").Commit(defaultCtx)
				require.NoError(t, err)
			},
			needsSave: false,
		},
		{
			desc: "DirAttr",
			changeFunc: func(t *testing.T, fs *TestFS) {
				var attr fuse.Attr
				err := fs.childDir("dir").Attr(ctx, &attr)
				require.NoError(t, err)
			},
			needsSave: false,
		},
		{
			desc: "DirReadDirAll",
			changeFunc: func(t *testing.T, fs *TestFS) {
				_, err := fs.childDir("dir").ReadDirAll(ctx)
				require.NoError(t, err)
			},
			needsSave: false,
		},
		{
			desc: "DirLookup",
			changeFunc: func(t *testing.T, fs *TestFS) {
				_, err := fs.childDir("dir").Lookup(ctx, "nonexistent")
				require.NotNil(t, err)
			},
			needsSave: false,
		},
		{
			desc: "DirSetattr",
			changeFunc: func(t *testing.T, fs *TestFS) {
				fs.childDir("dir").Setattr(ctx, &fuse.SetattrRequest{}, &fuse.SetattrResponse{})
			},
			needsSave: true,
		},
		{
			desc: "DirMkdir",
			changeFunc: func(t *testing.T, fs *TestFS) {
				fs.childDir("dir").Mkdir(ctx, &fuse.MkdirRequest{})
			},
			needsSave: true,
		},
		{
			desc: "DirCreate",
			changeFunc: func(t *testing.T, fs *TestFS) {
				fs.childDir("dir").Create(ctx, &fuse.CreateRequest{}, &fuse.CreateResponse{})
			},
			needsSave: true,
		},
		{
			desc: "DirRemove",
			changeFunc: func(t *testing.T, fs *TestFS) {
				fs.childDir("dir").Remove(ctx, &fuse.RemoveRequest{})
			},
			needsSave: true,
		},
		{
			desc: "DirRename",
			changeFunc: func(t *testing.T, fs *TestFS) {
				fs.childDir("dir").Rename(ctx, &fuse.RenameRequest{}, nil)
			},
			needsSave: true,
		},
		{
			desc: "DirSymlink",
			changeFunc: func(t *testing.T, fs *TestFS) {
				fs.childDir("dir").Symlink(ctx, &fuse.SymlinkRequest{})
			},
			needsSave: true,
		},
		{
			desc: "SymlinkAttr",
			changeFunc: func(t *testing.T, fs *TestFS) {
				var attr fuse.Attr
				err := fs.childSymlink("symlink").Attr(ctx, &attr)
				require.NoError(t, err)
			},
			needsSave: false,
		},
		{
			desc: "SymlinkReadlink",
			changeFunc: func(t *testing.T, fs *TestFS) {
				_, err := fs.childSymlink("symlink").Readlink(ctx, &fuse.ReadlinkRequest{})
				require.NoError(t, err)
			},
			needsSave: false,
		},
		{
			desc: "SymlinkSetattr",
			changeFunc: func(t *testing.T, fs *TestFS) {
				err := fs.childSymlink("symlink").Setattr(ctx, &fuse.SetattrRequest{}, &fuse.SetattrResponse{})
				require.NoError(t, err)
			},
			needsSave: true,
		},
	}
	for _, tC := range testCases {
		fs, err := NewTestFS(FromSnapshot(&Snapshot{
			Entries: []*Entry{&Entry{Path: "/"}, &Entry{Path: "/file"}, &Entry{Path: "/dir/"}},
			Links:   []*Link{&Link{Path: "/symlink", Target: "/target"}},
		}))
		require.NoError(t, err)

		t.Run(tC.desc, func(t *testing.T) {
			tC.changeFunc(t, fs)

			if tC.needsSave && !fs.FS.needsSave {
				t.Fatal("fs should need save")
			}
			if !tC.needsSave && fs.FS.needsSave {
				t.Fatal("fs should not need save")
			}
		})
		fs.Done()
	}
}

func TestGC(t *testing.T) {
	t.Run("EmptySnapshot", func(t *testing.T) {
		fs, err := NewTestFS(FromPaths("/"))
		require.NoError(t, err)
		storetest := fs.FS.store.(blocks.StoreTest)

		refs, err := storetest.Refs()
		require.NoError(t, err)
		require.Empty(t, refs.ExistExcept(fs.FS.ref, fs.FS.snapshotUpdater.updater.Ref))
		require.Empty(t, refs[fs.FS.ref])
		fs.Done()
		refs, err = storetest.Refs()
		require.NoError(t, err)
		require.Empty(t, refs.ExistExcept(fs.FS.snapshotUpdater.updater.Ref))
	})
	t.Run("SnapshotWithFileNoChange", func(t *testing.T) {
		fs, err := NewTestFS(FromPaths("/", "/file"))
		require.NoError(t, err)
		storetest := fs.FS.store.(blocks.StoreTest)

		refs, err := storetest.Refs()
		require.NoError(t, err)
		require.Empty(t, refs.ExistExcept(fs.InitialSnapshotRefRoot, fs.FS.ref, fs.FS.snapshotUpdater.updater.Ref))
		// check initial snapshot has only 3 refs, the snapshot and the 2 file blocks from our file
		require.Equal(t, 1+len(fs.FS.root.children["file"].(*FileNode).blocks), len(refs[fs.InitialSnapshotRefRoot]))
		// check fs has only 2 refs, the 2 file blocks from our file
		require.Equal(t, len(fs.FS.root.children["file"].(*FileNode).blocks), len(refs[fs.FS.ref]))

		fs.Done()
		refs, err = storetest.Refs()
		require.NoError(t, err)
		require.Empty(t, refs.ExistExcept(fs.InitialSnapshotRefRoot, fs.FS.snapshotUpdater.updater.Ref))
		// check initial snapshot has only 3 refs, the snapshot and the 2 file blocks from our file
		require.Equal(t, 1+len(fs.FS.root.children["file"].(*FileNode).blocks), len(refs[fs.InitialSnapshotRefRoot]))
	})
	t.Run("SnapshotWithFileChange", func(t *testing.T) {
		fs, err := NewTestFS(FromPaths("/", "/file"))
		require.NoError(t, err)
		storetest := fs.FS.store.(blocks.StoreTest)

		refs, err := storetest.Refs()
		require.NoError(t, err)
		require.Empty(t, refs.ExistExcept(fs.InitialSnapshotRefRoot, fs.FS.ref, fs.FS.snapshotUpdater.updater.Ref))

		// check fs has only 2 refs, the 2 file blocks from our file
		require.Equal(t, len(fs.FS.root.children["file"].(*FileNode).blocks), len(refs[fs.FS.ref]))
		// check initial snapshot has only 3 refs, the snapshot and the 2 file blocks from our file
		require.Equal(t, 1+len(fs.FS.root.children["file"].(*FileNode).blocks), len(refs[fs.InitialSnapshotRefRoot]))

		f, err := fs.OpenFile("/file", os.O_CREATE|os.O_WRONLY, 0755)
		require.NoError(t, err)

		n, err := f.Write(blocks.TestData("/file", 0, 1))
		require.NoError(t, err)
		require.Equal(t, 1, n)
		f.Close()

		fs.Done()
		refs, err = storetest.Refs()
		require.NoError(t, err)
		existExcept := refs.ExistExcept(fs.InitialSnapshotRefRoot, fs.FS.snapshotUpdater.updater.Ref)
		require.Equal(t, 1, len(existExcept))
		require.NotEqual(t, existExcept[0], fs.FS.ref)

		// check new snapshot has only 3 refs, the snapshot and the 2 file blocks from our file
		require.Equal(t, 1+len(fs.FS.root.children["file"].(*FileNode).blocks), len(refs[existExcept[0]]))
	})
	t.Run("SameFileSameFS", func(t *testing.T) {
		fs, err := NewTestFS(FromPaths("/"))
		require.NoError(t, err)
		storetest := fs.FS.store.(blocks.StoreTest)
		refs, err := storetest.Refs()
		require.NoError(t, err)
		require.Empty(t, refs.ExistExcept(fs.FS.ref, fs.FS.snapshotUpdater.updater.Ref))

		err = fs.WriteFile("/f1", blocks.TestData("/f1", 0, 7*1024*1024), 0755)
		require.NoError(t, err)
		err = fs.WriteFile("/f2", blocks.TestData("/f1", 0, 7*1024*1024), 0755)
		require.NoError(t, err)

		// keep track of snapshot refs
		var srefs []blocks.ID

		err = fs.FS.save(latestRetentionGroup)
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		existExcept := refs.ExistExcept(fs.FS.ref, fs.FS.snapshotUpdater.updater.Ref)
		require.Equal(t, 1, len(existExcept))
		srefs = addSnapshotRef(srefs, existExcept)
		// check new snapshot has only 3 refs, the snapshot and the 2 file blocks from our file
		require.Equal(t, 1+len(fs.FS.root.children["f1"].(*FileNode).blocks), len(refs[srefs[0]]))

		// remove one of the files
		err = fs.Remove("/f1")
		require.NoError(t, err)

		// snapshot, check no refs have been removed
		err = fs.FS.save(latestRetentionGroup)
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		existExcept = refs.ExistExcept(fs.FS.ref, fs.FS.snapshotUpdater.updater.Ref)
		srefs = addSnapshotRef(srefs, existExcept)

		// check new snapshot has only 3 refs, the snapshot and the 2 file blocks from our file
		require.Equal(t, 1+len(fs.FS.root.children["f2"].(*FileNode).blocks), len(refs[srefs[1]]))

		// remove second file
		err = fs.Remove("/f2")
		require.NoError(t, err)

		// snapshot, check refs have been removed
		err = fs.FS.save(latestRetentionGroup)
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		existExcept = refs.ExistExcept(fs.FS.ref, fs.FS.snapshotUpdater.updater.Ref)
		srefs = addSnapshotRef(srefs, existExcept)

		// check new snapshot has only 1 ref, the snapshot
		require.Equal(t, 1, len(refs[srefs[2]]))

		fs.Done()
	})
	t.Run("SameFileDifferentFS", func(t *testing.T) {
		// common store
		var store blocks.Store = blocks.NewMemoryStore("")
		storetest := store.(blocks.StoreTest)

		// setup fs1
		fs1, err := NewTestFS(FromPaths("/"), WithStore(store))
		require.NoError(t, err)
		refs, err := storetest.Refs()
		require.NoError(t, err)
		require.Empty(t, refs.ExistExcept(fs1.FS.ref, fs1.FS.snapshotUpdater.updater.Ref))
		err = fs1.WriteFile("/f1", blocks.TestData("/f1", 0, 7*1024*1024), 0755)
		require.NoError(t, err)
		// keep track of snapshot refs
		var srefs []blocks.ID
		err = fs1.FS.save(latestRetentionGroup)
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		existExcept := refs.ExistExcept(fs1.FS.ref, fs1.FS.snapshotUpdater.updater.Ref)
		require.Equal(t, 1, len(existExcept))
		srefs = addSnapshotRef(srefs, existExcept)
		// check new snapshot has only 3 refs, the snapshot and the 2 file blocks from our file
		require.Equal(t, 1+len(fs1.FS.root.children["f1"].(*FileNode).blocks), len(refs[srefs[0]]))

		// setup fs2
		fs2, err := NewTestFS(FromPaths("/"), WithStore(store))
		require.NoError(t, err)
		err = fs2.WriteFile("/f2", blocks.TestData("/f1", 0, 7*1024*1024), 0755)
		require.NoError(t, err)
		err = fs2.FS.save(latestRetentionGroup)
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		existExcept = refs.ExistExcept(fs1.FS.ref, fs1.FS.snapshotUpdater.updater.Ref, fs2.FS.ref, fs2.FS.snapshotUpdater.updater.Ref)
		require.Equal(t, 2, len(existExcept))
		srefs = addSnapshotRef(srefs, existExcept)
		// check new snapshot has only 3 refs, the snapshot and the 2 file blocks from our file
		require.Equal(t, 1+len(fs2.FS.root.children["f2"].(*FileNode).blocks), len(refs[srefs[1]]))

		// remove file from fs1
		err = fs1.Remove("/f1")
		require.NoError(t, err)
		// snapshot, check no refs have been removed
		err = fs1.FS.save(latestRetentionGroup)
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		existExcept = refs.ExistExcept(fs1.FS.ref, fs1.FS.snapshotUpdater.updater.Ref, fs2.FS.ref, fs2.FS.snapshotUpdater.updater.Ref)
		srefs = addSnapshotRef(srefs, existExcept)

		// check new snapshot has only 1 ref, the snapshot
		require.Equal(t, 1, len(refs[srefs[2]]))

		// remove second file
		err = fs2.Remove("/f2")
		require.NoError(t, err)

		// snapshot, check refs have been removed
		err = fs2.FS.save(latestRetentionGroup)
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		existExcept = refs.ExistExcept(fs1.FS.ref, fs1.FS.snapshotUpdater.updater.Ref, fs2.FS.ref, fs2.FS.snapshotUpdater.updater.Ref)
		srefs = addSnapshotRef(srefs, existExcept)

		// check new snapshot has only 1 ref, the snapshot
		require.Equal(t, 1, len(refs[srefs[3]]))

		fs1.Done()
		fs2.Done()
	})
	t.Run("SameSnapshotBlockDifferentFS", func(t *testing.T) {
		// Common store
		var store blocks.Store = blocks.NewMemoryStore("")
		storetest := store.(blocks.StoreTest)

		// Create fs1
		fs1, err := NewTestFS(FromPaths("/"), WithStore(store))
		require.NoError(t, err)
		refs, err := storetest.Refs()
		require.NoError(t, err)
		// Check no refs exist except the fs ref and the updater ref
		require.Empty(t, refs.ExistExcept(fs1.FS.ref, fs1.FS.snapshotUpdater.updater.Ref))

		// Create f1
		err = fs1.WriteFile("/f1", blocks.TestData("/f1", 0, 1), 0755)
		require.NoError(t, err)

		// Keep track of snapshot refs
		var srefs []blocks.ID
		// Create snapshot
		err = fs1.FS.save(latestRetentionGroup)
		require.NoError(t, err)
		refs, err = storetest.Refs()
		require.NoError(t, err)
		// Check snapshot was created
		existExcept := refs.ExistExcept(fs1.FS.ref, fs1.FS.snapshotUpdater.updater.Ref)
		require.Equal(t, 1, len(existExcept))
		srefs = addSnapshotRef(srefs, existExcept)
		// check new snapshot has only 3 refs, the snapshot and the 2 file blocks from our file
		require.Equal(t, 1+len(fs1.FS.root.children["f1"].(*FileNode).blocks), len(refs[srefs[0]]))

		// Get latest snapshot info for fs1
		fs1Infos, err := fs1.FS.snapshotUpdater.Get(fs1.FS.ctx)
		require.NoError(t, err)
		require.NotEmpty(t, fs1Infos)
		// Get snapshot that was just created for fs1
		fs1Snap, err := GetSnapshot(fs1.FS.ctx, store, blocks.HashFromSlice(fs1Infos[0].Hash))
		require.NoError(t, err)

		// Create fs2 with snapshot from fs1
		// q.Q(fs1Snap)
		fs2, err := NewTestFS(FromSnapshot(fs1Snap), InitializeEntries(false), WithStore(store))
		require.NoError(t, err)

		// Check refRoots that exist are: {fs1, fs2}: .ref, .updaterRef .snapshot.Ref
		existExcept = refs.ExistExcept(fs1.FS.ref, fs1.FS.snapshotUpdater.updater.Ref, fs2.FS.ref, fs2.FS.snapshotUpdater.updater.Ref)
		require.Equal(t, 2, len(existExcept)) // fs1.snapshot.ref, fs2.snapshot.ref

		// Check snapshot is OK, f1 which we created in fs1 exists in fs2 too
		fi, err := fs2.Stat("/f1")
		require.NoError(t, err)
		require.Equal(t, "f1", fi.Name())

		// Destroy fs2
		require.Nil(t, fs2.FS.destroy())
		fs2.Done()

		// Check fs1 snapshot refRoot still exists
		existExcept = refs.ExistExcept(fs1.FS.ref, fs1.FS.snapshotUpdater.updater.Ref)
		require.Equal(t, 1, len(existExcept))

		fs1.Done()
		require.Nil(t, fs1.FS.destroy())

		// Should be empty
		existExcept = refs.ExistExcept()
		require.Equal(t, 0, len(existExcept))
	})
	t.Run("Updater", func(t *testing.T) {
		fs, err := NewTestFS(FromPaths("/"))
		require.NoError(t, err)
		storetest := fs.FS.store.(blocks.StoreTest)
		refs, err := storetest.Refs()
		require.NoError(t, err)

		require.Empty(t, refs.ExistExcept(fs.FS.ref, fs.FS.snapshotUpdater.updater.Ref))

		err = fs.WriteFile("/file", blocks.TestData("/file", 0, 7*1024*1024), 0755)
		require.NoError(t, err)
		err = fs.FS.save(latestRetentionGroup)
		require.NoError(t, err)

		refs, err = storetest.Refs()
		require.NoError(t, err)
		require.Equal(t, 1, len(refs[fs.FS.snapshotUpdater.updater.Ref]))

		err = fs.FS.snapshotUpdater.updater.Delete(fs.FS.ctx)
		require.NoError(t, err)

		refs, err = storetest.Refs()
		require.NoError(t, err)
		require.Empty(t, len(refs[fs.FS.snapshotUpdater.updater.Ref]))

		fs.Done()
	})
}

func addSnapshotRef(existingRefs []blocks.ID, newRefs []blocks.ID) []blocks.ID {
	for _, nr := range newRefs {
		var skip bool
		for _, er := range existingRefs {
			if er == nr {
				skip = true
				break
			}
		}
		if skip {
			continue
		}
		return append(existingRefs, nr)
	}
	return nil
}

func countBlocks(t *testing.T, store blocks.Store) (numBlocks int) {
	lister, err := store.(blocks.StoreTest).List()
	require.NoError(t, err)
	for lister.Next(10) {
		require.Nil(t, lister.Err())
		numBlocks += len(lister.List())
	}
	return
}

func TestFSDestroy(t *testing.T) {
	fs, err := NewTestFS(Readonly(true), FromPaths("/", "/file1", "/file2", "/file3"))
	require.NoError(t, err)
	defer fs.Done()
	require.NotEqual(t, 0, countBlocks(t, fs.FS.store))

	err = fs.FS.destroy()
	require.NoError(t, err)

	require.Equal(t, 0, countBlocks(t, fs.FS.store))
}

func TestFSClone(t *testing.T) {
	testClonedBlocksEqual := func(t *testing.T, source *FS, store blocks.Store) {
		sourceInfos, err := source.snapshotUpdater.Get(source.ctx)
		require.NoError(t, err)
		require.NotEmpty(t, sourceInfos)
		sourceInfo := sourceInfos[0]

		cloned, err := source.clone(store)
		require.NoError(t, err)
		defer cloned.close()

		clonedInfos, err := cloned.snapshotUpdater.Get(cloned.ctx)
		require.NoError(t, err)
		require.NotEmpty(t, clonedInfos)
		clonedInfo := clonedInfos[0]

		require.NotEqual(t, sourceInfo, clonedInfo)
		require.Equal(t, len(sourceInfos), len(clonedInfos))

		sourceSnap, err := GetSnapshot(defaultCtx, source.store, blocks.HashFromSlice(sourceInfo.Hash))
		require.NoError(t, err)
		clonedSnap, err := GetSnapshot(defaultCtx, cloned.store, blocks.HashFromSlice(clonedInfo.Hash))
		require.NoError(t, err)

		for _, ce := range clonedSnap.Entries {
			for _, clonedBlock := range ce.Blocks {
				exists, err := cloned.store.Has(defaultCtx, blocks.HashFromSlice(clonedBlock.Hash))
				require.NoError(t, err)
				require.True(t, exists)
			}

			for _, se := range sourceSnap.Entries {
				if se.Path != ce.Path {
					continue
				}
				require.Equal(t, se.Blocks, ce.Blocks)
			}
		}

		sourceNumBlocks := countBlocks(t, source.store)
		clonedNumBlocks := countBlocks(t, cloned.store)
		require.Equal(t, sourceNumBlocks, clonedNumBlocks)

		require.NotEqual(t, sourceSnap.Meta.Ref, clonedSnap.Meta.Ref)
		require.NotEqual(t, source.snapshotUpdater.updater.Ref, cloned.snapshotUpdater.updater.Ref)
	}
	t.Run("SameStore", func(t *testing.T) {
		source, err := NewTestFS(Readonly(true), FromPaths("/", "/file1", "/file2", "/file3"))
		require.NoError(t, err)
		defer source.Done()

		testClonedBlocksEqual(t, source.FS, nil)
	})
	t.Run("DifferentStore", func(t *testing.T) {
		sourceStore := blocks.NewMemoryStore("")
		clonedStore := blocks.NewMemoryStore("")

		source, err := NewTestFS(Readonly(true), FromPaths("/", "/file1", "/file2", "/file3"), WithStore(sourceStore))
		require.NoError(t, err)
		defer source.Done()

		testClonedBlocksEqual(t, source.FS, clonedStore)
	})
}

func TestFSMigrateStore(t *testing.T) {
	var oldStore blocks.Store = blocks.NewMemoryStore("")

	fs, err := NewTestFS(Readonly(true), FromPaths("/", "/file1", "/file2", "/file3"), WithStore(oldStore))
	require.NoError(t, err)
	defer fs.Done()

	countBefore := countBlocks(t, oldStore)

	var newStore blocks.Store = blocks.NewMemoryStore("")
	err = fs.FS.migrateStore(newStore)
	require.NoError(t, err)

	// fs store should be the new store
	require.Equal(t, fs.FS.store, newStore)

	// new store block count should be the same as the old store block count
	countAfter := countBlocks(t, fs.FS.store)
	require.Equal(t, countBefore, countAfter)

	// old store should be empty
	require.Equal(t, 0, countBlocks(t, oldStore))
}

func TestFSReadonly(t *testing.T) {
	t.Fatalf("test unimplemented")
}

func TestFSVerify(t *testing.T) {
	testCases := []struct {
		desc         string
		paths        []string
		runBefore    func(*testing.T, *TestFS)
		checkHashes  bool
		allSnapshots bool
		verify       func(*testing.T, chan verifyResult)
	}{
		{
			desc:         "EmptyFS",
			paths:        []string{"/"},
			checkHashes:  false,
			allSnapshots: false,
			runBefore:    nil,
			verify: func(t *testing.T, replies chan verifyResult) {
				var got []verifyResult
				for reply := range replies {
					got = append(got, reply)
				}
				require.Len(t, got, 1)
				require.Equal(t, errVerifyEmptyFS, got[0].Err)
			},
		},
		{
			desc:         "Default",
			paths:        []string{"/", "/file"},
			checkHashes:  false,
			allSnapshots: false,
			runBefore:    nil,
			verify: func(t *testing.T, replies chan verifyResult) {
				var got []verifyResult
				for reply := range replies {
					got = append(got, reply)
				}
				// Need to get 1 reply for the snapshot block, and 1 reply for the file block
				require.Len(t, got, 2)
				for _, g := range got {
					require.NoError(t, g.Err)
					require.NotZero(t, g.Message)
					require.NotZero(t, g.Percent)
					require.NotZero(t, g.ETA)
				}
			},
		},
		{
			desc:         "CheckHashes",
			paths:        []string{"/", "/file"},
			checkHashes:  true,
			allSnapshots: false,
			runBefore:    nil,
			verify: func(t *testing.T, replies chan verifyResult) {
				var got []verifyResult
				for reply := range replies {
					got = append(got, reply)
				}
				// Need to get 1 reply for the snapshot block, and 1 reply for the file block
				require.Len(t, got, 2)
				for _, g := range got {
					require.NoError(t, g.Err)
					require.NotZero(t, g.Message)
					require.NotZero(t, g.Percent)
					require.NotZero(t, g.ETA)
				}
			},
		},
		{
			desc:         "AllSnapshots",
			paths:        []string{"/", "/file"},
			checkHashes:  false,
			allSnapshots: true,
			runBefore: func(t *testing.T, fs *TestFS) {
				// Make new snapshot
				f, err := fs.OpenFile("/file", os.O_WRONLY, 0755)
				require.NoError(t, err)
				defer f.Close()
				_, err = f.Write([]byte("trigger change"))
				require.NoError(t, err)
				require.NoError(t, fs.FS.save(manualRetentionGroup))
			},
			verify: func(t *testing.T, replies chan verifyResult) {
				var got []verifyResult
				for reply := range replies {
					got = append(got, reply)
				}
				// Need to get 2 replies for the snapshot blocks, and 2 replies for the file blocks
				require.Len(t, got, 4)
				for _, g := range got {
					require.NoError(t, g.Err)
					require.NotZero(t, g.Message)
					require.NotZero(t, g.Percent)
					require.NotZero(t, g.ETA)
				}
			},
		},
		{
			desc:         "MissingSnapshotBlock",
			paths:        []string{"/", "/file"},
			checkHashes:  false,
			allSnapshots: false,
			runBefore: func(t *testing.T, fs *TestFS) {
				infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
				require.NoError(t, err)
				require.Len(t, infos, 1)
				hash := blocks.HashFromSlice(infos[0].Hash)
				ref := blocks.IDFromByteSlice(fs.FS.snapshot.Meta.Ref)

				require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, ref, []blocks.Hash{hash}))
			},
			verify: func(t *testing.T, replies chan verifyResult) {
				var got []verifyResult
				for reply := range replies {
					got = append(got, reply)
				}
				// Need to get 1 error for the snapshot block
				require.Len(t, got, 1)
				g := got[0]
				require.True(t, errors.Is(g.Err, blocks.ErrBlockNotFound))
			},
		},
		{
			desc:         "MissingFileBlock",
			paths:        []string{"/", "/file"},
			checkHashes:  false,
			allSnapshots: false,
			runBefore: func(t *testing.T, fs *TestFS) {
				infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
				require.NoError(t, err)
				require.Len(t, infos, 1)
				ref := blocks.IDFromByteSlice(fs.FS.snapshot.Meta.Ref)

				for _, e := range fs.FS.snapshot.Entries {
					if e.Path != "/file" {
						continue
					}

					hash := blocks.HashFromSlice(e.Blocks[0].Hash)

					require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, ref, []blocks.Hash{hash}))
					require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, fs.FS.ref, []blocks.Hash{hash}))
					break
				}
			},
			verify: func(t *testing.T, replies chan verifyResult) {
				var got []verifyResult
				for reply := range replies {
					got = append(got, reply)
				}
				// Need to get 1 error for the file block
				for _, g := range got {
					if g.Err != nil {
						require.True(t, errors.Is(g.Err, blocks.ErrBlockNotFound))
					}
				}
			},
		},
		{
			desc:         "CorruptSnapshotBlock",
			paths:        []string{"/", "/file"},
			checkHashes:  true,
			allSnapshots: false,
			runBefore: func(t *testing.T, fs *TestFS) {
				infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
				require.NoError(t, err)
				require.Len(t, infos, 1)
				hash := blocks.HashFromSlice(infos[0].Hash)
				ref := blocks.IDFromByteSlice(fs.FS.snapshot.Meta.Ref)

				// Make a copy of the current snapshot, and modify somthing.
				corruptSnap := *fs.FS.snapshot
				corruptSnap.Meta.Tag = "corrupt"
				// Get the encoded "corrupted" snapshot data.
				corruptData, err := corruptSnap.data()
				require.NoError(t, err)

				// Remove the current (valid) snapshot block from the store.
				require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, ref, []blocks.Hash{hash}))
				// Add the corrupt snapshot block to the same hash.
				require.NoError(t, fs.FS.store.Put(defaultCtx, ref, hash, bytes.NewReader(corruptData)))
			},
			verify: func(t *testing.T, replies chan verifyResult) {
				var got []verifyResult
				for reply := range replies {
					got = append(got, reply)
				}

				// Need to get 1 error for the snapshot hash mismatch
				var hasHashMismatch bool
				for _, g := range got {
					var hashErr hashMismatchError
					if errors.As(g.Err, &hashErr) {
						hasHashMismatch = true
						break
					}
				}
				require.True(t, hasHashMismatch)
			},
		},
		{
			desc:         "CorruptFileData",
			paths:        []string{"/", "/file"},
			checkHashes:  true,
			allSnapshots: false,
			runBefore: func(t *testing.T, fs *TestFS) {
				infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
				require.NoError(t, err)
				require.Len(t, infos, 1)
				ref := blocks.IDFromByteSlice(fs.FS.snapshot.Meta.Ref)

				for _, e := range fs.FS.snapshot.Entries {
					if e.Path != "/file" {
						continue
					}

					// Remove first file block from store, and re-add to the same hash, but with different data.
					hash := blocks.HashFromSlice(e.Blocks[0].Hash)
					require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, ref, []blocks.Hash{hash}))
					require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, fs.FS.ref, []blocks.Hash{hash}))
					require.NoError(t, fs.FS.store.Put(defaultCtx, ref, hash, bytes.NewReader([]byte("corrupt data"))))
				}
			},
			verify: func(t *testing.T, replies chan verifyResult) {
				var got []verifyResult
				for reply := range replies {
					got = append(got, reply)
				}

				// Check that we have at least one hash mismatch error
				var hasHashMismatch bool
				for _, g := range got {
					var hashErr hashMismatchError
					if errors.As(g.Err, &hashErr) {
						hasHashMismatch = true
						break
					}
				}
				require.True(t, hasHashMismatch)
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			fs, err := NewTestFS(FromPaths(tc.paths...))
			require.NoError(t, err)
			defer fs.Done()

			if tc.runBefore != nil {
				tc.runBefore(t, fs)
			}

			replies := fs.FS.verify(tc.checkHashes, tc.allSnapshots)
			tc.verify(t, replies)
		})
	}
}

func BenchmarkWriteAndCommit(b *testing.B) {
	benchSizes := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "Size200MB", size: 200 * 1024 * 1024},
		{desc: "Size400MB", size: 400 * 1024 * 1024, noskip: true},
	}
	benchMaxCacheSizes := []struct {
		desc         string
		maxCacheSize int64
		noskip       bool
	}{
		{desc: "MaxCacheSize2MB", maxCacheSize: 2 * 1024 * 1024},
		{desc: "MaxCacheSize4MB", maxCacheSize: 4 * 1024 * 1024},
		{desc: "MaxCacheSize8MB", maxCacheSize: 8 * 1024 * 1024},
		{desc: "MaxCacheSize16MB", maxCacheSize: 16 * 1024 * 1024},
		{desc: "MaxCacheSize32MB", maxCacheSize: 32 * 1024 * 1024},
		{desc: "MaxCacheSize64MB", maxCacheSize: 64 * 1024 * 1024},
		{desc: "MaxCacheSize128MB", maxCacheSize: 128 * 1024 * 1024, noskip: true},
		{desc: "MaxCacheSize256MB", maxCacheSize: 256 * 1024 * 1024},
	}

	for _, bS := range benchSizes {
		for _, bMCS := range benchMaxCacheSizes {
			fs, err := NewTestFS(
				FromPaths("/", "/file"),
				CacheMeasureCommit(bMCS.maxCacheSize),
			)
			if err != nil {
				panic(err)
			}
			defer fs.Done()
			node := fs.FS.root.children["file"].(*FileNode)
			data := bytes.Repeat([]byte{'f'}, 4096)

			b.Run(bS.desc+"_"+bMCS.desc, func(b *testing.B) {
				if testing.Short() && (!bS.noskip || !bMCS.noskip) {
					b.SkipNow()
				}
				b.SetBytes(bS.size)

				for i := 0; i < b.N; i++ {
					req := &fuse.WriteRequest{}
					resp := &fuse.WriteResponse{}
					for o := int64(0); o <= bS.size; o += 4096 {
						req.Offset = o
						req.Data = data

						node.Write(context.Background(), req, resp)
					}
				}
			})
		}
	}
}

func BenchmarkSnapshot(b *testing.B) {
	fs, _ := NewTestFS(
		FromSnapshot(&Snapshot{
			Entries: []*Entry{
				&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
				&Entry{Path: "/foo", Meta: &EntryMeta{Size: 2 * 1024 * 1024}},
				&Entry{Path: "/diz/bar/baz", Meta: &EntryMeta{Size: 2 * 1024 * 1024}},
				&Entry{Path: "/diz/", Meta: &EntryMeta{Size: 4096}},
				&Entry{Path: "/diz/bar/", Meta: &EntryMeta{Size: 2 * 1024 * 1024}},
				&Entry{Path: "/diz/bar/kix", Meta: &EntryMeta{Size: 4096}},
				&Entry{Path: "/diz/bar/qux/", Meta: &EntryMeta{Size: 4096}},
				&Entry{Path: "/diz/bar/qux/foobie", Meta: &EntryMeta{Size: 1 * 1024 * 1024}},
			},
		}),
	)
	defer fs.Done()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		fs.FS.makeSnapshot(defaultCtx)
	}
}

func BenchmarkCommitSnapshotCache(b *testing.B) {
	benchCases := []struct {
		desc     string
		numFiles int
		fileSize int64
		noskip   bool
	}{
		{desc: "1Files_10MB", numFiles: 1, fileSize: 10 * 1024 * 1024},
		{desc: "2Files_10MB", numFiles: 2, fileSize: 10 * 1024 * 1024},
		{desc: "4Files_10MB", numFiles: 4, fileSize: 10 * 1024 * 1024},
		{desc: "8Files_10MB", numFiles: 8, fileSize: 10 * 1024 * 1024},
		{desc: "50Files_10MB", numFiles: 50, fileSize: 10 * 1024 * 1024},
		{desc: "100Files_10MB", numFiles: 100, fileSize: 10 * 1024 * 1024, noskip: true},
		{desc: "10Files_100MB", numFiles: 10, fileSize: 100 * 1024 * 1024},
	}

	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			entries := []*Entry{&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}}}
			// create numFiles files, of fileSize size
			for i := 0; i < bench.numFiles; i++ {
				entries = append(entries, &Entry{
					Path: fmt.Sprintf("/%v", i),
					Meta: &EntryMeta{Size: 0},
				})
			}
			fs, _ := NewTestFS(FromSnapshot(&Snapshot{Entries: entries}))
			defer fs.Done()

			for i := 0; i < bench.numFiles; i++ {
				f, _ := fs.OpenFile(fmt.Sprintf("/%v", i), os.O_WRONLY, 0000)

				r := bytes.NewReader(blocks.TestData("j", 0, bench.fileSize))
				io.Copy(f, r)
				f.Close()
			}

			nodeBefore := *fs.FS.root.children["0"].(*FileNode)
			metaBefore := *nodeBefore.meta

			b.SetBytes(bench.fileSize * int64(bench.numFiles))
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				for i := 0; i < bench.numFiles; i++ {
					nodeBench := nodeBefore
					nodeBench.meta = &metaBefore
					fs.FS.root.children[fmt.Sprintf("%v", i)] = &nodeBench
				}

				fs.FS.makeSnapshot(defaultCtx)
			}
		})
	}
}

func BenchmarkAttr(b *testing.B) {
	fs, _ := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()

	node := fs.FS.root.children["file"]

	for i := 0; i < b.N; i++ {
		var attr fuse.Attr
		node.Attr(context.Background(), &attr)
	}
}

func BenchmarkSetattrMode(b *testing.B) {
	fs, _ := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()

	node := fs.FS.root.children["file"].(*FileNode)

	req := &fuse.SetattrRequest{}
	req.Flags = uint32(fuse.SetattrMode)
	req.Mode = 0755
	resp := &fuse.SetattrResponse{}
	for i := 0; i < b.N; i++ {
		node.Setattr(context.Background(), req, resp)
	}
}

func BenchmarkSetattrUid(b *testing.B) {
	fs, _ := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()

	node := fs.FS.root.children["file"].(*FileNode)

	req := &fuse.SetattrRequest{}
	req.Flags = uint32(fuse.SetattrUid)
	req.Uid = 1001

	b.ResetTimer()

	resp := &fuse.SetattrResponse{}
	for i := 0; i < b.N; i++ {
		node.Setattr(context.Background(), req, resp)
	}
}

func BenchmarkSetattrGid(b *testing.B) {
	fs, _ := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()

	node := fs.FS.root.children["file"].(*FileNode)

	req := &fuse.SetattrRequest{}
	req.Flags = uint32(fuse.SetattrGid)
	req.Uid = 101
	resp := &fuse.SetattrResponse{}
	for i := 0; i < b.N; i++ {
		node.Setattr(context.Background(), req, resp)
	}
}

func BenchmarkSetattrSize(b *testing.B) {
	fs, _ := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/file", Meta: &EntryMeta{Size: 9 * 1024 * 1024}},
		},
	}))
	defer fs.Done()

	node := fs.FS.root.children["file"].(*FileNode)

	req := &fuse.SetattrRequest{}
	req.Flags = uint32(fuse.SetattrSize)
	req.Size = 7 * 1024 * 1024
	resp := &fuse.SetattrResponse{}
	for i := 0; i < b.N; i++ {
		node.Setattr(context.Background(), req, resp)
	}
}

func BenchmarkSymlink(b *testing.B) {
	fs, _ := NewTestFS()
	defer fs.Done()

	node := fs.FS.root

	b.ResetTimer()

	req := &fuse.SymlinkRequest{
		NewName: "symlink",
		Target:  "/some/target",
	}
	for i := 0; i < b.N; i++ {
		node.Symlink(context.Background(), req)
	}
}

func BenchmarkReadlink(b *testing.B) {
	fs, _ := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()
	fs.Symlink("/file", "/symlink")

	node := fs.FS.root.children["symlink"].(*SymlinkNode)

	req := &fuse.ReadlinkRequest{}
	for i := 0; i < b.N; i++ {
		node.Readlink(context.Background(), req)
	}
}
