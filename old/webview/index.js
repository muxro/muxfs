var remotefs = require('./proto/remotefs_pb')
var Hls = require('hls.js')

if (Hls.isSupported()) {
  var video = document.querySelector('video');
  var hls = new Hls();
  hls.loadSource('http://localhost:8000/lol.m3u8');
  hls.attachMedia(video);
  hls.on(Hls.Events.MANIFEST_PARSED,function() {
    video.play();
  });
  console.log("yes")

}

function indexInParent(node) {
    var children = node.parentNode.childNodes;
    var num = 0;
    for (var i=0; i<children.length; i++) {
         if (children[i]==node) return num;
         if (children[i].nodeType==1) num++;
    }
    return -1;
}
function bufferToHex (buffer) {
  return Array
    .from(new Uint8Array(buffer))
    .map(b => b.toString (16).padStart (2, "0"))
    .join("");
}

var snap;

function getSnapshot(hash) {
  var xhttp = new XMLHttpRequest()
  xhttp.responseType = 'arraybuffer'

  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        snap = remotefs.Snapshot.deserializeBinary(this.response)
        window.snap = snap

        snap.getEntriesList().forEach(e => {
          var li = document.createElement('li');
          li.appendChild(document.createTextNode(e.getPath()))
          li.onclick = function(e) {
            const idx = indexInParent(e.target)

            getFile(snap.getEntriesList()[idx])
          }

          document.querySelector("#entries-list").appendChild(li)
        })
      }
  }

  xhttp.open("GET", `http://localhost:8888/${hash}`, true)
  xhttp.send()
}

function downloadBlock(hash, oncomplete, onprogress) {
  var xhttp = new XMLHttpRequest()
  xhttp.responseType = 'arraybuffer'
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      oncomplete(this)
    }
  }
  xhttp.onprogress = onprogress
  xhttp.open("GET", `http://localhost:8888/${hash}`, true)
  xhttp.send()
}


function getFile(entry) {
  const blocks = entry.getBlocksList()
  for (const b of blocks) {
    console.log(bufferToHex(b.getHash()));
    
  }
  

  var video = document.querySelector('video');
  var mediaSource = new MediaSource();
  video.src = URL.createObjectURL(mediaSource);
  var mimeCodec = 'video/mp4; codecs="avc1.4D4028"';

  var sourceBuffer
  mediaSource.addEventListener('sourceopen', function() {
    var mediaSource = this;
    sourceBuffer = mediaSource.addSourceBuffer(mimeCodec);
  });

  var i = 0
  var downloadNextBlock = function(e) {
    sourceBuffer.addEventListener('updateend', function() {
      try {
        video.play();
      } catch (ex) {
        console.log('ex:', ex);
        
      }
    });
    sourceBuffer.appendBuffer(e.response);

    i++
    if (i == blocks.length) {
      console.log("finished")
      mediaSource.endOfStream();
      return
    }

    var block = blocks[i]
    var hash = bufferToHex(block.getHash())

    window.setTimeout(function() {
      downloadBlock(hash, downloadNextBlock)
    }, 0)
  }

  var firstBlock = blocks[0]
  var firstHash = bufferToHex(firstBlock.getHash())
  downloadBlock(firstHash, downloadNextBlock)
}

document.querySelector("#snapshot-input").value = '35f7853d453faced88361ead13fa958cf57b01a796baeaeab1d488dcb1343bb2'

document.querySelector("#snapshot-input").onkeypress = function(e){
  if (!e) e = window.event;
  var keyCode = e.keyCode || e.which;
  if (keyCode == '13'){
    // Enter pressed
    getSnapshot(e.target.value)
    return false;
  }
}