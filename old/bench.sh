#! /usr/bin/env nix-shell
#! nix-shell -i bash -p fio

fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename="$1" --bs=4k --iodepth=64 --size=4G --readwrite=read