//+build race

package remotefs

import (
	"context"
	"testing"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
)

func TestRaceSymlinkAttr(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()
	require.NoError(t, err)

	root := fs.FS.root
	root.Symlink(context.Background(),
		&fuse.SymlinkRequest{
			NewName: "symlink", Target: "file",
		},
	)

	n, ok := root.children["symlink"].(*SymlinkNode)
	require.True(t, ok)

	testRace(func() {
		req := &fuse.SetattrRequest{}
		req.Valid = fuse.SetattrUid
		req.Uid = 1004
		resp := &fuse.SetattrResponse{}
		n.Setattr(context.Background(), req, resp)
	}, func() {
		var attr fuse.Attr
		n.Attr(context.Background(), &attr)
	})
}
