#! /bin/sh

function doBenchmark() {
    go test -short -benchmem -bench "$1" -run "nothing" 2>&1 > "bench/benchcmp_$2"
}

doBenchmark $1 "1"
while true; do
    doBenchmark $1 "2"
    benchcmp bench/benchcmp_1 bench/benchcmp_2 | grep -A1 'ns/op' | grep "%" >> bench/benchcmp_results
    mv bench/benchcmp_2 bench/benchcmp_1
    sleep 1
done

