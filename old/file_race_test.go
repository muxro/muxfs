//+build race

package remotefs

import (
	"context"
	"testing"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
)

func TestRaceFileAttr(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()
	require.NoError(t, err)

	n, ok := fs.FS.root.children["file"].(*FileNode)
	require.True(t, ok)

	testRace(func() {
		req := &fuse.SetattrRequest{}
		req.Valid = fuse.SetattrMode
		req.Mode = 0755
		resp := &fuse.SetattrResponse{}
		n.Setattr(context.Background(), req, resp)
	}, func() {
		var attr fuse.Attr
		n.Attr(context.Background(), &attr)
	})
}

func TestRaceReadWrite(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()
	require.NoError(t, err)

	n, ok := fs.FS.root.children["file"].(*FileNode)
	require.True(t, ok)

	data := make([]byte, 1000)

	testRace(func() {
		req := &fuse.WriteRequest{Offset: 0, Data: data}
		resp := &fuse.WriteResponse{}
		n.Write(context.Background(), req, resp)
	}, func() {
		req := &fuse.ReadRequest{Offset: 0, Size: 1000}
		resp := &fuse.ReadResponse{}
		n.Read(context.Background(), req, resp)
	})
}

func TestRaceTruncate(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()
	require.NoError(t, err)

	n, ok := fs.FS.root.children["file"].(*FileNode)
	require.True(t, ok)

	testRace(func() {
		req := &fuse.SetattrRequest{}
		req.Valid = fuse.SetattrSize
		req.Size = 1000
		resp := &fuse.SetattrResponse{}
		n.Setattr(context.Background(), req, resp)
	}, func() {
		req := &fuse.ReadRequest{Offset: 0, Size: 1000}
		resp := &fuse.ReadResponse{}
		n.Read(context.Background(), req, resp)
	})
}

func TestRaceCommit(t *testing.T) {
	fs, err := NewTestFS(FromPaths("/", "/file"))
	defer fs.Done()
	require.NoError(t, err)

	n, ok := fs.FS.root.children["file"].(*FileNode)
	require.True(t, ok)

	req := &fuse.WriteRequest{Offset: 0, Data: make([]byte, 7*1024*1024)}
	resp := &fuse.WriteResponse{}
	n.Write(context.Background(), req, resp)

	data := make([]byte, 1000)

	testRaceShort(func() {
		req := &fuse.WriteRequest{Offset: 0, Data: data}
		resp := &fuse.WriteResponse{}
		n.Write(context.Background(), req, resp)
	}, func() {
		n.Commit(defaultCtx)
	})
}
