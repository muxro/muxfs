package remotefs

import (
	"bytes"
	"errors"
	fmt "fmt"
	"io"
	"io/ioutil"
	"os"
	"remotefs/blocks"
	"sync"
	"sync/atomic"
	"time"

	"bazil.org/fuse"
	bazilFS "bazil.org/fuse/fs"
	"golang.org/x/net/context"
)

var commitBlockPool = sync.Pool{
	New: func() interface{} {
		return make([]byte, blocks.MaxChunkSize)
	},
}

type FileNode struct {
	parent *DirNode
	meta   *EntryMeta

	blocks       []Block
	blocksSize   int64
	blockDataEnd int64
	cache        *Cache

	mu sync.RWMutex

	openFds int64

	FS *FS
}

type blockReader struct {
	ctx    context.Context
	store  blocks.Store
	blocks []Block
	size   int64
}

func newBlockReader(ctx context.Context, store blocks.Store, blocks []Block, size int64) *blockReader {
	return &blockReader{
		ctx:    ctx,
		store:  store,
		blocks: blocks,
		size:   size,
	}
}

func (n *FileNode) Open(ctx context.Context, req *fuse.OpenRequest, resp *fuse.OpenResponse) (bazilFS.Handle, error) {
	atomic.AddInt64(&n.openFds, 1)
	return n, nil
}

func (n *FileNode) Release(ctx context.Context, req *fuse.ReleaseRequest) error {
	atomic.AddInt64(&n.openFds, -1)
	return nil
}

func (n *FileNode) Attr(ctx context.Context, a *fuse.Attr) error {
	n.mu.RLock()
	defer n.mu.RUnlock()

	meta := n.meta

	a.Size = uint64(meta.Size)
	a.Mode = os.FileMode(meta.Mode)
	a.Mtime = time.Unix(meta.UpdatedAt, 0)
	a.Uid = meta.Uid
	a.Gid = meta.Gid

	return nil
}

// TODO: n.FS.changed() should only be called if the attributes are actually changed (not called in the case of an error)
func (n *FileNode) Setattr(ctx context.Context, req *fuse.SetattrRequest, resp *fuse.SetattrResponse) error {
	// rlock FS
	n.FS.commitLock.RLock()
	defer n.FS.commitLock.RUnlock()
	// lock file
	n.mu.Lock()
	defer n.FS.changed()
	defer n.mu.Unlock()

	if req.Valid.Size() {
		size := int64(req.Size)

		err := n.truncate(size)
		if err != nil {
			return err
		}
		n.meta.Size = size
		resp.Attr.Size = req.Size
	}
	if req.Valid.Mode() {
		n.meta.Mode = uint32(req.Mode)
		resp.Attr.Mode = req.Mode
	}
	if req.Valid.Uid() {
		n.meta.Uid = req.Uid
		resp.Attr.Uid = req.Uid
	}
	if req.Valid.Gid() {
		n.meta.Gid = req.Gid
		resp.Attr.Gid = req.Gid
	}

	return nil
}

// truncate iterates blocks in reverse, and as soon as we find a block with an
// offset STRICTLY GREATER THAN the new size,
// it means that's the last block and we can delete everything after its index.
// it's important that if the offset is strictly equal to the block,
// it shouldn't be considered, because we want that block to be removed as well.
// there's a special case when req.Size == 0, where we remove all the blocks
func (n *FileNode) truncate(size int64) error {
	if size < 0 {
		return errors.New("truncate limit cannot be less than 0")
	}

	n.cache.Truncate(size)

	if size < n.blockDataEnd {
		n.blockDataEnd = size
	}

	if size == 0 {
		n.blocks = nil
		n.blocksSize = 0

		return nil
	}

	var lastBlockSize int64
	// TODO: @performance make this a binary search
	for i := len(n.blocks) - 1; i >= 0; i-- {
		if size > n.blocks[i].Offset {
			// save the size of the last block, to be used in the new size calculation below
			if i == len(n.blocks)-1 {
				lastBlockSize = n.meta.Size - n.blocks[i].Offset
			} else {
				lastBlockSize = n.blocks[i+1].Offset - n.blocks[i].Offset
			}
			n.blocks = n.blocks[:i+1]
			break
		}
	}

	// calculate new blocksSize
	var totalSize int64
	for i, b := range n.blocks {
		// if we are at the last block, add the last block size
		if i == len(n.blocks)-1 {
			totalSize += lastBlockSize
		} else {
			nextBlock := n.blocks[i+1]
			totalSize += nextBlock.Offset - b.Offset
		}
	}
	n.blocksSize = totalSize

	return nil
}

func (n *FileNode) Read(ctx context.Context, req *fuse.ReadRequest, resp *fuse.ReadResponse) error {
	if req.Size <= 0 {
		return nil
	}

	data := make([]byte, req.Size)
	_, err := n.ReadAt(data, req.Offset)
	if err != nil && err != io.EOF {
		return err
	}

	resp.Data = data

	return nil
}

func (n *FileNode) readAtNoLock(p []byte, off int64) (read int, err error) {
	// nothing to read
	if len(p) == 0 {
		return 0, nil
	}

	// read past file size returns EOF
	if off >= n.meta.Size {
		return 0, io.EOF
	}

	br := newBlockReader(n.FS.ctx, n.FS.store, n.blocks, n.blockDataEnd)
	read, err = br.ReadAt(p, off)
	if err != nil {
		return read, err
	}

	// patch cache on top and determine how much memory has been patched
	patched := n.cache.PatchOnto(p, off)
	if read < patched {
		read = patched
	}

	// if we read less bytes than requested, or if we've read all the data,
	// signal that we've reached EOF
	if read < len(p) || off+int64(read) >= int64(n.meta.Size) {
		return read, io.EOF
	}

	return
}

func (n *FileNode) ReadAt(p []byte, off int64) (read int, err error) {
	n.mu.RLock()
	defer n.mu.RUnlock()

	return n.readAtNoLock(p, off)
}

func (n *FileNode) Write(ctx context.Context, req *fuse.WriteRequest, resp *fuse.WriteResponse) error {
	wrote, err := n.WriteAt(req.Data, req.Offset)
	if err != nil {
		return err
	}

	resp.Size = wrote

	if n.FS.activityTimeout != nil {
		n.FS.activityTimeout.tick()
	}

	return nil
}

func (n *FileNode) WriteAt(b []byte, off int64) (int, error) {
	n.FS.commitLock.RLock()
	defer n.FS.commitLock.RUnlock()
	defer n.FS.changed()

	n.mu.Lock()
	defer n.FS.changed()
	defer n.mu.Unlock()

	var br io.ReaderAt
	if off <= n.blockDataEnd {
		br = newBlockReader(n.FS.ctx, n.FS.store, n.blocks, n.blockDataEnd)
	}

	err := n.cache.Add(b, off, br)
	if err != nil {
		return 0, err
	}

	sizeDifference := (off + int64(len(b))) - n.meta.Size
	// Write should only increase the size of the file
	if sizeDifference > 0 {
		n.meta.Size += sizeDifference
	}

	return len(b), nil
}

func (n *FileNode) Commit(ctx context.Context) error {
	n.mu.Lock()
	defer n.mu.Unlock()

	if n.cache.IsEmpty() {
		return n.commitBlocks()
	}

	return n.commitCache(ctx)
}

func (n *FileNode) commitBlocks() error {
	if n.blocksSize == n.blockDataEnd && n.meta.Size == n.blockDataEnd {
		return nil
	}
	// last block has a different size, we have to rechunk

	// get a reader that has only the data starting from the last block, up to filesize
	blockReader := newFileReader(n, 0)
	var lastOffset int64
	if len(n.blocks) > 0 {
		lastOffset = n.blocks[len(n.blocks)-1].Offset
	}
	// TODO: if this is too slow, refactor to use block.Skip
	_, err := io.CopyN(ioutil.Discard, blockReader, lastOffset)
	if err != nil {
		return err
	}

	// make new blocks from an io.LimitReader
	limit := int64(n.blocksSize) - lastOffset
	limitedBlockReader := io.LimitReader(blockReader, limit)

	var zeroesLimit int64
	if n.blocksSize < n.meta.Size {
		zeroesLimit = int64(n.meta.Size - n.blocksSize)
	}

	zeroesReader := io.LimitReader(repeatZeroes{}, zeroesLimit)
	multiReader := io.MultiReader(limitedBlockReader, zeroesReader)

	lastBlocks, lastBlockSize, err := n.FS.putBlocksFromReader(multiReader)
	if err != nil {
		return err
	}

	// need to add the offset of the last block to the offset of every new block
	// because this new set of blocks starts off with offset = 0
	for _, b := range lastBlocks {
		b.Offset += lastOffset
	}

	derefLastBlocks := derefBlocks(lastBlocks)
	if len(n.blocks) == 0 {
		n.blocks = append(n.blocks, Block{})
	}
	// replace last block
	n.blocks[len(n.blocks)-1] = derefLastBlocks[0]
	// append the rest of the blocks, if the exist
	if len(derefLastBlocks) > 1 {
		n.blocks = append(n.blocks, derefLastBlocks[1:]...)
	}

	// lastBlockSize must be obtained from the chunker, because we only keep block.Offset and block.Hash
	if len(n.blocks) > 0 {
		// Add the last block's offset, if we have at least one block
		n.blockDataEnd = n.blocks[len(n.blocks)-1].Offset
	}
	// Add the last block's size, if we have at least one block (otherwise it will be 0)
	n.blockDataEnd += lastBlockSize
	// blocksSize should be the same value
	n.blocksSize = n.blockDataEnd

	return nil
}

func (n *FileNode) commitCache(ctx context.Context) error {
	buf := commitBlockPool.Get().([]byte)
	var newBlocks []Block
	var offset int64

Outer:
	for i := 0; i < len(n.blocks); i++ {
		block := n.blocks[i]

		offset = block.Offset
		end := n.blockDataEnd
		if i < len(n.blocks)-1 {
			end = n.blocks[i+1].Offset - 1
		}
		if end >= n.blockDataEnd {
			break Outer
		}

		// no data here, just skip
		if !n.cache.HasDataIn(offset, end-1) {
			newBlocks = append(newBlocks, block)
			continue
		}

		fileReader := newFileReader(n, offset)
		chunker := blocks.NewChunker(fileReader)

		// start chunking
		for {
			chunk, err := chunker.Next(buf)
			if err != nil {
				if err == io.EOF {
					break Outer
				}
				return err
			}

			newBlock := Block{
				Offset: offset,
				Hash:   chunk.Hash[:],
			}

			offset += int64(chunk.Length)
			newBlocks = append(newBlocks, newBlock)

			// find a block starting at index i, with given offset and cache
			idx, ok := findBlock(n.blocks, i, newBlock.Offset, chunk.Hash)
			if ok {
				// we found an old block, we can just repeat from here
				i = idx
				break
			}

			// if we didnt already have this block, add to store
			err = n.FS.store.Put(ctx, n.FS.ref, chunk.Hash, bytes.NewReader(chunk.Data))
			if err != nil {
				return err
			}
			n.FS.blockHashesLock.Lock()
			n.FS.blockHashes = append(n.FS.blockHashes, chunk.Hash)
			n.FS.blockHashesLock.Unlock()
		}
	}

	// read from cache, unless there's some data left in the blocks
	cacheOffset := n.blockDataEnd
	if offset > cacheOffset {
		cacheOffset = offset
	}
	r := newReaderFromReaderAt(n.cache, cacheOffset)
	if offset < n.blockDataEnd {
		fileReader := newFileReader(n, offset)
		withBlocks := io.LimitReader(fileReader, n.blockDataEnd-offset)
		// read data with blocks first
		r = io.MultiReader(withBlocks, r)
	}

	chunker := blocks.NewChunker(r)

	var lastBlockSize int64
	// start chunking from cache
	for {
		chunk, err := chunker.Next(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		err = n.FS.store.Put(ctx, n.FS.ref, chunk.Hash, bytes.NewReader(chunk.Data))
		if err != nil {
			return err
		}
		n.FS.blockHashesLock.Lock()
		n.FS.blockHashes = append(n.FS.blockHashes, chunk.Hash)
		n.FS.blockHashesLock.Unlock()

		newBlock := Block{
			Offset: int64(chunk.Start) + offset,
			Hash:   chunk.Hash[:],
		}
		newBlocks = append(newBlocks, newBlock)
		lastBlockSize = int64(chunk.Length)
	}

	// Add chunk bytes back to pool
	commitBlockPool.Put(buf)

	// Update blocks
	n.blocks = newBlocks

	// lastBlockSize must be obtained from the chunker, because we only keep block.Offset and block.Hash
	if len(n.blocks) > 0 {
		// Add the last block's offset, if we have at least one block
		n.blockDataEnd = n.blocks[len(n.blocks)-1].Offset
	}
	// Add the last block's size, if we have at least one block (otherwise it will be 0)
	n.blockDataEnd += lastBlockSize
	// blocksSize should be the same value
	n.blocksSize = n.blockDataEnd

	// Clear cache
	n.cache.Clear()

	return nil
}

func findBlock(bs []Block, startIdx int, offset int64, hash blocks.Hash) (int, bool) {
	for i, b := range bs {
		if i < startIdx {
			continue
		}
		if b.Offset > offset {
			break
		}

		if bytes.Equal(b.Hash, hash[:]) {
			return i, true
		}
	}

	return 0, false
}

func (br *blockReader) ReadAt(p []byte, off int64) (n int, err error) {
	for i, block := range br.blocks {
		blockOff := off + int64(n)

		// Determine how much we need to read from current block
		var readLen int64
		if i < len(br.blocks)-1 {
			nextBlock := br.blocks[i+1]

			// TODO: @performance make this a binary search
			// blockOff is not in the current block
			if nextBlock.Offset <= blockOff {
				continue
			}

			readLen = nextBlock.Offset - blockOff
		} else {
			readLen = br.size - blockOff
		}

		// nothing left to read
		if readLen <= 0 {
			break
		}

		// get block from store
		reader, err := br.store.Get(br.ctx, blocks.ByteSliceToArr(block.Hash))
		if err != nil {
			return n, err
		}

		// blockOff is not at the start of the block, we need to skip some data
		if blockOff > block.Offset {
			err = reader.Skip(blockOff - block.Offset)
			if err != nil {
				return n, err
			}
		}

		// compute how much of this block we need to read
		readLimit := n + int(readLen)
		if readLimit > len(p) {
			readLimit = len(p)
		}

		rn, err := io.ReadFull(reader, p[n:readLimit])
		// TODO: P3 retry block if we got an unexpected EOF
		n += rn
		reader.Close()
		if err != nil {
			return n, err
		}

		if n == len(p) {
			break
		}
	}

	return
}

// TODO: Commit all files and snapshot
func (n *FileNode) Fsync(ctx context.Context, req *fuse.FsyncRequest) error {
	fmt.Println("fsync file")
	return nil
}
