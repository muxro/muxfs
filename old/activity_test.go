package remotefs

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestActivityNoLeak(t *testing.T) {
	fs, err := NewTestFS()
	require.NoError(t, err)
	defer fs.Done()
}
