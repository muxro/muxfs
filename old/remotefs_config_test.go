package remotefs

import (
	"bytes"
	"errors"
	fmt "fmt"
	"io/ioutil"
	"os"
	"reflect"
	"remotefs/blocks"
	"sort"
	"strings"

	"golang.org/x/net/context"
)

type TestFSConfig struct {
	Mountpoint   string
	Readonly     bool
	FromPaths    []string
	FromSnapshot *Snapshot

	WithStore blocks.Store

	ShouldSort              bool
	ShouldInitializeEntries bool

	HasActivityCommit     bool
	HasCacheMeasureCommit bool
	MaxCacheSize          int64

	SnapshotRetentionGroups []*snapshotRetentionGroup

	// Instance fields
	Snapshot        *Snapshot
	SnapshotRefRoot blocks.ID
	SnapshotHash    blocks.Hash
	Store           blocks.Store
}

type TestFSOption func(*TestFSConfig) error

// TODO: P3 this function is a mess, refactor
func NewTestFSConfig(options ...TestFSOption) (*TestFSConfig, error) {
	// Create defaults
	cfg := &TestFSConfig{
		FromPaths:    nil,
		FromSnapshot: nil,

		WithStore: blocks.NewMemoryStore(""),

		ShouldSort:              true,
		ShouldInitializeEntries: true,

		HasActivityCommit:     false,
		HasCacheMeasureCommit: false,
		MaxCacheSize:          0,

		SnapshotRetentionGroups: nil,

		Snapshot:        nil,
		SnapshotRefRoot: blocks.ID{},
		SnapshotHash:    blocks.Hash{},
		Store:           nil,
	}

	if len(options) == 0 {
		options = append(options, FromPaths("/"))
	}

	// Apply options
	for _, option := range options {
		err := option(cfg)
		if err != nil {
			return nil, fmt.Errorf("error applying TestFS config option: %w", err)
		}
	}

	// Check for invalid fields
	if cfg.FromPaths == nil && cfg.FromSnapshot == nil {
		return nil, errors.New("neither entry paths nor snapshot were specified")
	}

	if cfg.Mountpoint == "" {
		mountpoint, err := ioutil.TempDir("", "testfs-mount")
		if err != nil {
			return nil, err
		}
		cfg.Mountpoint = mountpoint
	}

	cfg.Store = cfg.WithStore

	// Start initializing test FS
	var (
		snapshot           *Snapshot
		refRootInitialized bool
	)

	snapRef := blocks.GenerateRefRootID()
	// Add test entries from a list of paths
	if cfg.FromPaths != nil {
		snapshot = &Snapshot{Meta: &SnapshotMeta{}}
		if !reflect.DeepEqual(cfg.FromPaths, []string{"/"}) {
			snapshot.Meta.Ref = snapRef[:]
		}
		snapshot.Entries = testEntries(cfg.FromPaths...)
	} else if cfg.FromSnapshot != nil {
		snapshot = cfg.FromSnapshot
		// If no meta was given, set an empty one
		if snapshot.Meta == nil {
			snapshot.Meta = &SnapshotMeta{}
		}
		if (len(snapshot.Entries) > 1 && snapshot.Entries[0].Path == "/") || len(snapshot.Links) > 0 {
			if snapshot.Meta.Ref != nil {
				oldRefRoot := blocks.IDFromByteSlice(snapshot.Meta.Ref)
				err := cfg.Store.CopyRefRoot(context.Background(), snapRef, oldRefRoot)
				if err != nil {
					return nil, err
				}
				hash, err := snapshot.hash()
				if err != nil {
					return nil, err
				}
				err = cfg.Store.RemoveRefs(context.Background(), snapRef, []blocks.Hash{hash})
				if err != nil {
					return nil, err
				}
				refRootInitialized = true
			}
			snapshot.Meta.Ref = snapRef[:]
		}
	}

	if cfg.ShouldSort {
		sort.Sort(sortEntryAlphabetic(snapshot.Entries))
		sort.Sort(sortLinkAlphabetic(snapshot.Links))
	}

	if snapshot.Meta.Ref != nil {
		cfg.SnapshotRefRoot = snapRef
		if !refRootInitialized {
			err := cfg.Store.NewRefRoot(context.Background(), snapRef)
			if err != nil {
				return nil, err
			}
			refRootInitialized = true
		}
	}

	if cfg.ShouldInitializeEntries {
		// Loop through entries and generate blocks out of random data
		err := configDefaultTestEntries(cfg.Store, snapRef, snapshot.Entries)
		if err != nil {
			return nil, err
		}
		err = configDefaultTestLinks(cfg.Store, snapshot.Links)
		if err != nil {
			return nil, err
		}
	}

	if snapshot.Meta.Ref != nil {
		snapshot.Meta.Tag = latestRetentionGroup.name

		snapData, err := snapshot.data()
		if err != nil {
			return nil, err
		}
		hash, err := snapshot.hash()
		if err != nil {
			return nil, err
		}
		err = cfg.Store.Put(context.Background(), snapRef, hash, bytes.NewReader(snapData))
		if err != nil {
			return nil, fmt.Errorf("putting snapshot block to store: %w", err)
		}
		cfg.SnapshotHash = hash
	}

	cfg.Snapshot = snapshot

	return cfg, nil
}

func Mountpoint(mountpoint string) TestFSOption {
	return func(c *TestFSConfig) error {
		c.Mountpoint = mountpoint
		return nil
	}
}

func Readonly(readonly bool) TestFSOption {
	return func(c *TestFSConfig) error {
		c.Readonly = readonly
		return nil
	}
}

func FromPaths(paths ...string) TestFSOption {
	return func(c *TestFSConfig) error {
		if c.FromSnapshot != nil {
			return errors.New("cannot use options FromPaths and FromSnapshot at the same time")
		}

		c.FromPaths = paths
		return nil
	}
}

func FromSnapshot(snapshot *Snapshot) TestFSOption {
	return func(c *TestFSConfig) error {
		if c.FromPaths != nil {
			return errors.New("cannot use options FromPaths and FromSnapshot at the same time")
		}

		c.FromSnapshot = snapshot
		return nil
	}
}

func WithStore(store blocks.Store) TestFSOption {
	return func(c *TestFSConfig) error {
		c.WithStore = store
		return nil
	}
}

func Sort(enable bool) TestFSOption {
	return func(c *TestFSConfig) error {
		c.ShouldSort = enable
		return nil
	}
}

func InitializeEntries(enable bool) TestFSOption {
	return func(c *TestFSConfig) error {
		c.ShouldInitializeEntries = enable
		return nil
	}
}

func ActivityCommit(enable bool) TestFSOption {
	return func(c *TestFSConfig) error {
		c.HasActivityCommit = enable
		return nil
	}
}

func CacheMeasureCommit(size int64) TestFSOption {
	return func(c *TestFSConfig) error {
		if size == -1 {
			c.HasCacheMeasureCommit = false
			return nil
		}

		c.HasCacheMeasureCommit = true
		c.MaxCacheSize = size
		return nil
	}
}

func SnapshotRetentionGroups(groups []*snapshotRetentionGroup) TestFSOption {
	return func(c *TestFSConfig) error {
		c.SnapshotRetentionGroups = groups
		return nil
	}
}

// configDefaultTestEntries loops through a slice of barebones entries,
// automatically setting default values, for use in tests
// For files, it creates blocks out of random data.
// EntryMeta can be nil, or partially specified. If partially specified,
// it will use the defaults defined below, for each unspecified field
// The only required value is the entry path.
func configDefaultTestEntries(store blocks.Store, ref blocks.ID, entries []*Entry) error {
	const (
		DefaultFileSize = 7 * 1024 * 1024
		DefaultDirMode  = 0755 | os.ModeDir
		DefaultFileMode = 0644
		DefaultUid      = 0
		DefaultGid      = 0
	)

	for _, e := range entries {
		var IsDir bool
		if strings.HasSuffix(e.Path, "/") {
			IsDir = true
		}

		var metaWasNil bool
		if e.Meta == nil {
			e.Meta = &EntryMeta{}
			metaWasNil = true
		}

		var (
			size int64
			mode uint32
			uid  uint32
			gid  uint32
		)

		uid = DefaultUid
		gid = DefaultGid

		if IsDir {
			size = 4096
			mode = uint32(DefaultDirMode)
		} else { // File
			size = DefaultFileSize
			mode = DefaultFileMode
		}

		// Set each meta field only if it is the default value

		// Size is stricter, and is only set to the default size if the meta object was nil
		// because we sometimes want to set the file size to 0
		if metaWasNil {
			e.Meta.Size = size
		}
		if e.Meta.Mode == 0 {
			e.Meta.Mode = mode
		}
		if e.Meta.Uid == 0 {
			e.Meta.Uid = uid
		}
		if e.Meta.Gid == 0 {
			e.Meta.Gid = gid
		}

		if IsDir && !os.FileMode(e.Meta.Mode).IsDir() {
			return errors.New(e.Path + ": file is directory but does not have os.ModeDir set")
		}

		// Only generate data for non-empty files
		if IsDir || e.Meta.Size == 0 {
			continue
		}

		blocks, err := testFileBlocks(store, ref, e.Path, e.Meta.Size)
		if err != nil {
			return err
		}

		// Add blocks to file entry
		e.Blocks = blocks
	}

	return nil
}

// Currently just creates the meta
func configDefaultTestLinks(store blocks.Store, links []*Link) error {
	for _, l := range links {
		if l.Meta == nil {
			l.Meta = &EntryMeta{}
		}
	}
	return nil
}
