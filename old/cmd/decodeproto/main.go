package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"path/filepath"
	"reflect"
	"remotefs"
	"remotefs/blocks"
	"strings"
	"time"

	"github.com/golang/protobuf/proto"
)

type printerPrintf func(format string, v ...interface{})
type printerPrintln func(v ...interface{})
type indenter func()

func main() {
	var path string
	var auto, snapshot, snapshots, snapinfo, snapinfos bool
	flag.StringVar(&path, "path", "", "")
	flag.BoolVar(&auto, "auto", false, "")
	flag.BoolVar(&snapshot, "snapshot", false, "")
	flag.BoolVar(&snapshots, "snapshots", false, "")
	flag.BoolVar(&snapinfo, "snapinfo", false, "")
	flag.BoolVar(&snapinfos, "snapinfos", false, "")
	flag.Parse()

	if auto {
		decoded, err := tryDecodeSnapshotInfoList(path)
		if err != nil {
			decoded, err = tryDecodeSnapshot(path)
		}
		if err != nil {
			data, err := ioutil.ReadFile(path)
			if err != nil {
				panic(err)
			}
			decoded = string(data)
		}
		fmt.Println(decoded)
		return
	}
	if snapshot {
		decoded, err := tryDecodeSnapshot(path)
		if err != nil {
			fmt.Println("error while decoding snapshot:", err)
		}
		fmt.Println(decoded)
	}
	if snapinfo {
		decoded, err := tryDecodeSnapshotInfoList(path)
		if err != nil {
			fmt.Println("error while decoding snapshotinfolist:", err)
		}
		fmt.Println(decoded)
	}
	if snapshots {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			panic(err)
		}
		for _, fi := range files {
			if fi.IsDir() {
				continue
			}
			fpath := filepath.Join(path, fi.Name())
			decoded, err := tryDecodeSnapshot(fpath)
			if err != nil {
				continue
			}
			if decoded != "" {
				fmt.Println(decoded)
			}
		}
	}
	if snapinfos {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			panic(err)
		}
		for _, fi := range files {
			if fi.IsDir() {
				continue
			}
			fpath := filepath.Join(path, fi.Name())
			decoded, err := tryDecodeSnapshotInfoList(fpath)
			if err != nil {
				continue
			}
			if decoded != "" {
				fmt.Println(decoded)
			}
		}
	}
}

func printers(w io.Writer) (printerPrintf, printerPrintln, indenter, indenter) {
	indent := 0
	wpf := func(format string, v ...interface{}) {
		var indentStr string
		for i := 0; i < indent; i++ {
			indentStr += "\t"
		}
		fmt.Fprintf(w, indentStr+" "+format, v...)
	}
	wpl := func(v ...interface{}) {
		var indentStr string
		for i := 0; i < indent; i++ {
			indentStr += "\t"
		}
		message := make([]interface{}, len(v)+1)
		message[0] = indentStr
		copy(message[1:], v)
		fmt.Fprintln(w, message...)
	}
	ir := func() {
		indent++
	}
	il := func() {
		indent--
	}

	return wpf, wpl, ir, il
}

func tryDecodeSnapshotInfoList(filename string) (string, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	infos := &remotefs.SnapshotInfos{}
	err = proto.Unmarshal(data, infos)
	if err != nil {
		return "", err
	}
	if len(infos.Infos) == 0 {
		return "", errors.New("infos are empty")
	}
	for _, info := range infos.Infos {
		if reflect.DeepEqual(info, &remotefs.SnapshotInfo{}) {
			return "", errors.New("infos are empty")
		}
	}

	w := &strings.Builder{}

	wpf, wpl, ir, il := printers(w)

	wpl("SnapshotInfoList: [")
	ir()
	wpf("Hash: '%v',\n", filepath.Base(filename))
	for _, info := range infos.Infos {
		wpl("Infos: {")
		ir()
		if len(info.Hash) > 0 {
			wpf("Hash: '%v',\n", blocks.Hash(blocks.ByteSliceToArr(info.Hash)))
		}
		il()
		wpl("},")
	}
	il()
	wpl("]")

	return w.String(), nil
}

func tryDecodeSnapshot(filename string) (string, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	snap := &remotefs.Snapshot{}
	err = proto.Unmarshal(data, snap)
	if err != nil {
		return "", err
	}
	if len(snap.Entries) == 0 {
		return "", errors.New("snapshot is empty")
	}

	w := &strings.Builder{}
	wpf, wpl, ir, il := printers(w)
	wpl("Snapshot: {")
	ir()
	wpf("Hash: '%v',\n", filepath.Base(filename))
	wpl("Meta: {")
	ir()
	wpf("CreatedAt: %v,\n", time.Unix(snap.Meta.CreatedAt, 0))
	wpf("Ref: '%v',\n", blocks.IDFromByteSlice(snap.Meta.Ref))
	wpf("Tag: '%v',\n", snap.Meta.Tag)
	il()
	wpl("},")
	wpl("Entries: [")
	ir()
	for _, e := range snap.Entries {
		wpf("'%v',\n", e.Path)
	}
	il()
	wpl("],")
	il()
	wpl("}")

	return w.String(), nil
}
