package main

import (
	"flag"
	"fmt"
	"net/http"
	"remotefs/blocks"
)

var blocksPath = flag.String("blocks", "", "path to blocks to be used with disk store")

func main() {
	flag.Parse()
	if *blocksPath == "" {
		fmt.Println("please specify blocks path using -blocks")
		return
	}

	diskStore, err := blocks.NewDiskStore(*blocksPath)
	if err != nil {
		panic(err)
	}

	handler := blocks.HTTPHandler(diskStore)
	http.ListenAndServe(":8888", handler)
}
