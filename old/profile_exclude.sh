#! /bin/sh

go test -v -run "nothing" -mutexprofile=mutex.prof -cpuprofile cpu.prof -fs.cpuprofile fscpu.prof -memprofile mem.prof -bench "$1"
go tool pprof -top fscpu.prof