package remotefs

import (
	"context"
	fmt "fmt"
	"os"
	"strings"
	"testing"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
)

func TestMkdir(t *testing.T) {
	require := require.New(t)

	fs, err := NewTestFS()
	require.Nil(err)
	defer fs.Done()

	err = fs.Mkdir("/dir1", 0755)
	require.Nil(err)
	err = fs.Mkdir("/dir2", 0755)
	require.Nil(err)

	files, err := fs.ReadDir("/")
	require.Nil(err)

	require.Equal(2, len(files))
	file1 := files[0]
	file2 := files[1]
	require.True(file1.IsDir())
	require.Equal("dir1", file1.Name())
	require.True(file2.IsDir())
	require.Equal("dir2", file2.Name())

	// Nested dirs
	err = fs.Mkdir("/dir1/child1", 0755)
	require.Nil(err)
	err = fs.Mkdir("/dir2/child2", 0755)
	require.Nil(err)
	err = fs.Mkdir("/dir1/child1/child3", 0755)
	require.Nil(err)

	files, err = fs.ReadDir("/dir1")
	require.Nil(err)

	require.Equal(1, len(files))
	child1 := files[0]
	require.True(child1.IsDir())
	require.Equal("child1", child1.Name())

	files, err = fs.ReadDir("/dir2")
	require.Nil(err)

	require.Equal(1, len(files))
	child2 := files[0]
	require.True(child2.IsDir())
	require.Equal("child2", child2.Name())

	files, err = fs.ReadDir("/dir1/child1")
	require.Nil(err)

	require.Equal(1, len(files))
	child3 := files[0]
	require.True(child3.IsDir())
	require.Equal("child3", child3.Name())
}

func TestRemove(t *testing.T) {
	require := require.New(t)

	fs, err := NewTestFS(
		FromPaths(
			"/",
			"/justfile",
			"/dirwithfiles/",
			"/dirwithfiles/file",
		),
	)
	require.Nil(err)
	defer fs.Done()
	r := fs.FS.root
	dir := r.children["dirwithfiles"].(*DirNode)

	require.True(fileExists(r, "justfile"))
	err = rmFile(r, "justfile", false)
	require.Nil(err)
	require.False(fileExists(r, "justfile"))

	// cant remove dir with files in it
	err = rmFile(r, "dirwithfiles", true)
	require.NotNil(err)
	require.True(fileExists(r, "dirwithfiles"))

	err = rmFile(dir, "file", false)
	require.Nil(err)
	require.False(fileExists(dir, "file"))

	// can remove dir now that we removed the file
	err = rmFile(r, "dirwithfiles", true)
	require.Nil(err)
	require.False(fileExists(r, "dirwithfiles"))
}

func rmFile(parent *DirNode, name string, isDir bool) error {
	req := &fuse.RemoveRequest{Name: name, Dir: isDir}
	return parent.Remove(context.Background(), req)
}

func fileExists(parent *DirNode, name string) bool {
	_, err := parent.Lookup(context.Background(), name)
	return err == nil
}

func TestCreate(t *testing.T) {
	require := require.New(t)
	fs, err := NewTestFS()
	require.Nil(err)
	defer fs.Done()

	f, err := fs.Create("/file1")
	require.Nil(err)
	f.Close()

	files, err := fs.ReadDir("/")
	require.Nil(err)
	require.Equal(1, len(files))
	file1 := files[0]
	require.Equal("file1", file1.Name())
	require.False(file1.IsDir())

	f, err = fs.Create("/file2")
	require.Nil(err)
	f.Close()

	files, err = fs.ReadDir("/")
	require.Nil(err)
	require.Equal(2, len(files))
	file1 = files[0]
	require.Equal("file1", file1.Name())
	require.False(file1.IsDir())
	file2 := files[1]
	require.Equal("file2", file2.Name())
	require.False(file2.IsDir())
}

func TestRename(t *testing.T) {
	require := require.New(t)

	paths := []string{"/", "/file1", "/file2", "/file3", "/dir/"}

	// create nested dirs, of which the deepest one will have the path length close to 4096
	// so we can then rename another file to have > 4096 path length
	const repeatPathTimes = 16 // 16 * 255 + 1 = 4081
	for i := 1; i <= repeatPathTimes; i++ {
		name := strings.Repeat("a", 254)
		path := strings.Repeat("/"+name, i) + "/"

		paths = append(paths, path)
	}

	fs, err := NewTestFS(FromPaths(paths...))
	require.Nil(err)
	defer fs.Done()

	err = fs.Rename("/doesntexist", "/renamed")
	require.Error(err)

	err = fs.Rename("/file1", "/dir/renamed")
	require.Nil(err)

	// check the old file doesnt exist
	_, err = fs.Stat("/file1")
	require.NotNil(err)

	fi, err := fs.Stat("/dir/renamed")
	require.Nil(err)
	require.Equal("renamed", fi.Name())

	err = fs.Rename("/file2", "/"+strings.Repeat("x", 256))
	require.NotNil(err)

	name := strings.Repeat("a", 254)
	node := fs.FS.root
	for node.children != nil {
		node.mu.Lock()
		n := node.children[name]
		node.mu.Unlock()
		if n == nil {
			break
		}
		node = n.(*DirNode)
	}
	rootNode := fs.FS.root
	parentNode := node

	req := &fuse.RenameRequest{OldName: "file3", NewName: name}
	err = rootNode.Rename(context.Background(), req, parentNode)
	require.NotNil(err)
}

func BenchmarkReadDirAll(b *testing.B) {
	entries := []*Entry{&Entry{Path: "/"}}
	for i := 0; i < b.N; i++ {
		path := fmt.Sprintf("/file%v", i)
		entries = append(entries, &Entry{Path: path, Meta: &EntryMeta{Size: 0}})
	}
	fs, _ := NewTestFS(FromSnapshot(&Snapshot{Entries: entries}))
	defer fs.Done()

	node := fs.FS.root

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		node.ReadDirAll(context.Background())
	}
}

func BenchmarkMkdir(b *testing.B) {
	fs, _ := NewTestFS()
	defer fs.Done()

	node := fs.FS.root

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		req := &fuse.MkdirRequest{
			Name: fmt.Sprintf("dir%v", i),
			Mode: 0755 | os.ModeDir,
		}
		node.Mkdir(context.Background(), req)
	}
}

func BenchmarkCreate(b *testing.B) {
	fs, _ := NewTestFS()
	defer fs.Done()

	node := fs.FS.root

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		req := &fuse.CreateRequest{
			Name: fmt.Sprintf("file%v", i),
			Mode: 0644,
		}
		resp := &fuse.CreateResponse{}
		node.Create(context.Background(), req, resp)
	}
}

func BenchmarkRemove(b *testing.B) {
	fs, _ := NewTestFS()
	defer fs.Done()

	node := fs.FS.root

	for i := 0; i < b.N; i++ {
		createReq := &fuse.CreateRequest{
			Name: fmt.Sprintf("bench_remove_%v", i),
			Mode: 0644,
		}
		resp := &fuse.CreateResponse{}
		node.Create(context.Background(), createReq, resp)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		req := &fuse.RemoveRequest{
			Name: fmt.Sprintf("bench_remove_%v", i),
			Dir:  false,
		}
		node.Remove(context.Background(), req)
	}
}

func BenchmarkLookup(b *testing.B) {
	fs, _ := NewTestFS(FromPaths("/", "/dir/", "/dir/f1"))
	defer fs.Done()

	node := fs.FS.root.children["dir"].(*DirNode)

	for i := 0; i < b.N; i++ {
		node.Lookup(context.Background(), "f1")
	}
}
