package remotefs

import (
	fmt "fmt"
	"io"
	"os"
	"remotefs/blocks"
	"runtime/pprof"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
	"gitlab.com/go-figure/logr"
	"golang.org/x/net/context"
)

type readerFromReaderAt struct {
	readerAt io.ReaderAt
	pos      int64
}

func newReaderFromReaderAt(readerAt io.ReaderAt, off int64) io.Reader {
	return &readerFromReaderAt{
		readerAt: readerAt,
		pos:      off,
	}
}

func (ra *readerFromReaderAt) Read(b []byte) (int, error) {
	n, err := ra.readerAt.ReadAt(b, ra.pos)
	ra.pos += int64(n)
	return n, err
}

type fileReader struct {
	readerAt *FileNode
	pos      int64
}

func newFileReader(readerAt *FileNode, off int64) io.Reader {
	return &fileReader{
		readerAt: readerAt,
		pos:      off,
	}
}

func (ra *fileReader) Read(b []byte) (int, error) {
	n, err := ra.readerAt.readAtNoLock(b, ra.pos)
	ra.pos += int64(n)
	return n, err
}

func startTimeoutStacktracePrint() func() {
	var ticker *time.Ticker
	var timer *time.Timer

	go func() {
		timer = time.NewTimer(3 * time.Second)
		<-timer.C

		ticker = time.NewTicker(3 * time.Second)
		for range ticker.C {
			pprof.Lookup("goroutine").WriteTo(os.Stdout, 2)
		}
	}()

	return func() {
		if timer != nil {
			timer.Stop()
		}
		if ticker != nil {
			ticker.Stop()
		}
	}
}

type repeatZeroes struct{}

func (r repeatZeroes) Read(p []byte) (int, error) {
	for i := range p {
		p[i] = 0
	}
	return len(p), nil
}

func pow(a, b int) int {
	p := 1
	for b > 0 {
		if b&1 != 0 {
			p *= a
		}
		b >>= 1
		a *= a
	}
	return p
}

func retry(ctx context.Context, event string, fn func() error, tries int) (failed bool) {
	log := logr.FromContext(ctx)
	var err error
	for i := 0; i < tries; i++ {
		err = fn()
		if err == nil {
			return
		}
		log.Error(fmt.Sprintf("%v_retry", event), err, logr.KV{"retry_count": i + 1})
	}
	log.Error(fmt.Sprintf("%v_failed", event), err, nil)
	failed = true
	return
}

func sprintHashes(hashes []blocks.Hash) string {
	str := "["
	for i, hash := range hashes {
		str = fmt.Sprintf("%v%v, ", str, hash)
		if i == len(hashes)-1 {
			str = str[:len(str)-2]
		}
	}
	str += "]"
	return str
}

func humanizeBytes(bytes int64) string {
	humanized := humanize.IBytes(uint64(bytes))
	replaced := strings.ReplaceAll(humanized, "i", "")
	replaced = strings.ReplaceAll(replaced, " ", "")
	replaced = strings.ReplaceAll(replaced, ".0", "")
	return replaced
}
