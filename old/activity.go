package remotefs

import (
	"time"

	"golang.org/x/net/context"
)

var activityCommitTime = 60 * time.Second

func setActivityCommitTime(dur time.Duration) (revert func()) {
	prevDur := activityCommitTime
	activityCommitTime = dur
	return func() {
		activityCommitTime = prevDur
	}
}

type activityTimeout struct {
	activity chan bool
	done     chan bool
	t        *time.Timer
	fs       *FS
}

func newActivityTimeout(fs *FS) *activityTimeout {
	return &activityTimeout{
		fs:       fs,
		activity: make(chan bool),
		done:     make(chan bool),
	}
}

func (fs *FS) setupActivity() {
	fs.activityTimeout.done = make(chan bool)
	fs.activityTimeout.t = time.NewTimer(activityCommitTime)
	go fs.activityTimeout.awaitActivity(fs.ctx, activityCommitTime)
}

func (at *activityTimeout) awaitActivity(ctx context.Context, dur time.Duration) {
	select {
	case <-at.activity:
	case <-at.done:
		at.t.Stop()
		close(at.done)
		return
	}
	at.t.Reset(dur)

	for {
		if !at.t.Stop() {
			<-at.t.C
		}
		at.t.Reset(dur)

		if len(at.t.C) > 0 {
			<-at.t.C
		}

		select {
		case <-at.done:
			at.t.Stop()
			close(at.done)
			return
		case <-at.activity:
		case <-at.t.C:
			// default action is to commit all
			retry(ctx, "activity_commit", func() error {
				return at.fs.lockAndCommitAll(ctx)
			}, 5)

			go at.awaitActivity(ctx, dur)
			return
		}
	}
}

func (at *activityTimeout) stop() {
	at.done <- true
}

func (at *activityTimeout) tick() {
	at.activity <- true
}
