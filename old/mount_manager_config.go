package remotefs

import (
	"errors"
	fmt "fmt"
	"io/ioutil"
	"os"
	"remotefs/blocks"

	pb "remotefs/cmd/remotefs/proto"

	"gopkg.in/yaml.v2"
)

type Config struct {
	path         string                               `yaml:"-"`
	Mountset     map[string]string                    `yaml:"mountset"`    // FS name -> Path
	Filesystems  map[string]*FSConfig                 `yaml:"filesystems"` // FS name -> FSConfig
	StoreConfigs map[string]blocks.GenericStoreConfig `yaml:"stores"`      // StoreConfigName -> StoreConfig
}

type FSConfig struct {
	Name            string            `yaml:"-"`
	UUID            fsUUID            `yaml:"uuid"`
	StoreConfig     string            `yaml:"store"`
	SnapshotsConfig []*SnapshotConfig `yaml:"snapshots,omitempty"`
}

type SnapshotConfig struct {
	Name     string
	Capacity int
	Period   string
}

type fsUUID string

func NewConfig(path string) (*Config, error) {
	var config Config
	config.path = path
	config.Filesystems = make(map[string]*FSConfig)

	// create file if doesn't exist
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := config.Save()
		if err != nil {
			return nil, err
		}
	}

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return nil, fmt.Errorf("unmarshaling config file: %w", err)
	}

	// validate store configs
	for _, storeConfig := range config.StoreConfigs {
		err = storeConfig.Config.Validate()
		if err != nil {
			return nil, err
		}
	}

	// set fsConfig name
	for name, fsConfig := range config.Filesystems {
		fsConfig.Name = name
	}

	return &config, nil
}

func (c *Config) Save() error {
	data, err := yaml.Marshal(c)
	if err != nil {
		return fmt.Errorf("marshaling config file: %w", err)
	}

	return ioutil.WriteFile(c.path, data, 0755)
}

func (c *Config) FSAdd(fsConfig *FSConfig) error {
	c.Filesystems[fsConfig.Name] = fsConfig
	return c.Save()
}

func (c *Config) FSRemove(name string) error {
	_, ok := c.Filesystems[name]
	if !ok {
		return errFSNotFound(name)
	}
	delete(c.Filesystems, name)
	return c.Save()
}

func (c *Config) FSList() (pbFSes []*pb.FS) {
	for _, fsConfig := range c.Filesystems {
		pbFSes = append(pbFSes, &pb.FS{Name: fsConfig.Name})
	}
	return
}

func (c *Config) FSGet(name string) (*FSConfig, error) {
	fsConfig, ok := c.Filesystems[name]
	if !ok {
		return nil, errFSNotFound(name)
	}

	return fsConfig, nil
}

func (c *Config) MountsetAdd(path string, fs string) error {
	if _, err := c.FSGet(fs); err != nil {
		return err
	}

	for _, oldPath := range c.Mountset {
		if oldPath == path {
			return errors.New("a mountset entry with this path already exists")
		}
	}

	c.Mountset[fs] = path

	return c.Save()
}

func (c *Config) MountsetRemove(fs string) error {
	if _, ok := c.Mountset[fs]; !ok {
		return errMountsetNotFound(fs)
	}
	delete(c.Mountset, fs)
	return c.Save()
}

func (c *Config) MountsetRemoveByPath(byPath string) error {
	for fs, path := range c.Mountset {
		if path == byPath {
			return c.MountsetRemove(fs)
		}
	}
	return errors.New("no mountset entry found for that path")
}

func (c *Config) MountsetList() []*pb.MountsetEntry {
	var entries []*pb.MountsetEntry
	for fs, path := range c.Mountset {
		entries = append(entries, &pb.MountsetEntry{FS: fs, Path: path})
	}
	return entries
}

func (sc *SnapshotConfig) Instance() (*snapshotRetentionGroup, error) {
	group := &snapshotRetentionGroup{
		name:     sc.Name,
		capacity: sc.Capacity,
	}

	next, err := nextSnapshotterFromString(sc.Period)
	if err != nil {
		return nil, err
	}
	group.next = next

	return group, nil
}
