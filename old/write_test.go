package remotefs

import (
	"bytes"
	"context"
	"io"
	"math/rand"
	"os"
	"remotefs/blocks"
	"testing"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
)

func testTruncate(t *testing.T, f *os.File, truncate int64) {
	require := require.New(t)

	// get initial file size and the difference between it and
	// how much we want to truncate
	info, err := f.Stat()
	require.Nil(err)
	initialSize := info.Size()
	sizeDiff := truncate - initialSize

	// if the truncation extends the file, we need to add zeroes at the end
	// we use `bufSize` to allocate the whole buffer including zeroes,
	// and then only read up to `readSize` bytes from the file
	bufSize := truncate
	readSize := truncate
	if sizeDiff > 0 {
		bufSize = initialSize + sizeDiff
		readSize = initialSize
	}

	expected := make([]byte, bufSize)
	// read data that is already in the file to check if we truncated correctly
	f.Seek(0, io.SeekStart)
	read, err := f.Read(expected[:readSize])
	require.Nil(err)
	require.Equal(readSize, int64(read))

	// do truncation
	err = f.Truncate(truncate)
	require.Nil(err)
	// check file size is what we expect
	info, err = f.Stat()
	require.Nil(err)
	require.Equal(truncate, info.Size())

	// read data again, and check that it matches what we read at the start (+ zeroes at the end if extending)
	f.Seek(0, io.SeekStart)
	actual := make([]byte, truncate)
	read, err = f.Read(actual)
	require.Nil(err)
	require.Equal(truncate, int64(read))

	require.Equal(expected, actual)
}

// If you're here, and you think truncate isn't working. Have you tried os.O_TRUNC?
func TestTruncateCache(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/foo", Meta: &EntryMeta{Size: 10 * 1024 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	// truncate 100
	f, err := fs.OpenFile("/foo", os.O_RDWR, 0755)
	require.Nil(err)
	defer f.Close()

	testTruncate(t, f, 100)

	// write(100) at 100
	toWrite := blocks.TestData("TRUNCATEFILE", 100, 100)
	n, err := f.WriteAt(toWrite, 100)
	require.Nil(err)
	require.Equal(100, n)

	// check written data
	data, err := fs.ReadFile("/foo")
	require.Nil(err)
	expected := append(blocks.TestData("/foo", 0, 100), toWrite...)
	require.Equal(expected, data)

	// truncate 150
	testTruncate(t, f, 150)

	// truncate 300
	testTruncate(t, f, 300)
}

func TestTruncateBlocks(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/file", Meta: &EntryMeta{Size: 10 * 1024 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.OpenFile("/file", os.O_RDWR, 0755)
	require.Nil(err)
	defer f.Close()

	node, ok := fs.FS.root.children["file"].(*FileNode)
	require.True(ok)
	require.NotNil(node)
	require.Equal(4, len(node.blocks))

	// calculate block sizes
	var sizes []int64
	for i, b := range node.blocks {
		var size int64
		if i+1 < len(node.blocks) {
			size = node.blocks[i+1].Offset - b.Offset
		} else {
			size = int64(node.meta.Size) - b.Offset
		}
		sizes = append(sizes, size)
	}

	for i := len(node.blocks) - 1; i >= 0; i-- {
		off := node.blocks[i].Offset
		size := sizes[i]

		// len(blocks) == n + 1,    current block
		testTruncate(t, f, off+size-1)
		require.Equal(i+1, len(node.blocks))

		// len(blocks) == n + 1,    current block
		testTruncate(t, f, off+1)
		require.Equal(i+1, len(node.blocks))

		// len(blocks) == n
		testTruncate(t, f, off)
		require.Equal(i, len(node.blocks))
	}
}

func TestTruncateBlocksWrite(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/file", Meta: &EntryMeta{Size: 10 * 1024 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.OpenFile("/file", os.O_RDWR, 0755)
	require.Nil(err)
	defer f.Close()

	node, ok := fs.FS.root.children["file"].(*FileNode)
	require.True(ok)
	require.NotNil(node)
	require.Equal(4, len(node.blocks))

	// calculate block sizes
	var sizes []int64
	for i, b := range node.blocks {
		var size int64
		if i+1 < len(node.blocks) {
			size = node.blocks[i+1].Offset - b.Offset
		} else {
			size = int64(node.meta.Size) - b.Offset
		}
		sizes = append(sizes, size)
	}

	for i := len(node.blocks) - 1; i >= 0; i-- {
		off := node.blocks[i].Offset
		size := sizes[i]

		toWrite := blocks.TestData("TRUNCATEBLOCKSWRITE", 0, 1500)

		// write some data after the truncation point
		f.WriteAt(toWrite, off+size-1)
		// len(blocks) == n + 1,    current block
		testTruncate(t, f, off+size-1)
		require.Equal(i+1, len(node.blocks))

		// write some data after the truncation point
		f.WriteAt(toWrite, off+1)
		// len(blocks) == n + 1,    current block
		testTruncate(t, f, off+1)
		require.Equal(i+1, len(node.blocks))

		// write some data after the truncation point
		f.WriteAt(toWrite, off)
		// len(blocks) == n
		testTruncate(t, f, off)
		require.Equal(i, len(node.blocks))
	}
}

func TestTruncateExtend(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)
	const totalSize = 10 * 1024 * 1024
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/foo", Meta: &EntryMeta{Size: totalSize}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.OpenFile("/foo", os.O_RDWR, 0755)
	require.Nil(err)
	defer f.Close()

	extendSize := int64(5 * 1024 * 1024)
	extend := totalSize + extendSize

	testTruncate(t, f, extend)
}

func TestTruncatePartialDiscardData(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/foo", Meta: &EntryMeta{Size: 10 * 1024 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	foo, ok := fs.FS.root.children["foo"].(*FileNode)
	require.NotNil(foo)
	require.True(ok)

	// if this fails, make sure to set the file size so that we get 4 blocks
	require.Equal(4, len(foo.blocks))
	truncate := foo.blocks[2].Offset + 700

	f, err := fs.OpenFile("/foo", os.O_RDWR, 0755)
	require.Nil(err)
	defer f.Close()

	err = f.Truncate(truncate)
	require.Nil(err)

	info, err := f.Stat()
	require.Nil(err)
	require.Equal(int64(truncate), info.Size())

	actual := make([]byte, truncate)
	read, err := f.Read(actual)
	require.Nil(err)
	require.Equal(truncate, int64(read))
	require.Equal(blocks.TestData("/foo", 0, truncate), actual)

	actual = make([]byte, 1)
	read, err = f.Read(actual)
	require.Equal(io.EOF, err)
	require.Equal(0, read)
	require.Equal(make([]byte, 1), actual)
}
func TestWrite(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)

	const fileSize = 9 * 1024 * 1024
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/file", Meta: &EntryMeta{Size: fileSize}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.OpenFile("/file", os.O_RDWR, 0000)
	defer f.Close()
	require.Nil(err)

	minWriteSize := fileSize / 2
	writeSize := rand.Intn(fileSize-minWriteSize) + minWriteSize

	data := blocks.TestData("/file", 0, int64(writeSize))
	n, err := f.Write(data)
	require.Nil(err)
	require.Equal(writeSize, n)

	actual, err := fs.ReadFile("/file")
	require.Nil(err)

	expected := append(data, blocks.TestData("/file", int64(writeSize), int64(fileSize-writeSize))...)
	require.Equal(expected, actual)
}

func TestWriteCommitExistingFile(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	const (
		maxCacheSize   = 10 * 1024 * 1024
		totalWriteSize = maxCacheSize + 1*1024*1024
	)

	fs, err := NewTestFS(FromPaths("/"), CacheMeasureCommit(maxCacheSize))
	require.NoError(t, err)
	defer fs.Done()

	file, err := fs.OpenFile("/file", os.O_CREATE|os.O_RDWR, 0755)
	require.NoError(t, err)
	require.Nil(t, file.Truncate(10*1024*1024))
	file.Close()

	node := fs.FS.root.children["file"].(*FileNode)
	var written int
	for written < totalWriteSize {
		resp := &fuse.WriteResponse{}
		err := node.Write(defaultCtx, &fuse.WriteRequest{
			Offset: int64(written),
			Data:   blocks.TestData("/file", int64(written), 4096),
		}, resp)
		written += resp.Size
		require.NoError(t, err)
	}
}

func TestWriteSeek(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/file", Meta: &EntryMeta{Size: 0}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.OpenFile("/file", os.O_RDWR, 0000)
	defer f.Close()
	require.Nil(err)

	const maxWritePos = 7 * 1024 * 1024
	writePos := rand.Intn(maxWritePos-1) + 1
	minWriteSize := maxWritePos / 2
	writeSize := rand.Intn(maxWritePos-minWriteSize) + minWriteSize

	data := blocks.TestData("WRITE", int64(writePos), int64(writeSize))

	seek, err := f.Seek(int64(writePos), io.SeekStart)
	require.Nil(err)
	require.Equal(int64(writePos), seek)
	n, err := f.Write(data)
	require.Nil(err)
	require.Equal(writeSize, n)

	actual, err := fs.ReadFile("/file")
	require.Nil(err)

	expected := bytes.Repeat([]byte{0}, writePos)
	expected = append(expected, data...)

	require.Equal(len(expected), len(actual))
	require.Equal(expected, actual)
}

func TestWriteExistingSeek(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)

	const fileSize = 9 * 1024 * 1024
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/file", Meta: &EntryMeta{Size: fileSize}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.OpenFile("/file", os.O_RDWR, 0000)
	defer f.Close()
	require.Nil(err)

	minWriteSize := fileSize / 2
	writeSize := rand.Intn(fileSize-minWriteSize) + minWriteSize
	writePos := rand.Intn(fileSize-1) + 1

	data := blocks.TestData("WRITE", int64(writePos), int64(writeSize))

	seek, err := f.Seek(int64(writePos), io.SeekStart)
	require.Nil(err)
	require.Equal(int64(writePos), seek)
	n, err := f.Write(data)
	require.Nil(err)
	require.Equal(writeSize, n)

	actual, err := fs.ReadFile("/file")
	require.Nil(err)

	expected := blocks.TestData("/file", 0, int64(writePos))
	expected = append(expected, data...)

	require.Equal(actual, expected)
}

func TestWriteSizeIncrease(t *testing.T) {
	const fileSize = 20
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/file", Meta: &EntryMeta{Size: fileSize}},
		},
	}))
	require.NoError(t, err)
	defer fs.Done()

	node := fs.FS.root.children["file"].(*FileNode)
	initialSize := node.meta.Size

	increase := make([]byte, 50)
	// write some bytes at the end to increase the size
	wrote, err := node.WriteAt(increase, node.meta.Size)
	require.NoError(t, err)
	require.Equal(t, len(increase), wrote)
	// check file size increased
	require.Equal(t, initialSize+int64(len(increase)), int64(node.meta.Size))

	// now seek to start and write bytes, and check that it hasn't increased
	wrote, err = node.WriteAt(increase, 0)
	require.NoError(t, err)
	require.Equal(t, len(increase), wrote)
	require.Equal(t, initialSize+int64(len(increase)), int64(node.meta.Size))
}

func BenchmarkWriteSequential(b *testing.B) {
	benchCases := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "25MB", size: 25 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024, noskip: true},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			fs, _ := NewTestFS(FromSnapshot(&Snapshot{
				Entries: []*Entry{
					&Entry{Path: "/"},
					&Entry{Path: "/file", Meta: &EntryMeta{Size: bench.size}},
				},
			}))
			defer fs.Done()

			node := fs.FS.root.children["file"].(*FileNode)

			data := bytes.Repeat([]byte{'f'}, 4096)

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				req := &fuse.WriteRequest{}
				resp := &fuse.WriteResponse{}
				for o := int64(0); o <= bench.size; o += 4096 {
					req.Offset = o
					req.Data = data

					node.Write(context.Background(), req, resp)
				}
			}
		})
	}
}

func BenchmarkWriteRandom(b *testing.B) {
	benchCases := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "25MB", size: 25 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024, noskip: true},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			fs, _ := NewTestFS(FromSnapshot(&Snapshot{
				Entries: []*Entry{
					&Entry{Path: "/"},
					&Entry{Path: "/file", Meta: &EntryMeta{Size: bench.size}},
				},
			}))
			defer fs.Done()

			node := fs.FS.root.children["file"].(*FileNode)

			data := bytes.Repeat([]byte{'f'}, 4096)

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				req := &fuse.WriteRequest{}
				resp := &fuse.WriteResponse{}
				for toWrite := int64(bench.size); toWrite >= 0; toWrite -= 4096 {
					randOffset := int64(rand.Intn(int(bench.size)))
					req.Offset = randOffset
					req.Data = data

					node.Write(context.Background(), req, resp)
				}
			}
		})
	}
}
