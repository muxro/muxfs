#! /bin/sh

go test -benchmem -bench . -run "^Benchmark" 2>&1 | tee bench/results_$(date +'%Y%m%d_%H%M')