# Remotefs Server
## Methods:
  - `add(mountname, snapshotHash)`: Associates a name with a snapshot. Will be stored in `$XDG_DATA_HOME` aka (`~/.local/share`)

    Starts the server if it's not running.
  - `mount(mountname, path)`: Gets the snapshot hash associated to the name, creates a new FS, and mounts it at path.

    Starts the server if it's not running.

    Needs some sort of system for managing all the active mounts
  - `unmount(path)`: Unmounts the filesystem mounted at `path`.

    Server kills itself if no more mounts are active.

## Logging:
  - [INFO] daemon started
  - [INFO] FS mounted/unmounted
  - [INFO] FS snapshotted
  - [INFO] item added to mountset
  - [INFO] item removed from mountset
  - [ERROR] fuse operation errors
  - [ERROR] commit errors