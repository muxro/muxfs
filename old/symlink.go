package remotefs

import (
	"os"
	"sync"
	"time"

	"bazil.org/fuse"
	bazilFS "bazil.org/fuse/fs"
	"golang.org/x/net/context"
)

type SymlinkNode struct {
	parent *DirNode
	meta   *EntryMeta
	target string

	mu sync.RWMutex

	FS *FS
}

var _ bazilFS.NodeReadlinker = &SymlinkNode{} // Follow symlink

func (n *SymlinkNode) Attr(ctx context.Context, a *fuse.Attr) error {
	n.mu.RLock()
	defer n.mu.RUnlock()

	meta := n.meta
	a.Size = uint64(meta.Size)
	a.Mode = os.FileMode(meta.Mode)
	a.Mtime = time.Unix(meta.UpdatedAt, 0)
	a.Uid = meta.Uid
	a.Gid = meta.Gid

	return nil
}

func (n *SymlinkNode) Setattr(ctx context.Context, req *fuse.SetattrRequest, resp *fuse.SetattrResponse) error {
	defer n.FS.changed()

	n.mu.Lock()
	defer n.mu.Unlock()

	if req.Valid.Size() {
		return fuse.EIO
	}
	if req.Valid.Mode() {
		return nil
	}
	if req.Valid.Uid() {
		n.meta.Uid = req.Uid
		resp.Attr.Uid = req.Uid
	}
	if req.Valid.Gid() {
		n.meta.Gid = req.Gid
		resp.Attr.Gid = req.Gid
	}

	return nil
}

// Readlink reads a symbolic link, returning the path of the referenced node.
func (n *SymlinkNode) Readlink(ctx context.Context, req *fuse.ReadlinkRequest) (string, error) {
	n.mu.RLock()
	defer n.mu.RUnlock()
	return n.target, nil
}
