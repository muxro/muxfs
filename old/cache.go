package remotefs

import (
	"io"
	"sync"
	"sync/atomic"
)

const (
	cacheSliceGrowDoubleLimit = 1024
	cacheBlockSize            = 1 * 1024 * 1024
)

type Cache struct {
	blocks         [][]byte
	cacheBlockSize int64
	allocatedSize  int64
	dataEnd        int64
	onSizeChange   func(oldsize, newsize int64)
}

var cacheBlockPool = sync.Pool{
	New: func() interface{} {
		return make([]byte, cacheBlockSize)
	},
}

func NewCache() *Cache {
	return &Cache{cacheBlockSize: cacheBlockSize}
}

func (c *Cache) Add(data []byte, offset int64, readerAt io.ReaderAt) error {
	if len(data) == 0 {
		return nil
	}

	end := offset + int64(len(data))

	firstBlock := int(offset) / int(cacheBlockSize)
	lastBlock := int((end + cacheBlockSize - 1) / cacheBlockSize)

	// extend blocks if needed
	if newlen := lastBlock + 1; len(c.blocks) < newlen {
		c.grow(newlen)
	}

	oldAllocatedSize := c.allocatedSize
	newAllocatedSize := oldAllocatedSize

	for i := firstBlock; i < lastBlock; i++ {
		// TODO: @refactor make this better
		bStart := int64(i) * cacheBlockSize
		bEnd := bStart + cacheBlockSize

		// allocate this block if it doesn't exist
		if c.blocks[i] == nil {
			c.blocks[i] = cacheBlockPool.Get().([]byte)
			newAllocatedSize += cacheBlockSize

			if readerAt != nil {
				// fill our new cache block with node block data
				_, err := readerAt.ReadAt(c.blocks[i], bStart)
				if err != nil && err != io.EOF {
					return err
				}
			}
		}

		// limit left and right bounds:
		limStart, limEnd := bStart, bEnd
		// block start can't be before data start
		if limStart < offset {
			limStart = offset
		}
		// block end can't be after data end
		if limEnd > end {
			limEnd = end
		}

		srcFrom, srcTo := limStart-offset, limEnd-offset
		dstFrom, dstTo := limStart-bStart, limEnd-bStart

		copy(c.blocks[i][dstFrom:dstTo], data[srcFrom:srcTo])

		if limEnd > c.dataEnd {
			c.dataEnd = limEnd
		}
	}

	// set new size
	if newAllocatedSize != oldAllocatedSize {
		c.allocatedSize = newAllocatedSize

		// trigger callback
		if c.onSizeChange != nil {
			go c.onSizeChange(oldAllocatedSize, newAllocatedSize)
		}
	}

	return nil
}

func (c *Cache) grow(wantlen int) {
	oldlen := len(c.blocks)

	newlen := oldlen
	doublelen := newlen + newlen
	if wantlen > doublelen {
		newlen = wantlen
	} else {
		if oldlen < cacheSliceGrowDoubleLimit {
			newlen = doublelen
		} else {
			// Check 0 < newcap to detect overflow
			// and prevent an infinite loop.
			for 0 < newlen && newlen < wantlen {
				newlen += newlen / 4
			}
			// Set newcap to the requested cap when
			// the newcap calculation overflowed.
			if newlen <= 0 {
				newlen = wantlen
			}
		}
	}

	blocks := make([][]byte, newlen)
	copy(blocks, c.blocks)
	c.blocks = blocks
}

func (c *Cache) HasDataIn(start, end int64) bool {
	if c.IsEmpty() {
		return false
	}

	toFind := newIntervalRange(start, end)

	firstBlock := int(start / cacheBlockSize)
	lastBlock := int((end + cacheBlockSize - 1) / cacheBlockSize)

	for i := firstBlock; i < lastBlock; i++ {
		if i >= len(c.blocks) {
			return false
		}
		if c.blocks[i] == nil {
			continue
		}

		bStart := int64(i) * cacheBlockSize
		bEnd := bStart + cacheBlockSize
		// make sure we don't go over c.dataEnd
		if bEnd > c.dataEnd {
			bEnd = c.dataEnd
		}
		blockInterval := newIntervalRange(bStart, bEnd)

		_, ok := blockInterval.intersect(toFind)
		if ok {
			return true
		}
	}

	return false
}

func (c *Cache) PatchOnto(p []byte, offset int64) int {
	if len(c.blocks) == 0 {
		return 0
	}

	toFind := newInterval(offset, len(p))

	firstBlock := int(offset / cacheBlockSize)
	end := offset + int64(len(p))
	lastBlock := int((end + cacheBlockSize - 1) / cacheBlockSize)

	// it's possible that the lastBlock can be greater than the number of blocks we have
	// so we limit it
	if lastBlock+1 > len(c.blocks) {
		lastBlock = len(c.blocks) - 1
	}

	var lastPos int
	for i := firstBlock; i < lastBlock; i++ {
		if c.blocks[i] == nil {
			continue
		}

		bStart := int64(i) * cacheBlockSize
		bEnd := bStart + cacheBlockSize

		// make sure we don't go over c.dataEnd
		if bEnd > c.dataEnd {
			bEnd = c.dataEnd
		}

		curr := newIntervalRange(bStart, bEnd)

		intersection, found := curr.intersect(toFind)
		if !found {
			continue
		}

		srcFrom := intersection.start - bStart
		srcTo := srcFrom + intersection.length

		dstFrom := intersection.start - offset
		dstTo := dstFrom + intersection.length

		copy(p[dstFrom:dstTo], c.blocks[i][srcFrom:srcTo])

		lastPos = int(dstTo)
	}

	return lastPos
}

type interval struct {
	start, end int64
	length     int64
}

func newInterval(start int64, length int) interval {
	return interval{
		start:  start,
		end:    start + int64(length),
		length: int64(length),
	}
}

func newIntervalRange(start, end int64) interval {
	return interval{
		start:  start,
		end:    end,
		length: end - start,
	}
}

func (a interval) intersect(b interval) (interval, bool) {
	out := interval{}

	if a.start > b.end || b.start > a.end {
		return out, false
	}

	startMax := b.start
	if a.start > startMax {
		startMax = a.start
	}
	endMin := b.end
	if a.end < endMin {
		endMin = a.end
	}

	out.start = startMax
	out.end = endMin
	out.length = endMin - startMax

	return out, true
}

func (c *Cache) Truncate(size int64) {
	if size == 0 {
		c.Clear()
		return
	}

	lastBlock := int((size - 1) / cacheBlockSize)

	// starting from lastBlock (but not including), clear every block until the end
	for i := lastBlock + 1; i < len(c.blocks); i++ {
		c.blocks[i] = nil
	}

	// set dataEnd to the truncate amount
	if size < c.dataEnd {
		// TODO: @performance
		// make the existing data 0
		for i := size; i < c.dataEnd; i++ {
			b := i / cacheBlockSize
			bStart := int64(b * cacheBlockSize)

			pos := i - bStart
			if c.blocks[b] != nil {
				c.blocks[b][pos] = 0
			}
		}

		c.dataEnd = size
	}
}

func (c *Cache) ReadAt(b []byte, off int64) (n int, err error) {
	// read data from cache (does not include gaps between blocks)
	n = c.PatchOnto(b, off)

	// for each gap we have inbetween cache blocks, fill the buffer with zeroes
	firstBlock := int(off / cacheBlockSize)
	end := off + int64(len(b))
	lastBlock := int((end + cacheBlockSize - 1) / cacheBlockSize)

	toFind := newInterval(off, len(b))

	// it's possible that the lastBlock can be greater than the number of blocks we have
	// so we limit it
	if lastBlock+1 > len(c.blocks) {
		lastBlock = len(c.blocks) - 1
	}

	for i := firstBlock; i < lastBlock; i++ {
		if c.blocks[i] != nil {
			continue
		}

		bStart := int64(i) * cacheBlockSize

		// some blocks might be allocated beyond c.dataEnd, ignore them
		if bStart > c.dataEnd {
			continue
		}

		bEnd := bStart + cacheBlockSize

		// make sure we don't go over c.dataEnd
		if bEnd > c.dataEnd {
			bEnd = c.dataEnd
		}

		curr := newIntervalRange(bStart, bEnd)

		intersection, found := curr.intersect(toFind)
		if !found {
			continue
		}

		dstFrom := intersection.start - off
		dstTo := dstFrom + intersection.length

		for i := dstFrom; i < dstTo; i++ {
			b[i] = 0
			n++
		}
	}

	if n > len(b) {
		n = len(b)
	}

	if n != len(b) {
		err = io.EOF
	}

	return
}

func (c *Cache) IsEmpty() bool {
	if c.blocks == nil {
		return true
	}

	for _, b := range c.blocks {
		if b != nil && len(b) > 0 {
			return false
		}
	}
	return true
}

func (c *Cache) AllocatedSize() int64 {
	return c.allocatedSize
}

func (c *Cache) OnSizeChange(fn func(oldsize, newsize int64)) {
	c.onSizeChange = fn
}

func (c *Cache) Clear() {
	for _, b := range c.blocks {
		if b != nil {
			cacheBlockPool.Put(b)
		}
	}

	sizeBefore := c.allocatedSize

	c.blocks = nil
	c.allocatedSize = 0
	c.dataEnd = 0

	if c.onSizeChange != nil {
		go c.onSizeChange(sizeBefore, 0)
	}
}

// TODO: if cacheBlockSize > maxCacheSize, every write will trigger a commit
type CacheSizeMeasure struct {
	totalSize    int64
	onSizeChange func(oldsize, newsize int64)
}

func NewCacheSizeMeasure() *CacheSizeMeasure {
	return &CacheSizeMeasure{}
}

func (cm *CacheSizeMeasure) OnSizeChange(fn func(oldsize, newsize int64)) {
	cm.onSizeChange = fn
}

func (cm *CacheSizeMeasure) Subscribe(cache *Cache) {
	cache.OnSizeChange(func(oldsize, newsize int64) {
		oldtotalsize := cm.TotalSize()
		atomic.AddInt64(&cm.totalSize, newsize-oldsize)
		newtotalsize := cm.TotalSize()

		cm.onSizeChange(oldtotalsize, newtotalsize)
	})
}

func (cm *CacheSizeMeasure) TotalSize() int64 {
	return atomic.LoadInt64(&cm.totalSize)
}
