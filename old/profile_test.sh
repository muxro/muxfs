#! /bin/sh

go test -v -run "$1" -mutexprofile=mutex.prof -cpuprofile cpu.prof -memprofile mem.prof