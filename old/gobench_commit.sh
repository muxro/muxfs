#! /bin/sh

go test -benchmem -bench "^BenchmarkCommit" -run "nada" 2>&1 | tee bench/commit_results_$(date +'%Y%m%d_%H%M')