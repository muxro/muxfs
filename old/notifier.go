package remotefs

type notifier interface {
	Register(fs *FS)
	FSUnmounted() error
}

type FSNotifier struct {
	fs      *FS
	manager *mountManagerServer
}

func (n *FSNotifier) Register(fs *FS) {
	n.fs = fs
}

func (n *FSNotifier) FSUnmounted() error {
	mount, ok := n.manager.mounts[n.fs.mountpoint]
	if !ok {
		return errMountNotFound(n.fs.mountpoint)
	}

	mount.Unmounted <- true

	return nil
}

// nopNotifier does nothing. For use in tests
type nopNotifier struct {
}

func (n *nopNotifier) Register(fs *FS) {
}
func (n *nopNotifier) FSUnmounted() error {
	return nil
}
