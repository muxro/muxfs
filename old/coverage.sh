#! /usr/bin/env bash

go test -v -coverprofile=cover.out; cat cover.out | grep -v "\.pb\.go" > cover.out.ok; go tool cover -func=cover.out.ok