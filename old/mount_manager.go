package remotefs

import (
	"bytes"
	"errors"
	fmt "fmt"
	"os"
	"remotefs/blocks"
	"sync/atomic"
	"time"

	pb "remotefs/cmd/remotefs/proto"

	"bazil.org/fuse"
	"github.com/google/uuid"
	"gitlab.com/go-figure/logr"
	"golang.org/x/net/context"
)

var (
	SockPerm = 0600 | os.ModeSocket

	// TODO: these error functions need to be a separate error type
	errFSNotFound = func(s string) error {
		return fmt.Errorf("fs %v does not exist", s)
	}
	errFSAlreadyExists = func(s string) error {
		return fmt.Errorf("fs %v already exists", s)
	}
	errMountsetNotFound = func(name string) error {
		return fmt.Errorf("mountset entry for path %v does not exist", name)
	}
	errNoFSForPath = func(path string) error {
		return fmt.Errorf("no fs found for path %v", path)
	}
	errMountNotFound = func(path string) error {
		return fmt.Errorf("no mount found at path %v", path)
	}
	errStoreConfigNotFound = func(name string) error {
		return fmt.Errorf("no store config named %v could be found", name)
	}
	errDirNotEmpty = errors.New("target directory is not empty")
)

type mountManagerServer struct {
	cfg          *Config
	sockAddr     string
	mounts       map[string]*mountInfo
	initErrors   []error
	activeMounts int32
	suicideTimer *time.Timer
}

type fsInstanceOptions struct {
	Readonly        bool
	Name            string
	HashOverride    blocks.Hash
	UUID            fsUUID
	StoreConfig     string
	SnapshotsConfig []*SnapshotConfig
}

type fsMountOptions struct {
	Path string
}

type mountInfo struct {
	FS                *FS
	FSInstanceOptions *fsInstanceOptions
	Unmounted         chan bool
}

type fsAlreadyMountedError struct {
	fs   string
	path string
}

func NewMountManagerServer(configPath, sockAddr string) (*mountManagerServer, error) {
	s := &mountManagerServer{
		sockAddr: sockAddr,
		mounts:   make(map[string]*mountInfo),
	}

	// open config file
	cfg, err := NewConfig(configPath)
	if err != nil {
		err = fmt.Errorf("invalid mount manager config file remotefs.yaml: %w", err)
		s.InitError(err)
	}

	s.cfg = cfg

	return s, s.Errors()
}

func (s *mountManagerServer) Start(ctx context.Context) {
	ctx, cancel := context.WithCancel(ctx)
	s.startSuicideTimer(cancel)

	go func() {
		<-ctx.Done()
		s.close()
		// TODO: don't do this
		os.Exit(0)
	}()
}

func (s *mountManagerServer) InitError(err error) {
	s.initErrors = append(s.initErrors, err)
}

func (s *mountManagerServer) Errors() error {
	if len(s.initErrors) == 0 {
		return nil
	}

	concat := "Daemon started with errors: "
	for i, err := range s.initErrors {
		concat += err.Error()
		if i < len(s.initErrors)-1 {
			concat += ", "
		}
	}

	return errors.New(concat)
}

func (s *mountManagerServer) close() {
	fmt.Println("server.Close")
	if err := os.Remove(s.sockAddr); err != nil && !os.IsNotExist(err) {
		panic(err)
	}
}

func (s *mountManagerServer) startSuicideTimer(cancel context.CancelFunc) {
	// Init and stop suicide timer
	s.suicideTimer = time.NewTimer(999 * time.Second)
	s.suicideTimer.Stop()

	go func() {
		<-s.suicideTimer.C
		cancel()
	}()
}

func (s *mountManagerServer) SuicideAfter(d time.Duration) {
	s.suicideTimer.Reset(d)
}

func (s *mountManagerServer) Hello(ctx context.Context, req *pb.HelloRequest) (*pb.HelloReply, error) {
	reply := &pb.HelloReply{}
	if err := s.Errors(); err != nil {
		reply.Error = err.Error()
		s.SuicideAfter(time.Second)
	}

	return reply, nil
}

func (s *mountManagerServer) FSAdd(ctx context.Context, req *pb.FSAddRequest) (*pb.FSAddReply, error) {
	name := req.GetName()
	hashStr := req.GetHash()
	fsStoreConfigName, err := s.getDefaultStoreConfigName(req.GetFSStoreConfigName())
	if err != nil {
		return nil, err
	}

	snapStoreConfigName := req.GetSnapshotStoreConfigName()
	if snapStoreConfigName == "" {
		snapStoreConfigName = fsStoreConfigName
	} else {
		var err error
		snapStoreConfigName, err = s.getDefaultStoreConfigName(req.GetSnapshotStoreConfigName())
		if err != nil {
			return nil, err
		}
	}

	_, err = s.cfg.FSGet(name)
	if err == nil {
		return nil, errFSAlreadyExists(name)
	}

	fsStore, err := s.instanceStoreFromConfig(ctx, fsStoreConfigName)
	if err != nil {
		return nil, err
	}
	snapStore, err := s.instanceStoreFromConfig(ctx, snapStoreConfigName)
	if err != nil {
		return nil, err
	}

	uuid := fsUUID(uuid.New().String())
	if hashStr != "new" {
		snapHash, err := blocks.HashFromString(hashStr)
		if err != nil {
			return nil, err
		}

		var newSnapInfo *SnapshotInfo
		// If the stores are the same, that means the file blocks already exist,
		// in which case we can just copy the refRoot, and remove the ref to the old snapshot block.
		if snapStoreConfigName == fsStoreConfigName {
			snapshot, err := GetSnapshot(ctx, snapStore, snapHash)
			if err != nil {
				return nil, err
			}

			// Remember old snapshot refRoot, because we will need to remove the old snapshot block from it.
			oldRefRoot := blocks.IDFromByteSlice(snapshot.Meta.Ref)
			// Generate a new refRoot for the new snapshot block we are going to copy to FS store.
			newRefRoot := blocks.GenerateRefRootID()

			// Set new refRoot on snapshot
			snapshot.Meta.Ref = newRefRoot[:]
			// Get new snapshot data and hash
			data, err := snapshot.data()
			if err != nil {
				return nil, err
			}
			newSnapHash := snapshot.hashFromData(data)

			err = fsStore.CopyRefRoot(ctx, newRefRoot, oldRefRoot)
			if err != nil {
				return nil, err
			}
			// Add new snapshot block to FS store
			err = fsStore.Put(ctx, newRefRoot, newSnapHash, bytes.NewReader(data))
			if err != nil {
				return nil, err
			}
			// Remove old snapshot block
			err = fsStore.RemoveRefs(ctx, newRefRoot, []blocks.Hash{snapHash})
			if err != nil {
				return nil, err
			}

			newSnapInfo = &SnapshotInfo{
				Meta: snapshot.Meta,
				Hash: newSnapHash[:],
			}
		} else {
			// If the store where the snapshot (and blocks) are coming from is different from the FS store,
			// we have to *copy* all file blocks to the FS store.
			var err error
			newSnapInfo, err = copySnapshotToStore(ctx, snapHash, fsStore, snapStore)
			if err != nil {
				return nil, err
			}
		}

		snapshotUpdater, err := NewSnapshotUpdater(fsStore, uuid)
		if err != nil {
			return nil, err
		}
		err = snapshotUpdater.Set(ctx, []*SnapshotInfo{newSnapInfo})
		if err != nil {
			return nil, err
		}
	}

	err = s.cfg.FSAdd(&FSConfig{Name: name, UUID: fsUUID(uuid), StoreConfig: fsStoreConfigName})
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("fs_add", logr.KV{"fs.name": name})
	return &pb.FSAddReply{Message: fmt.Sprintf("Added FS %v", name)}, nil
}

func (s *mountManagerServer) FSClone(ctx context.Context, req *pb.FSCloneRequest) (*pb.FSCloneReply, error) {
	sourceName := req.GetSourceName()
	newName := req.GetNewName()

	sourceCfg, err := s.cfg.FSGet(sourceName)
	if err != nil {
		return nil, err
	}

	err = s.fsClone(ctx, sourceCfg, newName)
	if err != nil {
		return nil, err
	}

	return &pb.FSCloneReply{Message: "Cloned"}, nil
}

func (s *mountManagerServer) fsClone(ctx context.Context, sourceCfg *FSConfig, newName string) error {
	sourceFS, err := s.instanceFS(ctx, &fsInstanceOptions{
		Readonly:        true,
		Name:            sourceCfg.Name,
		UUID:            sourceCfg.UUID,
		StoreConfig:     sourceCfg.StoreConfig,
		SnapshotsConfig: sourceCfg.SnapshotsConfig,
	})
	if err != nil {
		return err
	}
	defer sourceFS.close()

	newFS, err := sourceFS.clone(nil)
	if err != nil {
		return err
	}
	defer newFS.close()

	return s.cfg.FSAdd(&FSConfig{
		Name:            newName,
		UUID:            newFS.uuid,
		StoreConfig:     sourceCfg.StoreConfig,
		SnapshotsConfig: sourceCfg.SnapshotsConfig,
	})
}

func (s *mountManagerServer) FSMigrateStore(ctx context.Context, req *pb.FSMigrateStoreRequest) (*pb.FSMigrateStoreReply, error) {
	fsName := req.GetFSName()
	newStoreConfigName := req.GetNewStoreConfig()

	fsCfg, err := s.cfg.FSGet(fsName)
	if err != nil {
		return nil, err
	}

	fs, err := s.instanceFS(ctx, &fsInstanceOptions{
		Readonly:        true,
		Name:            fsCfg.Name,
		UUID:            fsCfg.UUID,
		StoreConfig:     fsCfg.StoreConfig,
		SnapshotsConfig: fsCfg.SnapshotsConfig,
	})
	if err != nil {
		return nil, err
	}
	defer fs.close()

	store, err := s.instanceStoreFromConfig(ctx, newStoreConfigName)
	if err != nil {
		return nil, err
	}

	err = fs.migrateStore(store)
	if err != nil {
		return nil, err
	}

	fsCfg.StoreConfig = newStoreConfigName

	err = s.cfg.FSAdd(fsCfg)
	if err != nil {
		return nil, err
	}

	return &pb.FSMigrateStoreReply{Message: "MigrateStored"}, nil
}

func (s *mountManagerServer) FSDestroy(ctx context.Context, req *pb.FSDestroyRequest) (*pb.FSDestroyReply, error) {
	name := req.GetName()

	fsConfig, err := s.cfg.FSGet(name)
	if err != nil {
		return nil, err
	}

	err = s.fsDestroy(ctx, fsConfig)
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("fs_destroy", logr.KV{"fs.name": name})
	return &pb.FSDestroyReply{Message: fmt.Sprintf("Destroyed FS %v", name)}, nil
}

func (s *mountManagerServer) fsDestroy(ctx context.Context, fsConfig *FSConfig) error {
	fs, err := s.instanceFS(ctx, &fsInstanceOptions{
		Readonly:        true,
		Name:            fsConfig.Name,
		UUID:            fsConfig.UUID,
		StoreConfig:     fsConfig.StoreConfig,
		SnapshotsConfig: fsConfig.SnapshotsConfig,
	})
	if err != nil {
		return err
	}
	defer fs.close()

	err = fs.destroy()
	if err != nil {
		return fmt.Errorf("can't destroy fs: %w", err)
	}

	return s.cfg.FSRemove(fsConfig.Name)
}

func (s *mountManagerServer) FSList(ctx context.Context, req *pb.FSListRequest) (*pb.FSListReply, error) {
	fses := s.cfg.FSList()

	return &pb.FSListReply{
		FSes: fses,
	}, nil
}

func (s *mountManagerServer) FSVerify(req *pb.FSVerifyRequest, server pb.MountManager_FSVerifyServer) error {
	ctx := server.Context()
	name := req.GetName()
	checkHashes := req.CheckHashes
	allSnapshots := req.AllSnapshots

	fsConfig, err := s.cfg.FSGet(name)
	if err != nil {
		return err
	}
	fs, err := s.instanceFS(ctx, &fsInstanceOptions{
		Readonly:        true,
		Name:            fsConfig.Name,
		UUID:            fsConfig.UUID,
		StoreConfig:     fsConfig.StoreConfig,
		SnapshotsConfig: fsConfig.SnapshotsConfig,
	})
	if err != nil {
		return err
	}
	defer fs.close()

	results := fs.verify(checkHashes, allSnapshots)

	for result := range results {
		reply := pb.FSVerifyReply{
			Message: result.Message,
			Percent: result.Percent,
			ETA:     result.ETA,
		}
		if result.Err != nil {
			reply.Err = result.Err.Error()
		}
		err := server.Send(&reply)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *mountManagerServer) mountFS(ctx context.Context, instanceOpts *fsInstanceOptions, mountOpts *fsMountOptions) error {
	mounted := make(chan error)
	go func() {
		// TODO: cancel context
		atomic.AddInt32(&s.activeMounts, 1)

		// check if dir exists
		f, err := os.Open(mountOpts.Path)
		if err != nil {
			// TODO: only for debugging, remove at some point
			// if can't open dir, the server could have been killed without
			// properly unmounting, so try to unmount
			err := unmountAndWaitUntilUnmounted(mountOpts.Path)
			if err != nil {
				// dir cannot be opened and cannot be unmounted, abort
				mounted <- err
				return
			}
		}

		// check if dir is empty
		_, err = f.Readdirnames(1)
		if err == nil {
			// close dir file handle so we can unmount
			f.Close()
			// dir is not empty
			mounted <- errDirNotEmpty
			return
		}

		fs, err := s.instanceFS(ctx, instanceOpts)
		if err != nil {
			mounted <- fmt.Errorf("instancing FS from config: %w", err)
			return
		}
		mount, err := fs.Mount(mountOpts.Path)
		if err != nil {
			atomic.AddInt32(&s.activeMounts, -1)
			mounted <- err
			return
		}

		mounted <- nil

		// Keep track of what file systems we have mounted at which path
		s.mounts[mountOpts.Path] = newMountInfo(instanceOpts, mount.FS)

		// Wait for any errors
		err = <-mount.Err
		if err != nil {
			panic(err)
		}
	}()

	err := <-mounted
	if err != nil {
		return fmt.Errorf("cannot mount: %w", err)
	}

	s.suicideTimer.Stop()

	return nil
}

func (s *mountManagerServer) instanceStoreFromConfig(ctx context.Context, storeConfigName string) (blocks.Store, error) {
	// try to find store for this fs
	storeConfig, ok := s.cfg.StoreConfigs[storeConfigName]
	if !ok {
		return nil, errStoreConfigNotFound(storeConfigName)
	}

	return storeConfig.Config.Instance(ctx)
}

func (s *mountManagerServer) getDefaultStoreConfigName(storeConfigName string) (string, error) {
	if storeConfigName != "" {
		return storeConfigName, nil
	}

	if len(s.cfg.StoreConfigs) > 1 {
		return "", errors.New("more than one store configs exist and no store config name was specified")
	}

	// only one element
	for name := range s.cfg.StoreConfigs {
		return name, nil
	}

	// 0 elements
	return "", errors.New("no store configs exist")
}

func (s *mountManagerServer) Mount(ctx context.Context, req *pb.MountRequest) (*pb.MountReply, error) {
	fsName := req.GetFS()
	path := req.GetPath()
	readonly := req.GetReadonly()

	for path, mount := range s.mounts {
		if mount.FSInstanceOptions.Name == fsName {
			return nil, fsAlreadyMountedError{fs: fsName, path: path}
		}
	}

	fsConfig, err := s.cfg.FSGet(fsName)
	if err != nil {
		return nil, err
	}

	err = s.mountFS(ctx, &fsInstanceOptions{
		Readonly:        readonly,
		Name:            fsConfig.Name,
		UUID:            fsConfig.UUID,
		StoreConfig:     fsConfig.StoreConfig,
		SnapshotsConfig: fsConfig.SnapshotsConfig,
	}, &fsMountOptions{
		Path: path,
	})
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("fs_mount", logr.KV{"fs.name": fsName, "path": path})
	return &pb.MountReply{Message: fmt.Sprintf("Mounted %v at %v", fsName, path)}, nil
}

func (s *mountManagerServer) instanceFS(ctx context.Context, opts *fsInstanceOptions) (*FS, error) {
	var snapshotGroups []*snapshotRetentionGroup
	for _, groupCfg := range opts.SnapshotsConfig {
		group, err := groupCfg.Instance()
		if err != nil {
			return nil, err
		}
		snapshotGroups = append(snapshotGroups, group)
	}

	store, err := s.instanceStoreFromConfig(ctx, opts.StoreConfig)
	if err != nil {
		return nil, err
	}

	snapshotUpdater, err := NewSnapshotUpdater(store, opts.UUID)
	if err != nil {
		return nil, err
	}

	var (
		snapshot     *Snapshot
		snapshotHash blocks.Hash
		emptyHash    blocks.Hash
	)
	if opts.HashOverride != emptyHash {
		// Load snapshot from hash
		var err error
		snapshot, err = GetSnapshot(ctx, store, opts.HashOverride)
		if err != nil {
			return nil, err
		}
		snapshotHash = opts.HashOverride
	} else {
		// Get latest snapshot from updater
		infos, err := snapshotUpdater.Get(ctx)
		if err != nil {
			return nil, err
		}

		if len(infos) > 0 {
			var err error
			snapshotHash = blocks.HashFromSlice(infos[0].Hash)
			snapshot, err = GetSnapshot(ctx, store, snapshotHash)
			if err != nil {
				return nil, err
			}
		} else {
			// Instance new empty snapshot
			snapshot = &Snapshot{
				Meta: &SnapshotMeta{
					CreatedAt: time.Now().Unix(),
					Tag:       latestRetentionGroup.name,
					Ref:       nil,
				},
				Entries: []*Entry{&Entry{Path: "/", Meta: &EntryMeta{Size: 4096, Mode: uint32(0755 | os.ModeDir)}}},
			}
			snapshotHash, err = snapshot.hash()
			if err != nil {
				return nil, err
			}
		}
	}

	// Link notifier to our mount manager
	// It will also be registered to the new FS inside Mount
	notify := &FSNotifier{manager: s}

	fs, err := NewFS(&FSOptions{
		Context:                 ctx,
		UUID:                    opts.UUID,
		Store:                   store,
		Snapshot:                snapshot,
		SnapshotHash:            snapshotHash,
		SnapshotUpdater:         snapshotUpdater,
		SnapshotRetentionGroups: snapshotGroups,
		Notify:                  notify,
		HasCacheMeasureCommit:   true,
		MaxCacheSize:            400 * 1024 * 1024,
		HasActivityCommit:       true,
	})
	if err != nil {
		return nil, err
	}

	if !opts.Readonly {
		err = fs.enableWrites()
		if err != nil {
			return nil, err
		}
	}

	return fs, nil
}

func (s *mountManagerServer) Unmount(ctx context.Context, req *pb.UnmountRequest) (*pb.UnmountReply, error) {
	fs := req.GetFS()
	path := req.GetPath()

	var mount *mountInfo
	for mountpath, mountinfo := range s.mounts {
		if fs == mountinfo.FSInstanceOptions.Name {
			mount = mountinfo
			path = mountpath
		}
	}

	if mount == nil {
		var ok bool
		mount, ok = s.mounts[path]
		if !ok {
			return nil, errMountNotFound(path)
		}
	}

	err := fuse.Unmount(path)
	if err != nil {
		return nil, err
	}
	<-mount.Unmounted

	// remove this FSs mountinfo
	delete(s.mounts, path)
	atomic.AddInt32(&s.activeMounts, -1)

	// see if the server should be killed
	if atomic.LoadInt32(&s.activeMounts) == 0 {
		s.SuicideAfter(time.Second)
	}

	logr.FromContext(ctx).Event("fs_unmount", logr.KV{"fs.name": fs, "path": path})
	return &pb.UnmountReply{Message: fmt.Sprintf("Unmounted %v", path)}, nil
}

func (s *mountManagerServer) NukePath(ctx context.Context, req *pb.NukePathRequest) (*pb.NukePathReply, error) {
	fs := req.GetFS()
	filePath := req.GetFilePath()

	panic("nuke-path unimplemented")

	// // s.cfg.Filesystems[]
	// for fsName, fsConfig := range s.cfg.Filesystems {
	// 	if fsName == fs {

	// 	}
	// }

	logr.FromContext(ctx).Event("fs_nuke_path", logr.KV{"fs.name": fs, "file_path": filePath})
	return &pb.NukePathReply{Message: fmt.Sprintf("NukePath %v %v", fs, filePath)}, nil
}

func (s *mountManagerServer) SnapSave(ctx context.Context, req *pb.SnapSaveRequest) (*pb.SnapSaveReply, error) {
	path := req.GetPath()
	mount, ok := s.mounts[path]
	if !ok {
		return nil, errMountNotFound(path)
	}

	err := mount.FS.save(manualRetentionGroup)
	if err != nil {
		return nil, err
	}

	name := mount.FSInstanceOptions.Name
	logr.FromContext(ctx).Event("snap_save", logr.KV{"fs.name": name, "path": path})
	return &pb.SnapSaveReply{Message: fmt.Sprintf("Created snapshot for FS %v at path %v\n", name, path)}, nil
}

func (s *mountManagerServer) SnapPreview(ctx context.Context, req *pb.SnapPreviewRequest) (*pb.SnapPreviewReply, error) {
	hash := req.GetHash()
	path := req.GetPath()
	storeConfigName, err := s.getDefaultStoreConfigName(req.GetStoreConfigName())
	if err != nil {
		return nil, err
	}

	uuid := uuid.New().String()
	err = s.mountFS(ctx, &fsInstanceOptions{
		Readonly:        true,
		Name:            uuid,
		HashOverride:    blocks.HashFromSlice(hash),
		UUID:            "",
		StoreConfig:     storeConfigName,
		SnapshotsConfig: nil,
	}, &fsMountOptions{
		Path: path,
	})
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("snap_preview", logr.KV{"fs.uuid": uuid, "path": path})
	return &pb.SnapPreviewReply{Message: fmt.Sprintf("Mounted %v at %v (readonly)", uuid, path)}, nil
}

func (s *mountManagerServer) SnapList(ctx context.Context, req *pb.SnapListRequest) (*pb.SnapListReply, error) {
	fsName := req.GetFS()

	fsConfig, err := s.cfg.FSGet(fsName)
	if err != nil {
		return nil, err
	}

	fs, err := s.instanceFS(ctx, &fsInstanceOptions{
		Readonly:        true,
		Name:            fsConfig.Name,
		UUID:            fsConfig.UUID,
		StoreConfig:     fsConfig.StoreConfig,
		SnapshotsConfig: fsConfig.SnapshotsConfig,
	})
	if err != nil {
		return nil, err
	}
	infos, err := fs.snapshotUpdater.Get(ctx)
	if err != nil {
		return nil, err
	}

	var snapsDisplay []string
	for _, info := range infos {
		tag := info.Meta.Tag
		date := time.Unix(info.Meta.CreatedAt, 0).Format("2006.01.02-15:04:05")
		snapDisplay := fmt.Sprintf("%v@%v - %v", tag, date, blocks.HashToStr(blocks.ByteSliceToArr(info.Hash)))
		snapsDisplay = append(snapsDisplay, snapDisplay)
	}
	return &pb.SnapListReply{Snapshots: snapsDisplay}, nil
}

func (s *mountManagerServer) SnapDelete(ctx context.Context, req *pb.SnapDeleteRequest) (*pb.SnapDeleteReply, error) {
	hash := blocks.ByteSliceToArr(req.GetHash())

	var (
		// returned at the end
		errRet  error
		deleted bool
	)
	for _, storeConfig := range s.cfg.StoreConfigs {
		store, err := storeConfig.Config.Instance(ctx)
		if err != nil {
			errRet = err
			continue
		}

		exists, err := store.Has(ctx, hash)
		if err != nil {
			errRet = err
			continue
		}
		if !exists {
			continue
		}

		snap, err := GetSnapshot(ctx, store, hash)
		if err != nil {
			return nil, err
		}

		ref := blocks.ByteSliceToArr(snap.Meta.Ref)
		err = store.DeleteRefRoot(ctx, ref)
		if err != nil {
			errRet = err
			continue
		}
		deleted = true
	}

	message := fmt.Sprintf("Could not delete snapshot %v: not found", blocks.HashToStr(hash))
	if deleted {
		message = fmt.Sprintf("Deleted snapshot %v", blocks.HashToStr(hash))
	}
	return &pb.SnapDeleteReply{Message: message}, errRet
}

func (s *mountManagerServer) MountsetAdd(ctx context.Context, req *pb.MountsetAddRequest) (*pb.MountsetAddReply, error) {
	path := req.GetPath()
	fs := req.GetFS()

	err := s.cfg.MountsetAdd(path, fs)
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("mountset_add", logr.KV{"fs.name": fs, "path": path})
	return &pb.MountsetAddReply{Message: fmt.Sprintf("Added %v to mountset", path)}, nil
}

func (s *mountManagerServer) MountsetRemove(ctx context.Context, req *pb.MountsetRemoveRequest) (*pb.MountsetRemoveReply, error) {
	fs := req.GetFS()
	path := req.GetPath()

	// first try to remove by fs name
	err := s.cfg.MountsetRemove(fs)
	if err == nil {
		logr.FromContext(ctx).Event("mountset_remove", logr.KV{"fs.name": fs})
		return &pb.MountsetRemoveReply{Message: fmt.Sprintf("Removed %v from mountset", fs)}, nil
	}

	// if that fails, try to remove by path
	err = s.cfg.MountsetRemoveByPath(path)
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("mountset_remove", logr.KV{"path": path})
	return &pb.MountsetRemoveReply{Message: fmt.Sprintf("Removed %v from mountset", path)}, nil
}

func (s *mountManagerServer) MountsetList(ctx context.Context, req *pb.MountsetListRequest) (*pb.MountsetListReply, error) {
	entries := s.cfg.MountsetList()
	return &pb.MountsetListReply{Entries: entries}, nil
}

func (s *mountManagerServer) MountsetSave(ctx context.Context, req *pb.MountsetSaveRequest) (*pb.MountsetSaveReply, error) {
	// add/merge active mounts with existing mountset entries
	for path, mi := range s.mounts {
		err := s.cfg.MountsetAdd(path, mi.FSInstanceOptions.Name)
		if err != nil {
			return nil, err
		}
	}

	// return all mountset entries
	entries := s.cfg.MountsetList()
	return &pb.MountsetSaveReply{Entries: entries}, nil
}

func (s *mountManagerServer) MountsetMount(ctx context.Context, req *pb.MountsetMountRequest) (*pb.MountsetMountReply, error) {
	entries := s.cfg.MountsetList()

	fmt.Println("Mounting mountset...")
	for _, entry := range entries {
		resp, err := s.Mount(ctx, &pb.MountRequest{FS: entry.FS, Path: entry.Path})
		if err != nil {
			return nil, err
		}
		fmt.Println(resp.Message)
	}

	return &pb.MountsetMountReply{Entries: entries}, nil
}

func (s *mountManagerServer) MountsetUnmount(ctx context.Context, req *pb.MountsetUnmountRequest) (*pb.MountsetUnmountReply, error) {
	entries := s.cfg.MountsetList()

	fmt.Println("Unmounting mountset...")
	for _, entry := range entries {
		resp, err := s.Unmount(ctx, &pb.UnmountRequest{Path: entry.Path})
		if err != nil {
			return nil, err
		}
		fmt.Println(resp.Message)
	}

	return &pb.MountsetUnmountReply{Entries: entries}, nil
}

func newMountInfo(opts *fsInstanceOptions, fs *FS) *mountInfo {
	return &mountInfo{
		FSInstanceOptions: opts,
		Unmounted:         make(chan bool),
		FS:                fs,
	}
}

func (e fsAlreadyMountedError) Error() string {
	return fmt.Sprintf("fs %v is already mounted at path %v", e.fs, e.path)
}
