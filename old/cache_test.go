package remotefs

import (
	"bytes"
	"io"
	"math/rand"
	"remotefs/blocks"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

// TODO: @refactor Make all tests reuse the buffer, not just TestSliceNotReused
func TestCacheAdd(t *testing.T) {
	block := int64(cacheBlockSize)
	halfBlock := int64(cacheBlockSize / 2)

	// 0.5 blocks worth of "block" data at the start
	reader := bytes.NewReader(bytes.Repeat([]byte("a"), int(halfBlock)))
	cache := NewCache()
	// 2.5 blocks worth of data
	data := repeatByte('0', int(2*block))
	// starting at offset 0.5 blocks
	cache.Add(data, halfBlock, reader) // [0.5, 2.5]

	actual := make([]byte, 3*block)
	cache.PatchOnto(actual, 0) // [0, 3]

	// should have:
	expected := repeatByte(byte('a'), int(halfBlock))                   // block data from [0, 0.5]
	expected = append(expected, repeatByte('0', int(2*block))...)       // data from       [0.5, 2.5]
	expected = append(expected, repeatByte(byte(0), int(halfBlock))...) // zeroes from     [2.5, 3]

	require.Equal(t, expected, actual)
}

func TestCacheHasDataIn(t *testing.T) {
	block := int64(cacheBlockSize)
	halfBlock := int64(cacheBlockSize / 2)

	reader := bytes.NewReader([]byte{})
	cache := NewCache()

	// [0.5, 1.5]
	cache.Add(repeatByte('0', int(block)), halfBlock, reader)

	// [0, 0.49] -> true
	// because the whole block [0, 1] was prefilled with block data
	has := cache.HasDataIn(0, halfBlock-1)
	require.True(t, has)

	// [0, 0.99] -> true
	has = cache.HasDataIn(0, block-1)
	require.True(t, has)

	// [0, 7] -> true
	has = cache.HasDataIn(0, 7*block)
	require.True(t, has)

	// [2, 3] -> false
	has = cache.HasDataIn(2*block, 3*block)
	require.False(t, has)

	// [1.9, 3] -> false
	// even though the block was prefilled, c.dataEnd is 1.5, < 1.9
	has = cache.HasDataIn(2*block-1, 3*block)
	require.False(t, has)

	// [1.5, 2] -> true
	has = cache.HasDataIn(block+halfBlock, 2*block)
	require.True(t, has)

	// [1.51, 2] -> false
	has = cache.HasDataIn(block+halfBlock+1, 2*block)
	require.False(t, has)
}

func TestCachePatchOnto(t *testing.T) {
	block := int64(cacheBlockSize)
	halfBlock := int64(cacheBlockSize / 2)

	reader := bytes.NewReader([]byte{})
	cache := NewCache()

	// [0, 2]
	cache.Add(repeatByte('0', int(2*block)), 0, reader)
	// [2, 3]
	cache.Add(repeatByte('1', int(block)), 2*block, reader)
	// [3, 3.5]
	cache.Add(repeatByte('2', int(halfBlock)), 3*block, reader)

	// whole block
	actual := make([]byte, 1*block)
	cache.PatchOnto(actual, 0) // [0, 1]
	expected := repeatByte('0', int(block))
	require.Equal(t, expected, actual)

	// half a block
	actual = make([]byte, halfBlock)
	cache.PatchOnto(actual, 3*block) // [3, 3.5]
	expected = repeatByte('2', int(halfBlock))
	require.Equal(t, expected, actual)

	// more than one block
	actual = make([]byte, block)
	cache.PatchOnto(actual, block+halfBlock) // [1.5, 2.5]
	expected = append(repeatByte('0', int(halfBlock)), repeatByte('1', int(halfBlock))...)
	require.Equal(t, expected, actual)

	// 4 blocks
	actual = make([]byte, 4*block)
	cache.PatchOnto(actual, 0) // [0, 3]
	expected = append(repeatByte('0', int(2*block)), repeatByte('1', int(block))...)
	expected = append(expected, repeatByte('2', int(halfBlock))...)
	expected = append(expected, repeatByte(0, int(halfBlock))...)
	require.Equal(t, expected, actual)
}

func TestCacheSliceNotReused(t *testing.T) {
	reader := bytes.NewReader([]byte{})
	cache := NewCache()
	// testing inbetween/across blocks is not necessary,
	// we're just checking that the slice is copied and not reused
	data := make([]byte, 15)
	fillByte(data, '1', 15)
	cache.Add(data, 15, reader) // [15, 30]
	fillByte(data, '2', 10)

	expected := make([]byte, 15)
	cache.PatchOnto(expected, 15)
	require.Equal(t, repeatByte('1', 15), expected)
}

func TestCacheTruncate(t *testing.T) {
	block := int64(cacheBlockSize)
	halfBlock := int64(cacheBlockSize / 2)
	reader := bytes.NewReader([]byte{})

	t.Run("NoOverlap", func(t *testing.T) {
		cache := NewCache()
		cache.Add(repeatByte('1', int(block)), block, reader)   // [1, 2]
		cache.Add(repeatByte('1', int(block)), 2*block, reader) // [2, 3]
		cache.Add(repeatByte('1', int(block)), 3*block, reader) // [3, 4]

		// We should have no segments left
		cache.Truncate(halfBlock)

		actual := make([]byte, 4*block)
		cache.PatchOnto(actual, 0)
		expected := repeatByte(0, int(4*block))
		require.Equal(t, expected, actual)
	})
	t.Run("OverlapLeftEdge", func(t *testing.T) {
		cache := NewCache()
		cache.Add(repeatByte('1', int(block)), block, reader) // [1, 2]

		// We should have no segments left
		cache.Truncate(block)

		actual := make([]byte, 2*block)
		cache.PatchOnto(actual, 0)
		expected := repeatByte(0, int(2*block))
		require.Equal(t, expected, actual)
	})
	t.Run("FullAndPartialOverlap", func(t *testing.T) {
		cache := NewCache()
		cache.Add(repeatByte('1', int(block)), 0, reader)       // [0, 1]
		cache.Add(repeatByte('2', int(block)), 2*block, reader) // [2, 3]
		cache.Add(repeatByte('3', int(block)), 4*block, reader) // [4, 5]
		cache.Add(repeatByte('4', int(block)), 6*block, reader) // [6, 7]

		// We should have 3 segments left
		cache.Truncate(4*block + halfBlock)

		actual := make([]byte, 7*block)
		cache.PatchOnto(actual, 0)
		expected := repeatByte('1', int(block))
		expected = append(expected, repeatByte(0, int(block))...)
		expected = append(expected, repeatByte('2', int(block))...)
		expected = append(expected, repeatByte(0, int(block))...)
		expected = append(expected, repeatByte('3', int(halfBlock))...)
		expected = append(expected, repeatByte(0, int(2*block+halfBlock))...)
		require.Equal(t, expected, actual)
	})
	t.Run("OverlapRightEdge", func(t *testing.T) {
		cache := NewCache()
		cache.Add(repeatByte('1', int(block)), 0, reader)       // [0, 1]
		cache.Add(repeatByte('2', int(block)), 2*block, reader) // [2, 3]
		cache.Add(repeatByte('3', int(block)), 4*block, reader) // [4, 5]
		cache.Add(repeatByte('4', int(block)), 6*block, reader) // [6, 7]

		// We should have all segments the same
		cache.Truncate(7 * block)

		actual := make([]byte, 7*block)
		cache.PatchOnto(actual, 0)
		expected := repeatByte('1', int(block))
		expected = append(expected, repeatByte(0, int(block))...)
		expected = append(expected, repeatByte('2', int(block))...)
		expected = append(expected, repeatByte(0, int(block))...)
		expected = append(expected, repeatByte('3', int(block))...)
		expected = append(expected, repeatByte(0, int(block))...)
		expected = append(expected, repeatByte('4', int(block))...)
		require.Equal(t, expected, actual)
	})
	t.Run("FullOverlap", func(t *testing.T) {
		cache := NewCache()
		cache.Add(repeatByte('1', int(block)), 0, reader)       // [0, 1]
		cache.Add(repeatByte('2', int(block)), 2*block, reader) // [2, 3]
		cache.Add(repeatByte('3', int(block)), 4*block, reader) // [4, 5]
		cache.Add(repeatByte('4', int(block)), 6*block, reader) // [6, 7]

		// We should have all segments the same
		cache.Truncate(10 * block)

		actual := make([]byte, 7*block)
		cache.PatchOnto(actual, 0)
		expected := repeatByte('1', int(block))
		expected = append(expected, repeatByte(0, int(block))...)
		expected = append(expected, repeatByte('2', int(block))...)
		expected = append(expected, repeatByte(0, int(block))...)
		expected = append(expected, repeatByte('3', int(block))...)
		expected = append(expected, repeatByte(0, int(block))...)
		expected = append(expected, repeatByte('4', int(block))...)
		require.Equal(t, expected, actual)
	})
}
func TestCacheReadAt(t *testing.T) {
	block := int64(cacheBlockSize)
	halfBlock := int64(cacheBlockSize / 2)

	reader := bytes.NewReader([]byte{})
	cache := NewCache()

	// [0, 2]
	cache.Add(repeatByte('0', int(2*block)), 0, reader)
	// [2, 3]
	cache.Add(repeatByte('1', int(block)), 2*block, reader)
	// [3, 3.5]
	cache.Add(repeatByte('2', int(halfBlock)), 3*block, reader)

	// whole block
	actual := make([]byte, 1*block)
	n, err := cache.ReadAt(actual, 0) // [0, 1]
	require.NoError(t, err)
	expected := repeatByte('0', int(block))
	require.Equal(t, expected, actual)
	require.Equal(t, 1*block, int64(n))

	// between blocks
	actual = make([]byte, 1*block+halfBlock)
	n, err = cache.ReadAt(actual, block) // [1, 2.5]
	require.NoError(t, err)
	expected = append(repeatByte('0', int(block)), repeatByte('1', int(halfBlock))...)
	require.Equal(t, expected, actual)
	require.Equal(t, 1*block+halfBlock, int64(n))

	// between blocks and with gap at the end
	actual = make([]byte, 1*block+halfBlock)
	// fill buffer with data other than 0
	for i := range actual {
		actual[i] = 1
	}
	// cache ends at 3.5, so it shouldn't fill with zeroes from [3.5, 4]

	n, err = cache.ReadAt(actual, 2*block+halfBlock) // [2.5, 4]
	require.Equal(t, io.EOF, err)
	expected = append(repeatByte('1', int(halfBlock)), repeatByte('2', int(halfBlock))...)
	expected = append(expected, repeatByte(1, int(halfBlock))...)
	require.Equal(t, expected, actual)
	require.Equal(t, 1*block, int64(n))

	// now check that it handles gaps in the blocks
	// add something a few blocks away just to create some gap blocks
	cache.Add([]byte("abcd"), 8*block, reader) // [8, 9]
	// should have gaps [4, 8]

	actual = make([]byte, 2*block)
	// fill buffer with data other than 0
	for i := range actual {
		actual[i] = 1
	}

	// read [5, 7]
	n, err = cache.ReadAt(actual, 5*block)
	require.NoError(t, err)
	require.Equal(t, 2*block, int64(n))
	expected = repeatByte(0, int(2*block))
	require.Equal(t, expected, actual)
}

func TestCacheAlloc(t *testing.T) {
	block := int64(cacheBlockSize)

	reader := bytes.NewReader([]byte{})
	cache := NewCache()

	// [0, 1]
	cache.Add(repeatByte('0', int(block)), 0, reader)

	var blocksAllocated int
	for _, b := range cache.blocks {
		if b != nil {
			blocksAllocated++
		}
	}

	require.Equal(t, 1, blocksAllocated)
}

func TestCacheSize(t *testing.T) {
	block := int64(cacheBlockSize)

	reader := bytes.NewReader([]byte{})
	cache := NewCache()

	// [0, 1]
	cache.Add(repeatByte('0', int(block)), 0, reader)
	// [4, 5]
	cache.Add(repeatByte('1', int(block)), 4*block, reader)
	// [9, 10]
	cache.Add(repeatByte('2', int(block)), 9*block, reader)

	require.Equal(t, 3*block, cache.AllocatedSize())
}

func TestCacheOnSizeChange(t *testing.T) {
	block := int64(cacheBlockSize)

	reader := bytes.NewReader([]byte{})
	cache := NewCache()

	callbackCalled := make(chan bool)

	cache.OnSizeChange(func(oldsize, newsize int64) {
		callbackCalled <- true
	})

	// [0, 1]
	cache.Add(repeatByte('0', int(block)), 0, reader)
	<-callbackCalled

	// [4, 5]
	cache.Add(repeatByte('1', int(block)), 4*block, reader)
	<-callbackCalled

	// [9, 10]
	cache.Add(repeatByte('2', int(block)), 9*block, reader)
	<-callbackCalled
}

func TestCacheOnSizeChangeFrequency(t *testing.T) {
	block := int64(cacheBlockSize)

	reader := bytes.NewReader([]byte{})
	cache := NewCache()

	callbackCalled := make(chan bool)
	cache.OnSizeChange(func(oldsize, newsize int64) {
		callbackCalled <- true
		close(callbackCalled)
	})

	// [0, 5]
	cache.Add(repeatByte('0', int(5*block)), 0, reader)
	// 5 cache blocks get allocated, but the callback should only get called once
	var timesChanged int
	for range callbackCalled {
		timesChanged++
	}
	require.Equal(t, 1, timesChanged)
}

func BenchmarkCacheRead(b *testing.B) {
	const block = cacheBlockSize
	reader := bytes.NewReader([]byte{})
	cache := NewCache()

	for i := 0; i < 1000; i++ {
		randOffset := int64(rand.Intn(10 * block))
		randSize := rand.Intn(2 * block)
		randData := repeatByte('1', randSize)

		cache.Add(randData, randOffset, reader)
	}

	var totalBytes int64

	buf := make([]byte, 2*block)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		randOffset := int64(rand.Intn(10 * block))
		randSize := rand.Intn(2 * block)
		totalBytes += int64(randSize)
		cache.PatchOnto(buf[:randSize], randOffset)
	}

	b.SetBytes(totalBytes / int64(b.N))
}

func BenchmarkCacheWriteRandom(b *testing.B) {
	cache := NewCache()

	rand.Seed(time.Now().Unix())

	var totalBytes int64

	reader := bytes.NewReader([]byte("a"))

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		b.StopTimer()
		randOffset := int64(rand.Intn(50 * 1024 * 1024))
		randSize := rand.Intn(100 * 1024 * 1024)
		randData := repeatByte(2, randSize)
		totalBytes += int64(randSize)
		b.StartTimer()

		cache.Add(randData, randOffset, reader)
		cache.Clear()
	}

	b.SetBytes(totalBytes / int64(b.N))
}

func BenchmarkCacheWriteSequential(b *testing.B) {
	benchSizeCases := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "Size4KB", size: 4 * 1024},
		{desc: "Size16KB", size: 16 * 1024},
		{desc: "Size64KB", size: 64 * 1024},
		{desc: "Size256KB", size: 256 * 1024},
		{desc: "Size1MB", size: 1024 * 1024},
		{desc: "Size4MB", size: 4 * 1024 * 1024},
		{desc: "Size16MB", size: 16 * 1024 * 1024},
		{desc: "Size64MB", size: 64 * 1024 * 1024},
		{desc: "Size256MB", size: 256 * 1024 * 1024, noskip: true},
	}
	benchChunkSizeCases := []struct {
		desc      string
		chunkSize int64
		noskip    bool
	}{
		{desc: "ChunkSize4KB", chunkSize: 4 * 1024},
		{desc: "ChunkSize8KB", chunkSize: 8 * 1024},
		{desc: "ChunkSize16KB", chunkSize: 16 * 1024},
		{desc: "ChunkSize32KB", chunkSize: 32 * 1024},
		{desc: "ChunkSize64KB", chunkSize: 64 * 1024},
		{desc: "ChunkSize128KB", chunkSize: 128 * 1024, noskip: true},
		{desc: "ChunkSize256KB", chunkSize: 256 * 1024},
	}
	for _, bS := range benchSizeCases {
		for _, bC := range benchChunkSizeCases {
			data := blocks.TestData("f", 0, bC.chunkSize)
			reader := bytes.NewReader([]byte("a"))
			chunks := int(bS.size / bC.chunkSize)
			if chunks == 0 {
				chunks = 1
			}
			b.Run(bS.desc+"_"+bC.desc, func(b *testing.B) {
				if testing.Short() && (!bS.noskip || !bC.noskip) {
					b.SkipNow()
				}
				b.SetBytes(bS.size)

				for i := 0; i < b.N; i++ {
					cache := NewCache()
					var offset int64
					for j := 0; j < chunks; j++ {
						cache.Add(data, offset, reader)
						offset += bC.chunkSize
					}
				}
			})
		}
	}
}

func BenchmarkCacheWriteAndClear(b *testing.B) {
	const block = cacheBlockSize

	benchCases := []struct {
		desc   string
		size   int64
		times  int
		noskip bool
	}{
		{desc: "5MB_10Times", size: 5 * 1024 * 1024, times: 10},
		{desc: "10MB_5Times", size: 10 * 1024 * 1024, times: 5, noskip: true},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			reader := bytes.NewReader([]byte{})
			cache := NewCache()

			buf := make([]byte, bench.size)

			b.ResetTimer()
			b.SetBytes(bench.size * int64(bench.times))

			for i := 0; i < b.N; i++ {
				for j := 0; j < bench.times; j++ {
					cache.Add(buf, 0, reader)
					cache.Clear()
				}
			}
		})
	}

}

func fillByte(buf []byte, b byte, size int) {
	for i := 0; i < size; i++ {
		buf[i] = b
	}
}

func repeatByte(b byte, size int) []byte {
	return bytes.Repeat([]byte{b}, size)
}
