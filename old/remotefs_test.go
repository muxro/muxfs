package remotefs

import (
	"bytes"
	"flag"
	fmt "fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"remotefs/blocks"
	"strings"
	"syscall"
	"testing"
	"time"

	"bazil.org/fuse"
	"github.com/google/uuid"
	"go.uber.org/goleak"
	"golang.org/x/net/context"
)

var (
	fuseDebug   bool
	benchmarkOS bool
	cpuprofile  string

	defaultCtx     = context.Background()
	defaultRefRoot = blocks.ByteSliceToArr([]byte(strings.Repeat("a", 32)))
)

type TestFS struct {
	FS                     *FS
	Path                   string
	InitialSnapshotRefRoot blocks.ID
	InitialSnapshotHash    blocks.Hash
	ErrChan                chan error
}

func TestMain(m *testing.M) {
	flag.BoolVar(&fuseDebug, "fusedebug", false, "")
	flag.BoolVar(&benchmarkOS, "benchos", false, "")
	flag.StringVar(&cpuprofile, "fs.cpuprofile", "", "")

	flag.Parse()

	if fuseDebug {
		// Enable fuse debug messages
		fuse.Debug = func(msg interface{}) {
			fmt.Println(msg)
		}
	}

	// os.Exit(m.Run())
	goleak.VerifyTestMain(m)
}

func NewTestFS(options ...TestFSOption) (*TestFS, error) {
	cfg, err := NewTestFSConfig(options...)
	if err != nil {
		return nil, fmt.Errorf("could not create TestFS config instance: %w", err)
	}

	fs, err := NewFS(&FSOptions{
		Context:                 context.Background(),
		UUID:                    fsUUID(uuid.New().String()),
		Store:                   cfg.Store,
		Snapshot:                cfg.Snapshot,
		SnapshotHash:            cfg.SnapshotHash,
		SnapshotRetentionGroups: cfg.SnapshotRetentionGroups,
		Notify:                  &nopNotifier{},
		HasActivityCommit:       cfg.HasActivityCommit,
		HasCacheMeasureCommit:   cfg.HasCacheMeasureCommit,
		MaxCacheSize:            cfg.MaxCacheSize,
	})
	if err != nil {
		return nil, err
	}

	// Add snapshot to updater list, if exists
	if cfg.Snapshot.Meta.Ref != nil {
		err := fs.snapshotUpdater.Set(fs.ctx, []*SnapshotInfo{
			&SnapshotInfo{
				Hash: cfg.SnapshotHash[:],
				Meta: cfg.Snapshot.Meta,
			},
		})
		if err != nil {
			return nil, err
		}
	}

	if !cfg.Readonly {
		err := fs.enableWrites()
		if err != nil {
			return nil, err
		}
	}

	mount, err := fs.Mount(cfg.Mountpoint)
	if err != nil {
		return nil, err
	}

	// TODO: waitUntilMounted
	time.Sleep(100 * time.Millisecond)

	return &TestFS{
		FS:                     fs,
		Path:                   cfg.Mountpoint,
		InitialSnapshotRefRoot: cfg.SnapshotRefRoot,
		InitialSnapshotHash:    cfg.SnapshotHash,
		ErrChan:                mount.Err,
	}, nil
}

// Splits a file into blocks, using random data as the file data
func testFileBlocks(store blocks.Store, ref blocks.ID, name string, size int64) ([]*Block, error) {
	data := blocks.TestData(name, 0, size)

	return blocksFromReader(store, ref, bytes.NewReader(data))
}

func blocksFromReader(store blocks.Store, ref blocks.ID, r io.Reader) ([]*Block, error) {
	chunker := blocks.NewChunker(r)

	buf := make([]byte, blocks.MaxChunkSize)

	var newBlocks []*Block
	for {
		chunk, err := chunker.Next(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}

		err = store.Put(context.Background(), ref, chunk.Hash, bytes.NewReader(chunk.Data))
		if err != nil {
			return nil, err
		}

		block := &Block{
			Offset: int64(chunk.Start),
			Hash:   chunk.Hash[:],
		}

		newBlocks = append(newBlocks, block)
	}

	return newBlocks, nil
}

func testEntries(paths ...string) (entries []*Entry) {
	if len(paths) == 0 {
		paths = append(paths, "/")
	}

	for _, path := range paths {
		entries = append(entries, &Entry{Path: path})
	}
	return
}

func testLinks(pairs ...string) (links []*Link) {
	if len(pairs)%2 != 0 {
		panic("number of pairs must be even")
	}

	for i := 0; i < len(pairs); i += 2 {
		path, target := pairs[i], pairs[i+1]
		links = append(links, &Link{Path: path, Target: target})
	}

	return
}

func (t *TestFS) isOwnershipEqual(filename string, uid, gid uint32) bool {
	f, err := t.Open(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	info, err := f.Stat()
	if err != nil {
		panic(err)
	}
	linuxInfo, ok := info.Sys().(*syscall.Stat_t)
	if !ok {
		panic(err)
	}

	matches := linuxInfo.Uid == uid && linuxInfo.Gid == gid

	if !matches {
		fmt.Printf("ownership doesn't match:\nwanted uid: %v, have: %v\nwanted gid: %v, have gid: %v\n", uid, linuxInfo.Uid, gid, linuxInfo.Gid)
	}

	return matches
}

func (t *TestFS) Open(file string) (*os.File, error) {
	fullpath := filepath.Join(t.Path, file)
	return os.Open(fullpath)
}

func (t *TestFS) OpenFile(file string, flag int, perm os.FileMode) (*os.File, error) {
	fullpath := filepath.Join(t.Path, file)
	return os.OpenFile(fullpath, flag, perm)
}

func (t *TestFS) Create(name string) (*os.File, error) {
	fullpath := filepath.Join(t.Path, name)
	return os.Create(fullpath)
}

func (t *TestFS) Remove(name string) error {
	fullpath := filepath.Join(t.Path, name)
	return os.Remove(fullpath)
}

func (t *TestFS) Mkdir(dirname string, perm os.FileMode) error {
	fullpath := filepath.Join(t.Path, dirname)
	return os.Mkdir(fullpath, perm)
}

func (t *TestFS) Symlink(oldname, newname string) error {
	fulloldpath := filepath.Join(t.Path, oldname)
	fullnewpath := filepath.Join(t.Path, newname)
	return os.Symlink(fulloldpath, fullnewpath)
}

// SymlinkAbsolute creates a symlink to a target not relative to the test FS path
func (t *TestFS) SymlinkAbsolute(oldname, newname string) error {
	fullnewpath := filepath.Join(t.Path, newname)
	return os.Symlink(oldname, fullnewpath)
}

func (t *TestFS) Readlink(name string) (string, error) {
	fullpath := filepath.Join(t.Path, name)
	return os.Readlink(fullpath)
}

func (t *TestFS) ReadDir(dirname string) ([]os.FileInfo, error) {
	fullpath := filepath.Join(t.Path, dirname)
	return ioutil.ReadDir(fullpath)
}

func (t *TestFS) ReadFile(file string) ([]byte, error) {
	fullpath := filepath.Join(t.Path, file)
	return ioutil.ReadFile(fullpath)
}

func (t *TestFS) WriteFile(filename string, data []byte, perm os.FileMode) error {
	fullpath := filepath.Join(t.Path, filename)
	return ioutil.WriteFile(fullpath, data, perm)
}

func (t *TestFS) Stat(name string) (os.FileInfo, error) {
	fullpath := filepath.Join(t.Path, name)
	return os.Stat(fullpath)
}

func (t *TestFS) Lstat(name string) (os.FileInfo, error) {
	fullpath := filepath.Join(t.Path, name)
	return os.Lstat(fullpath)
}

func (t *TestFS) Exec(name string) error {
	fullpath := filepath.Join(t.Path, name)
	cmd := exec.Command(fullpath)
	return cmd.Run()
}

func (t *TestFS) Chmod(name string, mode os.FileMode) error {
	fullpath := filepath.Join(t.Path, name)
	return os.Chmod(fullpath, mode)
}

func (t *TestFS) Chown(name string, uid, gid int) error {
	fullpath := filepath.Join(t.Path, name)
	return os.Chown(fullpath, uid, gid)
}

func (t *TestFS) Rename(oldname, newname string) error {
	fulloldpath := filepath.Join(t.Path, oldname)
	fullnewpath := filepath.Join(t.Path, newname)
	return os.Rename(fulloldpath, fullnewpath)
}

func (t *TestFS) Done() {
	fuse.Unmount(t.Path)
	err := <-t.ErrChan
	if err != nil {
		panic(err)
	}
}

func (t *TestFS) childFile(file string) *FileNode {
	return t.FS.root.children[file].(*FileNode)
}

func (t *TestFS) childDir(dir string) *DirNode {
	return t.FS.root.children[dir].(*DirNode)
}

func (t *TestFS) childSymlink(link string) *SymlinkNode {
	return t.FS.root.children[link].(*SymlinkNode)
}
