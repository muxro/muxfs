package remotefs

import (
	"context"
	"io"
	"io/ioutil"
	"remotefs/blocks"
	"runtime/pprof"

	"math/rand"
	"os"
	"testing"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
)

func TestReadRandomOffsets(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readrandomoffsets", Meta: &EntryMeta{Size: 5 * 1024 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.Open("/test_readrandomoffsets")
	defer f.Close()
	require.Nil(err)

	fi, err := f.Stat()
	require.Nil(err)

	totalSize := int(fi.Size())

	for i := 0; i < 1000; i++ {
		// Get random offset at any point in the file
		offset := rand.Intn(totalSize)

		// Seek to offset
		_, err := f.Seek(int64(offset), io.SeekStart)
		require.Nil(err)

		// Read random amount of bytes from offset
		size := rand.Intn(totalSize - offset)

		data := make([]byte, size)
		n, err := f.Read(data)
		require.Nil(err)

		require.Equal(n, size)

		// Check the data matches
		require.Equal(blocks.TestData("/test_readrandomoffsets", int64(offset), int64(size)), data)
	}
}

func TestReadAll(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	const readAllSize = 8 * 1024 * 1024

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readall", Meta: &EntryMeta{Size: readAllSize}},
		},
	}))
	require.NoError(t, err)
	defer fs.Done()

	data, err := fs.ReadFile("/test_readall")
	require.NoError(t, err)

	require.Equal(t, len(data), readAllSize)
}

func TestReadRandomIntervals(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readrandomintervals", Meta: &EntryMeta{Size: 6 * 1024 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.Open("/test_readrandomintervals")
	defer f.Close()
	require.Nil(err)

	fi, err := f.Stat()
	require.Nil(err)

	totalSize := fi.Size()
	const maxSequenceSize = 150

	var offset int64
	for offset < totalSize {
		// Must be > 0
		seqSize := int64(rand.Intn(maxSequenceSize-1)) + 1

		if seqSize+offset > totalSize {
			seqSize = totalSize - offset
		}

		data := make([]byte, seqSize)
		n, err := f.Read(data)
		require.Nil(err)
		require.Equal(int64(n), seqSize)

		expected := blocks.TestData("/test_readrandomintervals", offset, seqSize)
		require.Equal(data, expected)

		offset += seqSize
	}
}

func TestReadEOF(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readeof", Meta: &EntryMeta{Size: 10}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.Open("/test_readeof")
	require.Nil(err)
	defer f.Close()

	// total file size + 1
	data := make([]byte, 11)
	n, err := f.Read(data)
	require.Equal(nil, err)
	require.Equal(10, n)

	n, err = f.Read(data)
	require.Equal(io.EOF, err)
	require.Equal(0, n)
}

func TestReadSequential(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readsequential", Meta: &EntryMeta{Size: 6 * 1024 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.Open("/test_readsequential")
	defer f.Close()
	require.Nil(err)

	testCases := []struct {
		desc    string
		seqSize int64
	}{
		{desc: "Sequence79", seqSize: 79},
		{desc: "Sequence256", seqSize: 256},
		{desc: "Sequence339", seqSize: 339},
		{desc: "Sequence4096", seqSize: 4096},
		{desc: "Sequence5112", seqSize: 5112},
	}

	fi, err := f.Stat()
	require.Nil(err)

	totalSize := fi.Size()

	for _, tC := range testCases {
		_, err := f.Seek(0, io.SeekStart)
		require.Nil(err)

		var wholeFile []byte

		t.Run(tC.desc, func(t *testing.T) {
			var offset int64
			for {
				if offset+tC.seqSize > totalSize {
					tC.seqSize = totalSize - offset
					if tC.seqSize == 0 {
						break
					}
				}

				data := make([]byte, tC.seqSize)
				n, err := f.Read(data)
				if err != nil {
					if err == io.EOF {
						break
					}
					require.Nil(err)
				}

				require.Equal(int64(n), tC.seqSize)

				expected := blocks.TestData("/test_readsequential", offset, tC.seqSize)
				require.Equal(expected, data)

				offset += int64(n)

				wholeFile = append(wholeFile, data...)
			}

			require.Equal(totalSize, offset)
			require.Equal(blocks.TestData("/test_readsequential", 0, totalSize), wholeFile)
		})
	}
}
func TestReadByteByByte(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readbytebybyte", Meta: &EntryMeta{Size: 1 * 1024}},
		},
	}))
	require.NoError(t, err)
	defer fs.Done()

	f, err := fs.Open("/test_readbytebybyte")
	defer f.Close()
	require.NoError(t, err)

	fi, err := f.Stat()
	require.NoError(t, err)

	totalSize := fi.Size()

	for i := int64(0); i < totalSize; i++ {
		data := make([]byte, 1)
		n, err := f.Read(data)
		if err != nil && err == io.EOF {
			require.Equal(t, i, totalSize-1, "Unexpected EOF")
			break
		}
		require.NoError(t, err)

		// Must have read exactly 1 byte
		require.Equal(t, n, 1)

		expected := blocks.TestData("/test_readbytebybyte", i, 1)
		require.Equal(t, data, expected)
	}
}

// TODO: write
func TestReadWrite(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readwrite", Meta: &EntryMeta{Size: 1 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.OpenFile("/test_readwrite", os.O_RDWR, 0755)
	require.Nil(err)
	defer f.Close()

	data := make([]byte, 500)
	n, err := f.Read(data)
	require.Nil(err)
	require.Equal(500, n)
	require.Equal(blocks.TestData("/test_readwrite", 0, 500), data)

	// rewind
	seek, err := f.Seek(0, io.SeekStart)
	require.Nil(err)
	require.Equal(int64(0), seek)

	n, err = f.Write(blocks.TestData("READWRITE", 0, 500))
	require.Nil(err)
	require.Equal(500, n)

	// rewind
	seek, err = f.Seek(0, io.SeekStart)
	require.Nil(err)
	require.Equal(int64(0), seek)

	data = make([]byte, 500)
	n, err = f.Read(data)
	require.Nil(err)
	require.Equal(500, n)
	require.Equal(blocks.TestData("READWRITE", 0, 500), data)

	// read the last half
	n, err = f.Read(data)
	require.Nil(err)
	require.Equal(500, n)
	require.Equal(blocks.TestData("/test_readwrite", 500, 500), data)
}

func TestReadMultipleHandles(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readmultiple", Meta: &EntryMeta{Size: 1 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f1, err := fs.OpenFile("/test_readmultiple", os.O_RDWR, 0755)
	require.Nil(err)
	f2, err := fs.OpenFile("/test_readmultiple", os.O_RDWR, 0755)
	require.Nil(err)

	defer f1.Close()
	defer f2.Close()

	// node := fs.FS.root.children["test_readmultiple"]

	data1 := make([]byte, 500)
	n, err := f1.Read(data1)
	require.Nil(err)
	require.Equal(500, n)
	require.Equal(blocks.TestData("/test_readmultiple", 0, 500), data1)
	// require.Equal(500, node.pos)

	data2 := make([]byte, 500)
	n, err = f2.Read(data2)
	require.Nil(err)
	require.Equal(500, n)
	require.Equal(blocks.TestData("/test_readmultiple", 0, 500), data2)
	// require.Equal(500, node.pos)

	require.Equal(data1, data2)
}

func TestReadTruncated(t *testing.T) {
	require := require.New(t)
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_trunccache", Meta: &EntryMeta{Size: 7 * 1024 * 1024}},
		},
	}))
	require.Nil(err)
	defer fs.Done()

	f, err := fs.OpenFile("/test_trunccache", os.O_RDWR, 0000)
	require.Nil(err)
	defer f.Close()

	// write bytes at the start of the file
	writeSize := 5
	toWrite := make([]byte, writeSize)
	n, err := f.Write(toWrite)
	require.Nil(err)
	require.Equal(writeSize, n)

	err = f.Truncate(int64(writeSize))
	require.Nil(err)

	_, err = f.Seek(0, io.SeekStart)
	require.Nil(err)

	// try to read more bytes than the file has
	readSize := writeSize * 2
	actual := make([]byte, readSize)
	n, err = io.ReadFull(f, actual)
	require.Equal(io.ErrUnexpectedEOF, err)
	require.Equal(writeSize, n)
}

func BenchmarkOSReadRandomOffsets(b *testing.B) {
	if !benchmarkOS {
		b.SkipNow()
	}

	const size = 8 * 1024 * 1024

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readrandom", Meta: &EntryMeta{Size: size}},
		},
	}))
	if err != nil {
		panic(err)
	}
	defer fs.Done()

	f, err := fs.Open("/test_readrandom")
	defer f.Close()
	if err != nil {
		panic(err)
	}

	fi, err := f.Stat()
	if err != nil {
		panic(err)
	}

	totalSize := int(fi.Size())

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		// Get random offset at any point in the file
		offset := rand.Intn(totalSize)

		// Seek to offset
		_, err := f.Seek(int64(offset), io.SeekStart)
		if err != nil {
			panic(err)
		}

		// Read random amount of bytes from offset
		size := rand.Intn(totalSize - offset)
		b.SetBytes(int64(size))

		data := make([]byte, size)
		_, err = f.Read(data)
		if err != nil && err != io.EOF {
			panic(err)
		}
	}
}

func BenchmarkOSReadOneByte(b *testing.B) {
	if !benchmarkOS {
		b.SkipNow()
	}

	const size = 8 * 1024 * 1024

	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_readone", Meta: &EntryMeta{Size: size}},
		},
	}))
	if err != nil {
		panic(err)
	}
	defer fs.Done()

	f, err := fs.Open("/test_readone")
	defer f.Close()
	if err != nil {
		panic(err)
	}

	b.SetBytes(1)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		data := make([]byte, 1)
		_, err = f.Read(data)
		if err != nil && err != io.EOF {
			panic(err)
		}
	}
}

var cpuprofileFile *os.File

func startProfile(b *testing.B) func() {
	// Stop profiling if "fs.cpuprofile" flag is defined.
	if cpuprofile == "" || cpuprofileFile != nil {
		return func() {}
	}

	pprof.StopCPUProfile()
	var err error
	cpuprofileFile, err = os.OpenFile(cpuprofile, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		panic(err)
	}
	err = pprof.StartCPUProfile(cpuprofileFile)
	if err != nil {
		panic(err)
	}

	return func() {
		pprof.StopCPUProfile()
		err = cpuprofileFile.Close()
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkReadSequential(b *testing.B) {
	fs, _ := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/"},
			&Entry{Path: "/file", Meta: &EntryMeta{Size: 100 * 1024 * 1024}},
		}}),
		Readonly(true),
	)
	defer fs.Done()
	node := fs.FS.root.children["file"].(*FileNode)

	benchSizes := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "Size2MB", size: 2 * 1024 * 1024},
		{desc: "Size25MB", size: 25 * 1024 * 1024},
		{desc: "Size50MB", size: 50 * 1024 * 1024},
		{desc: "Size100MB", size: 100 * 1024 * 1024, noskip: true},
	}
	benchReadSizes := []struct {
		desc     string
		readSize int64
		noskip   bool
	}{
		{desc: "ReadSize4KB", readSize: 4 * 1024},
		{desc: "ReadSize8KB", readSize: 8 * 1024},
		{desc: "ReadSize16KB", readSize: 16 * 1024},
		{desc: "ReadSize32KB", readSize: 32 * 1024},
		{desc: "ReadSize128KB", readSize: 128 * 1024, noskip: true},
	}

	for _, bRS := range benchReadSizes {
		for _, bS := range benchSizes {
			b.Run(bRS.desc+"_"+bS.desc, func(b *testing.B) {
				if testing.Short() && (!bRS.noskip || !bS.noskip) {
					b.SkipNow()
				}
				b.SetBytes(bS.size)

				for i := 0; i < b.N; i++ {
					req := &fuse.ReadRequest{}
					resp := &fuse.ReadResponse{}
					for o := bRS.readSize; o <= bS.size; o += bRS.readSize {
						req.Offset = o
						req.Size = int(bRS.readSize)

						node.Read(context.Background(), req, resp)
					}
				}
			})
		}
	}
}

func BenchmarkReadRandom(b *testing.B) {
	benchCases := []struct {
		desc   string
		size   int64
		noskip bool
	}{
		{desc: "2MB", size: 2 * 1024 * 1024},
		{desc: "25MB", size: 25 * 1024 * 1024},
		{desc: "50MB", size: 50 * 1024 * 1024},
		{desc: "100MB", size: 100 * 1024 * 1024, noskip: true},
	}
	for _, bench := range benchCases {
		b.Run(bench.desc, func(b *testing.B) {
			if testing.Short() && !bench.noskip {
				b.SkipNow()
			}
			fs, _ := NewTestFS(FromSnapshot(&Snapshot{
				Entries: []*Entry{
					&Entry{Path: "/"},
					&Entry{Path: "/file", Meta: &EntryMeta{Size: bench.size}},
				},
			}))
			defer fs.Done()

			node := fs.FS.root.children["file"].(*FileNode)

			b.SetBytes(bench.size)
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				req := &fuse.ReadRequest{}
				resp := &fuse.ReadResponse{}
				for toRead := int64(bench.size); toRead >= 0; toRead -= 128 * 1024 {
					randOffset := int64(rand.Intn(int(bench.size)))
					req.Offset = randOffset
					req.Size = 128 * 1024

					node.Read(context.Background(), req, resp)
				}
			}

			b.StopTimer()
		})
	}
}

func BenchmarkOSOpen(b *testing.B) {
	if !benchmarkOS {
		b.SkipNow()
	}

	const size = 8 * 1024 * 1024
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test_open", Meta: &EntryMeta{Size: size}},
		},
	}))
	if err != nil {
		panic(err)
	}
	defer fs.Done()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		b.StartTimer()
		f, err := fs.Open("/test_open")
		b.StopTimer()

		if err != nil {
			panic(err)
		}
		f.Close()
	}
}

func BenchmarkOSClose(b *testing.B) {
	if !benchmarkOS {
		b.SkipNow()
	}

	const size = 8 * 1024 * 1024
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test", Meta: &EntryMeta{Size: size}},
		},
	}))
	if err != nil {
		panic(err)
	}
	defer fs.Done()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		b.StopTimer()
		f, err := fs.Open("/test")

		if err != nil {
			panic(err)
		}
		b.StartTimer()
		f.Close()
	}
}

func BenchmarkOSStat(b *testing.B) {
	if !benchmarkOS {
		b.SkipNow()
	}

	const size = 8 * 1024 * 1024
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test", Meta: &EntryMeta{Size: size}},
		},
	}))
	if err != nil {
		panic(err)
	}
	defer fs.Done()

	f, err := fs.Open("/test")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		f.Stat()
	}
}

func BenchmarkOSListDir(b *testing.B) {
	if !benchmarkOS {
		b.SkipNow()
	}

	const size = 5 * 1024 * 1024
	fs, err := NewTestFS(FromSnapshot(&Snapshot{
		Entries: []*Entry{
			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
			&Entry{Path: "/test1", Meta: &EntryMeta{Size: size}},
			&Entry{Path: "/test2", Meta: &EntryMeta{Size: size}},
			&Entry{Path: "/test3", Meta: &EntryMeta{Size: size}},
			&Entry{Path: "/test4", Meta: &EntryMeta{Size: size}},
			&Entry{Path: "/test5", Meta: &EntryMeta{Size: size}},
			&Entry{Path: "/test6", Meta: &EntryMeta{Size: size}},
			&Entry{Path: "/test7", Meta: &EntryMeta{Size: size}},
			&Entry{Path: "/test8", Meta: &EntryMeta{Size: size}},
		},
	}))
	if err != nil {
		panic(err)
	}
	defer fs.Done()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		ioutil.ReadDir(fs.Path)
	}
}

// func TestBlockReader(t *testing.T) {
// 	if testing.Short() {
// 		t.SkipNow()
// 	}
// 	fs, err := NewTestFS(FromSnapshot(&Snapshot{
// 		Entries: []*Entry{
// 			&Entry{Path: "/"},
// 			&Entry{Path: "/readcacheshorter", Meta: &EntryMeta{Size: 10 * 1024 * 1024}},
// 		},
// 	})
// 	require.NoError(t, err)
// 	defer fs.Done()

// 	f, err := fs.OpenFile("/readcacheshorter", os.O_RDWR, 0000)
// 	require.NoError(t, err)
// 	defer f.Close()

// 	cacheSize := 50
// 	cacheOffset := 50
// 	readSize := 150

// 	_, err = f.Seek(int64(cacheOffset), io.SeekStart)
// 	require.NoError(t, err)

// 	cacheData := bytes.Repeat([]byte{1}, cacheSize)
// 	wrote, err := f.Write(cacheData)
// 	require.NoError(t, err)
// 	require.Equal(t, cacheSize, wrote)

// 	n := fs.FS.root.children["readcacheshorter"].(*FileNode)

// 	blockReader := newReaderFromReaderAt(n, 0)
// 	actual := make([]byte, readSize)
// 	read, err := blockReader.Read(actual)
// 	require.Equal(t, io.EOF, err)
// 	require.Equal(t, cacheOffset+cacheSize, read)

// 	expected := append(blocks.TestData("/readcacheshorter", 0, int64(cacheOffset)), cacheData...)
// 	expected = append(expected, bytes.Repeat([]byte{0}, readSize-(cacheOffset+cacheSize))...)
// 	require.Equal(t, expected, actual)
// }
