package muxfs

import (
	"context"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/muxro/muxfs/blocks"
)

const testJournalFileSize = 10 * 1024 * 1024

func TestOps(t *testing.T) {
	testOps(t, "CreateDir", func(t *testing.T, fs *FS) {
		n, ok := fs.rootNode.children["dir"]
		require.True(t, ok)
		d, ok := n.(*dir)
		require.True(t, ok)
		expectedMeta := Meta{Size: 0, Mode: uint32(0654 | os.ModeDir), Uid: 1000, Gid: 100}
		require.Equal(t, expectedMeta.Size, d.meta.Size)
		require.Equal(t, expectedMeta.Mode, d.meta.Mode)
		require.Equal(t, expectedMeta.Uid, d.meta.Uid)
		require.Equal(t, expectedMeta.Gid, d.meta.Gid)

		// Already exists
		err := fs.CreateDir(context.TODO(), "/dir", Meta{})
		_, ok = err.(NodeAlreadyExistsError)
		require.True(t, ok)

		// Destination dir doesn't exist
		err = fs.CreateDir(context.TODO(), "/notexist/dir", Meta{})
		_, ok = err.(*NodeNotFoundError)
		require.True(t, ok)

		// Destination dir is not a dir
		err = fs.CreateDir(context.TODO(), "/notdir/dir", Meta{})
		require.Equal(t, ErrNodeNotDir, err)
	},
		&opCreateDir{
			Path: "/dir",
			Meta: Meta{Mode: uint32(0654 | os.ModeDir), Uid: 1000, Gid: 100},
		},
		&opCreateFile{
			Path: "/notdir",
			Meta: Meta{Mode: 0654, Uid: 1000, Gid: 100},
		})

	testOps(t, "CreateFile", func(t *testing.T, fs *FS) {
		n, ok := fs.rootNode.children["file"]
		require.True(t, ok)
		f, ok := n.(*file)
		require.True(t, ok)
		expectedMeta := Meta{Size: 666, Mode: 0654, Uid: 1000, Gid: 100}
		require.Equal(t, expectedMeta.Size, f.meta.Size)
		require.Equal(t, expectedMeta.Mode, f.meta.Mode)
		require.Equal(t, expectedMeta.Uid, f.meta.Uid)
		require.Equal(t, expectedMeta.Gid, f.meta.Gid)

		// Already exists
		err := fs.CreateFile(context.TODO(), "/file", Meta{})
		_, ok = err.(NodeAlreadyExistsError)
		require.True(t, ok)

		// Destination dir doesn't exist
		err = fs.CreateFile(context.TODO(), "/notexist/file", Meta{})
		_, ok = err.(*NodeNotFoundError)
		require.True(t, ok)

		// Destination dir is not a dir
		err = fs.CreateFile(context.TODO(), "/notdir/file", Meta{})
		require.Equal(t, ErrNodeNotDir, err)
	},
		&opCreateFile{
			Path: "/file",
			Meta: Meta{Size: 666, Mode: 0654, Uid: 1000, Gid: 100},
		},
		&opCreateFile{
			Path: "/notdir",
			Meta: Meta{Mode: 0654, Uid: 1000, Gid: 100},
		})

	testOps(t, "CreateLink", func(t *testing.T, fs *FS) {
		n, ok := fs.rootNode.children["link"]
		require.True(t, ok)
		f, ok := n.(*link)
		require.True(t, ok)
		expectedMeta := Meta{Size: int64(len("/target")), Mode: uint32(0777 | os.ModeSymlink), Uid: 1000, Gid: 100}
		require.Equal(t, expectedMeta.Size, f.meta.Size)
		require.Equal(t, expectedMeta.Mode, f.meta.Mode)
		require.Equal(t, expectedMeta.Uid, f.meta.Uid)
		require.Equal(t, expectedMeta.Gid, f.meta.Gid)

		// Already exists
		err := fs.CreateLink(context.TODO(), "/link", "/target", Meta{})
		_, ok = err.(NodeAlreadyExistsError)
		require.True(t, ok)

		// Destination dir doesn't exist
		err = fs.CreateLink(context.TODO(), "/notexist/link", "/target", Meta{})
		_, ok = err.(*NodeNotFoundError)
		require.True(t, ok)

		// Destination dir is not a dir
		err = fs.CreateLink(context.TODO(), "/notdir/link", "/target", Meta{})
		require.Equal(t, ErrNodeNotDir, err)
	},
		&opCreateLink{
			Path:   "/link",
			Target: "/target",
			Meta:   Meta{Uid: 1000, Gid: 100},
		},
		&opCreateFile{
			Path: "/notdir",
			Meta: Meta{Mode: 0654, Uid: 1000, Gid: 100},
		})

	testOps(t, "SetMeta", func(t *testing.T, fs *FS) {
		file := fs.rootNode.children["file"].(*file)
		expFileMeta := Meta{Size: 666, Mode: 0654, Uid: 1000, Gid: 100}
		require.Equal(t, expFileMeta.Size, file.meta.Size)
		require.Equal(t, expFileMeta.Mode, file.meta.Mode)
		require.Equal(t, expFileMeta.Uid, file.meta.Uid)
		require.Equal(t, expFileMeta.Gid, file.meta.Gid)

		dir := fs.rootNode.children["dir"].(*dir)
		expDirMeta := Meta{Size: 0, Mode: uint32(0654 | os.ModeDir), Uid: 1000, Gid: 100}
		require.Equal(t, expDirMeta.Size, dir.meta.Size)
		require.Equal(t, expDirMeta.Mode, dir.meta.Mode)
		require.Equal(t, expDirMeta.Uid, dir.meta.Uid)
		require.Equal(t, expDirMeta.Gid, dir.meta.Gid)

		link := fs.rootNode.children["link"].(*link)
		expLinkMeta := Meta{Size: int64(len("/target")), Uid: 1000, Gid: 100}
		require.Equal(t, expLinkMeta.Size, link.meta.Size)
		require.Equal(t, expLinkMeta.Uid, link.meta.Uid)
		require.Equal(t, expLinkMeta.Gid, link.meta.Gid)
	},
		&opCreateFile{
			Path: "/file",
			Meta: Meta{Size: 1, Mode: 0755, Uid: 2, Gid: 3},
		},
		&opCreateDir{
			Path: "/dir",
			Meta: Meta{Mode: uint32(0755 | os.ModeDir), Uid: 2, Gid: 3},
		},
		&opCreateLink{
			Path:   "/link",
			Target: "/target",
			Meta:   Meta{Mode: 0755, Uid: 2, Gid: 3},
		},
		&opSetMeta{
			Path:  "/file",
			Valid: SetMetaSize | SetMetaMode | SetMetaUid | SetMetaGid,
			Meta:  Meta{Size: 666, Mode: 0654, Uid: 1000, Gid: 100},
		},
		&opSetMeta{
			Path:  "/dir",
			Valid: SetMetaMode | SetMetaUid | SetMetaGid,
			Meta:  Meta{Mode: uint32(0654 | os.ModeDir), Uid: 1000, Gid: 100},
		},
		&opSetMeta{
			Path:  "/link",
			Valid: SetMetaUid | SetMetaGid,
			Meta:  Meta{Uid: 1000, Gid: 100},
		})

	testOps(t, "Rename", func(t *testing.T, fs *FS) {
		testRenamed := func(oldpath, newpath string, kind EntryType) {
			// Old node must not exist
			n, err := fs.findNode(oldpath)
			_, ok := err.(*NodeNotFoundError)
			require.True(t, ok)
			// New node must exist
			n, err = fs.findNode(newpath)
			require.NoError(t, err)
			// Type must match
			switch kind {
			case EntryFile:
				_, ok := n.(*file)
				require.True(t, ok)
			case EntryDir:
				_, ok := n.(*dir)
				require.True(t, ok)
			case EntryLink:
				_, ok := n.(*link)
				require.True(t, ok)
			}
		}

		testRenamed("/dir", "/renameddir", EntryDir)
		testRenamed("/file", "/renamedfile", EntryFile)
		testRenamed("/link", "/renamedlink", EntryLink)
		testRenamed("/nested/nesteddir", "/renamednesteddir", EntryDir)
		testRenamed("/nested/nestedfile", "/renamednestedfile", EntryFile)
		testRenamed("/nested/nestedlink", "/renamednestedlink", EntryLink)

		// File to be renamed doesn't exist
		err := fs.Rename(context.TODO(), "/notexist", "/file")
		_, ok := err.(*NodeNotFoundError)
		require.True(t, ok)

		// Source dir doesn't exist
		err = fs.Rename(context.TODO(), "/notexist/file", "/file")
		_, ok = err.(*NodeNotFoundError)
		require.True(t, ok)

		// Destination dir doesn't exist
		err = fs.Rename(context.TODO(), "/renamedfile", "/notexist/file")
		_, ok = err.(*NodeNotFoundError)
		require.True(t, ok)

		// Destination dir is not a dir
		err = fs.Rename(context.TODO(), "/renamedfile", "/notdir/file")
		require.Equal(t, ErrNodeNotDir, err)

		// New name is too long
		nameTooLong := strings.Repeat("a", 256)
		err = fs.Rename(context.TODO(), "/renamedfile", nameTooLong)
		require.Equal(t, ErrNameTooLong, err)

		// New path is too long
		// Create nested dirs, of which the deepest one will have the path length close to 4096
		// so we can then rename another file to have > 4096 path length
		const repeatPathTimes = 16 // 16 * 255 + 1 = 4081
		var fullpath string
		for i := 1; i <= repeatPathTimes; i++ {
			name := strings.Repeat("a", 254)
			path := strings.Repeat("/"+name, i)
			fullpath = path

			err := fs.CreateDir(context.TODO(), path, Meta{})
			require.NoError(t, err)
		}

		pathTooLong := fullpath + strings.Repeat("a", 255)
		err = fs.Rename(context.TODO(), "/renamedfile", pathTooLong)
		require.Equal(t, ErrNameTooLong, err)
	},
		// Create file to rename
		&opCreateFile{
			Path: "/file",
			Meta: Meta{Mode: 0654, Uid: 1000, Gid: 100},
		},
		// Create dir to rename
		&opCreateDir{
			Path: "/dir",
			Meta: Meta{Mode: uint32(0654 | os.ModeDir), Uid: 1000, Gid: 100},
		},
		// Create link to rename
		&opCreateLink{
			Path:   "/link",
			Target: "/target",
			Meta:   Meta{Mode: 0654, Uid: 1000, Gid: 100},
		},
		// Create nested dir
		&opCreateDir{
			Path: "/nested",
			Meta: Meta{Mode: uint32(0654 | os.ModeDir), Uid: 1000, Gid: 100},
		},
		// Create nested file to rename
		&opCreateFile{
			Path: "/nested/nestedfile",
			Meta: Meta{Mode: 0654, Uid: 1000, Gid: 100},
		},
		// Create nested dir to rename
		&opCreateDir{
			Path: "/nested/nesteddir",
			Meta: Meta{Mode: uint32(0654 | os.ModeDir), Uid: 1000, Gid: 100},
		},
		// Create nested link to rename
		&opCreateLink{
			Path:   "/nested/nestedlink",
			Target: "/target",
			Meta:   Meta{Mode: 0654, Uid: 1000, Gid: 100},
		},
		// Create file that is not dir
		&opCreateFile{
			Path: "/notdir",
			Meta: Meta{Mode: 0654, Uid: 1000, Gid: 100},
		},
		// Do rename operations
		&opRename{
			Path:    "/dir",
			NewPath: "/renameddir",
		},
		&opRename{
			Path:    "/file",
			NewPath: "/renamedfile",
		},
		&opRename{
			Path:    "/link",
			NewPath: "/renamedlink",
		},
		&opRename{
			Path:    "/nested/nesteddir",
			NewPath: "/renamednesteddir",
		},
		&opRename{
			Path:    "/nested/nestedfile",
			NewPath: "/renamednestedfile",
		},
		&opRename{
			Path:    "/nested/nestedlink",
			NewPath: "/renamednestedlink",
		})

	testOps(t, "Remove", func(t *testing.T, fs *FS) {
		_, ok := fs.rootNode.children["dir"]
		require.False(t, ok)

		_, ok = fs.rootNode.children["file"]
		require.False(t, ok)

		// Dir doesn't exist
		err := fs.Remove(context.TODO(), "/notexist/file")
		_, ok = err.(*NodeNotFoundError)
		require.True(t, ok)

		// File to remove doesn't exist
		err = fs.Remove(context.TODO(), "/notexist")
		_, ok = err.(*NodeNotFoundError)
		require.True(t, ok)
	},
		&opCreateFile{
			Path: "/file",
			Meta: Meta{Size: 666, Mode: 0654, Uid: 1000, Gid: 100},
		},
		&opCreateDir{
			Path: "/dir",
			Meta: Meta{Mode: uint32(0654 | os.ModeDir), Uid: 1000, Gid: 100},
		},
		&opRemove{
			Path: "/file",
		},
		&opRemove{
			Path: "/dir",
		})
}

func testOps(t *testing.T, name string, check func(t *testing.T, fs *FS), ops ...operation) {
	fs := CreateTestFS(t, blocks.NewMemoryStore(""))

	t.Run(name, func(t *testing.T) {
		err := fs.applyOps(context.TODO(), ops...)
		require.NoError(t, err)

		check(t, fs)
	})
}

// func TestJournal(t *testing.T) {
// 	ctx := context.TODO()
// 	expectedFS := CreateTestFS(t, blocks.NewMemoryStore(""))

// 	// Do some ops
// 	err := expectedFS.CreateDir(ctx, "/dir", Meta{
// 		Mode: uint32(0755 | os.ModeDir),
// 		Uid:  1000, Gid: 100,
// 	})
// 	require.NoError(t, err)

// 	// Apply ops on a blank FS
// 	fs := CreateTestFS(t, blocks.NewMemoryStore(""))
// 	err = fs.applyOps(ctx, expectedFS.journal.getOps()...)
// 	require.NoError(t, err)

// 	// The only difference should be the journal, so we make them the same
// 	expectedFS.journal = newJournal()

// 	// FSs should be the same
// 	require.Equal(t, expectedFS, fs)
// }

func TestJournalOpDataEncoding(t *testing.T) {
	// List of op pairs: [opToEncode1, emptyOp1, ...]
	ops := []operation{
		&opCreateDir{Path: "/dirpath", Meta: testMeta()}, &opCreateDir{},
		&opCreateFile{Path: "/filepath", Meta: testMeta()}, &opCreateFile{},
		&opCreateLink{Path: "/linkpath", Meta: testMeta(), Target: "/target"}, &opCreateLink{},
		&opSetMeta{Path: "/setmetapath", Meta: testMeta(), Valid: SetMetaValid(6)}, &opSetMeta{},
		&opRename{Path: "/renamepath", NewPath: "/newpath"}, &opRename{},
		&opRemove{Path: "/removepath"}, &opRemove{},
	}

	for i := 0; i < len(ops); i += 2 {
		op := ops[i]
		emptyOp := ops[i+1]

		t.Run(op.kv()["op"].(string), func(t *testing.T) {
			// Encode op into buffer
			b := make([]byte, op.size())
			op.encode(b)
			// Decode buffer into emptyOp
			emptyOp.decode(b)
			require.Equal(t, op, emptyOp)
		})
	}
}

func TestJournalOps(t *testing.T) {
	// List of op pairs: [opToEncode1, emptyOp1, ...]
	ops := []operation{
		&opCreateDir{Path: "/dirpath", Meta: testMeta()}, &opCreateDir{},
		&opCreateFile{Path: "/filepath", Meta: testMeta()}, &opCreateFile{},
		&opCreateLink{Path: "/linkpath", Meta: testMeta(), Target: "/target"}, &opCreateLink{},
		&opSetMeta{Path: "/setmetapath", Meta: testMeta(), Valid: SetMetaValid(6)}, &opSetMeta{},
		&opRename{Path: "/renamepath", NewPath: "/newpath"}, &opRename{},
		&opRemove{Path: "/removepath"}, &opRemove{},
	}

	tmpfile, err := ioutil.TempFile("", "journal")
	require.NoError(t, err)
	path := tmpfile.Name()
	tmpfile.Close()

	var snaphash blocks.Hash

	j, err := newJournal(path, testJournalFileSize, snaphash)
	require.NoError(t, err)

	for i := 0; i < len(ops); i += 2 {
		err := j.write(context.TODO(), ops[i])
		require.NoError(t, err)
	}
	err = j.close()
	require.NoError(t, err)

	jr, err := newJournalReader(path, snaphash)
	require.NoError(t, err)

	for i := 0; i < len(ops); i += 2 {
		op, err := jr.read(context.TODO())
		require.NoError(t, err)
		require.Equal(t, ops[i], op)
	}
	jr.close()
}

func TestJournalEncodingHeader(t *testing.T) {
	tmpfile, err := ioutil.TempFile("", "journal")
	require.NoError(t, err)
	path := tmpfile.Name()
	tmpfile.Close()

	var snaphash blocks.Hash

	j, err := newJournal(path, testJournalFileSize, snaphash)
	require.NoError(t, err)

	err = j.close()
	require.NoError(t, err)

	jr, err := newJournalReader(path, snaphash)
	require.NoError(t, err)
	err = jr.close()
	require.NoError(t, err)
}

// func TestOpEncoding(t *testing.T) {
// 	ctx := context.TODO()

// 	j := newJournal()

// 	op := &opCreateDir{Path: "/path", Meta: testMeta()}
// 	err := j.write(ctx, op)
// 	require.NoError(t, err)

// 	decodedOp, err := j.read(ctx)
// 	require.NoError(t, err)

// 	require.Equal(t, op, decodedOp)

// 	// EOF
// 	nilop, err := j.read(ctx)
// 	require.Equal(t, io.EOF, err)
// 	require.Nil(t, nilop)
// }

func TestJournalEmpty(t *testing.T) {
	tmpfile, err := ioutil.TempFile("", "journal")
	require.NoError(t, err)
	path := tmpfile.Name()
	tmpfile.Close()

	var snaphash blocks.Hash

	j, err := newJournal(path, testJournalFileSize, snaphash)
	require.NoError(t, err)

	writtenOp := &opCreateDir{Path: "/path", Meta: testMeta()}
	err = j.write(context.TODO(), writtenOp)
	require.NoError(t, err)
	err = j.close()

	jr, err := newJournalReader(path, snaphash)
	require.NoError(t, err)
	defer jr.close()
	empty, err := jr.empty(context.TODO())
	require.NoError(t, err)
	require.False(t, empty)

	_, err = jr.read(context.TODO())
	require.NoError(t, err)

	empty, err = jr.empty(context.TODO())
	require.NoError(t, err)
	require.True(t, empty)

	// Read when empty
	_, err = jr.read(context.TODO())
	require.Error(t, err)
}

func TestJournalReset(t *testing.T) {
	tmpfile, err := ioutil.TempFile("", "journal")
	require.NoError(t, err)
	path := tmpfile.Name()
	tmpfile.Close()

	var snaphash blocks.Hash

	// Write op to journal, then close.
	j, err := newJournal(path, testJournalFileSize, snaphash)
	require.NoError(t, err)
	writtenOp := &opCreateDir{Path: "/path", Meta: testMeta()}
	err = j.write(context.TODO(), writtenOp)
	require.NoError(t, err)
	err = j.close()

	// Open same journal, reset (which resets), then try to read an operation: it should be empty.
	j, err = newJournal(path, testJournalFileSize, snaphash)
	require.NoError(t, err)
	err = j.close()
	require.NoError(t, err)

	jr, err := newJournalReader(path, snaphash)
	require.NoError(t, err)
	defer jr.close()

	empty, err := jr.empty(context.TODO())
	require.NoError(t, err)
	require.True(t, empty)

	_, err = jr.read(context.TODO())
	require.Error(t, err)
}

func testMeta() Meta {
	return Meta{Size: 1, Mode: 2, Uid: 3, Gid: 4, UpdatedAt: 5}
}

func BenchmarkJournalWrite(b *testing.B) {
	tmpfile, _ := ioutil.TempFile("", "journal")
	path := tmpfile.Name()
	tmpfile.Close()

	var snaphash blocks.Hash

	j, _ := newJournal(path, testJournalFileSize, snaphash)
	defer j.close()

	op := &opCreateDir{Path: "/path", Meta: testMeta()}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		j.write(context.TODO(), op)
	}
}

func BenchmarkJournalRead(b *testing.B) {
	tmpfile, _ := ioutil.TempFile("", "journal")
	path := tmpfile.Name()
	tmpfile.Close()

	var snaphash blocks.Hash

	j, _ := newJournal(path, testJournalFileSize, snaphash)

	op := &opCreateDir{Path: "/path", Meta: testMeta()}

	for i := 0; i < b.N; i++ {
		j.write(context.TODO(), op)
	}
	j.close()

	jr, _ := newJournalReader(path, snaphash)
	defer jr.close()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		jr.read(context.TODO())
	}
}
