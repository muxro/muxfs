package muxfs

// func TestVerify(t *testing.T) {
// 	store := blocks.NewMemoryStore("")
// 	fs := CreateTestFS(t, store)
// 	Verify(context.TODO())
// }

// func TestFSVerify(t *testing.T) {
// 	testCases := []struct {
// 		desc         string
// 		paths        []string
// 		runBefore    func(*testing.T, *TestFS)
// 		checkHashes  bool
// 		allSnapshots bool
// 		verify       func(*testing.T, chan verifyResult)
// 	}{
// 		{
// 			desc:         "EmptyFS",
// 			paths:        []string{"/"},
// 			checkHashes:  false,
// 			allSnapshots: false,
// 			runBefore:    nil,
// 			verify: func(t *testing.T, replies chan verifyResult) {
// 				var got []verifyResult
// 				for reply := range replies {
// 					got = append(got, reply)
// 				}
// 				require.Len(t, got, 1)
// 				require.Equal(t, errVerifyEmptyFS, got[0].Err)
// 			},
// 		},
// 		{
// 			desc:         "Default",
// 			paths:        []string{"/", "/file"},
// 			checkHashes:  false,
// 			allSnapshots: false,
// 			runBefore:    nil,
// 			verify: func(t *testing.T, replies chan verifyResult) {
// 				var got []verifyResult
// 				for reply := range replies {
// 					got = append(got, reply)
// 				}
// 				// Need to get 1 reply for the snapshot block, and 1 reply for the file block
// 				require.Len(t, got, 2)
// 				for _, g := range got {
// 					require.NoError(t, g.Err)
// 					require.NotZero(t, g.Message)
// 					require.NotZero(t, g.Percent)
// 					require.NotZero(t, g.ETA)
// 				}
// 			},
// 		},
// 		{
// 			desc:         "CheckHashes",
// 			paths:        []string{"/", "/file"},
// 			checkHashes:  true,
// 			allSnapshots: false,
// 			runBefore:    nil,
// 			verify: func(t *testing.T, replies chan verifyResult) {
// 				var got []verifyResult
// 				for reply := range replies {
// 					got = append(got, reply)
// 				}
// 				// Need to get 1 reply for the snapshot block, and 1 reply for the file block
// 				require.Len(t, got, 2)
// 				for _, g := range got {
// 					require.NoError(t, g.Err)
// 					require.NotZero(t, g.Message)
// 					require.NotZero(t, g.Percent)
// 					require.NotZero(t, g.ETA)
// 				}
// 			},
// 		},
// 		{
// 			desc:         "AllSnapshots",
// 			paths:        []string{"/", "/file"},
// 			checkHashes:  false,
// 			allSnapshots: true,
// 			runBefore: func(t *testing.T, fs *TestFS) {
// 				// Make new snapshot
// 				f, err := fs.OpenFile("/file", os.O_WRONLY, 0755)
// 				require.NoError(t, err)
// 				defer f.Close()
// 				_, err = f.Write([]byte("trigger change"))
// 				require.NoError(t, err)
// 				require.NoError(t, fs.FS.save(manualRetentionGroup))
// 			},
// 			verify: func(t *testing.T, replies chan verifyResult) {
// 				var got []verifyResult
// 				for reply := range replies {
// 					got = append(got, reply)
// 				}
// 				// Need to get 2 replies for the snapshot blocks, and 2 replies for the file blocks
// 				require.Len(t, got, 4)
// 				for _, g := range got {
// 					require.NoError(t, g.Err)
// 					require.NotZero(t, g.Message)
// 					require.NotZero(t, g.Percent)
// 					require.NotZero(t, g.ETA)
// 				}
// 			},
// 		},
// 		{
// 			desc:         "MissingSnapshotBlock",
// 			paths:        []string{"/", "/file"},
// 			checkHashes:  false,
// 			allSnapshots: false,
// 			runBefore: func(t *testing.T, fs *TestFS) {
// 				infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
// 				require.NoError(t, err)
// 				require.Len(t, infos, 1)
// 				hash := blocks.HashFromSlice(infos[0].Hash)
// 				ref := blocks.IDFromByteSlice(fs.FS.snapshot.Meta.Ref)

// 				require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, ref, []blocks.Hash{hash}))
// 			},
// 			verify: func(t *testing.T, replies chan verifyResult) {
// 				var got []verifyResult
// 				for reply := range replies {
// 					got = append(got, reply)
// 				}
// 				// Need to get 1 error for the snapshot block
// 				require.Len(t, got, 1)
// 				g := got[0]
// 				require.True(t, errors.Is(g.Err, blocks.ErrBlockNotFound))
// 			},
// 		},
// 		{
// 			desc:         "MissingFileBlock",
// 			paths:        []string{"/", "/file"},
// 			checkHashes:  false,
// 			allSnapshots: false,
// 			runBefore: func(t *testing.T, fs *TestFS) {
// 				infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
// 				require.NoError(t, err)
// 				require.Len(t, infos, 1)
// 				ref := blocks.IDFromByteSlice(fs.FS.snapshot.Meta.Ref)

// 				for _, e := range fs.FS.snapshot.Entries {
// 					if e.Path != "/file" {
// 						continue
// 					}

// 					hash := blocks.HashFromSlice(e.Blocks[0].Hash)

// 					require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, ref, []blocks.Hash{hash}))
// 					require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, fs.FS.ref, []blocks.Hash{hash}))
// 					break
// 				}
// 			},
// 			verify: func(t *testing.T, replies chan verifyResult) {
// 				var got []verifyResult
// 				for reply := range replies {
// 					got = append(got, reply)
// 				}
// 				// Need to get 1 error for the file block
// 				for _, g := range got {
// 					if g.Err != nil {
// 						require.True(t, errors.Is(g.Err, blocks.ErrBlockNotFound))
// 					}
// 				}
// 			},
// 		},
// 		{
// 			desc:         "CorruptSnapshotBlock",
// 			paths:        []string{"/", "/file"},
// 			checkHashes:  true,
// 			allSnapshots: false,
// 			runBefore: func(t *testing.T, fs *TestFS) {
// 				infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
// 				require.NoError(t, err)
// 				require.Len(t, infos, 1)
// 				hash := blocks.HashFromSlice(infos[0].Hash)
// 				ref := blocks.IDFromByteSlice(fs.FS.snapshot.Meta.Ref)

// 				// Make a copy of the current snapshot, and modify somthing.
// 				corruptSnap := *fs.FS.snapshot
// 				corruptSnap.Meta.Tag = "corrupt"
// 				// Get the encoded "corrupted" snapshot data.
// 				corruptData, err := corruptSnap.data()
// 				require.NoError(t, err)

// 				// Remove the current (valid) snapshot block from the store.
// 				require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, ref, []blocks.Hash{hash}))
// 				// Add the corrupt snapshot block to the same hash.
// 				require.NoError(t, fs.FS.store.Put(defaultCtx, ref, hash, bytes.NewReader(corruptData)))
// 			},
// 			verify: func(t *testing.T, replies chan verifyResult) {
// 				var got []verifyResult
// 				for reply := range replies {
// 					got = append(got, reply)
// 				}

// 				// Need to get 1 error for the snapshot hash mismatch
// 				var hasHashMismatch bool
// 				for _, g := range got {
// 					var hashErr hashMismatchError
// 					if errors.As(g.Err, &hashErr) {
// 						hasHashMismatch = true
// 						break
// 					}
// 				}
// 				require.True(t, hasHashMismatch)
// 			},
// 		},
// 		{
// 			desc:         "CorruptFileData",
// 			paths:        []string{"/", "/file"},
// 			checkHashes:  true,
// 			allSnapshots: false,
// 			runBefore: func(t *testing.T, fs *TestFS) {
// 				infos, err := fs.FS.snapshotUpdater.Get(fs.FS.ctx)
// 				require.NoError(t, err)
// 				require.Len(t, infos, 1)
// 				ref := blocks.IDFromByteSlice(fs.FS.snapshot.Meta.Ref)

// 				for _, e := range fs.FS.snapshot.Entries {
// 					if e.Path != "/file" {
// 						continue
// 					}

// 					// Remove first file block from store, and re-add to the same hash, but with different data.
// 					hash := blocks.HashFromSlice(e.Blocks[0].Hash)
// 					require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, ref, []blocks.Hash{hash}))
// 					require.NoError(t, fs.FS.store.RemoveRefs(defaultCtx, fs.FS.ref, []blocks.Hash{hash}))
// 					require.NoError(t, fs.FS.store.Put(defaultCtx, ref, hash, bytes.NewReader([]byte("corrupt data"))))
// 				}
// 			},
// 			verify: func(t *testing.T, replies chan verifyResult) {
// 				var got []verifyResult
// 				for reply := range replies {
// 					got = append(got, reply)
// 				}

// 				// Check that we have at least one hash mismatch error
// 				var hasHashMismatch bool
// 				for _, g := range got {
// 					var hashErr hashMismatchError
// 					if errors.As(g.Err, &hashErr) {
// 						hasHashMismatch = true
// 						break
// 					}
// 				}
// 				require.True(t, hasHashMismatch)
// 			},
// 		},
// 	}
// 	for _, tc := range testCases {
// 		t.Run(tc.desc, func(t *testing.T) {
// 			fs, err := NewTestFS(FromPaths(tc.paths...))
// 			require.NoError(t, err)
// 			defer fs.Done()

// 			if tc.runBefore != nil {
// 				tc.runBefore(t, fs)
// 			}

// 			replies := fs.FS.verify(tc.checkHashes, tc.allSnapshots)
// 			tc.verify(t, replies)
// 		})
// 	}
// }
