package muxfs

import (
	"context"
	"io"

	"gitlab.com/muxro/muxfs/blocks"
	pb "gitlab.com/muxro/muxfs/muxfs/internal/pb"
)

// TODO: ugly
type blockReader struct {
	ctx             context.Context
	store           blocks.Store
	blocks          []*pb.Block
	blocksLocations []blocksLocation
	size            int64
}

// TODO: ugly
// TODO: if something is broken remember that i changed []pb.Block to [][]*pb.Block
func newBlockReader(ctx context.Context, store blocks.Store, allFileBlocks [][]*pb.Block, blocksLocations []blocksLocation, size int64) *blockReader {
	var blocks []*pb.Block
	for _, loc := range blocksLocations {
		if loc.BlockList == 0 {
			blocks = append(blocks, allFileBlocks[0]...)
			continue
		}
		blocks = append(blocks, allFileBlocks[loc.BlockList]...)
	}

	return &blockReader{
		ctx:             ctx,
		store:           store,
		blocks:          blocks,
		blocksLocations: blocksLocations,
		size:            size,
	}
}

func (br *blockReader) ReadAt(p []byte, off int64) (n int, err error) {
	for i, block := range br.blocks {
		blockOff := off + int64(n)

		// Determine how much we need to read from current block
		var readLen int64
		if i < len(br.blocks)-1 {
			nextBlock := br.blocks[i+1]

			// TODO: @performance make this a binary search
			// blockOff is not in the current block
			if nextBlock.Offset <= blockOff {
				continue
			}

			readLen = nextBlock.Offset - blockOff
		} else {
			readLen = br.size - blockOff
		}

		// nothing left to read
		if readLen <= 0 {
			break
		}

		// get block from store
		reader, err := br.store.Get(br.ctx, blocks.HashFromSlice(block.Hash))
		if err != nil {
			return n, err
		}

		// blockOff is not at the start of the block, we need to skip some data
		if blockOff > block.Offset {
			err = reader.Skip(br.ctx, blockOff-block.Offset)
			if err != nil {
				return n, err
			}
		}

		// compute how much of this block we need to read
		readLimit := n + int(readLen)
		if readLimit > len(p) {
			readLimit = len(p)
		}

		rn, err := io.ReadFull(blocks.Reader(br.ctx, reader), p[n:readLimit])
		// TODO: P3 retry block if we got an unexpected EOF
		n += rn
		reader.Close()
		if err != nil {
			return n, err
		}

		if n == len(p) {
			break
		}
	}

	return
}
