package muxfs

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/golang/protobuf/proto"
	"gitlab.com/go-figure/logr"
	"gitlab.com/muxro/muxfs/blocks"
	pb "gitlab.com/muxro/muxfs/muxfs/internal/pb"
)

const (
	EntryFile EntryType = 0
	EntryDir  EntryType = iota
	EntryLink
)

var (
	errMergeFileAlreadyExists = errors.New("a file with the same name already exists in the target merge tree")
	errMergeFileWithDir       = errors.New("trying to merge a directory with a file")
)

func defaultRoot() dir {
	return dir{
		meta:     Meta{Mode: uint32(0777 | os.ModeDir), Uid: 1000, Gid: 100},
		children: make(map[string]node),
	}
}

const (
	journalFileSize = 500 * 1024 * 1024
)

type FS struct {
	store      blocks.Store
	journal    journal
	snapshots  []SnapshotInfo
	rootNode   *dir
	fileBlocks [][]*pb.Block
}

type DirEntry struct {
	Name string
	Type EntryType
}

type EntryType int

func New(ctx context.Context, store blocks.Store) (*FS, error) {
	fs := newFS(store)
	// Copy defaultRoot
	root := defaultRoot()
	fs.rootNode = &root
	return fs, nil
}

func FromBlock(ctx context.Context, store blocks.Store, hash blocks.Hash) (*FS, error) {
	fs := newFS(store)

	fsBlock, err := fetchFSBlock(ctx, store, hash)
	if err != nil {
		return nil, err
	}

	fs.snapshots = fsBlock.Snapshots
	latestSnap := fsBlock.LatestSnapshot()
	err = fs.initialize(ctx, store, latestSnap.Hash)
	return fs, err
}

func FromSnapshot(ctx context.Context, store blocks.Store, snapHash blocks.Hash) (*FS, error) {
	fs := newFS(store)

	err := fs.initialize(ctx, store, snapHash)
	if err != nil {
		return nil, err
	}

	return fs, nil
}

func newFS(store blocks.Store) *FS {
	return &FS{
		// rootNode is set either in muxfs.New (if instancing a blank FS), or
		// in fs.initialize, when instancing from a Snapshot.
		rootNode: nil,
		store:    store,
	}
}

func (fs *FS) initialize(ctx context.Context, store blocks.Store, snaphash blocks.Hash) error {
	// Fetch snapshot
	snap, err := fetchSnapshot(ctx, store, snaphash)
	if err != nil {
		logr.FromContext(ctx).Error("fs_initialize_error", err, nil)
		return err
	}

	fs.rootNode = snap.Root
	// Copy file blocks
	fs.fileBlocks = make([][]*pb.Block, len(snap.FileBlocks))
	copy(fs.fileBlocks, snap.FileBlocks)

	// Create journal reader
	journalPath := filepath.Join("/home/ed/journals", snaphash.String())
	jr, err := newJournalReader(journalPath, snaphash)
	if err != nil && err != errJournalNotExist {
		return err
	}
	if err == nil {
		defer jr.close()

		// If not empty, apply all operations
		empty, err := jr.empty(ctx)
		if err != nil {
			return err
		}
		if !empty {
			err = jr.apply(ctx, fs)
			if err != nil {
				return err
			}

			// TODO: make fs snapshot
		}
		jr.close()
	}

	// Create new journal writer, reset journal
	j, err := newJournal(journalPath, journalFileSize, snaphash)
	if err != nil {
		return err
	}

	fs.journal = j

	return nil
}

// addChildren merges and converts a Snapshot node tree with a FS node tree
func addChildren(fsDir *dir, snapDir *pb.Dir) error {
	for snapChildName, snapChild := range snapDir.Children {
		fsChild, ok := fsDir.children[snapChildName]
		if !ok {
			// We convert the node to a FS node and add it to parent
			fsNode, err := convertNode(snapChild)
			if err != nil {
				return err
			}
			fsDir.children[snapChildName] = fsNode
			continue
		}

		snapChildDir, ok := snapChild.Value.(*pb.Node_Dir)
		fsChildDir, ok := fsChild.(*dir)
		if !ok {
			return errMergeFileAlreadyExists
		}
		if !ok {
			// If fsChild is not a dir, we can't merge it
			return errMergeFileWithDir
		}

		// Otherwise they are both directories, and we merge them
		err := addChildren(fsChildDir, snapChildDir.Dir)
		if err != nil {
			return err
		}
	}

	return nil
}

func convertNode(pnode *pb.Node) (node, error) {
	switch pnode := pnode.Value.(type) {
	case *pb.Node_Dir:
		dir := &dir{
			meta:     metaFromProto(pnode.Dir.Meta),
			children: make(map[string]node),
		}
		addChildren(dir, pnode.Dir)
		return dir, nil

	case *pb.Node_File:
		// Convert BlocksLocation
		var locs []blocksLocation
		for _, loc := range pnode.File.BlocksLocations {
			locs = append(locs, blocksLocationFromProto(loc))
		}

		return &file{
			meta:            metaFromProto(pnode.File.Meta),
			blocksLocations: locs,
		}, nil

	case *pb.Node_Link:
		return &link{
			meta:   metaFromProto(pnode.Link.Meta),
			target: pnode.Link.Target,
		}, nil
	}

	return nil, errors.New("unknown snapshot node type")
}

func deserializeTree(data []byte) (*pb.Dir, error) {
	root := &pb.Dir{}
	err := proto.Unmarshal(data, root)
	return root, err
}

func deserializeBlockList(data []byte) ([]*pb.Block, error) {
	blockList := &pb.BlockList{}
	err := proto.Unmarshal(data, blockList)
	return blockList.Blocks, err
}

func (fs *FS) findNode(path string) (node, error) {
	if path == "/" {
		return fs.rootNode, nil
	}

	parts := strings.Split(path, "/")
	if len(parts) > 1 {
		parts = parts[1:]
	}

	var d *dir = fs.rootNode
	for i, part := range parts {
		child, ok := d.children[part]
		if !ok {
			return nil, &NodeNotFoundError{Path: path}
		}

		if i == len(parts)-1 {
			return child, nil
		}

		childdir, ok := child.(*dir)
		if !ok {
			return nil, &NodeNotFoundError{Path: path}
		}
		d = childdir
	}

	return nil, &NodeNotFoundError{Path: path}
}

type fsBlock struct {
	Snapshots []SnapshotInfo
}

func fsBlockFromProto(pbfsb *pb.FSBlock) *fsBlock {
	fsb := &fsBlock{}
	for _, pbSnapInfo := range pbfsb.Snapshots {
		fsb.Snapshots = append(fsb.Snapshots,
			SnapshotInfo{
				Hash: blocks.HashFromSlice(pbSnapInfo.Hash),
				Meta: snapshotMetaFromProto(pbSnapInfo.Meta),
			},
		)
	}
	return fsb
}

func (fsb *fsBlock) LatestSnapshot() SnapshotInfo {
	return fsb.Snapshots[len(fsb.Snapshots)-1]
}

// fetchFSBlock reads a FS block from the store, and returns it as an unmarshaled FSBlock object.
func fetchFSBlock(ctx context.Context, store blocks.Store, hash blocks.Hash) (*fsBlock, error) {
	pbfsb := &pb.FSBlock{}

	rc, err := store.Get(ctx, hash)
	if err != nil {
		return nil, fmt.Errorf("fs block does not exist: %w", err)
	}
	defer rc.Close()

	data, err := ioutil.ReadAll(blocks.Reader(ctx, rc))
	if err != nil {
		return nil, err
	}

	err = proto.Unmarshal(data, pbfsb)
	if err != nil {
		return nil, err
	}

	if len(pbfsb.Snapshots) == 0 {
		return nil, errors.New("fs block does not contain any snapshots")
	}

	logr.FromContext(ctx).Event("fetched_fsb", logr.KV{"fsb.hash": hash})

	return fsBlockFromProto(pbfsb), nil
}

func (fs *FS) applyOps(ctx context.Context, ops ...operation) (err error) {
	ctx, job := logr.Task(ctx, "fs.applyOps", nil)
	defer job.DeferDone(&err, nil)

	for _, op := range ops {
		err := op.apply(fs)
		if err != nil {
			return err
		}
	}

	return nil
}

func (fs *FS) do(ctx context.Context, op operation) error {
	err := fs.journal.write(ctx, op)
	if err != nil {
		return err
	}

	err = op.apply(fs)
	if err != nil {
		undoErr := fs.journal.undo(ctx)
		if undoErr != nil {
			return undoErr
		}

		return err
	}

	return nil
}

func (fs *FS) CreateFile(ctx context.Context, path string, meta Meta) (err error) {
	ctx, job := logr.Task(ctx, "CreateFile", logr.KV{"path": path, "meta": meta})
	defer job.DeferDone(&err, nil)
	return fs.do(ctx, &opCreateFile{Path: path, Meta: meta})
}

func (fs *FS) CreateDir(ctx context.Context, path string, meta Meta) (err error) {
	ctx, job := logr.Task(ctx, "CreateDir", logr.KV{"path": path, "meta": meta})
	defer job.DeferDone(&err, nil)
	return fs.do(ctx, &opCreateDir{Path: path, Meta: meta})
}

func (fs *FS) CreateLink(ctx context.Context, path, target string, meta Meta) (err error) {
	ctx, job := logr.Task(ctx, "CreateLink", logr.KV{"path": path, "target": target, "meta": meta})
	defer job.DeferDone(&err, nil)
	return fs.do(ctx, &opCreateLink{
		Path:   path,
		Target: target,
		Meta:   meta,
	})
}

func (fs *FS) Remove(ctx context.Context, path string) (err error) {
	ctx, job := logr.Task(ctx, "Remove", logr.KV{"path": path})
	defer job.DeferDone(&err, nil)
	return fs.do(ctx, &opRemove{Path: path})
}

func (fs *FS) Rename(ctx context.Context, path, newPath string) (err error) {
	ctx, job := logr.Task(ctx, "Rename", logr.KV{"path": path, "newPath": newPath})
	defer job.DeferDone(&err, nil)
	return fs.do(ctx, &opRename{Path: path, NewPath: newPath})
}

func (fs *FS) Open(ctx context.Context, path string) (_ *File, err error) {
	ctx, job := logr.Task(ctx, "Open", logr.KV{"path": path})
	defer job.DeferDone(&err, nil)

	// TODO: orphaned files
	// atomic.AddInt64(&n.openFds, 1)
	node, err := fs.findNode(path)
	if err != nil {
		return nil, err
	}

	f, ok := node.(*file)
	if !ok {
		return nil, errors.New("node is not a file")
	}

	return &File{
		file:         f,
		fs:           fs,
		blockDataEnd: f.meta.Size,
	}, nil
}

func (fs *FS) ReadDir(ctx context.Context, path string) (_ []DirEntry, err error) {
	ctx, job := logr.Task(ctx, "ReadDir", logr.KV{"path": path})
	defer job.DeferDone(&err, nil)

	node, err := fs.findNode(path)
	if err != nil {
		return nil, err
	}

	d, ok := node.(*dir)
	if !ok {
		return nil, errors.New("node is not a dir")
	}

	dirents := make([]DirEntry, len(d.children))
	i := 0
	for name, child := range d.children {
		dirent := DirEntry{Name: name}

		switch child.(type) {
		case *file:
			dirent.Type = EntryFile

		case *dir:
			dirent.Type = EntryDir

		case *link:
			dirent.Type = EntryLink
		}

		dirents[i] = dirent
		i++
	}

	return dirents, nil
}

func (fs *FS) Readlink(ctx context.Context, path string) (_ string, err error) {
	ctx, job := logr.Task(ctx, "Readlink", logr.KV{"path": path})
	defer job.DeferDone(&err, nil)

	node, err := fs.findNode(path)
	if err != nil {
		return "", err
	}

	l, ok := node.(*link)
	if !ok {
		return "", errors.New("node is not a symlink")
	}

	return l.target, nil
}

func (fs *FS) Meta(ctx context.Context, path string) (_ Meta, err error) {
	ctx, job := logr.Task(ctx, "Meta", logr.KV{"path": path})
	defer job.DeferDone(&err, nil)

	node, err := fs.findNode(path)
	if err != nil {
		return Meta{}, err
	}

	return node.Meta(), nil
}

func (fs *FS) SetMeta(ctx context.Context, path string, valid SetMetaValid, meta Meta) (err error) {
	ctx, job := logr.Task(ctx, "SetMeta", logr.KV{"path": path, "valid": valid, "meta": meta})
	defer job.DeferDone(&err, nil)

	return fs.do(ctx, &opSetMeta{
		Path:  path,
		Valid: valid,
		Meta:  meta,
	})
}

func (fs *FS) Lookup(ctx context.Context, path string) (_ EntryType, err error) {
	ctx, job := logr.Task(ctx, "Lookup", logr.KV{"path": path})
	defer job.DeferDone(&err, nil)

	node, err := fs.findNode(path)
	if err != nil {
		return EntryDir, err
	}

	switch node.(type) {
	case *file:
		return EntryFile, nil

	case *dir:
		return EntryDir, nil

	case *link:
		return EntryLink, nil
	}

	return EntryDir, ErrUnknownNodeType
}

func (fs *FS) Snapshots() []SnapshotInfo {
	return fs.snapshots
}
