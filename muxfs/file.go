package muxfs

import (
	"context"
	"io"

	"gitlab.com/muxro/muxfs/muxfs/internal/pb"
)

type File struct {
	file         *file
	fs           *FS
	blocksSize   int64
	blockDataEnd int64
}

type file struct {
	meta            Meta
	blocksLocations []blocksLocation
}

type blocksLocation struct {
	BlockList uint32
	BlockNo   uint32
}

func blocksLocationFromProto(pbloc *pb.BlocksLocation) blocksLocation {
	return blocksLocation{
		BlockList: pbloc.BlockList,
		BlockNo:   pbloc.BlockNo,
	}
}

func (f *file) Meta() Meta {
	return f.meta
}

func (f *File) Close() error {
	// TODO: close
	// return f.file.Close()
	return nil
}

func (f *File) ReadAt(p []byte, off int64) (read int, err error) {
	// TODO: mutex?
	// n.mu.RLock()
	// defer n.mu.RUnlock()

	// nothing to read
	if len(p) == 0 {
		return 0, nil
	}

	// read past file size returns EOF
	if off >= f.file.meta.Size {
		return 0, io.EOF
	}

	br := newBlockReader(context.TODO(), f.fs.store, f.fs.fileBlocks, f.file.blocksLocations, f.blockDataEnd)
	read, err = br.ReadAt(p, off)
	if err != nil {
		return read, err
	}

	// TODO: cache
	// // patch cache on top and determine how much memory has been patched
	// patched := n.cache.PatchOnto(p, off)
	// if read < patched {
	// 	read = patched
	// }

	return
}

func (f *File) Write(b []byte) (n int, err error) {
	return 0, nil
}

func (f *File) WriteAt(b []byte, off int64) (n int, err error) {
	return 0, nil
}
