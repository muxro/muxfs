package muxfs

import (
	"io"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/muxro/muxfs/blocks"
)

func TestReadAll(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	const readAllSize = 8 * 1024 * 1024

	store := blocks.NewMemoryStore("")
	fs := CreateTestFS(t, store,
		CreateFile("file", readAllSize),
	)

	f := openFile(t, fs, "file")
	defer f.Close()

	data := make([]byte, readAllSize)
	n, err := f.ReadAt(data, 0)
	require.NoError(t, err)
	require.Equal(t, readAllSize, n)

	expectedData := blocks.TestData("file", 0, readAllSize)
	require.Equal(t, expectedData, data)
}

func TestReadRandomOffsets(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	fs := CreateTestFS(t, blocks.NewMemoryStore(""),
		CreateFile("file", 5*1024*1024),
	)

	f := openFile(t, fs, "file")
	defer f.Close()

	totalSize := f.file.meta.Size

	for i := 0; i < 1000; i++ {
		// Get random offset at any point in the file
		offset := rand.Int63n(totalSize)

		// Read random amount of bytes from offset
		size := rand.Int63n(totalSize - offset)

		data := make([]byte, size)
		n, err := f.ReadAt(data, offset)
		require.NoError(t, err)

		require.Equal(t, int64(n), size)

		// Check the data matches
		require.Equal(t, blocks.TestData("file", int64(offset), int64(size)), data)
	}
}

func TestReadRandomIntervals(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	fs := CreateTestFS(t, blocks.NewMemoryStore(""),
		CreateFile("file", 6*1024*1024),
	)

	f := openFile(t, fs, "file")
	defer f.Close()

	totalSize := f.file.meta.Size
	const maxSequenceSize = 150

	var offset int64
	for offset < totalSize {
		// Must be > 0
		seqSize := int64(rand.Intn(maxSequenceSize-1)) + 1

		if seqSize+offset > totalSize {
			seqSize = totalSize - offset
		}

		data := make([]byte, seqSize)
		n, err := f.Read(data)
		require.NoError(t, err)
		require.Equal(t, int64(n), seqSize)

		expected := blocks.TestData("file", offset, seqSize)
		require.Equal(t, expected, data)

		offset += seqSize
	}
}

func TestReadEOF(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)

	fs := CreateTestFS(t, blocks.NewMemoryStore(""),
		CreateFile("file", 10),
	)
	f := openFile(t, fs, "file")
	defer f.Close()

	// total file size + 1
	data := make([]byte, 11)
	n, err := f.Read(data)
	require.NoError(err)
	require.Equal(10, n)

	n, err = f.Read(data)
	require.Equal(io.EOF, err)
	require.Equal(0, n)
}

func TestReadSequential(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)

	const size = 6 * 1024 * 1024

	fs := CreateTestFS(t, blocks.NewMemoryStore(""),
		CreateFile("file", size),
	)

	testCases := []struct {
		desc    string
		seqSize int64
	}{
		{desc: "Sequence79", seqSize: 79},
		{desc: "Sequence256", seqSize: 256},
		{desc: "Sequence339", seqSize: 339},
		{desc: "Sequence4096", seqSize: 4096},
		{desc: "Sequence5112", seqSize: 5112},
	}

	for _, tC := range testCases {
		f := openFile(t, fs, "file")
		defer f.Close()

		var wholeFile []byte

		t.Run(tC.desc, func(t *testing.T) {
			var offset int64
			for {
				if offset+tC.seqSize > size {
					tC.seqSize = size - offset
					if tC.seqSize == 0 {
						break
					}
				}

				data := make([]byte, tC.seqSize)
				n, err := f.Read(data)
				if err != nil {
					if err == io.EOF {
						break
					}
					require.Nil(err)
				}

				require.Equal(int64(n), tC.seqSize)

				expected := blocks.TestData("file", offset, tC.seqSize)
				require.Equal(expected, data)

				offset += int64(n)

				wholeFile = append(wholeFile, data...)
			}

			require.Equal(int64(size), offset)
			require.Equal(blocks.TestData("file", 0, size), wholeFile)
		})
	}
}

func TestReadByteByByte(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	const size = 1024

	fs := CreateTestFS(t, blocks.NewMemoryStore(""),
		CreateFile("file", size),
	)

	f := openFile(t, fs, "file")
	defer f.Close()

	for i := int64(0); i < size; i++ {
		data := make([]byte, 1)
		n, err := f.Read(data)
		if err != nil && err == io.EOF {
			require.Equal(t, i, size-1, "Unexpected EOF")
			break
		}
		require.NoError(t, err)

		// Must have read exactly 1 byte
		require.Equal(t, n, 1)

		expected := blocks.TestData("file", i, 1)
		require.Equal(t, data, expected)
	}
}

func TestReadMultipleHandles(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	require := require.New(t)

	const size = 1024

	fs := CreateTestFS(t, blocks.NewMemoryStore(""),
		CreateFile("file", size),
	)

	f1 := openFile(t, fs, "file")
	f2 := openFile(t, fs, "file")
	defer f1.Close()
	defer f2.Close()

	data1 := make([]byte, 500)
	n, err := f1.Read(data1)
	require.Nil(err)
	require.Equal(500, n)
	require.Equal(blocks.TestData("file", 0, 500), data1)

	data2 := make([]byte, 500)
	n, err = f2.Read(data2)
	require.Nil(err)
	require.Equal(500, n)
	require.Equal(blocks.TestData("file", 0, 500), data2)

	require.Equal(data1, data2)
}

// TODO: write
// func TestReadTruncated(t *testing.T) {
// 	require := require.New(t)

// 	const size = 7 * 1024 * 1024

// 	fs := CreateTestFS(t, blocks.NewMemoryStore(""),
// 		CreateFile("file", size),
// 	)

// 	f := openFile(t, fs, "file")

// 	// write bytes at the start of the file
// 	writeSize := 5
// 	toWrite := make([]byte, writeSize)
// 	n, err := f.Write(toWrite)
// 	require.Nil(err)
// 	require.Equal(writeSize, n)

// 	err = f.Truncate(int64(writeSize))
// 	require.Nil(err)

// 	_, err = f.Seek(0, io.SeekStart)
// 	require.Nil(err)

// 	// try to read more bytes than the file has
// 	readSize := writeSize * 2
// 	actual := make([]byte, readSize)
// 	n, err = io.ReadFull(f, actual)
// 	require.Equal(io.ErrUnexpectedEOF, err)
// 	require.Equal(writeSize, n)
// }

// TODO: write
// func TestReadWrite(t *testing.T) {
// 	if testing.Short() {
// 		t.SkipNow()
// 	}
// 	require := require.New(t)
// 	fs, err := NewTestFS(FromSnapshot(&Snapshot{
// 		Entries: []*Entry{
// 			&Entry{Path: "/", Meta: &EntryMeta{Size: 4096}},
// 			&Entry{Path: "/test_readwrite", Meta: &EntryMeta{Size: 1 * 1024}},
// 		},
// 	}))
// 	require.Nil(err)
// 	defer fs.Done()

// 	f, err := fs.OpenFile("/test_readwrite", os.O_RDWR, 0755)
// 	require.Nil(err)
// 	defer f.Close()

// 	data := make([]byte, 500)
// 	n, err := f.Read(data)
// 	require.Nil(err)
// 	require.Equal(500, n)
// 	require.Equal(blocks.TestData("/test_readwrite", 0, 500), data)

// 	// rewind
// 	seek, err := f.Seek(0, io.SeekStart)
// 	require.Nil(err)
// 	require.Equal(int64(0), seek)

// 	n, err = f.Write(blocks.TestData("READWRITE", 0, 500))
// 	require.Nil(err)
// 	require.Equal(500, n)

// 	// rewind
// 	seek, err = f.Seek(0, io.SeekStart)
// 	require.Nil(err)
// 	require.Equal(int64(0), seek)

// 	data = make([]byte, 500)
// 	n, err = f.Read(data)
// 	require.Nil(err)
// 	require.Equal(500, n)
// 	require.Equal(blocks.TestData("READWRITE", 0, 500), data)

// 	// read the last half
// 	n, err = f.Read(data)
// 	require.Nil(err)
// 	require.Equal(500, n)
// 	require.Equal(blocks.TestData("/test_readwrite", 500, 500), data)
// }
