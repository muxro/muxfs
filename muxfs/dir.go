package muxfs

type dir struct {
	meta     Meta
	children map[string]node
}

func (d *dir) Meta() Meta {
	return d.meta
}
