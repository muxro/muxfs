package muxfs

import (
	"bytes"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"github.com/minio/highwayhash"
	"gitlab.com/go-figure/logr"
	"gitlab.com/muxro/muxfs/blocks"
)

var bin = binary.BigEndian

var (
	ErrUnknownNodeType = errors.New("unknown node type")
	ErrLinkSetSize     = errors.New("cannot set size on a symlink")
	ErrDirSetSize      = errors.New("cannot set size on a dir")
	ErrDirNotEmpty     = errors.New("directory not empty")
	ErrNodeNotDir      = errors.New("node is not a directory")
	ErrNameTooLong     = errors.New("either node name is > 255 characters long, or node path is > 4095 characters long")
	errJournalNotExist = errors.New("journal does not exist")
)

var journalVersion = []byte("MFS1")

// 4 bytes journal version
// 8 bytes timestamp
// 33 bytes snapshot hash
const journalHeaderSize = 4 + 8 + 33

type journal struct {
	snaphash blocks.Hash
	f        *os.File
}

type journalReader struct {
	f         *os.File
	createdAt time.Time
	snaphash  blocks.Hash
}

type opType uint8

const (
	opTypeCreateDir opType = iota + 1
	opTypeCreateFile
	opTypeCreateLink
	opTypeSetMeta
	opTypeRename
	opTypeRemove

	opTypeEnd opType = 69
)

type operation interface {
	opType() opType
	size() int
	apply(fs *FS) error
	encode(b []byte)
	decode(b []byte)

	kv() logr.KV
}

func newJournal(path string, size int64, snaphash blocks.Hash) (journal, error) {
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return journal{}, err
	}

	j := journal{snaphash: snaphash, f: f}

	// Overwrite journal with "size" zeroes
	var writeSize int64 = 1024*1024
	buf := make([]byte, writeSize)
	for i := int64(0); i < size; {
		n, err := f.Write(buf)
		if err != nil {
			return j, err
		}
		i += int64(n)
	}

	// j.reset will seek the file to the start and write the journal header
	return j, j.reset(snaphash)
}

// reset seeks the journal file to the start and writes a new header.
func (j *journal) reset(snaphash blocks.Hash) error {
	_, err := j.f.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	j.snaphash = snaphash

	return j.writeHeader(snaphash)
}

// writeHeader writes the journal header, followed by a nil op (journal end marker).
func (j *journal) writeHeader(snaphash blocks.Hash) error {
	b := make([]byte, journalHeaderSize+5)

	var off int

	copy(b, journalVersion)
	off += 4

	bin.PutUint64(b[off:], uint64(time.Now().Unix()))
	off += 8

	if len(snaphash) != 33 {
		return fmt.Errorf("snapshot hash needs to be 33 bytes long, is actually: %v", len(snaphash))
	}

	copy(b[off:], snaphash[:])
	off += 33

	// Write empty header for next op (journal end marker)
	copy(b[off:], []byte{byte(opTypeEnd)})

	n, err := j.f.Write(b)

	// Seek cursor back 5 bytes (journal end marker op header size) in preparation for next write
	_, err = j.f.Seek(-5, io.SeekCurrent)
	if err != nil {
		return err
	}

	if n != journalHeaderSize+5 {
		return fmt.Errorf("didn't write enough bytes to journal header: want %v, got %v", journalHeaderSize+5, n)
	}

	return j.f.Sync()
}

// op_header = [1 byte op_type][4 bytes op_size]
// op = [5 bytes op_header][n bytes op_data][8 bytes op_hash]
func (j *journal) write(ctx context.Context, op operation) error {
	// Total op size is op header (1 byte opType + 4 bytes totalOpSize) + opData + 8 bytes hash
	// and another nil op header at the end (+ 5 bytes)
	opDataSize := op.size()
	totalOpSize := 1 + 4 + opDataSize + 8
	b := make([]byte, totalOpSize+1+4)

	var off int

	// Encode op header
	// 1 byte op_type
	b[off] = byte(op.opType())
	off++
	// 4 bytes op_size
	bin.PutUint32(b[off:], uint32(totalOpSize))
	off += 4

	// Encode op data
	op.encode(b[off:])
	off += opDataSize

	// Hash all op data including header, but not including the last 8 bytes reserved for the hash
	hhkey := j.snaphash[1:] // skip 1st byte to get a 32-byte key
	hash := highwayhash.Sum64(b[:off], hhkey)
	// Encode hash
	bin.PutUint64(b[off:], hash)
	off += 8

	// Write empty header for next op (journal end marker)
	copy(b[off:], []byte{byte(opTypeEnd)})

	// Write encoded op to file
	_, err := j.f.Write(b)
	if err != nil {
		return err
	}
	// Seek cursor back 5 bytes (journal end marker op header size) in preparation for next write
	_, err = j.f.Seek(-5, io.SeekCurrent)
	if err != nil {
		return err
	}

	return j.f.Sync()
}

func (j *journal) undo(ctx context.Context) error {
	panic("undo not implemented")
	// logr.FromContext(ctx).Event("journal_undo_op", j.ops[len(j.ops)-1].kv())
	// j.ops = j.ops[:len(j.ops)-1]
	return nil
}

func (j *journal) close() error {
	return j.f.Close()
}

// newJournalReader tries to read the journal header after opening the journal file. If the header cannot be read, we return an error.
// After the header is read, the journal operations may be read.
func newJournalReader(path string, snaphash blocks.Hash) (*journalReader, error) {
	f, err := os.Open(path)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, errJournalNotExist
		}

		return nil, err
	}

	r := &journalReader{f: f, snaphash: snaphash}
	err = r.readHeader()
	if err != nil {
		return nil, err
	}

	return r, nil
}

func (r *journalReader) readHeader() error {
	b := make([]byte, journalHeaderSize)
	n, err := r.f.Read(b)
	if err != nil {
		return err
	}

	if n != journalHeaderSize {
		return fmt.Errorf("didn't read enough bytes from journal header: want %v, got %v", journalHeaderSize, n)
	}

	jversion := make([]byte, 4)
	copy(jversion, b)
	b = b[4:]

	if !bytes.Equal(jversion, journalVersion) {
		return fmt.Errorf("journal version not equal: want %v, got %v", journalVersion, jversion)
	}

	r.createdAt = time.Unix(int64(bin.Uint64(b)), 0)
	b = b[8:]

	snaphashslice := make([]byte, 33)
	copy(snaphashslice, b)
	snaphash := blocks.HashFromSlice(snaphashslice)

	if r.snaphash != snaphash {
		return fmt.Errorf("journal snapshot hash not equal: want %v, got %v", r.snaphash, snaphash)
	}

	return nil
}

func (r *journalReader) read(ctx context.Context) (operation, error) {
	// Make byte slice equal to op header size
	b := make([]byte, 1+4)
	// Read op header
	_, err := r.f.Read(b)
	if err != nil {
		return nil, err
	}

	// Hit journal end marker
	if bytes.Equal(b, []byte{byte(opTypeEnd), 0, 0, 0, 0}) {
		return nil, io.EOF
	}

	var off int

	// Decode op header
	opType := opType(b[0])
	off++
	totalOpSize := int(bin.Uint32(b[off:]))
	off += 4

	// Extend buffer by total op size - op header size
	b = append(b, make([]byte, totalOpSize-1-4)...)

	// Read op data (including trailing hash)
	_, err = r.f.Read(b[off:])
	if err != nil {
		return nil, err
	}

	// Instantiate op and decode op data
	var op operation
	switch opType {
	case opTypeCreateDir:
		op = &opCreateDir{}
	case opTypeCreateFile:
		op = &opCreateFile{}
	case opTypeCreateLink:
		op = &opCreateLink{}
	case opTypeSetMeta:
		op = &opSetMeta{}
	case opTypeRename:
		op = &opRename{}
	case opTypeRemove:
		op = &opRemove{}
	default:
		return nil, fmt.Errorf("unknown op type: %v", opType)
	}

	op.decode(b[off:])
	off += op.size()

	// Calculate hash of op header + op data
	hhkey := r.snaphash[1:] // skip 1st byte to get a 32-byte key
	gotHash := highwayhash.Sum64(b[:off], hhkey)

	// Decode hash
	wantHash := bin.Uint64(b[off:])

	// TODO: option to ignore errors
	if wantHash != gotHash {
		return nil, fmt.Errorf("op hash mismatch: want %v, got: %v", wantHash, gotHash)
	}

	return op, nil
}

// If we can read one operation header, the journal is not empty. Afterwards, the file is seeked back to the original position.
// If no operations can be read, then the journal is empty.
func (r *journalReader) empty(ctx context.Context) (bool, error) {
	b := make([]byte, 1)
	_, err := r.f.Read(b)
	if err != nil {
		if err == io.EOF {
			return true, nil
		}

		return true, err
	}

	_, seekErr := r.f.Seek(-1, io.SeekCurrent)

	if b[0] == byte(opTypeEnd) {
		return true, nil
	}

	return false, seekErr
}

func (r *journalReader) apply(ctx context.Context, fs *FS) error {
	for {
		op, err := r.read(ctx)
		if err != nil {
			if err == io.EOF {
				return nil
			}

			return err
		}

		// TODO: restart applying journal, end at last non-error op
		err = op.apply(fs)
		if err != nil {
			panic(err)
		}
	}
}

func (r *journalReader) close() error {
	return r.f.Close()
}

// NodeNotFoundError is the error returned when a node cannot be found in the filesystem tree.
type NodeNotFoundError struct {
	Path string
}

func (e *NodeNotFoundError) Error() string {
	return fmt.Sprint("cannot find node ", e.Path)
}

// NodeAlreadyExistsError is the error returned when an operation wants to create a node, but a node
// already exists.
type NodeAlreadyExistsError struct {
	Path string
	Name string
}

func (e NodeAlreadyExistsError) Error() string {
	return fmt.Sprintf("node '%v' already exists at path '%v'", e.Name, e.Path)
}

type opCreateDir struct {
	Path string
	Meta Meta
}

func (o *opCreateDir) opType() opType {
	return opTypeCreateDir
}

func (o *opCreateDir) kv() logr.KV {
	return logr.KV{"op": "create_dir"}
}

func (o *opCreateDir) size() int {
	return 2 + len(o.Path) + metaSize
}

func (o *opCreateDir) encode(b []byte) {
	bin.PutUint16(b, uint16(len(o.Path)))
	b = b[2:]
	copy(b, []byte(o.Path))
	b = b[len(o.Path):]

	encodeMeta(&o.Meta, b)
}

func (o *opCreateDir) decode(b []byte) {
	pathlen := bin.Uint16(b)
	b = b[2:]
	pathbuf := make([]byte, pathlen)
	copy(pathbuf, b)
	o.Path = string(pathbuf)
	b = b[pathlen:]

	decodeMeta(&o.Meta, b)
}

func (o opCreateDir) apply(fs *FS) error {
	destDirPath := filepath.Dir(o.Path)
	newName := filepath.Base(o.Path)

	if len(o.Path) > 4095 || len(newName) > 255 {
		return ErrNameTooLong
	}

	destnode, err := fs.findNode(destDirPath)
	if err != nil {
		return err
	}

	destdir, ok := destnode.(*dir)
	if !ok {
		return ErrNodeNotDir
	}

	// Already exists
	if _, ok := destdir.children[newName]; ok {
		return NodeAlreadyExistsError{Path: destDirPath, Name: newName}
	}

	dir := &dir{
		meta: Meta{
			Size:      0,
			Mode:      uint32(o.Meta.Mode),
			Uid:       o.Meta.Uid,
			Gid:       o.Meta.Gid,
			UpdatedAt: time.Now().Unix(),
		},
		children: make(map[string]node),
	}
	destdir.children[newName] = dir

	return nil
}

type opSetMeta struct {
	Path  string
	Meta  Meta
	Valid SetMetaValid
}

func (o *opSetMeta) opType() opType {
	return opTypeSetMeta
}

func (o opSetMeta) kv() logr.KV {
	return logr.KV{"op": "set_meta"}
}

func (o opSetMeta) size() int {
	return 2 + len(o.Path) + 4 + metaSize
}

func (o *opSetMeta) encode(b []byte) {
	bin.PutUint16(b, uint16(len(o.Path)))
	b = b[2:]
	copy(b, []byte(o.Path))
	b = b[len(o.Path):]

	encodeMeta(&o.Meta, b)
	b = b[metaSize:]

	bin.PutUint32(b, uint32(o.Valid))
}

func (o *opSetMeta) decode(b []byte) {
	pathlen := bin.Uint16(b)
	b = b[2:]
	pathbuf := make([]byte, pathlen)
	copy(pathbuf, b)
	o.Path = string(pathbuf)
	b = b[pathlen:]

	decodeMeta(&o.Meta, b)
	b = b[metaSize:]

	o.Valid = SetMetaValid(bin.Uint32(b))
}

func (o opSetMeta) apply(fs *FS) error {
	n, err := fs.findNode(o.Path)
	if err != nil {
		return err
	}

	switch n := n.(type) {
	case *dir:
		if o.Valid.Size() {
			return ErrDirSetSize
		}
		if o.Valid.Mode() {
			n.meta.Mode = o.Meta.Mode
		}
		if o.Valid.Uid() {
			n.meta.Uid = o.Meta.Uid
		}
		if o.Valid.Gid() {
			n.meta.Gid = o.Meta.Gid
		}
	case *file:
		if o.Valid.Size() {
			size := o.Meta.Size
			// TODO: truncate
			// err := n.truncate(size)
			// if err != nil {
			// 	return err
			// }
			n.meta.Size = size
		}
		if o.Valid.Mode() {
			n.meta.Mode = o.Meta.Mode
		}
		if o.Valid.Uid() {
			n.meta.Uid = o.Meta.Uid
		}
		if o.Valid.Gid() {
			n.meta.Gid = o.Meta.Gid
		}

	case *link:
		if o.Valid.Size() {
			return ErrLinkSetSize
		}
		if o.Valid.Mode() {
			// Nothing
		}
		if o.Valid.Uid() {
			n.meta.Uid = o.Meta.Uid
		}
		if o.Valid.Gid() {
			n.meta.Gid = o.Meta.Gid
		}
	default:
		return ErrUnknownNodeType
	}

	return nil
}

type opRename struct {
	Path    string
	NewPath string
}

func (o *opRename) opType() opType {
	return opTypeRename
}

func (o opRename) kv() logr.KV {
	return logr.KV{"op": "rename"}
}

func (o opRename) size() int {
	return 4 + len(o.Path) + len(o.NewPath)
}

func (o *opRename) encode(b []byte) {
	bin.PutUint16(b, uint16(len(o.Path)))
	b = b[2:]
	copy(b, []byte(o.Path))
	b = b[len(o.Path):]

	bin.PutUint16(b, uint16(len(o.NewPath)))
	b = b[2:]
	copy(b, []byte(o.NewPath))
	b = b[len(o.NewPath):]
}

func (o *opRename) decode(b []byte) {
	pathlen := bin.Uint16(b)
	b = b[2:]
	pathbuf := make([]byte, pathlen)
	copy(pathbuf, b)
	o.Path = string(pathbuf)
	b = b[pathlen:]

	newpathlen := bin.Uint16(b)
	b = b[2:]
	newpathbuf := make([]byte, newpathlen)
	copy(newpathbuf, b)
	b = b[newpathlen:]
	o.NewPath = string(newpathbuf)
}

func (o opRename) apply(fs *FS) error {
	oldName := filepath.Base(o.Path)
	newName := filepath.Base(o.NewPath)

	if len(o.NewPath) > 4095 || len(newName) > 255 {
		return ErrNameTooLong
	}

	// Old node must exist
	n, err := fs.findNode(o.Path)
	if err != nil {
		return err
	}

	// Destination dir must exist
	destDirPath := filepath.Dir(o.NewPath)
	destNode, err := fs.findNode(destDirPath)
	if err != nil {
		return err
	}
	destDir, ok := destNode.(*dir)
	if !ok {
		return ErrNodeNotDir
	}
	// Source dir must exist
	sourceDirPath := filepath.Dir(o.Path)
	sourceNode, err := fs.findNode(sourceDirPath)
	if err != nil {
		return err
	}
	sourceDir, ok := sourceNode.(*dir)
	if !ok {
		return ErrNodeNotDir
	}

	destDir.children[newName] = n
	delete(sourceDir.children, oldName)

	return nil
}

type opCreateFile struct {
	Path string
	Meta Meta
}

func (o *opCreateFile) opType() opType {
	return opTypeCreateFile
}

func (o opCreateFile) kv() logr.KV {
	return logr.KV{"op": "create_file"}
}

func (o opCreateFile) size() int {
	return 2 + len(o.Path) + metaSize
}

func (o *opCreateFile) encode(b []byte) {
	bin.PutUint16(b, uint16(len(o.Path)))
	b = b[2:]
	copy(b, []byte(o.Path))
	b = b[len(o.Path):]

	encodeMeta(&o.Meta, b)
}

func (o *opCreateFile) decode(b []byte) {
	pathlen := bin.Uint16(b)
	b = b[2:]
	pathbuf := make([]byte, pathlen)
	copy(pathbuf, b)
	o.Path = string(pathbuf)
	b = b[pathlen:]

	decodeMeta(&o.Meta, b)
}

func (o opCreateFile) apply(fs *FS) error {
	destDirPath := filepath.Dir(o.Path)
	newName := filepath.Base(o.Path)

	if len(o.Path) > 4095 || len(newName) > 255 {
		return ErrNameTooLong
	}

	destnode, err := fs.findNode(destDirPath)
	if err != nil {
		return err
	}

	destdir, ok := destnode.(*dir)
	if !ok {
		return ErrNodeNotDir
	}

	// Already exists
	if _, ok := destdir.children[newName]; ok {
		return NodeAlreadyExistsError{Path: destDirPath, Name: newName}
	}

	f := &file{
		meta: Meta{
			Size:      o.Meta.Size,
			Mode:      uint32(o.Meta.Mode),
			Uid:       o.Meta.Uid,
			Gid:       o.Meta.Gid,
			UpdatedAt: time.Now().Unix(),
		},
		blocksLocations: []blocksLocation{},
	}
	destdir.children[newName] = f

	return nil
}

type opRemove struct {
	Path string
}

func (o *opRemove) opType() opType {
	return opTypeRemove
}

func (o opRemove) kv() logr.KV {
	return logr.KV{"op": "remove"}
}

func (o opRemove) size() int {
	return 2 + len(o.Path)
}

func (o *opRemove) encode(b []byte) {
	bin.PutUint16(b, uint16(len(o.Path)))
	b = b[2:]
	copy(b, []byte(o.Path))
}

func (o *opRemove) decode(b []byte) {
	pathlen := bin.Uint16(b)
	b = b[2:]
	pathbuf := make([]byte, pathlen)
	copy(pathbuf, b)
	o.Path = string(pathbuf)
}

func (o opRemove) apply(fs *FS) error {
	dirPath := filepath.Dir(o.Path)
	name := filepath.Base(o.Path)

	node, err := fs.findNode(dirPath)
	if err != nil {
		return err
	}

	d, ok := node.(*dir)
	if !ok {
		return ErrNodeNotDir
	}

	child, ok := d.children[name]
	if !ok {
		return &NodeNotFoundError{Path: filepath.Join(dirPath, name)}
	}

	switch c := child.(type) {
	case *file:
		// TODO: orphaned files
		// val := atomic.LoadInt64(&c.openFds)
		// if val != 0 {
		// 	c.FS.orphanedFiles = append(c.FS.orphanedFiles, c)
		// }

	case *dir:
		// Can only rmdir if dir is empty
		if len(c.children) > 0 {
			return ErrDirNotEmpty
		}

	case *link:
		// Nothing
	}

	delete(d.children, name)

	return nil
}

type opCreateLink struct {
	Path   string
	Meta   Meta
	Target string
}

func (o *opCreateLink) opType() opType {
	return opTypeCreateLink
}

func (o opCreateLink) kv() logr.KV {
	return logr.KV{"op": "create_link"}
}

func (o opCreateLink) size() int {
	return 4 + len(o.Path) + len(o.Target) + metaSize
}

func (o *opCreateLink) encode(b []byte) {
	bin.PutUint16(b, uint16(len(o.Path)))
	b = b[2:]
	copy(b, []byte(o.Path))
	b = b[len(o.Path):]

	encodeMeta(&o.Meta, b)
	b = b[metaSize:]

	bin.PutUint16(b, uint16(len(o.Target)))
	b = b[2:]
	copy(b, []byte(o.Target))
}

func (o *opCreateLink) decode(b []byte) {
	pathlen := bin.Uint16(b)
	b = b[2:]
	pathbuf := make([]byte, pathlen)
	copy(pathbuf, b)
	o.Path = string(pathbuf)
	b = b[pathlen:]

	decodeMeta(&o.Meta, b)
	b = b[metaSize:]

	targetlen := bin.Uint16(b)
	b = b[2:]
	targetbuf := make([]byte, targetlen)
	copy(targetbuf, b)
	o.Target = string(targetbuf)
}

func (o opCreateLink) apply(fs *FS) error {
	destDirPath := filepath.Dir(o.Path)
	newName := filepath.Base(o.Path)

	if len(o.Path) > 4095 || len(newName) > 255 {
		return ErrNameTooLong
	}

	node, err := fs.findNode(destDirPath)
	if err != nil {
		return err
	}

	destdir, ok := node.(*dir)
	if !ok {
		return ErrNodeNotDir
	}

	// Already exists
	if _, ok := destdir.children[newName]; ok {
		return NodeAlreadyExistsError{Path: destDirPath, Name: newName}
	}

	l := &link{
		meta: Meta{
			Size:      int64(len(o.Target)),
			Mode:      uint32(0777 | os.ModeSymlink),
			Uid:       o.Meta.Uid,
			Gid:       o.Meta.Gid,
			UpdatedAt: time.Now().Unix(),
		},
		target: o.Target,
	}
	destdir.children[newName] = l

	return nil
}
