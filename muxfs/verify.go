package muxfs

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/go-figure/logr"
	"gitlab.com/muxro/muxfs/blocks"
)

var ErrVerifyEmptyFS = errors.New("empty FS, nothing to verify")

type VerifyNotifier interface {
	Update(message string, percent float32, eta string) error
	Error(err error) error
}

type VerifyOptions struct {
	CheckHashes  bool
	AllSnapshots bool
}

func Verify(ctx context.Context, store blocks.Store, fsbhash blocks.Hash, opts VerifyOptions, notif VerifyNotifier) (err error) {
	ctx, job := logr.Task(ctx, "muxfs.Verify", logr.KV{"fsbhash": fsbhash})
	defer job.DeferDone(&err, nil)

	fsb, err := fetchFSBlock(ctx, store, fsbhash)
	if err != nil {
		return err
	}

	if len(fsb.Snapshots) == 0 {
		return ErrVerifyEmptyFS
	}

	// Figure out total number of blocks we have to check
	totalBlocks := len(fsb.Snapshots) // How many snapshots blocks we have

	var (
		totalSnapBlockDur time.Duration
		blockCount        int
		snapshots         []*Snapshot
	)
	for i, snapInfo := range fsb.Snapshots {
		blockCount++

		// Start measuring how much time it takes to verify one snapshot block.
		start := time.Now()

		// Check that the snapshot block exists
		partialSnap, err := fetchPartialSnapshot(ctx, store, snapInfo.Hash)
		if err != nil {
			err := notif.Error(fmt.Errorf("fetching partial snapshot: %w", err))
			if err != nil {
				return err
			}
			continue
		}

		if opts.CheckHashes {
			// Check snapshot block hash matches the hash we have
			calculatedHash, err := hashPartialSnapshot(partialSnap)
			if err != nil {
				err := notif.Error(fmt.Errorf("calculating snapshot block hash: %w", err))
				if err != nil {
					return err
				}
				continue
			}

			if snapInfo.Hash != calculatedHash {
				err := notif.Error(HashMismatchError{Got: calculatedHash, Want: snapInfo.Hash})
				if err != nil {
					return err
				}
				continue
			}
		}

		// Turn partial snapshot into full snapshot (involves fetching extra blocks, if any exist)
		snap, err := snapshotFromPartialSnapshot(ctx, store, partialSnap)
		if err != nil {
			err := notif.Error(fmt.Errorf("turning partial snapshot into full snapshot: %w", err))
			if err != nil {
				return err
			}

			continue
		}

		// Add to snapshot list so we can iterate through file blocks below, without fetching the
		// snapshot again.
		snapshots = append(snapshots, snap)

		// Calculate total number of blocks.
		for _, blocks := range snap.FileBlocks {
			totalBlocks += len(blocks)
		}

		dur := time.Now().Sub(start)
		totalSnapBlockDur += dur

		// ETA during snapshot block verification will be a rough estimate, because they are usually
		// smaller than file blocks. The estimation will get more accurate once we start verifying a few
		// file blocks.
		// How many snapshot blocks there are, minus how many we've verified so far
		eta := time.Duration(len(fsb.Snapshots)-i) * totalSnapBlockDur / time.Duration(blockCount)

		err = notif.Update(
			fmt.Sprintf("verified snapshot block %v", i),
			float32(blockCount)*100.0/float32(totalBlocks),
			eta.String(),
		)
		if err != nil {
			return err
		}

		// Only check first snapshot's block
		if !opts.AllSnapshots {
			break
		}
	}

	var averageCount int
	var totalFileBlockDur time.Duration
	// TODO: Don't process same block multiple times
	for i, snap := range snapshots {
		// Verify file blocks
		for j, fblocks := range snap.FileBlocks {
			if len(fblocks) == 0 {
				continue
			}

			for _, block := range fblocks {
				blockCount++

				// Start measuring how much time it takes to verify one file block.
				var start time.Time
				if averageCount < 10 {
					start = time.Now()
				}

				hash := blocks.HashFromSlice(block.Hash)
				br, err := store.Get(ctx, hash)
				if err != nil {
					err := notif.Error(fmt.Errorf("fetching file block: %w", err))
					if err != nil {
						return err
					}
					continue
				}

				if opts.CheckHashes {
					blockData, err := blocks.ReadBlock(ctx, br)
					br.Close()
					if err != nil {
						err := notif.Error(fmt.Errorf("reading file block data: %w", err))
						if err != nil {
							return err
						}
						continue
					}

					calculatedHash := blocks.HashData(blockData)
					if hash != calculatedHash {
						err := notif.Error(HashMismatchError{Got: calculatedHash, Want: hash})
						if err != nil {
							return err
						}
						continue
					}
				} else {
					// Don't forget to close block
					br.Close()
				}

				if averageCount < 10 {
					dur := time.Now().Sub(start)
					totalFileBlockDur += dur
					averageCount++
				}
			}

			eta := time.Duration(totalBlocks-blockCount) * totalFileBlockDur / time.Duration(averageCount)

			err := notif.Update(
				fmt.Sprintf("(snapshot %v): verified file block set %%%v", i, j),
				float32(blockCount)*100.0/float32(totalBlocks),
				eta.String(),
			)
			if err != nil {
				return err
			}
		}
		// Only check first snapshot's file blocks
		if !opts.AllSnapshots {
			break
		}
	}

	return nil
}

type HashMismatchError struct {
	Got, Want blocks.Hash
}

func (e HashMismatchError) Error() string {
	return fmt.Sprintf("hash doesn't match: got: %v, want: %v", e.Got, e.Want)
}
