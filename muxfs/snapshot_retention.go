package muxfs

// import (
// 	"errors"
// 	"sort"
// 	"time"

// 	iso8601 "github.com/senseyeio/duration"
// )

// const (
// 	periodHourly  = "hourly"
// 	periodDaily   = "daily"
// 	periodWeekly  = "weekly"
// 	periodMonthly = "monthly"
// 	periodYearly  = "yearly"
// )

// type snapshotRetentionGroup struct {
// 	name     string
// 	capacity int
// 	next     nextSnapshot
// }

// type nextSnapshot interface {
// 	Date(last time.Time) time.Time
// }

// type snapshotPeriod time.Duration
// type snapshotPeriodFixed string

// var (
// 	// Special snapshot retention group used for keeping the latest snapshots
// 	// that are created whenever the filesystem is unmounted.
// 	latestRetentionGroup = &snapshotRetentionGroup{
// 		name:     "latest",
// 		capacity: 1,   // keep only the latest one
// 		next:     nil, // `next` is always nil, because no snapshots should be automatically scheduled.
// 	}

// 	// Special snapshot retention group used for keeping manual snapshots
// 	// that are created whenever the user runs the 'snap save' command.
// 	manualRetentionGroup = &snapshotRetentionGroup{
// 		name:     "manual",
// 		capacity: 0,   // Keep all snapshots forever
// 		next:     nil, // `next` is always nil, because no snapshots should be automatically scheduled.
// 	}

// 	errSnapshotInfosNotSorted = errors.New("snapshot infos are not sorted from recent to old")
// )

// func newGroup(name string, capacity int, period string) *snapshotRetentionGroup {
// 	next, err := nextSnapshotterFromString(period)
// 	if err != nil {
// 		panic(err)
// 	}

// 	return &snapshotRetentionGroup{
// 		name:     name,
// 		capacity: capacity,
// 		next:     next,
// 	}
// }

// func nextSnapshotterFromString(period string) (nextSnapshot, error) {
// 	switch period {
// 	case periodHourly, periodDaily, periodWeekly, periodMonthly, periodYearly:
// 		return snapshotPeriodFixed(period), nil
// 	default:
// 		isodur, err := iso8601.ParseISO8601(period)
// 		if err != nil {
// 			return nil, err
// 		}
// 		return snapshotPeriod(durationFromISO8601Duration(isodur)), nil
// 	}
// }

// func durationFromISO8601Duration(isodur iso8601.Duration) time.Duration {
// 	var total int64
// 	total += int64(isodur.Y) * 31556952000000000
// 	total += int64(isodur.M) * 2592000000000000
// 	total += int64(isodur.W) * 604800000000000
// 	total += int64(isodur.D) * 86400000000000
// 	total += int64(isodur.TH) * 3600000000000
// 	total += int64(isodur.TM) * 60000000000
// 	total += int64(isodur.TS) * 1000000000
// 	return time.Duration(total)
// }

// func (g *snapshotRetentionGroup) NextSnapshot(infos []*SnapshotInfo) time.Time {
// 	if len(infos) == 0 {
// 		return g.next.Date(time.Now())
// 	}

// 	// find most recent snapshot belonging to this group
// 	var recent *SnapshotInfo = infos[0]
// 	for _, info := range infos {
// 		if info.Meta.Tag != g.name {
// 			continue
// 		}
// 		if info.Meta.CreatedAt > recent.Meta.CreatedAt {
// 			recent = info
// 		}
// 	}

// 	recentTime := recent.timestamp()
// 	return g.next.Date(recentTime)
// }

// func (g *snapshotRetentionGroup) GetSnapshotsToRemove(infos []*SnapshotInfo) ([]*SnapshotInfo, error) {
// 	// keep all
// 	if g.capacity == 0 {
// 		return nil, nil
// 	}

// 	sorted := sort.SliceIsSorted(infos, func(i, j int) bool {
// 		return infos[i].Meta.CreatedAt > infos[j].Meta.CreatedAt
// 	})
// 	if !sorted {
// 		return nil, errSnapshotInfosNotSorted
// 	}

// 	var keep []*SnapshotInfo
// 	// keep and update previous snapshot every iteration
// 	var prevInfo *SnapshotInfo
// 	// keep track of how many snapshots we've kept for this group
// 	var kept int
// 	for _, info := range infos {
// 		// ignore snapshots not belonging to this group
// 		if info.Meta.Tag != g.name {
// 			continue
// 		}
// 		// always keep the first snapshot
// 		if prevInfo == nil {
// 			keep = append(keep, info)
// 			kept++
// 			prevInfo = info
// 			continue
// 		}
// 		// we have all the snapshots we need for this group, stop
// 		if kept >= g.capacity {
// 			break
// 		}

// 		prevInfoTime, infoTime := prevInfo.timestamp(), info.timestamp()

// 		// if the difference is greater than the frequency declared in the group, we keep it
// 		if g.next.Date(prevInfoTime).After(g.next.Date(infoTime)) {
// 			keep = append(keep, info)
// 			kept++
// 			prevInfo = info
// 		}
// 	}

// 	for _, info := range infos {
// 		// ignore snapshots not belonging to this group
// 		if info.Meta.Tag != g.name {
// 			continue
// 		}
// 		// we have all the snapshots we need for this group, stop
// 		if kept >= g.capacity {
// 			break
// 		}

// 		// dont add already kept snapshots
// 		for _, k := range keep {
// 			if k == info {
// 				continue
// 			}
// 		}

// 		keep = append(keep, info)
// 		kept++
// 	}

// 	var toRemove []*SnapshotInfo
// Outer:
// 	for _, info := range infos {
// 		// ignore snapshots not belonging to this group
// 		if info.Meta.Tag != g.name {
// 			continue
// 		}

// 		for _, k := range keep {
// 			if info == k {
// 				continue Outer
// 			}
// 		}
// 		toRemove = append(toRemove, info)
// 	}

// 	return toRemove, nil
// }

// func (p snapshotPeriod) Date(last time.Time) time.Time {
// 	return last.Add(time.Duration(p))
// }

// func (p snapshotPeriodFixed) Date(last time.Time) time.Time {
// 	var next time.Time
// 	switch p {
// 	case "hourly":
// 		// next hour
// 		next = time.Date(last.Year(), last.Month(), last.Day(), last.Hour()+1, 0, 0, 0, last.Location())
// 	case "daily":
// 		// next day
// 		next = time.Date(last.Year(), last.Month(), last.Day()+1, 0, 0, 0, 0, last.Location())
// 	case "weekly":
// 		// next week
// 		startDay := time.Date(last.Year(), last.Month(), last.Day(), 0, 0, 0, 0, last.Location())
// 		weekday := startDay.Weekday()
// 		if weekday < time.Monday {
// 			weekday = weekday + 7 - time.Monday
// 		} else {
// 			weekday = weekday - time.Monday
// 		}
// 		next = startDay.AddDate(0, 0, int(-weekday)).AddDate(0, 0, 7)
// 	case "monthly":
// 		// next month
// 		y, m, _ := last.Date()
// 		next = time.Date(y, m+1, 1, 0, 0, 0, 0, last.Location())
// 	case "yearly":
// 		// next year
// 		next = time.Date(last.Year()+1, 1, 1, 0, 0, 0, 0, last.Location())
// 	}
// 	return next
// }
