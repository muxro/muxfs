package muxfs

import (
	"encoding/binary"

	pb "gitlab.com/muxro/muxfs/muxfs/internal/pb"
)

type node interface {
	Meta() Meta
}

const metaSize = 28

type Meta struct {
	Size      int64
	Mode      uint32
	Uid       uint32
	Gid       uint32
	UpdatedAt int64
}

func metaFromProto(pbmeta *pb.NodeMeta) Meta {
	return Meta{
		Size:      pbmeta.Size,
		Mode:      pbmeta.Mode,
		Uid:       pbmeta.Uid,
		Gid:       pbmeta.Gid,
		UpdatedAt: pbmeta.UpdatedAt,
	}
}

func encodeMeta(m *Meta, b []byte) {
	binary.BigEndian.PutUint64(b, uint64(m.Size))
	b = b[8:]

	binary.BigEndian.PutUint32(b, m.Mode)
	b = b[4:]

	binary.BigEndian.PutUint32(b, m.Uid)
	b = b[4:]

	binary.BigEndian.PutUint32(b, m.Gid)
	b = b[4:]

	binary.BigEndian.PutUint64(b, uint64(m.UpdatedAt))
}

func decodeMeta(m *Meta, b []byte) {
	m.Size = int64(binary.BigEndian.Uint64(b))
	b = b[8:]

	m.Mode = binary.BigEndian.Uint32(b)
	b = b[4:]

	m.Uid = binary.BigEndian.Uint32(b)
	b = b[4:]

	m.Gid = binary.BigEndian.Uint32(b)
	b = b[4:]

	m.UpdatedAt = int64(binary.BigEndian.Uint64(b))
}

// The SetMetaValid are bit flags describing which fields in the opSetMeta
// are included in the change.
type SetMetaValid uint32

const (
	SetMetaMode      SetMetaValid = 1 << 0
	SetMetaUid       SetMetaValid = 1 << 1
	SetMetaGid       SetMetaValid = 1 << 2
	SetMetaSize      SetMetaValid = 1 << 3
	SetMetaUpdatedAt SetMetaValid = 1 << 4
)

func (fl SetMetaValid) Mode() bool      { return fl&SetMetaMode != 0 }
func (fl SetMetaValid) Uid() bool       { return fl&SetMetaUid != 0 }
func (fl SetMetaValid) Gid() bool       { return fl&SetMetaGid != 0 }
func (fl SetMetaValid) Size() bool      { return fl&SetMetaSize != 0 }
func (fl SetMetaValid) UpdatedAt() bool { return fl&SetMetaUpdatedAt != 0 }
