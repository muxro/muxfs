package muxfs

import (
	"context"
	"os"
	"testing"

	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/require"
	"gitlab.com/muxro/muxfs/blocks"
	pb "gitlab.com/muxro/muxfs/muxfs/internal/pb"
)

type fileReader struct {
	*File
	pos int64
}

func (ra *fileReader) Read(b []byte) (int, error) {
	n, err := ra.File.ReadAt(b, ra.pos)
	ra.pos += int64(n)
	return n, err
}

func openFile(t *testing.T, fs *FS, path string) fileReader {
	f, err := fs.Open(context.TODO(), path)
	require.NoError(t, err)

	return fileReader{File: f}
}

// snapRefRoot global so we can compare FS objects directly (they all have the same refRoot)
var snapRefRoot = blocks.GenerateRefRootID()

func CreateTestFS(t *testing.T, store blocks.Store, ops ...func(*testing.T, blocks.Store, *pb.Snapshot)) *FS {
	snap := &pb.Snapshot{
		Root: &pb.Dir{
			Meta:     &pb.NodeMeta{Mode: uint32(0755 | os.ModeDir)},
			Children: make(map[string]*pb.Node),
		},
		Meta: &pb.SnapshotMeta{
			Tag: "",
		},
	}
	//	Create refRoot
	require.NoError(t, store.NewRefRoot(context.TODO(), snapRefRoot))

	for _, op := range ops {
		op(t, store, snap)
	}

	// Add snapshot to store so we can fetch it
	data, err := proto.Marshal(snap)
	require.NoError(t, err)
	hash := blocks.HashData(data)
	err = store.Put(context.TODO(), snapRefRoot, hash, blocks.NewMemoryBlock(data))
	require.NoError(t, err)

	fs, err := FromSnapshot(context.TODO(), store, hash)
	require.NoError(t, err)

	return fs
}

func CreateFile(name string, size int64) func(t *testing.T, store blocks.Store, snap *pb.Snapshot) {
	return func(t *testing.T, store blocks.Store, snap *pb.Snapshot) {
		// Create file node
		snap.Root.Children[name] = &pb.Node{Value: &pb.Node_File{File: &pb.File{
			Meta: &pb.NodeMeta{
				Size: size,
				Mode: 0755,
				Uid:  1000,
				Gid:  100,
			},
			BlocksLocations: []*pb.BlocksLocation{
				&pb.BlocksLocation{BlockList: 0, BlockNo: uint32(len(snap.FileBlocks) - 1)},
			},
		}}}

		data := blocks.TestData(name, 0, size)
		hash := blocks.HashData(data)

		// Add snap file blocks
		snap.FileBlocks = append(snap.FileBlocks,
			&pb.BlockList{Blocks: []*pb.Block{
				&pb.Block{Hash: hash[:], Offset: 0},
			}},
		)

		// Add block to store
		// FIXME: no more refroot
		err := store.Put(context.TODO(), snapRefRoot, hash, blocks.NewMemoryBlock(data))
		require.NoError(t, err)
	}
}
