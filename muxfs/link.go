package muxfs

type link struct {
	meta   Meta
	target string
}

func (l *link) Meta() Meta {
	return l.meta
}
