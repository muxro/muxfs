package muxfs

import (
	"context"
	"fmt"
	"io/ioutil"

	"github.com/golang/protobuf/proto"
	"gitlab.com/muxro/muxfs/blocks"
	pb "gitlab.com/muxro/muxfs/muxfs/internal/pb"
)

type Snapshot struct {
	Version uint32
	Meta    SnapshotMeta
	Users   []*pb.Owner
	Groups  []*pb.Owner

	Root       *dir
	FileBlocks [][]*pb.Block
}

type SnapshotInfo struct {
	Hash blocks.Hash
	Meta SnapshotMeta
}

type SnapshotMeta struct {
	RefRoot   []byte
	Tag       string
	CreatedAt int64
}

func snapshotMetaFromProto(pbmeta *pb.SnapshotMeta) SnapshotMeta {
	return SnapshotMeta{
		Tag:       pbmeta.Tag,
		CreatedAt: pbmeta.CreatedAt,
	}
}

func fetchSnapshot(ctx context.Context, store blocks.Store, hash blocks.Hash) (*Snapshot, error) {
	pbsnap, err := fetchPartialSnapshot(ctx, store, hash)
	if err != nil {
		return nil, err
	}

	return snapshotFromPartialSnapshot(ctx, store, pbsnap)
}

// fetchPartialSnapshot reads a snapshot block from the store, and returns it as an unmarshaled *pb.Snapshot object.
func fetchPartialSnapshot(ctx context.Context, store blocks.Store, hash blocks.Hash) (*pb.Snapshot, error) {
	snapshot := &pb.Snapshot{}

	// get Snapshot from store
	snapReader, err := store.Get(ctx, hash)
	if err != nil {
		return nil, fmt.Errorf("snapshot block does not exist: %w", err)
	}
	defer snapReader.Close()

	snapData, err := ioutil.ReadAll(blocks.Reader(ctx, snapReader))
	if err != nil {
		return nil, err
	}

	err = proto.Unmarshal(snapData, snapshot)
	if err != nil {
		return nil, err
	}

	return snapshot, nil
}

func snapshotFromPartialSnapshot(ctx context.Context, store blocks.Store, psnap *pb.Snapshot) (*Snapshot, error) {
	root := defaultRoot()
	snap := &Snapshot{
		Version: psnap.Version,
		Meta:    snapshotMetaFromProto(psnap.Meta),
		Users:   psnap.Users,
		Groups:  psnap.Groups,
		Root:    &root,
	}

	// Merge and convert initial snapshot root
	err := addChildren(snap.Root, psnap.Root)
	if err != nil {
		return nil, err
	}

	// Download extra entry root blocks if they exist, and convert and merge them
	if psnap.ExtraRoots != nil {
		for _, rootBlock := range psnap.ExtraRoots.Blocks {
			br, err := store.Get(ctx, blocks.HashFromSlice(rootBlock.Hash))
			if err != nil {
				return nil, err
			}
			blockData, err := ioutil.ReadAll(blocks.Reader(ctx, br))
			if err != nil {
				return nil, err
			}
			br.Close()

			root, err := deserializeTree(blockData)
			if err != nil {
				return nil, err
			}

			err = addChildren(snap.Root, root)
			if err != nil {
				return nil, err
			}
		}
	}

	// Add local FileBlocks
	for _, blockList := range psnap.FileBlocks {
		snap.FileBlocks = append(snap.FileBlocks, blockList.Blocks)
	}
	// Download the rest of the FileBlocks and add them
	if psnap.ExtraFileBlocks != nil {
		for _, extraBlock := range psnap.ExtraFileBlocks.Blocks {
			br, err := store.Get(ctx, blocks.HashFromSlice(extraBlock.Hash))
			if err != nil {
				return nil, err
			}
			extraBlockData, err := ioutil.ReadAll(blocks.Reader(ctx, br))
			if err != nil {
				return nil, err
			}
			br.Close()
			blocks, err := deserializeBlockList(extraBlockData)
			if err != nil {
				return nil, err
			}
			snap.FileBlocks = append(snap.FileBlocks, blocks)
		}
	}

	return snap, nil
}

func hashPartialSnapshot(psnap *pb.Snapshot) (blocks.Hash, error) {
	var emptyHash blocks.Hash
	data, err := proto.Marshal(psnap)
	if err != nil {
		return emptyHash, err
	}

	return blocks.HashData(data), nil
}
