package daemon

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"sync/atomic"
	"time"

	bazilfuse "bazil.org/fuse"
	"github.com/google/uuid"
	middleware "gitlab.com/go-figure/grpc-middleware"
	common "gitlab.com/go-figure/grpc-middleware-common"
	"gitlab.com/go-figure/logr"
	"gitlab.com/muxro/muxfs/blocks"
	"gitlab.com/muxro/muxfs/fuse"
	"gitlab.com/muxro/muxfs/muxfs"
	"google.golang.org/grpc"

	pb "gitlab.com/muxro/muxfs/daemon/pb"
)

var (
	SockPerm = 0600 | os.ModeSocket

	// TODO: these error functions need to be a separate error type
	errFSAlreadyExists = func(s string) error {
		return fmt.Errorf("fs %v already exists", s)
	}
	errMountsetNotFound = func(name string) error {
		return fmt.Errorf("mountset entry for path %v does not exist", name)
	}
	errNoFSForPath = func(path string) error {
		return fmt.Errorf("no fs found for path %v", path)
	}
	errMountNotFound = func(path string) error {
		return fmt.Errorf("no mount found at path %v", path)
	}
	errStoreConfigNotFound = func(name string) error {
		return fmt.Errorf("no store config named %v could be found", name)
	}
	errDirNotEmpty = errors.New("target directory is not empty")
)

type daemon struct {
	ln           net.Listener
	srv          *grpc.Server
	cfg          *Config
	sockAddr     string
	mounts       map[string]*mountInfo
	startupError error
	activeMounts int32
	suicideTimer *time.Timer
}

type fsInstanceOptions struct {
	Readonly     bool
	Name         string
	StoreConfig  string
	HashOverride blocks.Hash
	// SnapshotsConfig []*SnapshotConfig
}

type fsMountOptions struct {
	Path string
}

type mountInfo struct {
	FS                *muxfs.FS
	FSInstanceOptions *fsInstanceOptions
	Unmounted         chan bool
}

type fsAlreadyMountedError struct {
	fs   string
	path string
}

func New(ctx context.Context, configPath, sockAddr string) (_ *daemon, err error) {
	ctx, job := logr.Task(ctx, "startup", nil)
	defer job.DeferDone(&err, nil)

	// Remove old UNIX socket if it exists
	if err := os.Remove(sockAddr); err != nil && !os.IsNotExist(err) {
		return nil, err
	}

	// Get UNIX socket listener (creates a new socket)
	ln, err := net.Listen("unix", sockAddr)
	if err != nil {
		// startupJob.Error("listen_error", err, nil)
		return nil, err
	}

	// Make sure the socket has the correct permissions
	err = os.Chmod(sockAddr, SockPerm)
	if err != nil {
		// startupJob.Error("chmod_error", err, nil)
		return nil, err
	}

	s := &daemon{
		ln:       ln,
		sockAddr: sockAddr,
		mounts:   make(map[string]*mountInfo),
	}

	// Daemon.Start()
	// ctx, cancel := context.WithCancel(ctx)
	s.startSuicideTimer(nil)
	// ----

	// Open config file
	cfg, err := NewConfig(configPath)
	if err != nil {
		// Daemon should still run, but only so it can reply with the
		// startup error in the Hello method, and then kill itself.
		// If no Hello request is sent, then the server kills itself after 10 seconds anyway.
		s.startupError = fmt.Errorf("invalid daemon config file: %w", err)
		s.SuicideAfter(10 * time.Second)
	} else {
		s.cfg = cfg
	}

	// Middlewares
	mids := []middleware.Middleware{
		common.AddInstrumentation(ctx),
		common.CallID,
		common.Logging,
		common.RecoverPanic,
	}

	// Make GRPC server and use our middleware
	srv := grpc.NewServer(
		grpc.UnaryInterceptor(middleware.UnaryServerInterceptor(mids...)),
		grpc.StreamInterceptor(middleware.StreamServerInterceptor(mids...)),
	)
	pb.RegisterDaemonServer(srv, s)
	s.srv = srv

	return s, nil
}

func (s *daemon) Start() error {
	return s.srv.Serve(s.ln)
}

func (s *daemon) Stop() {
	s.srv.GracefulStop()
}

func (s *daemon) startSuicideTimer(cancel context.CancelFunc) {
	// Init and stop suicide timer
	s.suicideTimer = time.NewTimer(999 * time.Second)
	s.suicideTimer.Stop()

	go func() {
		<-s.suicideTimer.C
		cancel()
	}()
}

func (s *daemon) SuicideAfter(d time.Duration) {
	s.suicideTimer.Reset(d)
}

func (s *daemon) Hello(ctx context.Context, req *pb.HelloRequest) (*pb.HelloReply, error) {
	reply := &pb.HelloReply{}
	if s.startupError != nil {
		reply.Error = s.startupError.Error()
		s.SuicideAfter(time.Second)
	}

	return reply, nil
}

func (s *daemon) FSAdd(ctx context.Context, req *pb.FSAddRequest) (*pb.FSAddReply, error) {
	name := req.GetName()
	hashStr := req.GetHash()
	fsStoreConfigName, err := s.getDefaultStoreConfigName(req.GetFSStoreConfigName())
	if err != nil {
		return nil, err
	}

	snapStoreConfigName := req.GetSnapshotStoreConfigName()
	if snapStoreConfigName == "" {
		snapStoreConfigName = fsStoreConfigName
	} else {
		var err error
		snapStoreConfigName, err = s.getDefaultStoreConfigName(req.GetSnapshotStoreConfigName())
		if err != nil {
			return nil, err
		}
	}

	_, err = s.cfg.FSGet(name)
	if err == nil {
		return nil, errFSAlreadyExists(name)
	}

	// fsStore, err := s.instanceStoreFromConfig(ctx, fsStoreConfigName)
	// if err != nil {
	// 	return nil, err
	// }
	// snapStore, err := s.instanceStoreFromConfig(ctx, snapStoreConfigName)
	// if err != nil {
	// 	return nil, err
	// }

	// TODO: when creating a new snapshot as a clone of another snapshot, need to copy refroots (and
	// possibly all file blocks)
	if hashStr != "new" {
		panic("fs add snapshot unimplemented")
		// snapHash, err := blocks.HashFromString(hashStr)
		// if err != nil {
		// 	return nil, err
		// }

		// var newSnapInfo *SnapshotInfo
		// // If the stores are the same, that means the file blocks already exist,
		// // in which case we can just copy the refRoot, and remove the ref to the old snapshot block.
		// if snapStoreConfigName == fsStoreConfigName {
		// 	snapshot, err := GetSnapshot(ctx, snapStore, snapHash)
		// 	if err != nil {
		// 		return nil, err
		// 	}

		// 	// Remember old snapshot refRoot, because we will need to remove the old snapshot block from it.
		// 	oldRefRoot := blocks.IDFromByteSlice(snapshot.Meta.Ref)
		// 	// Generate a new refRoot for the new snapshot block we are going to copy to FS store.
		// 	newRefRoot := blocks.GenerateRefRootID()

		// 	// Set new refRoot on snapshot
		// 	snapshot.Meta.Ref = newRefRoot[:]
		// 	// Get new snapshot data and hash
		// 	data, err := snapshot.data()
		// 	if err != nil {
		// 		return nil, err
		// 	}
		// 	newSnapHash := snapshot.hashFromData(data)

		// 	err = fsStore.CopyRefRoot(ctx, newRefRoot, oldRefRoot)
		// 	if err != nil {
		// 		return nil, err
		// 	}
		// 	// Add new snapshot block to FS store
		// 	err = fsStore.Put(ctx, newRefRoot, newSnapHash, bytes.NewReader(data))
		// 	if err != nil {
		// 		return nil, err
		// 	}
		// 	// Remove old snapshot block
		// 	err = fsStore.RemoveRefs(ctx, newRefRoot, []blocks.Hash{snapHash})
		// 	if err != nil {
		// 		return nil, err
		// 	}

		// 	newSnapInfo = &SnapshotInfo{
		// 		Meta: snapshot.Meta,
		// 		Hash: newSnapHash[:],
		// 	}
		// } else {
		// 	// If the store where the snapshot (and blocks) are coming from is different from the FS store,
		// 	// we have to *copy* all file blocks to the FS store.
		// 	var err error
		// 	newSnapInfo, err = copySnapshotToStore(ctx, snapHash, fsStore, snapStore)
		// 	if err != nil {
		// 		return nil, err
		// 	}
		// }

		// snapshotUpdater, err := NewSnapshotUpdater(fsStore, uuid)
		// if err != nil {
		// 	return nil, err
		// }
		// err = snapshotUpdater.Set(ctx, []*SnapshotInfo{newSnapInfo})
		// if err != nil {
		// 	return nil, err
		// }
	}

	err = s.cfg.FSAdd(&FSConfig{Name: name, StoreConfig: fsStoreConfigName})
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("fs_add", logr.KV{"fs.name": name})
	return &pb.FSAddReply{Message: fmt.Sprintf("Added FS %v", name)}, nil
}

func (s *daemon) FSClone(ctx context.Context, req *pb.FSCloneRequest) (*pb.FSCloneReply, error) {
	panic("fs clone unimplemented")
	// sourceName := req.GetSourceName()
	// newName := req.GetNewName()

	// sourceCfg, err := s.cfg.FSGet(sourceName)
	// if err != nil {
	// 	return nil, err
	// }

	// err = s.fsClone(ctx, sourceCfg, newName)
	// if err != nil {
	// 	return nil, err
	// }

	// return &pb.FSCloneReply{Message: "Cloned"}, nil
}

// TODO: fs.clone()
// func (s *daemon) fsClone(ctx context.Context, sourceCfg *FSConfig, newName string) error {
// 	sourceFS, err := s.instanceFS(ctx, &fsInstanceOptions{
// 		Readonly:        true,
// 		Name:            sourceCfg.Name,
// 		UUID:            sourceCfg.UUID,
// 		StoreConfig:     sourceCfg.StoreConfig,
// 		SnapshotsConfig: sourceCfg.SnapshotsConfig,
// 	})
// 	if err != nil {
// 		return err
// 	}
// 	defer sourceFS.close()

// 	newFS, err := sourceFS.clone(nil)
// 	if err != nil {
// 		return err
// 	}
// 	defer newFS.close()

// 	return s.cfg.FSAdd(&FSConfig{
// 		Name:            newName,
// 		UUID:            newFS.uuid,
// 		StoreConfig:     sourceCfg.StoreConfig,
// 		SnapshotsConfig: sourceCfg.SnapshotsConfig,
// 	})
// }

func (s *daemon) FSMigrateStore(ctx context.Context, req *pb.FSMigrateStoreRequest) (*pb.FSMigrateStoreReply, error) {
	// TODO: migrate store
	panic("migrate store unimplemented")
	// fsName := req.GetFSName()
	// newStoreConfigName := req.GetNewStoreConfig()

	// fsCfg, err := s.cfg.FSGet(fsName)
	// if err != nil {
	// 	return nil, err
	// }

	// fs, err := s.instanceFS(ctx, &fsInstanceOptions{
	// 	Readonly:        true,
	// 	Name:            fsCfg.Name,
	// 	UUID:            fsCfg.UUID,
	// 	StoreConfig:     fsCfg.StoreConfig,
	// 	SnapshotsConfig: fsCfg.SnapshotsConfig,
	// })
	// if err != nil {
	// 	return nil, err
	// }
	// defer fs.close()

	// store, err := s.instanceStoreFromConfig(ctx, newStoreConfigName)
	// if err != nil {
	// 	return nil, err
	// }

	// err = fs.migrateStore(store)
	// if err != nil {
	// 	return nil, err
	// }

	// fsCfg.StoreConfig = newStoreConfigName

	// err = s.cfg.FSAdd(fsCfg)
	// if err != nil {
	// 	return nil, err
	// }

	// return &pb.FSMigrateStoreReply{Message: "MigrateStored"}, nil
}

func (s *daemon) FSDestroy(ctx context.Context, req *pb.FSDestroyRequest) (*pb.FSDestroyReply, error) {
	// TODO: FS destroy
	panic("fs destroy unimplemented")
	// name := req.GetName()

	// fsConfig, err := s.cfg.FSGet(name)
	// if err != nil {
	// 	return nil, err
	// }

	// err = s.fsDestroy(ctx, fsConfig)
	// if err != nil {
	// 	return nil, err
	// }

	// logr.FromContext(ctx).Event("fs_destroy", logr.KV{"fs.name": name})
	// return &pb.FSDestroyReply{Message: fmt.Sprintf("Destroyed FS %v", name)}, nil
}

// TODO: fs.destroy()
// func (s *daemon) fsDestroy(ctx context.Context, fsConfig *FSConfig) error {
// 	fs, err := s.instanceFS(ctx, &fsInstanceOptions{
// 		Readonly:        true,
// 		Name:            fsConfig.Name,
// 		UUID:            fsConfig.UUID,
// 		StoreConfig:     fsConfig.StoreConfig,
// 		SnapshotsConfig: fsConfig.SnapshotsConfig,
// 	})
// 	if err != nil {
// 		return err
// 	}
// 	defer fs.close()

// 	err = fs.destroy()
// 	if err != nil {
// 		return fmt.Errorf("can't destroy fs: %w", err)
// 	}

// 	return s.cfg.FSRemove(fsConfig.Name)
// }

func (s *daemon) FSList(ctx context.Context, req *pb.FSListRequest) (*pb.FSListReply, error) {
	fses := s.cfg.FSList()

	return &pb.FSListReply{
		FSes: fses,
	}, nil
}

type verifyNotifier struct {
	server pb.Daemon_FSVerifyServer
}

func newVerifyNotifier(server pb.Daemon_FSVerifyServer) verifyNotifier {
	return verifyNotifier{server: server}
}

func (v verifyNotifier) Update(message string, percent float32, eta string) error {
	return v.server.Send(&pb.FSVerifyReply{
		Message: message,
		Percent: percent,
		ETA:     eta,
	})
}

func (v verifyNotifier) Error(err error) error {
	return v.server.Send(&pb.FSVerifyReply{
		Err: err.Error(),
	})
}

func (s *daemon) FSVerify(req *pb.FSVerifyRequest, server pb.Daemon_FSVerifyServer) error {
	ctx := server.Context()
	name := req.GetName()
	checkHashes := req.CheckHashes
	allSnapshots := req.AllSnapshots

	fsConfig, err := s.cfg.FSGet(name)
	if err != nil {
		return err
	}

	fsbhash, err := blocks.HashFromString(fsConfig.Block)
	if err != nil {
		return err
	}

	store, err := s.instanceStoreFromConfig(ctx, fsConfig.StoreConfig)
	if err != nil {
		return err
	}

	return muxfs.Verify(ctx, store, fsbhash,
		muxfs.VerifyOptions{CheckHashes: checkHashes, AllSnapshots: allSnapshots},
		newVerifyNotifier(server))
}

func (s *daemon) mountFS(ctx context.Context, instanceOpts *fsInstanceOptions, mountOpts *fsMountOptions) error {
	fs, err := s.instanceFS(ctx, instanceOpts)
	if err != nil {
		return fmt.Errorf("instancing FS from config: %w", err)
	}

	ready := make(chan error)
	go func() {
		err := fuse.Mount(ctx, mountOpts.Path, fs, ready)
		if err != nil {
			ready <- err
		}
	}()

	err = <-ready
	// Mount has errors
	if err != nil {
		return err
	}
	// When mount is OK and ready

	// Keep track of active mounts
	atomic.AddInt32(&s.activeMounts, 1)
	s.mounts[mountOpts.Path] = newMountInfo(instanceOpts, fs)

	// Don't suicide
	s.suicideTimer.Stop()

	return nil
}

func (s *daemon) instanceStoreFromConfig(ctx context.Context, storeConfigName string) (blocks.Store, error) {
	// try to find store for this fs
	storeConfig, ok := s.cfg.StoreConfigs[storeConfigName]
	if !ok {
		return nil, errStoreConfigNotFound(storeConfigName)
	}

	return storeConfig.Config.Instance(ctx)
}

func (s *daemon) getDefaultStoreConfigName(storeConfigName string) (string, error) {
	if storeConfigName != "" {
		return storeConfigName, nil
	}

	if len(s.cfg.StoreConfigs) > 1 {
		return "", errors.New("more than one store configs exist and no store config name was specified")
	}

	// only one element
	for name := range s.cfg.StoreConfigs {
		return name, nil
	}

	// 0 elements
	return "", errors.New("no store configs exist")
}

func (s *daemon) Mount(ctx context.Context, req *pb.MountRequest) (*pb.MountReply, error) {
	fsName := req.GetFS()
	path := req.GetPath()
	readonly := req.GetReadonly()

	for path, mount := range s.mounts {
		if mount.FSInstanceOptions.Name == fsName {
			return nil, fsAlreadyMountedError{fs: fsName, path: path}
		}
	}

	fsConfig, err := s.cfg.FSGet(fsName)
	if err != nil {
		return nil, err
	}

	err = s.mountFS(ctx, &fsInstanceOptions{
		Readonly:    readonly,
		Name:        fsConfig.Name,
		StoreConfig: fsConfig.StoreConfig,
		// TODO: snapshotconfig
		// SnapshotsConfig: fsConfig.SnapshotsConfig,
	}, &fsMountOptions{
		Path: path,
	})
	if err != nil {
		return nil, err
	}

	return &pb.MountReply{Message: fmt.Sprintf("Mounted %v at %v", fsName, path)}, nil
}

func (s *daemon) instanceFS(ctx context.Context, opts *fsInstanceOptions) (*muxfs.FS, error) {
	// TODO: snapshotconfig
	// var snapshotGroups []*snapshotRetentionGroup
	// for _, groupCfg := range opts.SnapshotsConfig {
	// 	group, err := groupCfg.Instance()
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	snapshotGroups = append(snapshotGroups, group)
	// }

	store, err := s.instanceStoreFromConfig(ctx, opts.StoreConfig)
	if err != nil {
		return nil, err
	}

	fsCfg, ok := s.cfg.Filesystems[opts.Name]
	if !ok {
		return nil, &fsNotFoundError{Name: opts.Name}
	}

	// If the fs block is empty, we return a new blank FS.
	if fsCfg.Block == "" {
		return muxfs.New(ctx, store)
	}

	fsBlockHash, err := blocks.HashFromString(fsCfg.Block)
	if err != nil {
		return nil, err
	}

	return muxfs.FromBlock(ctx, store, fsBlockHash)

	// var (
	// 	snapshot     *Snapshot
	// 	snapshotHash blocks.Hash
	// 	emptyHash    blocks.Hash
	// )
	// if opts.HashOverride != emptyHash {
	// 	// TODO: Load snapshot from a specific hash
	// 	// // Load snapshot from a specific hash
	// 	// var err error
	// 	// snapshot, err = GetSnapshot(ctx, store, opts.HashOverride)
	// 	// if err != nil {
	// 	// 	return nil, err
	// 	// }
	// 	// snapshotHash = opts.HashOverride
	// } else {
	// 	// Get latest snapshot from updater
	// 	infos, err := snapshotUpdater.Get(ctx)
	// 	if err != nil {
	// 		return nil, err
	// 	}

	// 	if len(infos) > 0 {
	// 		var err error
	// 		snapshotHash = blocks.HashFromSlice(infos[0].Hash)
	// 		snapshot, err = GetSnapshot(ctx, store, snapshotHash)
	// 		if err != nil {
	// 			return nil, err
	// 		}
	// 	} else {
	// 		// Instance new empty snapshot
	// 		snapshot = &Snapshot{
	// 			Meta: &SnapshotMeta{
	// 				CreatedAt: time.Now().Unix(),
	// 				Tag:       latestRetentionGroup.name,
	// 				Ref:       nil,
	// 			},
	// 			Entries: []*Entry{&Entry{Path: "/", Meta: &EntryMeta{Size: 4096, Mode: uint32(0755 | os.ModeDir)}}},
	// 		}
	// 		snapshotHash, err = snapshot.hash()
	// 		if err != nil {
	// 			return nil, err
	// 		}
	// 	}
	// }

	// // Link notifier to our mount manager
	// // It will also be registered to the new FS inside Mount
	// notify := &FSNotifier{manager: s}

	// fs, err := NewFS(&FSOptions{
	// 	Context:         ctx,
	// 	UUID:            opts.UUID,
	// 	Store:           store,
	// 	Snapshot:        snapshot,
	// 	SnapshotHash:    snapshotHash,
	// 	SnapshotUpdater: snapshotUpdater,
	// 	// TODO: snapshotconfig
	// 	// SnapshotRetentionGroups: snapshotGroups,
	// 	Notify:                notify,
	// 	HasCacheMeasureCommit: true,
	// 	MaxCacheSize:          400 * 1024 * 1024,
	// 	HasActivityCommit:     true,
	// })
	// if err != nil {
	// 	return nil, err
	// }

	// if !opts.Readonly {
	// 	err = fs.enableWrites()
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// }

	// return fs, nil
}

type fsNotFoundError struct {
	Name string
}

func (e *fsNotFoundError) Error() string {
	return fmt.Sprintf("fs %v not found", e.Name)
}

func (s *daemon) Unmount(ctx context.Context, req *pb.UnmountRequest) (*pb.UnmountReply, error) {
	fs := req.GetFS()
	path := req.GetPath()

	var mount *mountInfo
	for mountpath, mountinfo := range s.mounts {
		if fs == mountinfo.FSInstanceOptions.Name {
			mount = mountinfo
			path = mountpath
		}
	}

	if mount == nil {
		var ok bool
		mount, ok = s.mounts[path]
		if !ok {
			return nil, errMountNotFound(path)
		}
	}

	err := bazilfuse.Unmount(path)
	if err != nil {
		return nil, err
	}
	<-mount.Unmounted

	// remove this FSs mountinfo
	delete(s.mounts, path)
	atomic.AddInt32(&s.activeMounts, -1)

	// see if the server should be killed
	if atomic.LoadInt32(&s.activeMounts) == 0 {
		s.SuicideAfter(time.Second)
	}

	logr.FromContext(ctx).Event("fs_unmount", logr.KV{"fs.name": fs, "path": path})
	return &pb.UnmountReply{Message: fmt.Sprintf("Unmounted %v", path)}, nil
}

func (s *daemon) NukePath(ctx context.Context, req *pb.NukePathRequest) (*pb.NukePathReply, error) {
	fs := req.GetFS()
	filePath := req.GetFilePath()

	panic("nuke-path unimplemented")

	// // s.cfg.Filesystems[]
	// for fsName, fsConfig := range s.cfg.Filesystems {
	// 	if fsName == fs {

	// 	}
	// }

	logr.FromContext(ctx).Event("fs_nuke_path", logr.KV{"fs.name": fs, "file_path": filePath})
	return &pb.NukePathReply{Message: fmt.Sprintf("NukePath %v %v", fs, filePath)}, nil
}

func (s *daemon) SnapSave(ctx context.Context, req *pb.SnapSaveRequest) (*pb.SnapSaveReply, error) {
	path := req.GetPath()
	mount, ok := s.mounts[path]
	if !ok {
		return nil, errMountNotFound(path)
	}

	// TODO: snapshotconfig
	// err := mount.FS.save(manualRetentionGroup)
	// if err != nil {
	// 	return nil, err
	// }

	name := mount.FSInstanceOptions.Name
	logr.FromContext(ctx).Event("snap_save", logr.KV{"fs.name": name, "path": path})
	return &pb.SnapSaveReply{Message: fmt.Sprintf("Created snapshot for FS %v at path %v\n", name, path)}, nil
}

func (s *daemon) SnapPreview(ctx context.Context, req *pb.SnapPreviewRequest) (*pb.SnapPreviewReply, error) {
	hash := req.GetHash()
	path := req.GetPath()
	storeConfigName, err := s.getDefaultStoreConfigName(req.GetStoreConfigName())
	if err != nil {
		return nil, err
	}

	uuid := uuid.New().String()
	err = s.mountFS(ctx, &fsInstanceOptions{
		Readonly:     true,
		Name:         uuid,
		HashOverride: blocks.HashFromSlice(hash),
		StoreConfig:  storeConfigName,
		// TODO: snapshotconfig
		// SnapshotsConfig: nil,
	}, &fsMountOptions{
		Path: path,
	})
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("snap_preview", logr.KV{"fs.uuid": uuid, "path": path})
	return &pb.SnapPreviewReply{Message: fmt.Sprintf("Mounted %v at %v (readonly)", uuid, path)}, nil
}

func (s *daemon) SnapList(ctx context.Context, req *pb.SnapListRequest) (*pb.SnapListReply, error) {
	fsName := req.GetFS()

	fsConfig, err := s.cfg.FSGet(fsName)
	if err != nil {
		return nil, err
	}

	fs, err := s.instanceFS(ctx, &fsInstanceOptions{
		Readonly:    true,
		Name:        fsConfig.Name,
		StoreConfig: fsConfig.StoreConfig,
		// TODO: snapshotconfig
		// SnapshotsConfig: fsConfig.SnapshotsConfig,
	})
	if err != nil {
		return nil, err
	}

	var snapsDisplay []string
	for _, snapInfo := range fs.Snapshots() {
		tag := snapInfo.Meta.Tag
		date := time.Unix(snapInfo.Meta.CreatedAt, 0).Format("2006.01.02-15:04:05")
		snapDisplay := fmt.Sprintf("%v@%v - %v", tag, date, blocks.HashToStr(snapInfo.Hash))
		snapsDisplay = append(snapsDisplay, snapDisplay)
	}
	return &pb.SnapListReply{Snapshots: snapsDisplay}, nil
}

func (s *daemon) SnapDelete(ctx context.Context, req *pb.SnapDeleteRequest) (*pb.SnapDeleteReply, error) {
	panic("snap delete unimplemented")
	// hash := blocks.ByteSliceToArr(req.GetHash())

	// var (
	// 	// returned at the end
	// 	errRet  error
	// 	deleted bool
	// )
	// for _, storeConfig := range s.cfg.StoreConfigs {
	// 	store, err := storeConfig.Config.Instance(ctx)
	// 	if err != nil {
	// 		errRet = err
	// 		continue
	// 	}

	// 	exists, err := store.Has(ctx, hash)
	// 	if err != nil {
	// 		errRet = err
	// 		continue
	// 	}
	// 	if !exists {
	// 		continue
	// 	}

	// 	snap, err := GetSnapshot(ctx, store, hash)
	// 	if err != nil {
	// 		return nil, err
	// 	}

	// 	ref := blocks.ByteSliceToArr(snap.Meta.Ref)
	// 	err = store.DeleteRefRoot(ctx, ref)
	// 	if err != nil {
	// 		errRet = err
	// 		continue
	// 	}
	// 	deleted = true
	// }

	// message := fmt.Sprintf("Could not delete snapshot %v: not found", blocks.HashToStr(hash))
	// if deleted {
	// 	message = fmt.Sprintf("Deleted snapshot %v", blocks.HashToStr(hash))
	// }
	// return &pb.SnapDeleteReply{Message: message}, errRet
}

func (s *daemon) MountsetAdd(ctx context.Context, req *pb.MountsetAddRequest) (*pb.MountsetAddReply, error) {
	path := req.GetPath()
	fs := req.GetFS()

	err := s.cfg.MountsetAdd(path, fs)
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("mountset_add", logr.KV{"fs.name": fs, "path": path})
	return &pb.MountsetAddReply{Message: fmt.Sprintf("Added %v to mountset", path)}, nil
}

func (s *daemon) MountsetRemove(ctx context.Context, req *pb.MountsetRemoveRequest) (*pb.MountsetRemoveReply, error) {
	fs := req.GetFS()
	path := req.GetPath()

	// first try to remove by fs name
	err := s.cfg.MountsetRemove(fs)
	if err == nil {
		logr.FromContext(ctx).Event("mountset_remove", logr.KV{"fs.name": fs})
		return &pb.MountsetRemoveReply{Message: fmt.Sprintf("Removed %v from mountset", fs)}, nil
	}

	// if that fails, try to remove by path
	err = s.cfg.MountsetRemoveByPath(path)
	if err != nil {
		return nil, err
	}

	logr.FromContext(ctx).Event("mountset_remove", logr.KV{"path": path})
	return &pb.MountsetRemoveReply{Message: fmt.Sprintf("Removed %v from mountset", path)}, nil
}

func (s *daemon) MountsetList(ctx context.Context, req *pb.MountsetListRequest) (*pb.MountsetListReply, error) {
	entries := s.cfg.MountsetList()
	return &pb.MountsetListReply{Entries: entries}, nil
}

func (s *daemon) MountsetSave(ctx context.Context, req *pb.MountsetSaveRequest) (*pb.MountsetSaveReply, error) {
	// add/merge active mounts with existing mountset entries
	for path, mi := range s.mounts {
		err := s.cfg.MountsetAdd(path, mi.FSInstanceOptions.Name)
		if err != nil {
			return nil, err
		}
	}

	// return all mountset entries
	entries := s.cfg.MountsetList()
	return &pb.MountsetSaveReply{Entries: entries}, nil
}

func (s *daemon) MountsetMount(ctx context.Context, req *pb.MountsetMountRequest) (*pb.MountsetMountReply, error) {
	entries := s.cfg.MountsetList()

	fmt.Println("Mounting mountset...")
	for _, entry := range entries {
		resp, err := s.Mount(ctx, &pb.MountRequest{FS: entry.FS, Path: entry.Path})
		if err != nil {
			return nil, err
		}
		fmt.Println(resp.Message)
	}

	return &pb.MountsetMountReply{Entries: entries}, nil
}

func (s *daemon) MountsetUnmount(ctx context.Context, req *pb.MountsetUnmountRequest) (*pb.MountsetUnmountReply, error) {
	entries := s.cfg.MountsetList()

	fmt.Println("Unmounting mountset...")
	for _, entry := range entries {
		resp, err := s.Unmount(ctx, &pb.UnmountRequest{Path: entry.Path})
		if err != nil {
			return nil, err
		}
		fmt.Println(resp.Message)
	}

	return &pb.MountsetUnmountReply{Entries: entries}, nil
}

func newMountInfo(opts *fsInstanceOptions, fs *muxfs.FS) *mountInfo {
	return &mountInfo{
		FSInstanceOptions: opts,
		Unmounted:         make(chan bool),
		FS:                fs,
	}
}

func (e fsAlreadyMountedError) Error() string {
	return fmt.Sprintf("fs %v is already mounted at path %v", e.fs, e.path)
}
