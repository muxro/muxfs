package fuse

import (
	"context"
	"errors"
	"syscall"

	"bazil.org/fuse"
	"gitlab.com/muxro/muxfs/muxfs"
)

// getAttr returns the node attributes for all node types (file, dir, link). The response wants the
// resp.Attr field to be filled with the attributes.
func (s *server) getAttr(ctx context.Context, req *fuse.GetattrRequest, resp *fuse.GetattrResponse) error {
	path := s.nodes[req.Node]

	meta, err := s.fs.Meta(ctx, path)
	if err != nil {
		return err
	}

	// TODO: inode
	resp.Attr.Inode = 23
	setAttr(&resp.Attr, &meta)

	return nil
}

// setAttr sets the node attributes for all node types. Returns the new attributes (file, dir,
// link). The request wants the resp.Attr field to be filled with the new attributes.
func (s *server) setAttr(ctx context.Context, req *fuse.SetattrRequest, resp *fuse.SetattrResponse) error {
	path := s.nodes[req.Node]

	err := s.fs.SetMeta(ctx, path, muxfs.SetMetaValid(req.Valid), muxfs.Meta{
		Mode:      uint32(req.Mode),
		Size:      int64(req.Size),
		Uid:       req.Uid,
		Gid:       req.Gid,
		UpdatedAt: req.Mtime.Unix(),
	})
	if err != nil {
		if errors.Is(err, muxfs.ErrDirSetSize) {
			return syscall.EIO
		}
		if errors.Is(err, muxfs.ErrLinkSetSize) {
			return syscall.EIO
		}

		return err
	}

	meta, err := s.fs.Meta(ctx, path)
	if err != nil {
		return err
	}
	setAttr(&resp.Attr, &meta)

	return nil
}

// open opens a node (dir, file), creating a handle. In the case of a file, the handle will contain
// a pointer to the actual file handle that can be used to read data from. Note that in the case of
// a directory, we only create a new handle just to be able to know which dir FUSE wants to read
// from in Readdirall requests. There is no underlying handle object created, just a handle ID. The
// response needs the ID of the handle that was created.
func (s *server) open(ctx context.Context, req *fuse.OpenRequest, resp *fuse.OpenResponse) error {
	var file *muxfs.File
	if !req.Dir {
		// File
		path := s.nodes[req.Node]
		var err error
		file, err = s.fs.Open(ctx, path)
		if err != nil {
			return err
		}
	}

	// File and dir
	// For dirs, file == nil
	resp.Handle = s.newHandle(req.Node, file)

	return nil
}
