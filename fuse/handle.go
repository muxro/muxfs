package fuse

import (
	"context"
	"io"

	"bazil.org/fuse"
	"bazil.org/fuse/fuseutil"
	"gitlab.com/muxro/muxfs/muxfs"
)

// read either reads file data from the underlying file handle, in the case of a file, or reads the
// directory entries in the case of a dir. The response wants the resp.Data field to be filled with
// the relevant data.
func (s *server) read(ctx context.Context, req *fuse.ReadRequest, resp *fuse.ReadResponse) error {
	handle := s.handles[req.Handle]
	path := s.nodes[handle.node]

	// File
	if !req.Dir {
		resp.Data = make([]byte, req.Size)
		_, err := handle.file.ReadAt(resp.Data, req.Offset)
		if err != nil {
			if err == io.EOF {
				return nil
			}

			return err
		}
	}

	// Read dir
	resp.Data = make([]byte, 0, req.Size)
	dirents, err := s.fs.ReadDir(ctx, path)
	if err != nil {
		return err
	}

	var data []byte
	for _, dirent := range dirents {
		var fusedirent fuse.Dirent
		fusedirent.Name = dirent.Name
		// TODO: generate inode
		fusedirent.Inode = 23

		switch dirent.Type {
		case muxfs.EntryDir:
			fusedirent.Type = fuse.DT_Dir
		case muxfs.EntryFile:
			fusedirent.Type = fuse.DT_File
		case muxfs.EntryLink:
			fusedirent.Type = fuse.DT_Link
		}

		data = fuse.AppendDirent(data, fusedirent)
	}
	fuseutil.HandleRead(req, resp, data)

	return nil
}

// write writes data to a file handle. Writing to a dir handle should not happen. The response wants
// the resp.Size field to be filled with how many bytes were written.
func (s *server) write(ctx context.Context, req *fuse.WriteRequest, resp *fuse.WriteResponse) error {
	handle := s.handles[req.Handle]
	_, err := handle.file.WriteAt(req.Data, req.Offset)
	resp.Size = len(req.Data)
	return err
}

// release drops a handle, when it is no longer needed. In the case of a file, it is called by the
// kernel when the file.handle.Close() method is called. In the case of a dir, it is called by the
// kernel some time after the Readdir request is complete.
func (s *server) release(ctx context.Context, req *fuse.ReleaseRequest) error {
	s.dropHandle(req.Handle)
	return nil
}
