package fuse

import (
	"context"
	"io/ioutil"
	"os"
	"path/filepath"
	"syscall"
	"testing"
	"time"

	"bazil.org/fuse"
	"github.com/stretchr/testify/require"
	"gitlab.com/muxro/muxfs/blocks"
	"gitlab.com/muxro/muxfs/muxfs"
)

func mount(t *testing.T) string {
	ctx := context.TODO()

	store := blocks.NewMemoryStore("")
	fs, err := muxfs.New(ctx, store)
	require.NoError(t, err)

	mountpoint, err := ioutil.TempDir(os.TempDir(), "muxfs-fuse-mount")
	require.NoError(t, err)
	go func() {
		err := Mount(ctx, mountpoint, fs, nil)
		require.NoError(t, err)
	}()

	time.Sleep(100 * time.Millisecond)

	return mountpoint
}

func Cleanup(t *testing.T, path string) {
	err := fuse.Unmount(path)
	require.NoError(t, err)
}

func mkdir(mountpoint, path string) error {
	full := filepath.Join(mountpoint, path)
	return os.Mkdir(full, 0777)
}

func mustMkdir(t *testing.T, mountpoint, path string) {
	err := mkdir(mountpoint, path)
	require.NoError(t, err)
}

func mustReadDir(t *testing.T, mountpoint, path string) []os.FileInfo {
	full := filepath.Join(mountpoint, path)
	files, err := ioutil.ReadDir(full)
	require.NoError(t, err)
	return files
}

func create(mp, path string) (*os.File, error) {
	full := filepath.Join(mp, path)
	return os.Create(full)
}

func mustCreate(t *testing.T, mp, path string) *os.File {
	f, err := create(mp, path)
	require.NoError(t, err)
	return f
}

func link(mp, target, path string) error {
	full := filepath.Join(mp, path)
	return os.Symlink(target, full)
}

func mustLink(t *testing.T, mp, target, path string) {
	err := link(mp, target, path)
	require.NoError(t, err)
}

func remove(mp, path string) error {
	full := filepath.Join(mp, path)
	return os.Remove(full)
}

func mustRemove(t *testing.T, mp, path string) {
	err := remove(mp, path)
	require.NoError(t, err)
}

func rename(mp, path, renamed string) error {
	fullpath := filepath.Join(mp, path)
	fullrenamed := filepath.Join(mp, renamed)
	return os.Rename(fullpath, fullrenamed)
}

func mustRename(t *testing.T, mp, path, renamed string) {
	err := rename(mp, path, renamed)
	require.NoError(t, err)
}

func fileExists(mp, path string) (bool, error) {
	full := filepath.Join(mp, path)
	_, err := os.Stat(full)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}

		return false, err
	}

	return true, nil
}

func fileMustExist(t *testing.T, mp, path string) {
	ok, err := fileExists(mp, path)
	require.NoError(t, err)
	require.True(t, ok)
}

func fileMustNotExist(t *testing.T, mp, path string) {
	ok, err := fileExists(mp, path)
	require.NoError(t, err)
	require.False(t, ok)
}

// TODO: not cross-platform
func getUidGid(fi os.FileInfo) (int, int) {
	if stat, ok := fi.Sys().(*syscall.Stat_t); ok {
		return int(stat.Uid), int(stat.Gid)
	}
	return -1, -1
}

func testErrno(t *testing.T, err error, expErrno syscall.Errno) {
	require.Error(t, err)

	var innerErr error
	switch err := err.(type) {
	case *os.PathError:
		innerErr = err.Err
	case *os.LinkError:
		innerErr = err.Err
	}
	require.NotNil(t, innerErr)

	errno, ok := innerErr.(syscall.Errno)
	require.True(t, ok)

	require.Equal(t, expErrno, errno)
}
