package fuse

import (
	"context"
	"syscall"

	"bazil.org/fuse"
	"gitlab.com/muxro/muxfs/muxfs"
)

// readlink reads the target of a link node.
func (s *server) readlink(ctx context.Context, req *fuse.ReadlinkRequest) (string, error) {
	path := s.nodes[req.Node]

	target, err := s.fs.Readlink(ctx, path)
	if _, ok := err.(*muxfs.NodeNotFoundError); ok {
		return "", syscall.ENOENT
	}

	return target, err
}
