package fuse

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLinkAttr(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)
	require := require.New(t)

	mustLink(t, mp, "/target", "/link")

	fi, err := os.Lstat(filepath.Join(mp, "/link"))
	require.NoError(err)

	require.Equal("link", fi.Name())
	require.False(fi.IsDir())
	require.Equal(os.FileMode(0777|os.ModeSymlink), fi.Mode())
	require.Equal(int64(len("/target")), fi.Size())
	uid, gid := getUidGid(fi)
	require.Equal(1000, uid)
	require.Equal(100, gid)
}

func TestLinkSetattr(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)
	require := require.New(t)

	mustLink(t, mp, "/target", "/link")

	linkpath := filepath.Join(mp, "/link")
	fi, err := os.Lstat(linkpath)
	require.NoError(err)

	require.Equal("link", fi.Name())
	require.False(fi.IsDir())
	require.Equal(os.FileMode(0777|os.ModeSymlink), fi.Mode())
	require.Equal(int64(len("/target")), fi.Size())
	uid, gid := getUidGid(fi)
	require.Equal(1000, uid)
	require.Equal(100, gid)

	err = os.Lchown(linkpath, 1000, 100)
	require.NoError(err)
	fi, err = os.Lstat(linkpath)
	require.NoError(err)
	uid, gid = getUidGid(fi)
	require.Equal(1000, uid)
	require.Equal(100, gid)
}

func TestReadlink(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)
	require := require.New(t)

	mustLink(t, mp, "/target", "/link")

	target, err := os.Readlink(filepath.Join(mp, "/link"))
	require.NoError(err)
	require.Equal("/target", target)
}
