package fuse

import (
	"context"
	"encoding/binary"
	"fmt"
	"hash/fnv"
	"io"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"syscall"
	"time"

	"bazil.org/fuse"
	"gitlab.com/go-figure/logr"
	"gitlab.com/muxro/muxfs/muxfs"
)

type server struct {
	fs             *muxfs.FS
	activeRequests map[fuse.RequestID]func()
	nodes          map[fuse.NodeID]string
	handles        map[fuse.HandleID]handle
	freeNodes      []fuse.NodeID
	nodeCount      int
	freeHandles    []fuse.HandleID
	handleCount    int
}

type handle struct {
	node fuse.NodeID
	// Files will have a file handle
	file *muxfs.File
}

func newServer(fs *muxfs.FS) *server {
	return &server{
		fs:             fs,
		activeRequests: make(map[fuse.RequestID]func()),
		nodes: map[fuse.NodeID]string{
			0x1: "/",
		},
		nodeCount:   1,
		handleCount: 0,
		handles:     make(map[fuse.HandleID]handle),
	}
}

func Mount(ctx context.Context, mountpoint string, fs *muxfs.FS, ready chan error) (err error) {
	log.SetOutput(ioutil.Discard)

	ctx, job := logr.Task(ctx, "fuse.Mount", nil)
	defer job.DeferDone(&err, nil)

	s := newServer(fs)

	conn, err := fuse.Mount(mountpoint,
		fuse.MaxReadahead(1*1024*1024),
		fuse.DefaultPermissions(),
		fuse.FSName("muxfs"),
		fuse.Subtype("muxfs"),
		fuse.LocalVolume(),
		fuse.VolumeName("muxfs"))
	if err != nil {
		return err
	}

	// Wait for mount to be ready
	<-conn.Ready
	if ready != nil {
		ready <- nil
	}

	for {
		req, err := conn.ReadRequest()
		if err != nil {
			if err == io.EOF {
				// Done
				break
			}

			return err
		}

		// TODO: handle in another goroutine
		s.handle(ctx, req)
	}

	return nil
}

func (s *server) handle(ctx context.Context, req fuse.Request) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	s.activeRequests[req.Hdr().ID] = cancel

	switch req := req.(type) {
	case *fuse.GetattrRequest:
		resp := &fuse.GetattrResponse{}
		err := s.getAttr(ctx, req, resp)
		if err != nil {
			req.RespondError(err)
			unexpectedError("getattr", err)
			return
		}
		req.Respond(resp)

	case *fuse.SetattrRequest:
		resp := &fuse.SetattrResponse{}
		err := s.setAttr(ctx, req, resp)
		if err != nil {
			req.RespondError(err)
			unexpectedError("setattr", err)
			return
		}
		req.Respond(resp)

	case *fuse.OpenRequest:
		resp := &fuse.OpenResponse{}
		err := s.open(ctx, req, resp)
		if err != nil {
			req.RespondError(err)
			unexpectedError("open", err)
			return
		}
		req.Respond(resp)

	case *fuse.ReleaseRequest:
		err := s.release(ctx, req)
		if err != nil {
			req.RespondError(err)
			unexpectedError("release", err)
			return
		}
		req.Respond()

	case *fuse.ReadRequest:
		resp := &fuse.ReadResponse{}
		err := s.read(ctx, req, resp)
		if err != nil {
			req.RespondError(err)
			unexpectedError("read", err)
			return
		}
		req.Respond(resp)

	case *fuse.LookupRequest:
		resp := &fuse.LookupResponse{}
		err := s.lookup(ctx, req, resp)
		if err != nil {
			req.RespondError(err)
			if !os.IsNotExist(err) {
				unexpectedError("lookup", err)
			}
			return
		}
		req.Respond(resp)

	case *fuse.RenameRequest:
		err := s.rename(ctx, req)
		if err != nil {
			req.RespondError(err)
			unexpectedError("rename", err)
			return
		}
		req.Respond()

	case *fuse.ForgetRequest:
		s.dropNode(req.Node)
		req.Respond()

	case *fuse.ReadlinkRequest:
		target, err := s.readlink(ctx, req)
		if err != nil {
			req.RespondError(err)
			unexpectedError("readlink", err)
			return
		}
		req.Respond(target)

	case *fuse.RemoveRequest:
		err := s.remove(ctx, req)
		if err != nil {
			req.RespondError(err)
			unexpectedError("remove", err)
			return
		}
		req.Respond()

	case *fuse.MkdirRequest:
		resp := &fuse.MkdirResponse{}
		err := s.mkdir(ctx, req, resp)
		if err != nil {
			req.RespondError(err)
			unexpectedError("mkdir", err)
			return
		}
		req.Respond(resp)

	case *fuse.CreateRequest:
		resp := &fuse.CreateResponse{}
		err := s.create(ctx, req, resp)
		if err != nil {
			req.RespondError(err)
			unexpectedError("create", err)
			return
		}
		req.Respond(resp)

	case *fuse.SymlinkRequest:
		resp := &fuse.SymlinkResponse{}
		err := s.symlink(ctx, req, resp)
		if err != nil {
			req.RespondError(err)
			unexpectedError("symlink", err)
			return
		}
		req.Respond(resp)

	case *fuse.WriteRequest:
		resp := &fuse.WriteResponse{}
		err := s.write(ctx, req, resp)
		if err != nil {
			req.RespondError(err)
			unexpectedError("write", err)
			return
		}
		req.Respond(resp)

	case *fuse.FlushRequest:
		// Don't do anything for now
		req.Respond()

	case *fuse.InterruptRequest:
		cancel := s.activeRequests[req.IntrID]
		cancel()
		req.Respond()

	default:
		req.RespondError(syscall.ENOSYS)
		fuse.Debug(fmt.Errorf("unknown op: %v", reflect.TypeOf(req)))
	}
}

func unexpectedError(op string, err error) {
	fuse.Debug(fmt.Errorf("op %v: %w", op, err))
}

func (s *server) newNode(path string) fuse.NodeID {
	var nodeID fuse.NodeID
	if len(s.freeNodes) > 0 {
		// Take first ID from free nodes and then remove it
		nodeID = s.freeNodes[0]
		s.freeNodes = s.freeNodes[1:]
	} else {
		s.nodeCount++
		nodeID = fuse.NodeID(s.nodeCount)
	}
	s.nodes[nodeID] = path
	log.Println("NODE CREATE", nodeID, path)
	return nodeID
}

func (s *server) newHandle(node fuse.NodeID, file *muxfs.File) fuse.HandleID {
	var handleID fuse.HandleID
	if len(s.freeHandles) > 0 {
		// Take first ID from free handles and then remove it
		handleID = s.freeHandles[0]
		s.freeHandles = s.freeHandles[1:]
	} else {
		s.handleCount++
		handleID = fuse.HandleID(s.handleCount)
	}
	path := s.nodes[node]
	log.Println("HANDLE CREATE", handleID, path)
	s.handles[handleID] = handle{node: node, file: file}
	return handleID
}

func (s *server) dropNode(node fuse.NodeID) {
	path := s.nodes[node]
	log.Println("NODE DELETE", node, path)
	delete(s.nodes, node)
	s.freeNodes = append(s.freeNodes, node)
}

func (s *server) dropHandle(handleID fuse.HandleID) {
	handle := s.handles[handleID]
	if handle.file != nil {
		// TODO: error check?
		handle.file.Close()
	}
	path := s.nodes[handle.node]
	log.Println("HANDLE DELETE ", handleID, path)
	delete(s.handles, handleID)
	s.freeHandles = append(s.freeHandles, handleID)
}

func setAttr(attr *fuse.Attr, meta *muxfs.Meta) {
	attr.Valid = 60 * time.Second
	attr.Size = uint64(meta.Size)
	attr.Mode = os.FileMode(meta.Mode)
	attr.Uid = meta.Uid
	attr.Gid = meta.Gid
	attr.Mtime = time.Unix(meta.UpdatedAt, 0)
}

// generateDynamicInode returns a dynamic inode. The parent inode and current entry name are used as
// the criteria for choosing a pseudorandom inode. This makes it likely the same entry will get the
// same inode on multiple runs.
func generateDynamicInode(parent uint64, name string) uint64 {
	h := fnv.New64a()
	var buf [8]byte
	binary.LittleEndian.PutUint64(buf[:], parent)
	_, _ = h.Write(buf[:])
	_, _ = h.Write([]byte(name))
	var inode uint64
	for {
		inode = h.Sum64()
		if inode > 1 {
			break
		}
		// There's a tiny probability that result is zero or the
		// hardcoded root inode 1; Change the input a little and try
		// again
		_, _ = h.Write([]byte{'x'})
	}
	return inode
}
