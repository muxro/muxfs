package fuse

import (
	"os"
	"path/filepath"
	"strings"
	"syscall"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDirAttr(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)
	require := require.New(t)

	mustMkdir(t, mp, "/dir")

	fi, err := os.Stat(filepath.Join(mp, "/dir"))
	require.NoError(err)
	require.Equal("dir", fi.Name())
	require.True(fi.IsDir())
	require.Equal(os.FileMode(0755|os.ModeDir), fi.Mode())
	require.Equal(int64(0), fi.Size())
	uid, gid := getUidGid(fi)
	require.Equal(1000, uid)
	require.Equal(100, gid)
}

func TestDirSetattr(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)
	require := require.New(t)

	mustMkdir(t, mp, "/dir")

	path := filepath.Join(mp, "/dir")
	fi, err := os.Stat(path)
	require.NoError(err)
	require.Equal("dir", fi.Name())
	require.True(fi.IsDir())
	require.Equal(os.FileMode(0755|os.ModeDir), fi.Mode())
	require.Equal(int64(0), fi.Size())
	uid, gid := getUidGid(fi)
	require.Equal(1000, uid)
	require.Equal(100, gid)

	err = os.Chmod(path, 0654)
	require.NoError(err)
	fi, err = os.Stat(path)
	require.NoError(err)
	require.Equal(os.FileMode(0654|os.ModeDir), fi.Mode())

	err = os.Chown(path, 1000, 100)
	require.NoError(err)
	fi, err = os.Stat(path)
	require.NoError(err)
	uid, gid = getUidGid(fi)
	require.Equal(1000, uid)
	require.Equal(100, gid)
}

func TestLookup(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)
	require := require.New(t)

	mustMkdir(t, mp, "/dir1")
	mustMkdir(t, mp, "/dir2")
	mustCreate(t, mp, "/dir1/file1").Close()
	mustCreate(t, mp, "/dir2/file2").Close()

	files := mustReadDir(t, mp, "/")
	require.Len(files, 2)

	mustFind := []string{"dir1", "dir2"}
	var found int
	for _, file := range files {
		for _, mf := range mustFind {
			if file.IsDir() && file.Name() == mf {
				found++
			}
		}
	}
	require.Equal(2, found, "did not find expected files")

	files = mustReadDir(t, mp, "/dir1")
	require.Len(files, 1)
	require.Equal("file1", files[0].Name())
	require.False(files[0].IsDir())

	files = mustReadDir(t, mp, "/dir2")
	require.Len(files, 1)
	require.Equal("file2", files[0].Name())
	require.False(files[0].IsDir())
}

func TestCreate(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)
	require := require.New(t)

	files := mustReadDir(t, mp, "")
	numFilesExist := len(files)

	mustCreate(t, mp, "/file1").Close()

	files = mustReadDir(t, mp, "/")
	require.Equal(numFilesExist+1, len(files))
	file1 := files[0]
	require.Equal("file1", file1.Name())
	require.False(file1.IsDir())

	mustCreate(t, mp, "/file2").Close()

	files = mustReadDir(t, mp, "/")
	require.Equal(numFilesExist+2, len(files))
	file1 = files[0]
	require.Equal("file1", file1.Name())
	require.False(file1.IsDir())
	file2 := files[1]
	require.Equal("file2", file2.Name())
	require.False(file2.IsDir())

	// File name too long
	_, err := create(mp, "/"+strings.Repeat("a", 256))
	testErrno(t, err, syscall.ENAMETOOLONG)

	// Destination dir doesn't exist
	_, err = create(mp, "/notexist/file")
	testErrno(t, err, syscall.ENOENT)

	// Destination dir is not a dir
	mustCreate(t, mp, "/notdir").Close()
	_, err = create(mp, "/notdir/file")
	testErrno(t, err, syscall.ENOTDIR)
}

func TestMkdir(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)
	require := require.New(t)

	files := mustReadDir(t, mp, "/")
	numFilesExist := len(files)

	mustMkdir(t, mp, "/dir1")
	mustMkdir(t, mp, "/dir2")

	files = mustReadDir(t, mp, "/")
	require.Equal(numFilesExist+2, len(files))
	file1 := files[0]
	file2 := files[1]
	require.True(file1.IsDir())
	require.Equal("dir1", file1.Name())
	require.True(file2.IsDir())
	require.Equal("dir2", file2.Name())

	// Nested dirs
	mustMkdir(t, mp, "/dir1/child1")
	mustMkdir(t, mp, "/dir2/child2")
	mustMkdir(t, mp, "/dir1/child1/child3")

	files = mustReadDir(t, mp, "/dir1")
	require.Equal(1, len(files))
	child1 := files[0]
	require.True(child1.IsDir())
	require.Equal("child1", child1.Name())

	files = mustReadDir(t, mp, "/dir2")
	require.Equal(1, len(files))
	child2 := files[0]
	require.True(child2.IsDir())
	require.Equal("child2", child2.Name())

	files = mustReadDir(t, mp, "/dir1/child1")
	require.Equal(1, len(files))
	child3 := files[0]
	require.True(child3.IsDir())
	require.Equal("child3", child3.Name())

	// File name too long
	err := mkdir(mp, "/"+strings.Repeat("a", 256))
	testErrno(t, err, syscall.ENAMETOOLONG)

	// Dir already exists
	mustMkdir(t, mp, "/alreadyexists")
	err = mkdir(mp, "/alreadyexists")
	testErrno(t, err, syscall.EEXIST)

	// Destination dir doesn't exist
	err = mkdir(mp, "/notexist/dir")
	testErrno(t, err, syscall.ENOENT)

	// Destination dir is not a dir
	mustCreate(t, mp, "/notdir").Close()
	err = mkdir(mp, "/notdir/dir")
	testErrno(t, err, syscall.ENOTDIR)
}

func TestSymlink(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)

	mustLink(t, mp, "/target", "/link")
	// Link must exist
	_, err := os.Lstat(filepath.Join(mp, "/link"))
	require.NoError(t, err)

	// File name too long
	err = link(mp, "/target", "/"+strings.Repeat("a", 256))
	testErrno(t, err, syscall.ENAMETOOLONG)

	// Link already exists
	err = link(mp, "/target", "/link")
	testErrno(t, err, syscall.EEXIST)

	// Destination dir doesn't exist
	err = link(mp, "/target", "/notexist/link")
	testErrno(t, err, syscall.ENOENT)

	// Destination dir is not a dir
	mustCreate(t, mp, "/notdir").Close()
	err = link(mp, "/target", "/notdir/link")
	testErrno(t, err, syscall.ENOTDIR)
}

func TestRename(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)

	// File must exist
	err := rename(mp, "/doesntexist", "/renamed")
	testErrno(t, err, syscall.ENOENT)

	// Move to same dir
	mustCreate(t, mp, "/file").Close()
	mustRename(t, mp, "/file", "/renamed")

	// Check the old file doesnt exist
	fileMustNotExist(t, mp, "/file")
	// Check the new file exists
	fileMustExist(t, mp, "/renamed")

	// Create nested dirs, of which the deepest one will have the path length close to 4096
	// so we can then rename another file to have > 4096 path length
	const repeatPathTimes = 16 // 16 * 255 + 1 = 4081
	var path string
	for i := 1; i <= repeatPathTimes; i++ {
		nameLen := 254
		// Subtract mountpoint from last name so we don't go over
		if i == repeatPathTimes {
			nameLen -= len(mp)
		}
		name := strings.Repeat("a", nameLen)
		path += "/" + name
		mustMkdir(t, mp, path)
	}

	pathTooLong := filepath.Join(path, strings.Repeat("a", 250))
	err = mkdir(mp, pathTooLong)
	testErrno(t, err, syscall.ENAMETOOLONG)

	mustMkdir(t, mp, "/nested")
	mustMkdir(t, mp, "/nested/dir")
	mustCreate(t, mp, "/nested/file").Close()
	mustMkdir(t, mp, "/nested/deeply")
	mustCreate(t, mp, "/nested/deeply/file").Close()
	mustMkdir(t, mp, "/another")

	mustRename(t, mp, "/nested/file", "/another/file")
	fileMustNotExist(t, mp, "/nested/file")
	fileMustExist(t, mp, "/another/file")

	mustRename(t, mp, "/nested/deeply/file", "/nested/file")
	fileMustNotExist(t, mp, "/nested/deeply/file")
	fileMustExist(t, mp, "/nested/file")

	mustRename(t, mp, "/nested/dir", "/another/dir")
	fileMustNotExist(t, mp, "/nested/dir")
	fileMustExist(t, mp, "/another/dir")
}

func TestRemove(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)

	mustCreate(t, mp, "/file").Close()
	fileMustExist(t, mp, "/file")

	mustMkdir(t, mp, "/dirwithfiles")
	mustCreate(t, mp, "/dirwithfiles/file").Close()
	fileMustExist(t, mp, "/dirwithfiles/file")

	mustRemove(t, mp, "/file")
	fileMustNotExist(t, mp, "/file")

	// Can't remove dir with files in it
	err := remove(mp, "/dirwithfiles")
	testErrno(t, err, syscall.ENOTEMPTY)
	fileMustExist(t, mp, "/dirwithfiles")

	mustRemove(t, mp, "/dirwithfiles/file")
	fileMustNotExist(t, mp, "/dirwithfiles/file")

	// Can remove dir now that we removed the file
	mustRemove(t, mp, "/dirwithfiles")
	fileMustNotExist(t, mp, "/dirwithfiles")
}
