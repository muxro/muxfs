package fuse

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFileOpen(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)

	err := mustCreate(t, mp, "/file").Close()
	require.NoError(t, err)
}

func TestFileAttr(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)
	require := require.New(t)

	f := mustCreate(t, mp, "/file")
	defer f.Close()

	fi, err := f.Stat()
	require.NoError(err)
	require.Equal("file", fi.Name())
	require.False(fi.IsDir())
	require.Equal(os.FileMode(0644), fi.Mode())
	require.Equal(int64(0), fi.Size())
	uid, gid := getUidGid(fi)
	require.Equal(1000, uid)
	require.Equal(100, gid)
}

func TestFileSetattr(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)

	f := mustCreate(t, mp, "/file")
	defer f.Close()

	fi, err := f.Stat()
	require.NoError(t, err)
	require.Equal(t, "file", fi.Name())
	require.False(t, fi.IsDir())
	require.Equal(t, os.FileMode(0644), fi.Mode())
	require.Equal(t, int64(0), fi.Size())
	uid, gid := getUidGid(fi)
	require.Equal(t, 1000, uid)
	require.Equal(t, 100, gid)

	err = f.Truncate(3 * 1024)
	require.NoError(t, err)
	fi, err = f.Stat()
	require.NoError(t, err)
	require.Equal(t, int64(3*1024), fi.Size())

	err = f.Chmod(0654)
	require.NoError(t, err)
	fi, err = f.Stat()
	require.NoError(t, err)
	require.Equal(t, os.FileMode(0654), fi.Mode())

	err = f.Chown(1000, 100)
	require.NoError(t, err)
	fi, err = f.Stat()
	require.NoError(t, err)
	uid, gid = getUidGid(fi)
	require.Equal(t, 1000, uid)
	require.Equal(t, 100, gid)
}

func TestFileRead(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)

	mustCreate(t, mp, "/file").Close()

	_, err := ioutil.ReadFile(filepath.Join(mp, "/file"))
	require.NoError(t, err)
}

func TestFileWrite(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)

	mustCreate(t, mp, "/file").Close()

	f, err := os.OpenFile(filepath.Join(mp, "/file"), os.O_WRONLY, 0644)
	require.NoError(t, err)
	defer f.Close()

	_, err = f.Write([]byte("data"))
	require.NoError(t, err)
}

func TestFileClose(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)

	f := mustCreate(t, mp, "/file")
	err := f.Close()
	require.NoError(t, err)
}

func TestFileFsync(t *testing.T) {
	mp := mount(t)
	defer Cleanup(t, mp)

	f := mustCreate(t, mp, "/file")
	defer f.Close()

	err := f.Sync()
	require.NoError(t, err)
}
