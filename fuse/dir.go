package fuse

import (
	"context"
	"errors"
	"path/filepath"
	"syscall"
	"time"

	"bazil.org/fuse"
	"gitlab.com/muxro/muxfs/muxfs"
)

// lookup searches a directory node for a specific child node. Creates a new node if it finds the
// entry. The response wants the resp.Attr field to be filled with the child node's attributes.
func (s *server) lookup(ctx context.Context, req *fuse.LookupRequest, resp *fuse.LookupResponse) error {
	dirpath := s.nodes[req.Node]
	path := filepath.Join(dirpath, req.Name)

	_, err := s.fs.Lookup(ctx, path)
	if err != nil {
		if _, ok := err.(*muxfs.NodeNotFoundError); ok {
			return syscall.ENOENT
		}

		return err
	}

	// Create new node ID
	resp.Node = s.newNode(path)

	meta, err := s.fs.Meta(ctx, path)
	if err != nil {
		return err
	}
	setAttr(&resp.Attr, &meta)

	return nil
}

// create creates a new empty file in a directory node. Creates a new file node and file handle for
// the file. The response wants the resp.Node and resp.Handle fields to be filled with the IDs of
// the new file node and handle.
func (s *server) create(ctx context.Context, req *fuse.CreateRequest, resp *fuse.CreateResponse) error {
	dirPath := s.nodes[req.Node]
	path := filepath.Join(dirPath, req.Name)

	err := s.fs.CreateFile(ctx, path, muxfs.Meta{
		Mode:      uint32(req.Mode),
		Size:      0,
		Uid:       req.Uid,
		Gid:       req.Gid,
		UpdatedAt: time.Now().Unix(),
	})
	if err != nil {
		if errors.Is(err, muxfs.ErrNameTooLong) {
			return syscall.ENAMETOOLONG
		}
		return err
	}

	// TODO: data race between creating the file and opening the file?

	file, err := s.fs.Open(ctx, path)
	if err != nil {
		return err
	}

	// TODO: data race between opening the file and creating the new file node and handle?

	resp.Node = s.newNode(path)
	resp.Handle = s.newHandle(resp.Node, file)

	return nil
}

// mkdir creates a new empty dir in a directory node. Creates a new dir node. The response wants the
// resp.Node field to be filled with the ID of the new dir node.
func (s *server) mkdir(ctx context.Context, req *fuse.MkdirRequest, resp *fuse.MkdirResponse) error {
	dirPath := s.nodes[req.Node]
	path := filepath.Join(dirPath, req.Name)

	err := s.fs.CreateDir(ctx, path, muxfs.Meta{
		Mode:      uint32(req.Mode),
		Size:      0,
		Uid:       req.Uid,
		Gid:       req.Gid,
		UpdatedAt: time.Now().Unix(),
	})
	if err != nil {
		if errors.Is(err, muxfs.ErrNameTooLong) {
			return syscall.ENAMETOOLONG
		}

		return err
	}

	resp.Node = s.newNode(path)

	meta, err := s.fs.Meta(ctx, path)
	if err != nil {
		return err
	}
	setAttr(&resp.Attr, &meta)

	// TODO: generation
	resp.Generation = 666

	return nil
}

// symlink creates a new symlink node in a directory node. Creates a new node for the symlink. The
// response wants the resp.Attr field to be filled with the new symlink nodes' attributes.
func (s *server) symlink(ctx context.Context, req *fuse.SymlinkRequest, resp *fuse.SymlinkResponse) error {
	dirPath := s.nodes[req.Node]
	path := filepath.Join(dirPath, req.NewName)

	err := s.fs.CreateLink(ctx, path, req.Target, muxfs.Meta{
		Uid:       req.Uid,
		Gid:       req.Gid,
		UpdatedAt: time.Now().Unix(),
	})
	if err != nil {
		if errors.Is(err, muxfs.ErrNameTooLong) {
			return syscall.ENAMETOOLONG
		}

		return err
	}

	meta, err := s.fs.Meta(ctx, path)
	if err != nil {
		return err
	}
	setAttr(&resp.Attr, &meta)

	resp.Node = s.newNode(path)

	return nil
}

// rename moves a node from a dir to another dir, possibly renaming the node to the new name.
func (s *server) rename(ctx context.Context, req *fuse.RenameRequest) error {
	oldDirPath := s.nodes[req.Node]
	newDirPath := s.nodes[req.NewDir]
	oldpath := filepath.Join(oldDirPath, req.OldName)
	newpath := filepath.Join(newDirPath, req.NewName)

	err := s.fs.Rename(ctx, oldpath, newpath)
	if _, ok := err.(*muxfs.NodeNotFoundError); ok {
		return syscall.ENOENT
	}
	if errors.Is(err, muxfs.ErrNameTooLong) {
		return syscall.ENAMETOOLONG
	}

	// TODO: drop node / update s.nodes[] paths?

	return err
}

// remove removes a child node from a dir node.
func (s *server) remove(ctx context.Context, req *fuse.RemoveRequest) error {
	path := s.nodes[req.Node]
	fullpath := filepath.Join(path, req.Name)

	err := s.fs.Remove(ctx, fullpath)
	if _, ok := err.(*muxfs.NodeNotFoundError); ok {
		return syscall.ENOENT
	}
	if errors.Is(err, muxfs.ErrDirNotEmpty) {
		return syscall.ENOTEMPTY
	}

	// TODO: drop node?

	return err
}
